﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

[System.Serializable]
public class Message
{
    public string nickname;
    public string messageTxt;
    public GameObject textObject;
}

public class ChatManager : MonoBehaviour
{
    private InputManager im;
    private PhotonView pv;

    //enum des panels du menu de chat
    public enum chatPanels { Closed, Exclamations, Questions, Places, Others, Themes };

    public GameObject[] chatPanelsGO;
    public GameObject panelBackground;

    public GameObject[] textForTalking;
    public GameObject[] textCloseChat;
    public GameObject[] crossChoices;

    public int idController;
    public Text[] chatTextsPanel0;
    public Text[] chatTextsPanel1;
    public Text[] chatTextsPanel2;
    public Text[] chatTextsPanel3;

    [Tooltip("Choix du panel de Chat")]
    public chatPanels CurrentPanelChat;

    public int sendIdPanel;
    public int sendIdMessage;

    [SerializeField]
    List<Message> messagesList = new List<Message>();

    public int maxMessages = 6;

    public int myMessages = 0;

    public int timeBeforeDestroyMessage;
    public int timeBeforeDestroyMessageSpam;

    public GameObject messageGO;
    public GameObject messageSpamGO;
    public GameObject contentChat;

    // Start is called before the first frame update
    void Start()
    {
        im = FindObjectOfType<InputManager>();
        pv = GetComponent<PhotonView>();

        CurrentPanelChat = chatPanels.Closed;
        textForTalking[idController].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        //Dans le sens horaire
        if(im.inputChatUp)
        {
            Debug.Log("Up");
            ChatInput(0);
        }
        if (im.inputChatRight)
        {
            Debug.Log("Right");
            ChatInput(1);
        }
        if (im.inputChatDown)
        {
            Debug.Log("Down");
            ChatInput(2);
        }
        if (im.inputChatLeft)
        {
            Debug.Log("Left");
            ChatInput(3);
        }

        if (im.inputCloseChat)
        {
            CloseInputChat();
        }

        if (im.inputOpenChat)
        {
            Debug.Log("coucoucou");
            if (CurrentPanelChat == chatPanels.Closed)
            {
                OpenInputChat();
            }
            else
            {
                CloseInputChat();
            }
        }

        //UI Manette ou Clavier
        /*if(im.controlType == InputManager.ControlsType.Gamepad)
        {
            idController = 0;
        }
        else
        {
            idController = 1;
        }*/
    }

    public void OpenInputChat()
    {
        if (CurrentPanelChat == chatPanels.Closed)
        {
            panelBackground.SetActive(true);
            CurrentPanelChat = chatPanels.Themes;
            textCloseChat[idController].SetActive(true);
            chatPanelsGO[4].SetActive(true);
            crossChoices[idController].SetActive(true);
            foreach (GameObject textForTalkingGO in textForTalking)
            {
                textForTalkingGO.SetActive(false);
            }
            //textForTalking[idController].SetActive(false);
        }
        else
        {
            CurrentPanelChat = chatPanels.Closed;
            foreach (GameObject panel in chatPanelsGO)
            {
                panel.SetActive(false);
            }
            panelBackground.SetActive(false);
            foreach (GameObject textCloseChatGO in textCloseChat)
            {
                textCloseChatGO.SetActive(false);
            }
            foreach (GameObject crossChoicesGO in crossChoices)
            {
                crossChoicesGO.SetActive(false);
            }
            textForTalking[idController].SetActive(true);
        }
    }

    public void ChatInput(int IdInput)
    {
        if(CurrentPanelChat == chatPanels.Closed || CurrentPanelChat == chatPanels.Themes)
        {
            switch (IdInput)
            {
                case 0:
                    CurrentPanelChat = chatPanels.Exclamations;
                    sendIdPanel = 0;
                    break;

                case 1:
                    CurrentPanelChat = chatPanels.Questions;
                    sendIdPanel = 1;
                    break;

                case 2:
                    CurrentPanelChat = chatPanels.Places;
                    sendIdPanel = 2;
                    break;

                case 3:
                    CurrentPanelChat = chatPanels.Others;
                    sendIdPanel = 3;
                    break;
            }

            foreach (GameObject textForTalkingGO in textForTalking)
            {
                textForTalkingGO.SetActive(false);
            }
            //textForTalking[idController].SetActive(false);
            panelBackground.SetActive(true);
            textCloseChat[idController].SetActive(true);
            crossChoices[idController].SetActive(true);
            chatPanelsGO[4].SetActive(false);
            chatPanelsGO[IdInput].SetActive(true);
        }
        else
        {
            switch (IdInput)
            {
                case 0:
                    CurrentPanelChat = chatPanels.Exclamations;
                    sendIdMessage = 0;
                    break;

                case 1:
                    CurrentPanelChat = chatPanels.Questions;
                    sendIdMessage = 1;
                    break;

                case 2:
                    CurrentPanelChat = chatPanels.Places;
                    sendIdMessage = 2;
                    break;

                case 3:
                    CurrentPanelChat = chatPanels.Others;
                    sendIdMessage = 3;
                    break;
            }

            if (myMessages < 3)
            {
                pv.RPC("RPC_SendMessage", RpcTarget.All, sendIdPanel, sendIdMessage, PlayerPrefs.GetString("Nickname"));
                Debug.Log(PlayerPrefs.GetString(1+ "Nickname"));
                myMessages++;
                StartCoroutine(CountSpam());
            }
            else
            {
                if (messagesList.Count >= maxMessages)
                {
                    Destroy(messagesList[0].textObject);
                    messagesList.Remove(messagesList[0]);
                }

                Message newMessageSpam = new Message();
                newMessageSpam.nickname = "Spam";
                newMessageSpam.messageTxt = "Spam";

                GameObject newMessageGoSpam = Instantiate(messageSpamGO, contentChat.transform);
                newMessageSpam.textObject = newMessageGoSpam;

                messagesList.Add(newMessageSpam);

                StartCoroutine(DestroyThisMessage(newMessageSpam, newMessageGoSpam, timeBeforeDestroyMessageSpam));
            }
            

            CurrentPanelChat = chatPanels.Closed;
            foreach (GameObject panel in chatPanelsGO)
            {
                panel.SetActive(false);
            }
            panelBackground.SetActive(false);
            foreach (GameObject crossChoicesGO in crossChoices)
            {
                crossChoicesGO.SetActive(false);
            }
            foreach (GameObject textCloseChatGO in textCloseChat)
            {
                textCloseChatGO.SetActive(false);
            }
            textForTalking[idController].SetActive(true);
        }
    }

    public void RecieveMessage(int idPanel, int IdMessage, string pseudo)
    {
        string sendingMessage = "";

        switch (idPanel)
        {
            case 0:
                sendingMessage = chatTextsPanel0[IdMessage].text;
                break;

            case 1:
                sendingMessage = chatTextsPanel1[IdMessage].text;
                break;

            case 2:
                sendingMessage = chatTextsPanel2[IdMessage].text;
                break;

            case 3:
                sendingMessage = chatTextsPanel3[IdMessage].text;
                break;
        }

        if(messagesList.Count >= maxMessages)
        {
            Destroy(messagesList[0].textObject);
            messagesList.Remove(messagesList[0]);
        }

        Message newMessage = new Message();
        newMessage.nickname = pseudo;
        newMessage.messageTxt = sendingMessage;

        GameObject newMessageGo = Instantiate(messageGO, contentChat.transform);
        newMessage.textObject = newMessageGo;
        newMessageGo.GetComponentInChildren<Text>().text = newMessage.nickname + " : " + newMessage.messageTxt;

        messagesList.Add(newMessage);

        StartCoroutine(DestroyThisMessage(newMessage, newMessageGo, timeBeforeDestroyMessage));


        //Debug.Log("message de " + pseudo + " : " + sendingMessage);
    }

    private void CloseInputChat()
    {
        CurrentPanelChat = chatPanels.Closed;
        foreach (GameObject panel in chatPanelsGO)
        {
            panel.SetActive(false);
        }
        panelBackground.SetActive(false);
        foreach (GameObject crossChoicesGO in crossChoices)
        {
            crossChoicesGO.SetActive(false);
        }
        foreach (GameObject textCloseChatGO in textCloseChat)
        {
            textCloseChatGO.SetActive(false);
        }
        textForTalking[idController].SetActive(true);
    }

    public void ChangeControllerChat()
    {
        if (im.controlType == InputManager.ControlsType.Gamepad)
        {
            idController = 0;
        }
        else
        {
            idController = 1;
        }

        ChangeUiController();
    }

    public void ChangeUiController()
    {
        for (int i = 0; i < textForTalking.Length; i++)
        {
            if (textForTalking[i].activeSelf && i != idController)
            {
                if(i == 0)
                {
                    textForTalking[0].SetActive(false);
                    textForTalking[1].SetActive(true);
                }
                else
                {
                    textForTalking[1].SetActive(false);
                    textForTalking[0].SetActive(true);
                }
            }
        }

        for (int i = 0; i < textCloseChat.Length; i++)
        {
            if (textCloseChat[i].activeSelf && i != idController)
            {
                if (i == 0)
                {
                    textCloseChat[0].SetActive(false);
                    textCloseChat[1].SetActive(true);
                }
                else
                {
                    textCloseChat[1].SetActive(false);
                    textCloseChat[0].SetActive(true);
                }
            }
        }

        for (int i = 0; i < crossChoices.Length; i++)
        {
            if (crossChoices[i].activeSelf && i != idController)
            {
                if (i == 0)
                {
                    crossChoices[0].SetActive(false);
                    crossChoices[1].SetActive(true);
                }
                else
                {
                    crossChoices[1].SetActive(false);
                    crossChoices[0].SetActive(true);
                }
            }
        }
    }

    public IEnumerator DestroyThisMessage(Message thisMessage, GameObject thisMessageGO, int timing)
    {
        yield return new WaitForSeconds(timing);
        if(thisMessageGO != null)
        {
            messagesList.Remove(thisMessage);
            Destroy(thisMessageGO);
        }       
    }

    public IEnumerator CountSpam()
    {
        yield return new WaitForSeconds(5f);
        myMessages--;
    }

    [PunRPC]
    void RPC_SendMessage(int idPanel, int IdMessage, string pseudo)
    {
        RecieveMessage(idPanel, IdMessage, pseudo);
        Debug.Log("send message de " + pseudo + " : ");
    }
}
