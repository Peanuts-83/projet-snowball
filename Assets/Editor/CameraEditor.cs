﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Camera))]
public class CameraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Some help", "Some other text");
        Debug.Log("OnInspector");
        DrawDefaultInspector();

        Camera myTarget = (Camera)target;

        if (myTarget.gameObject.GetComponent(typeof(CameraParameters)) == null)
            myTarget.gameObject.AddComponent(typeof(CameraParameters));
    }
}
