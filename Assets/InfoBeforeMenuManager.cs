﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InfoBeforeMenuManager : MonoBehaviour
{
    [SerializeField]
    private int sceneTime;
    [SerializeField]
    private GameObject blackFade;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitBeforeChangeScene());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator WaitBeforeChangeScene()
    {
        yield return new WaitForSeconds(sceneTime);
        blackFade.GetComponent<Animator>().Play("FonduDisappear");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);
    }
}
