// GENERATED AUTOMATICALLY FROM 'Assets/_DumballRush/3rd-party/Inputs/PlayerInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""1f0acb0a-1512-481d-94f2-1e17646c7c89"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""072feb0e-fbda-4333-9616-2afed891203e"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a02e6989-f3f3-4faf-971b-97d4b25fa721"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Camera"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4aca9968-1f7d-44f8-8d14-d2d018e24f79"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""ReverseCamera"",
                    ""type"": ""Button"",
                    ""id"": ""962b5422-55d0-4613-b096-e51557ed176b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""PowerUpForward"",
                    ""type"": ""Button"",
                    ""id"": ""a4845a65-c167-427e-b81e-da699fed745d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""PowerUpBack"",
                    ""type"": ""Button"",
                    ""id"": ""112d9c67-c164-414a-9027-382aea6169c4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""Value"",
                    ""id"": ""31322760-f0e0-4319-b1b0-b766efe60c79"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseClic"",
                    ""type"": ""Button"",
                    ""id"": ""dab05652-8934-4acc-bcbe-24133a5afbd6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Option"",
                    ""type"": ""Button"",
                    ""id"": ""13772c47-d581-4ffd-8eb8-fd2b6d0be9d7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Any"",
                    ""type"": ""Button"",
                    ""id"": ""5e8b75f0-bc86-422d-ad29-ad16569b9ed7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Debug Quit"",
                    ""type"": ""Button"",
                    ""id"": ""6b61e747-20f1-449c-ae19-3817f72329c6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Debug Shrink"",
                    ""type"": ""Button"",
                    ""id"": ""af9166ba-b0a5-4483-a682-2a7837319127"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Debug PowerUp"",
                    ""type"": ""Button"",
                    ""id"": ""02c0c6fb-f3cb-4e8d-abc5-e6e0d5171887"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Debug Fonction"",
                    ""type"": ""Button"",
                    ""id"": ""df71f56c-225b-4167-8631-f8e824fd3abe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ChatUp"",
                    ""type"": ""Button"",
                    ""id"": ""76bdbd31-29c7-46e7-b786-fc5a45c99b4c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ChatDown"",
                    ""type"": ""Button"",
                    ""id"": ""315a83e5-384f-496c-9fe0-446deaea8479"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ChatLeft"",
                    ""type"": ""Button"",
                    ""id"": ""0d1fb517-cb33-460f-b3a3-6cb405a6e982"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ChatRight"",
                    ""type"": ""Button"",
                    ""id"": ""b4c36bf9-e629-436d-94a7-39561766a92d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CloseChat"",
                    ""type"": ""Button"",
                    ""id"": ""9c7f230b-f455-47df-ab7d-f74d4c2f5b2c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""OpenChat"",
                    ""type"": ""Button"",
                    ""id"": ""2f1b4b9d-2944-497b-9032-c6843b0f9cd2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""ZQSD"",
                    ""id"": ""de2ce0c5-eb5d-43fc-a331-b6d33dfe61a9"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0ac79ca1-343f-492b-b37d-2f6186ba115e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0bfc1df1-1603-4fb7-9ef4-f1d6649c764f"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""2685d98f-5457-4c49-a225-6846572b65cc"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""6c821ed8-954f-4a21-b135-4f9eb1cececb"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrow"",
                    ""id"": ""46524a5d-39d7-41f2-8ba9-e06687040761"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""9580cd42-791b-4076-b2fa-f3e41b1d92be"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""12721832-a3bf-4ab2-8c33-2fb7bc0edfe2"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5eda9104-2e3a-4d49-b792-18a0b9d0dc7c"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b252188b-2b07-4671-b82e-c8e7c60da273"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Manette"",
                    ""id"": ""49d3bfb1-d66b-444a-80e8-5e02946b134c"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""57524b95-8088-4f9f-a846-b5a7cda720de"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0f09cff4-8098-4ed0-b193-78148d24e8a0"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""d0d7da48-ff83-48eb-b35e-390d18e0ee19"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""75186035-a997-4032-89a4-e5191264a5e6"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""588f1861-4acd-42e9-92cb-cc1ea3285bb5"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""76d06a07-8dcc-4b1e-b4eb-69286612d88c"",
                    ""path"": ""<Keyboard>/rightShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ba9e5a3f-1d22-47a5-954a-f955d15b8964"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5b5f0b11-2e74-481a-af5d-4c6bb204c704"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": ""NormalizeVector2"",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""RightPad"",
                    ""id"": ""cf80d6e9-0c55-4276-a442-a34cfcab0ff0"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": ""NormalizeVector2"",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""bdc39f86-b0b4-4e44-9349-33db660acb41"",
                    ""path"": ""<Gamepad>/rightStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c51760f0-e859-45c0-be84-460ff8d2e6dd"",
                    ""path"": ""<Gamepad>/rightStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""1bbb2372-718f-42d5-8a8e-c18ad92c363d"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""8bd128f2-d5fa-47bf-b8d0-29521a8b39f7"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""39ad9479-8063-47db-b58a-b9f850676ea7"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PowerUpForward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b1dabbdc-26cb-4cae-b418-254e90fd6401"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PowerUpForward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c0a3cf3e-9793-44f3-a771-f6ec759c3982"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PowerUpForward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee481e72-4d7a-4010-baee-a8f44c3f672b"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ab2ad6ca-f60b-4dcc-8f19-2b5be8e6ffc7"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MouseClic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""966db5f6-4e4e-4208-8013-9325975cc6fe"",
                    ""path"": ""La touche était u"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Debug Fonction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bd0fcdaf-01e5-4a33-895a-66d31684bc1a"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Debug PowerUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8b94bc51-0137-4720-8780-05fea87dc145"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Debug Shrink"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca6891de-d4a7-4217-9c06-1b50fdfbc49c"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ReverseCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8b3d36be-b960-46f3-ac26-16e2e0214f21"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ReverseCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""43e84092-c1a6-43cd-9d09-a0a6bb9cb24b"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PowerUpBack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f29b209f-bed4-4f7c-9d0f-4f5f7f4544cf"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PowerUpBack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7140642d-5881-486f-84df-85ef15b355f4"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PowerUpBack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9ca3926c-7bb2-481a-bc9d-db2acc8d0be0"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Option"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""069a6f51-af1a-422b-aec3-476b6b87ec4e"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Option"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""366e6235-2f02-4858-9448-9cb731658a6b"",
                    ""path"": ""<Keyboard>/anyKey"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5cdc5154-1368-41f8-a8b5-d402bd7f64d8"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""88b66665-706f-4717-a04c-3d9c67a246fc"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""caff8965-ceb5-41c7-848a-68e448f733a9"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b0de2ddd-99e7-4a79-94f1-54ca3efd1a63"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b782c7e3-76a7-48e8-80bf-2ad4dd524916"",
                    ""path"": ""<Mouse>/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""efdb0469-a850-4e9c-85a0-34a4820b4852"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChatUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""db0769bb-d94c-4512-9482-e8e2dbdae6b6"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ChatUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6a9fea59-c85d-4f00-ba5a-6946d62c8ccc"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChatDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""87159f9f-98b5-4107-b94b-e756030f84b9"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ChatDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""165b4884-a70d-46d3-aeb9-518b9897f12d"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChatLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7adbfe1b-c8d2-4916-ac5f-c68da0dff398"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ChatLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""13f8f550-9831-49f8-8216-bde848582420"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChatRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf0524c3-1545-46a0-9bbb-22a059c605a0"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ChatRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""64972eef-c505-4250-8371-36b3fdcd6a93"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""CloseChat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d0bb86e-e211-415e-b528-9d3be8a3f03c"",
                    ""path"": ""<Keyboard>/5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""CloseChat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c35e4210-021b-4908-a29e-b0eeefdd64b3"",
                    ""path"": ""<Keyboard>/c"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""OpenChat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menus"",
            ""id"": ""e8127d51-c39f-4dbe-bda5-90b8da527504"",
            ""actions"": [
                {
                    ""name"": ""NextStep"",
                    ""type"": ""Button"",
                    ""id"": ""34587131-e07f-406c-b80e-e4cfc160f404"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SquareOrX"",
                    ""type"": ""Button"",
                    ""id"": ""049fff91-47af-4cd6-9390-c52018bad366"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TriangleOrY"",
                    ""type"": ""Button"",
                    ""id"": ""1d5f5576-8706-459f-ba63-0573e4acf5f6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PreviousStep"",
                    ""type"": ""Button"",
                    ""id"": ""ab3730bb-6bc9-4845-9e86-9eb5bb992601"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Navigation"",
                    ""type"": ""Button"",
                    ""id"": ""45ff342e-2ef8-44c5-b36e-48c762097796"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DetectGamepad"",
                    ""type"": ""Button"",
                    ""id"": ""495d66e9-6d01-47e5-9d68-ad14c97eb236"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DetectKeyboardAndMouse"",
                    ""type"": ""Button"",
                    ""id"": ""7b39f94b-56f6-4a16-a523-c290099a21e5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""L2OrLT"",
                    ""type"": ""Button"",
                    ""id"": ""752bb817-2a85-401b-a170-0f8548277d59"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""StickDeadzone"",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0415794b-c2ec-4c2d-966f-118827013401"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""NextStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6fd5f404-c5dc-4313-960a-3ee916b12228"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""NextStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4b493cfd-b2d2-4930-9863-916522047987"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""NextStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7df2da36-5fc7-46b8-9744-b7c7aff1d8da"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""NextStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d3305539-4544-4ea8-8ce2-f71c2526bfa6"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PreviousStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c83544f1-6283-4575-b993-119eec0ab06b"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PreviousStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a8a1f5e1-ef16-4ef1-8c8b-853bd44c599b"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PreviousStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a24ff4f5-10d6-4c1d-afae-9269620753f3"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PreviousStep"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6bb624b2-5be9-43b7-8433-e1e886faf649"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Navigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a896f8ee-9898-4a15-bf3b-f913f1c59870"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e825af0f-d407-4f10-b320-56f83d4742e6"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SquareOrX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a17281f-8348-44ab-9a54-f288ee1983d7"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""TriangleOrY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d379f278-16d2-42b5-9f1c-2cb7a44079a6"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84ae4776-422f-41f8-8bf1-1c61efc20b33"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25e975a6-54bf-4a0b-8a76-a97d34d64c1e"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""299bb787-12a7-4a4a-9379-cf37628b986b"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6e80acef-0c2a-4b69-9e7d-26df18972e9b"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""75946c1e-40e0-4fc4-b843-80bb1a0d7fd4"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae7e3908-a345-42b9-bdcd-63036c328591"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a03584ff-c4ad-4341-9681-1d4d460d4393"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf0b4755-43ea-48e2-ac41-00ee15910f2a"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b445e3b2-b63e-45a5-8099-b7a048174fb6"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ecf2fa3-9722-4688-b100-de418940985a"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8249877c-810e-4d98-bc54-d295ced592b7"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""995090e8-ba78-4c7a-a582-ca4deeaea6b2"",
                    ""path"": ""<Gamepad>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b86b065c-cbaa-4c0e-9b44-81a35c2d41b5"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef196544-1003-4ecc-aca9-9fdf1fec57bb"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3bfe9762-9493-4e0f-82a7-a7c2f74cf21e"",
                    ""path"": ""<Gamepad>/rightStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac0f67ef-3bae-4263-ab35-1b5a9b8767a8"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""526043ab-1067-4cf0-9465-48adcb2ccbcd"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""92035c1c-01e2-41f6-ba7e-e47ac220c5db"",
                    ""path"": ""<Gamepad>/rightStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cb3ad7be-1061-4238-9c69-784f07cc5358"",
                    ""path"": ""<Gamepad>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3934e41c-06bc-42cf-ab12-5d1725076175"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8b37c5ca-60d8-415f-8b48-e146eb041b0f"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e7fb101a-7c95-4acf-a433-295ecccf14dc"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1353c747-7758-428d-966d-cf86f1adf26a"",
                    ""path"": ""<Keyboard>/anyKey"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf2691ea-f9ca-45b4-a4a1-cdd3c5d9e40c"",
                    ""path"": ""<Mouse>/backButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""984f560f-3e42-4237-86e1-895d5c814e87"",
                    ""path"": ""<Mouse>/forwardButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ea481727-6d98-4bb4-8eeb-ba4b91edd420"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd5fcb93-7bf7-489b-b84c-be62f5171e44"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""631f0d84-b5b8-4df2-81c5-51bc6aec4a0d"",
                    ""path"": ""<Mouse>/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4fa314a8-1e07-4dbe-988b-189d43ee6383"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Mouse"",
                    ""id"": ""fea3e2ab-0f8c-419c-883d-c67a2fca837a"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""9a481c68-7470-4670-9f58-e92dbd6210d7"",
                    ""path"": ""<Mouse>/position/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""fe6146be-c316-4b7c-8a7a-ef100252f061"",
                    ""path"": ""<Mouse>/position/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DetectKeyboardAndMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""2f3eecb7-d860-4992-839c-3798eaaf15d4"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""L2OrLT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard & Mouse"",
            ""bindingGroup"": ""Keyboard & Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<DualShockGamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Movement = m_Player.FindAction("Movement", throwIfNotFound: true);
        m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
        m_Player_Camera = m_Player.FindAction("Camera", throwIfNotFound: true);
        m_Player_ReverseCamera = m_Player.FindAction("ReverseCamera", throwIfNotFound: true);
        m_Player_PowerUpForward = m_Player.FindAction("PowerUpForward", throwIfNotFound: true);
        m_Player_PowerUpBack = m_Player.FindAction("PowerUpBack", throwIfNotFound: true);
        m_Player_MousePosition = m_Player.FindAction("MousePosition", throwIfNotFound: true);
        m_Player_MouseClic = m_Player.FindAction("MouseClic", throwIfNotFound: true);
        m_Player_Option = m_Player.FindAction("Option", throwIfNotFound: true);
        m_Player_Any = m_Player.FindAction("Any", throwIfNotFound: true);
        m_Player_DebugQuit = m_Player.FindAction("Debug Quit", throwIfNotFound: true);
        m_Player_DebugShrink = m_Player.FindAction("Debug Shrink", throwIfNotFound: true);
        m_Player_DebugPowerUp = m_Player.FindAction("Debug PowerUp", throwIfNotFound: true);
        m_Player_DebugFonction = m_Player.FindAction("Debug Fonction", throwIfNotFound: true);
        m_Player_ChatUp = m_Player.FindAction("ChatUp", throwIfNotFound: true);
        m_Player_ChatDown = m_Player.FindAction("ChatDown", throwIfNotFound: true);
        m_Player_ChatLeft = m_Player.FindAction("ChatLeft", throwIfNotFound: true);
        m_Player_ChatRight = m_Player.FindAction("ChatRight", throwIfNotFound: true);
        m_Player_CloseChat = m_Player.FindAction("CloseChat", throwIfNotFound: true);
        m_Player_OpenChat = m_Player.FindAction("OpenChat", throwIfNotFound: true);
        // Menus
        m_Menus = asset.FindActionMap("Menus", throwIfNotFound: true);
        m_Menus_NextStep = m_Menus.FindAction("NextStep", throwIfNotFound: true);
        m_Menus_SquareOrX = m_Menus.FindAction("SquareOrX", throwIfNotFound: true);
        m_Menus_TriangleOrY = m_Menus.FindAction("TriangleOrY", throwIfNotFound: true);
        m_Menus_PreviousStep = m_Menus.FindAction("PreviousStep", throwIfNotFound: true);
        m_Menus_Navigation = m_Menus.FindAction("Navigation", throwIfNotFound: true);
        m_Menus_DetectGamepad = m_Menus.FindAction("DetectGamepad", throwIfNotFound: true);
        m_Menus_DetectKeyboardAndMouse = m_Menus.FindAction("DetectKeyboardAndMouse", throwIfNotFound: true);
        m_Menus_L2OrLT = m_Menus.FindAction("L2OrLT", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Movement;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_Camera;
    private readonly InputAction m_Player_ReverseCamera;
    private readonly InputAction m_Player_PowerUpForward;
    private readonly InputAction m_Player_PowerUpBack;
    private readonly InputAction m_Player_MousePosition;
    private readonly InputAction m_Player_MouseClic;
    private readonly InputAction m_Player_Option;
    private readonly InputAction m_Player_Any;
    private readonly InputAction m_Player_DebugQuit;
    private readonly InputAction m_Player_DebugShrink;
    private readonly InputAction m_Player_DebugPowerUp;
    private readonly InputAction m_Player_DebugFonction;
    private readonly InputAction m_Player_ChatUp;
    private readonly InputAction m_Player_ChatDown;
    private readonly InputAction m_Player_ChatLeft;
    private readonly InputAction m_Player_ChatRight;
    private readonly InputAction m_Player_CloseChat;
    private readonly InputAction m_Player_OpenChat;
    public struct PlayerActions
    {
        private @PlayerInputActions m_Wrapper;
        public PlayerActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Player_Movement;
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @Camera => m_Wrapper.m_Player_Camera;
        public InputAction @ReverseCamera => m_Wrapper.m_Player_ReverseCamera;
        public InputAction @PowerUpForward => m_Wrapper.m_Player_PowerUpForward;
        public InputAction @PowerUpBack => m_Wrapper.m_Player_PowerUpBack;
        public InputAction @MousePosition => m_Wrapper.m_Player_MousePosition;
        public InputAction @MouseClic => m_Wrapper.m_Player_MouseClic;
        public InputAction @Option => m_Wrapper.m_Player_Option;
        public InputAction @Any => m_Wrapper.m_Player_Any;
        public InputAction @DebugQuit => m_Wrapper.m_Player_DebugQuit;
        public InputAction @DebugShrink => m_Wrapper.m_Player_DebugShrink;
        public InputAction @DebugPowerUp => m_Wrapper.m_Player_DebugPowerUp;
        public InputAction @DebugFonction => m_Wrapper.m_Player_DebugFonction;
        public InputAction @ChatUp => m_Wrapper.m_Player_ChatUp;
        public InputAction @ChatDown => m_Wrapper.m_Player_ChatDown;
        public InputAction @ChatLeft => m_Wrapper.m_Player_ChatLeft;
        public InputAction @ChatRight => m_Wrapper.m_Player_ChatRight;
        public InputAction @CloseChat => m_Wrapper.m_Player_CloseChat;
        public InputAction @OpenChat => m_Wrapper.m_Player_OpenChat;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Camera.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCamera;
                @Camera.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCamera;
                @Camera.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCamera;
                @ReverseCamera.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReverseCamera;
                @ReverseCamera.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReverseCamera;
                @ReverseCamera.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReverseCamera;
                @PowerUpForward.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpForward;
                @PowerUpForward.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpForward;
                @PowerUpForward.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpForward;
                @PowerUpBack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpBack;
                @PowerUpBack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpBack;
                @PowerUpBack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPowerUpBack;
                @MousePosition.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @MouseClic.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseClic;
                @MouseClic.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseClic;
                @MouseClic.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseClic;
                @Option.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOption;
                @Option.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOption;
                @Option.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOption;
                @Any.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAny;
                @Any.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAny;
                @Any.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAny;
                @DebugQuit.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugQuit;
                @DebugQuit.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugQuit;
                @DebugQuit.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugQuit;
                @DebugShrink.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugShrink;
                @DebugShrink.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugShrink;
                @DebugShrink.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugShrink;
                @DebugPowerUp.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugPowerUp;
                @DebugPowerUp.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugPowerUp;
                @DebugPowerUp.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugPowerUp;
                @DebugFonction.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugFonction;
                @DebugFonction.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugFonction;
                @DebugFonction.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDebugFonction;
                @ChatUp.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatUp;
                @ChatUp.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatUp;
                @ChatUp.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatUp;
                @ChatDown.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatDown;
                @ChatDown.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatDown;
                @ChatDown.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatDown;
                @ChatLeft.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatLeft;
                @ChatLeft.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatLeft;
                @ChatLeft.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatLeft;
                @ChatRight.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatRight;
                @ChatRight.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatRight;
                @ChatRight.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChatRight;
                @CloseChat.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCloseChat;
                @CloseChat.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCloseChat;
                @CloseChat.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCloseChat;
                @OpenChat.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenChat;
                @OpenChat.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenChat;
                @OpenChat.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenChat;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Camera.started += instance.OnCamera;
                @Camera.performed += instance.OnCamera;
                @Camera.canceled += instance.OnCamera;
                @ReverseCamera.started += instance.OnReverseCamera;
                @ReverseCamera.performed += instance.OnReverseCamera;
                @ReverseCamera.canceled += instance.OnReverseCamera;
                @PowerUpForward.started += instance.OnPowerUpForward;
                @PowerUpForward.performed += instance.OnPowerUpForward;
                @PowerUpForward.canceled += instance.OnPowerUpForward;
                @PowerUpBack.started += instance.OnPowerUpBack;
                @PowerUpBack.performed += instance.OnPowerUpBack;
                @PowerUpBack.canceled += instance.OnPowerUpBack;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
                @MouseClic.started += instance.OnMouseClic;
                @MouseClic.performed += instance.OnMouseClic;
                @MouseClic.canceled += instance.OnMouseClic;
                @Option.started += instance.OnOption;
                @Option.performed += instance.OnOption;
                @Option.canceled += instance.OnOption;
                @Any.started += instance.OnAny;
                @Any.performed += instance.OnAny;
                @Any.canceled += instance.OnAny;
                @DebugQuit.started += instance.OnDebugQuit;
                @DebugQuit.performed += instance.OnDebugQuit;
                @DebugQuit.canceled += instance.OnDebugQuit;
                @DebugShrink.started += instance.OnDebugShrink;
                @DebugShrink.performed += instance.OnDebugShrink;
                @DebugShrink.canceled += instance.OnDebugShrink;
                @DebugPowerUp.started += instance.OnDebugPowerUp;
                @DebugPowerUp.performed += instance.OnDebugPowerUp;
                @DebugPowerUp.canceled += instance.OnDebugPowerUp;
                @DebugFonction.started += instance.OnDebugFonction;
                @DebugFonction.performed += instance.OnDebugFonction;
                @DebugFonction.canceled += instance.OnDebugFonction;
                @ChatUp.started += instance.OnChatUp;
                @ChatUp.performed += instance.OnChatUp;
                @ChatUp.canceled += instance.OnChatUp;
                @ChatDown.started += instance.OnChatDown;
                @ChatDown.performed += instance.OnChatDown;
                @ChatDown.canceled += instance.OnChatDown;
                @ChatLeft.started += instance.OnChatLeft;
                @ChatLeft.performed += instance.OnChatLeft;
                @ChatLeft.canceled += instance.OnChatLeft;
                @ChatRight.started += instance.OnChatRight;
                @ChatRight.performed += instance.OnChatRight;
                @ChatRight.canceled += instance.OnChatRight;
                @CloseChat.started += instance.OnCloseChat;
                @CloseChat.performed += instance.OnCloseChat;
                @CloseChat.canceled += instance.OnCloseChat;
                @OpenChat.started += instance.OnOpenChat;
                @OpenChat.performed += instance.OnOpenChat;
                @OpenChat.canceled += instance.OnOpenChat;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);

    // Menus
    private readonly InputActionMap m_Menus;
    private IMenusActions m_MenusActionsCallbackInterface;
    private readonly InputAction m_Menus_NextStep;
    private readonly InputAction m_Menus_SquareOrX;
    private readonly InputAction m_Menus_TriangleOrY;
    private readonly InputAction m_Menus_PreviousStep;
    private readonly InputAction m_Menus_Navigation;
    private readonly InputAction m_Menus_DetectGamepad;
    private readonly InputAction m_Menus_DetectKeyboardAndMouse;
    private readonly InputAction m_Menus_L2OrLT;
    public struct MenusActions
    {
        private @PlayerInputActions m_Wrapper;
        public MenusActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @NextStep => m_Wrapper.m_Menus_NextStep;
        public InputAction @SquareOrX => m_Wrapper.m_Menus_SquareOrX;
        public InputAction @TriangleOrY => m_Wrapper.m_Menus_TriangleOrY;
        public InputAction @PreviousStep => m_Wrapper.m_Menus_PreviousStep;
        public InputAction @Navigation => m_Wrapper.m_Menus_Navigation;
        public InputAction @DetectGamepad => m_Wrapper.m_Menus_DetectGamepad;
        public InputAction @DetectKeyboardAndMouse => m_Wrapper.m_Menus_DetectKeyboardAndMouse;
        public InputAction @L2OrLT => m_Wrapper.m_Menus_L2OrLT;
        public InputActionMap Get() { return m_Wrapper.m_Menus; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenusActions set) { return set.Get(); }
        public void SetCallbacks(IMenusActions instance)
        {
            if (m_Wrapper.m_MenusActionsCallbackInterface != null)
            {
                @NextStep.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnNextStep;
                @NextStep.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnNextStep;
                @NextStep.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnNextStep;
                @SquareOrX.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnSquareOrX;
                @SquareOrX.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnSquareOrX;
                @SquareOrX.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnSquareOrX;
                @TriangleOrY.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnTriangleOrY;
                @TriangleOrY.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnTriangleOrY;
                @TriangleOrY.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnTriangleOrY;
                @PreviousStep.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnPreviousStep;
                @PreviousStep.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnPreviousStep;
                @PreviousStep.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnPreviousStep;
                @Navigation.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnNavigation;
                @Navigation.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnNavigation;
                @Navigation.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnNavigation;
                @DetectGamepad.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectGamepad;
                @DetectGamepad.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectGamepad;
                @DetectGamepad.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectGamepad;
                @DetectKeyboardAndMouse.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectKeyboardAndMouse;
                @DetectKeyboardAndMouse.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectKeyboardAndMouse;
                @DetectKeyboardAndMouse.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnDetectKeyboardAndMouse;
                @L2OrLT.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnL2OrLT;
                @L2OrLT.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnL2OrLT;
                @L2OrLT.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnL2OrLT;
            }
            m_Wrapper.m_MenusActionsCallbackInterface = instance;
            if (instance != null)
            {
                @NextStep.started += instance.OnNextStep;
                @NextStep.performed += instance.OnNextStep;
                @NextStep.canceled += instance.OnNextStep;
                @SquareOrX.started += instance.OnSquareOrX;
                @SquareOrX.performed += instance.OnSquareOrX;
                @SquareOrX.canceled += instance.OnSquareOrX;
                @TriangleOrY.started += instance.OnTriangleOrY;
                @TriangleOrY.performed += instance.OnTriangleOrY;
                @TriangleOrY.canceled += instance.OnTriangleOrY;
                @PreviousStep.started += instance.OnPreviousStep;
                @PreviousStep.performed += instance.OnPreviousStep;
                @PreviousStep.canceled += instance.OnPreviousStep;
                @Navigation.started += instance.OnNavigation;
                @Navigation.performed += instance.OnNavigation;
                @Navigation.canceled += instance.OnNavigation;
                @DetectGamepad.started += instance.OnDetectGamepad;
                @DetectGamepad.performed += instance.OnDetectGamepad;
                @DetectGamepad.canceled += instance.OnDetectGamepad;
                @DetectKeyboardAndMouse.started += instance.OnDetectKeyboardAndMouse;
                @DetectKeyboardAndMouse.performed += instance.OnDetectKeyboardAndMouse;
                @DetectKeyboardAndMouse.canceled += instance.OnDetectKeyboardAndMouse;
                @L2OrLT.started += instance.OnL2OrLT;
                @L2OrLT.performed += instance.OnL2OrLT;
                @L2OrLT.canceled += instance.OnL2OrLT;
            }
        }
    }
    public MenusActions @Menus => new MenusActions(this);
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard & Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCamera(InputAction.CallbackContext context);
        void OnReverseCamera(InputAction.CallbackContext context);
        void OnPowerUpForward(InputAction.CallbackContext context);
        void OnPowerUpBack(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
        void OnMouseClic(InputAction.CallbackContext context);
        void OnOption(InputAction.CallbackContext context);
        void OnAny(InputAction.CallbackContext context);
        void OnDebugQuit(InputAction.CallbackContext context);
        void OnDebugShrink(InputAction.CallbackContext context);
        void OnDebugPowerUp(InputAction.CallbackContext context);
        void OnDebugFonction(InputAction.CallbackContext context);
        void OnChatUp(InputAction.CallbackContext context);
        void OnChatDown(InputAction.CallbackContext context);
        void OnChatLeft(InputAction.CallbackContext context);
        void OnChatRight(InputAction.CallbackContext context);
        void OnCloseChat(InputAction.CallbackContext context);
        void OnOpenChat(InputAction.CallbackContext context);
    }
    public interface IMenusActions
    {
        void OnNextStep(InputAction.CallbackContext context);
        void OnSquareOrX(InputAction.CallbackContext context);
        void OnTriangleOrY(InputAction.CallbackContext context);
        void OnPreviousStep(InputAction.CallbackContext context);
        void OnNavigation(InputAction.CallbackContext context);
        void OnDetectGamepad(InputAction.CallbackContext context);
        void OnDetectKeyboardAndMouse(InputAction.CallbackContext context);
        void OnL2OrLT(InputAction.CallbackContext context);
    }
}
