﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class KeyboardScript : MonoBehaviour
{
    enum KeyboardLayouts { MajusculeLayout, MinusculeLayout};
    private KeyboardLayouts currentKeyboardLayout;

    public GameObject MajLayout;
    public GameObject MinLayout;

    public Button min_MajButton;
    public Button maj_MajButton;

    public Button btn_Ok;
    public Button savePreviousBackBtn;

    public EventSystem evt;

    public Text TextInput;

    public Text keyboardText;

    public Button a_majuscule;

    public Button[] allButtonsSize1;
    public Button[] allButtonsSize2;
    public Button[] allButtonsSize3;

    public Button[] allButtonsMin;
    public Button[] allButtonsMaj;
    public Button[] allButtonsSpecial;

    public Sprite spriteNoSelectedBtn1;
    public Sprite spriteSelectedBtn1;
    public Sprite spriteNoSelectedBtn2;
    public Sprite spriteSelectedBtn2;
    public Sprite spriteNoSelectedBtn3;
    public Sprite spriteSelectedBtn3;

    public int maxCharacters;

    public List<GameObject> goToDisable;
    private List<GameObject> goToDisableCopy;

    public GameObject UiIcons;

    private InputManager im;
    private ButtonManager bm;

    //public GameObject RusLayoutSml, RusLayoutBig, EngLayoutSml, EngLayoutBig, SymbLayout;

    public void OnEnable()
    {
        Debug.Log("startkeyboard");
        if (evt == null)
        {
            Debug.Log("startkeyboard2");
            evt = EventSystem.current;
            Debug.Log("startkeyboard3");
        }

        //On récupère l'InputManager
        im = FindObjectOfType<InputManager>();

        bm = FindObjectOfType<ButtonManager>();

        currentKeyboardLayout = KeyboardLayouts.MajusculeLayout;

        UiIcons.SetActive(true);

        //Destroy(a_minuscule.gameObject);
        Invoke("OnEnableFinished",0.1f);
        //evt.SetSelectedGameObject(a_minuscule.gameObject);
        Debug.Log("startkeyboard4");
    }

    public void OnEnableFinished()
    {
        evt.SetSelectedGameObject(a_majuscule.gameObject);

        if(TextInput != null)
        {
            keyboardText.text = TextInput.text;
        }
        else
        {
            keyboardText.text = PlayerPrefs.GetString("Nickname");
        }

        bm.currentBackBtn = btn_Ok;

        goToDisableCopy = new List<GameObject>();

        foreach (GameObject go in goToDisable)
        {
            goToDisableCopy.Add(go);

            if (go.activeSelf)
            {
                go.SetActive(false);
            }
            else
            {
                goToDisableCopy.Remove(go);
            }
        }
    }

    public void Update()
    {
        foreach (Button btn in allButtonsSize1)
        {
            if(btn.gameObject == evt.currentSelectedGameObject)
            {
                btn.image.sprite = spriteSelectedBtn1;
            }
            else
            {
                btn.image.sprite = spriteNoSelectedBtn1;
            }
        }

        foreach (Button btn in allButtonsSize2)
        {
            if (btn.gameObject == evt.currentSelectedGameObject)
            {
                btn.image.sprite = spriteSelectedBtn2;
            }
            else
            {
                btn.image.sprite = spriteNoSelectedBtn2;
            }
        }

        foreach (Button btn in allButtonsSize3)
        {
            if (btn.gameObject == evt.currentSelectedGameObject)
            {
                btn.image.sprite = spriteSelectedBtn3;
            }
            else
            {
                btn.image.sprite = spriteNoSelectedBtn3;
            }
        }

        //Si on appui sur la touche de suppression (Carré ou X)
        if (im.menu_SquareOrX)
        {
            BackSpace();
        }

        //Si on appui sur la touche de suppression (Carré ou X)
        if (im.menu_TriangleOrY)
        {
            alphabetFunction(" ");
        }

        //Si on appui sur la touche de suppression (Carré ou X)
        if (im.menu_L2OrLT)
        {
            ChangeMajMin();
        }
    }

    public void alphabetFunction(string alphabet)
    {
        if (keyboardText.text.Length < maxCharacters)
        {
            keyboardText.text = keyboardText.text + alphabet;
        }
    }

    public void BackSpace()
    { 
        if (keyboardText.text.Length > 0)
        {
            keyboardText.text = keyboardText.text.Remove(keyboardText.text.Length - 1);
        }
    }

    public void ChangeMajMin()
    {
        if(currentKeyboardLayout == KeyboardLayouts.MajusculeLayout)
        {
            currentKeyboardLayout = KeyboardLayouts.MinusculeLayout;

            MinLayout.SetActive(true);
            //min_MajButton.Select();

            for (int i = 0; i < allButtonsMaj.Length; i++)
            {
                if (evt.currentSelectedGameObject == allButtonsMaj[i].gameObject)
                {
                    allButtonsMin[i].Select();
                    break;
                }
            }

            MajLayout.SetActive(false);
        }
        else
        {
            currentKeyboardLayout = KeyboardLayouts.MajusculeLayout;

            MajLayout.SetActive(true);

            for (int i = 0; i < allButtonsMin.Length; i++)
            {
                if (evt.currentSelectedGameObject == allButtonsMin[i].gameObject)
                {
                    allButtonsMaj[i].Select();
                    break;
                }
            }

            MinLayout.SetActive(false);
        }
    }

    public void ReEnableAllGameObjects()
    {
        if(TextInput != null)
        {
            TextInput.text = keyboardText.text;
        }       

        foreach (GameObject go in goToDisableCopy)
        {
            go.SetActive(true);
        }

        bm.currentBackBtn = savePreviousBackBtn;

        UiIcons.SetActive(false);
    }

    public void ShowLayout(GameObject SetLayout)
    {
        SetLayout.SetActive(true);
    }
}
