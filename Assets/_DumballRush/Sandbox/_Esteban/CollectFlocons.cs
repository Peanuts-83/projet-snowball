﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectFlocons : MonoBehaviour
{
    //Le nombre de flocons obtenus
    [SerializeField]
    private int numberFindFlocons;

    //Le nombre total de flocon
    [SerializeField]
    private int totalFlocons;

    //Si le script a trouvé les flocons
    private bool hasFindFlocons = true;

    private void Start()
    {
        //On cherche l'objet contenant tous les flocons
        GameObject floconParent = GameObject.Find("Flocons");

        //Si l'objet n'est pas trouvé on n'execute pas la suite du code
        if (floconParent == null)
        {
            hasFindFlocons = false;
            return;
        }

        //On donne le total de flocons
        totalFlocons = floconParent.transform.childCount;
        //On reset la variable de compte de flocon
        numberFindFlocons = 0;
    }

    /// <summary>
    /// Lorsque le joueur entre en collision avec un objet
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //Si les flocons n'ont pas été trouvé, on n'exécute pas la suite du code
        if (!hasFindFlocons)
            return;

        //On ajoute un flocon au compte si le joueur percute un flocon
        if (other.gameObject.tag == "Flocon")
        {
            other.gameObject.SetActive(false);

            numberFindFlocons++;
            Debug.Log($"Flocons trouvés : {numberFindFlocons}/{totalFlocons} | ({Mathf.Floor((float)numberFindFlocons/(float)totalFlocons*100f)}%)");
        }
    }

    #region Ancien

    /*public List<bool> allBooleans;
    public List<GameObject> allFlocons;*/

    /*
    // Start is called before the first frame update
    void Start()
    {
        floconParent = GameObject.Find("Flocons");

        if (floconParent == null)
            return;

        for (int i = 0; i < floconParent.transform.childCount; i++)
        {
            allFlocons.Add(floconParent.transform.GetChild(i).gameObject);
        }

        foreach (GameObject flocon in allFlocons)
        {
            bool newbool = new bool();
            allBooleans.Add(newbool);
        }

        HowManyFindFlocons();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (floconParent == null)
            return;

        if (other.gameObject.tag == "Flocon")
        {
            for (int i = 0; i < allFlocons.Count; i++)
            {
                if(allFlocons[i] == other.gameObject)
                {
                    allFlocons[i].SetActive(false);

                    allBooleans[i] = true;
                    HowManyFindFlocons();
                }
            }
        }
    }

    public void HowManyFindFlocons()
    {
        numberFindFlocons = 0;

        foreach  (bool floconBoolean in allBooleans)
        {
            if(floconBoolean == true)
            {
                numberFindFlocons++;
            }
        }

        Debug.Log("Flocons trouvés : " + numberFindFlocons + " / " + allFlocons.Count);
    }*/
    #endregion
}
