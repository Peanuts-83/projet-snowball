﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// ANALYTICS 
/// </summary>
public class AnalyticsDB : Singleton<AnalyticsDB>
{

    #region Singleton
    //Pour être sur que le constructeur est un Singleton
    protected AnalyticsDB() { }
    #endregion

    public void UpdateDebug(string name)
    {
        Debug.Log("... Sending analytics : " + name);
    }

    /// <summary>
    /// quand le jeu est ouvert
    /// </summary>
    public void Ana_GameStart()
    {
        AnalyticsEvent.GameStart();
    } 

    /// <summary>
    /// quand le jeu est fermé
    /// </summary>
    public void Ana_GameStop()
    {
        AnalyticsEvent.GameOver();
    }


    /// <summary>
    /// Stats course basique 
    /// </summary>
    /// <param name="dict"></param>
    public void Ana_StartRace(Dictionary<string,object> dict)
    {
        AnalyticsEvent.Custom("Start_race", dict);
        UpdateDebug("Ana_StartRace");
    }

    /// <summary>
    /// Temps passé dans le lobby
    /// </summary>
    /// <param name="dict"></param>
    public void Ana_TimeInLobby(Dictionary<string, object> dict)
    {
        AnalyticsEvent.Custom("Time_in_lobby", dict);
        UpdateDebug("Ana_TimeInLobby");
    }
    
    /// <summary>
    /// timer par course 
    /// </summary>
    public void Ana_TimerRace(Dictionary<string, object> dict)
    {
        //Peut être ne pas envoyer le timer en lui même mais une échelle: "J'envoie "Compris entre 1m10 et 1m12" si le timer est 1m11" par exemple

        AnalyticsEvent.Custom("Timer_race", dict);
        UpdateDebug("Ana_TimerRace");
    }

    /// <summary>
    /// Quand le joueur récupère un power up
    /// </summary>
    /// <param name="dict"></param>
    public void Ana_PowerUpTaken(Dictionary<string, object> dict)
    {
        AnalyticsEvent.Custom("PowerUpTaken", dict);
        UpdateDebug("Ana_PowerUpTaken");
    }

    /// <summary>
    /// Change le statut du joueur
    /// </summary>
    /// <param name="dict"></param>
    public void Ana_PlayerState(Dictionary<string,object> dict)
    {
        AnalyticsEvent.Custom("State", dict);
        UpdateDebug("Ana_PlayerState");
    }

    public enum Menus
    {
        principal,
        play,
        custom,
        option,
        credit,
        create_room,
        search_room,
        quit
    }

    /// <summary>
    /// Transmet les infos pour savoir dans quels menus les joueurs se rendent
    /// </summary>
    /// <param name="menu"></param>
    public void Ana_Menus(Menus menu)
    {
        Dictionary<string,object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"menu", menu}
        };

        AnalyticsEvent.Custom("Menus", dict);
        UpdateDebug("Ana_Menus " + menu);
    }

    public enum Socials
    {
        tiktok,
        instagram,
        youtube,
        discord,
        twitter
    }

    /// <summary>
    /// Transmet l'information si les gens clique sur les réseaux
    /// </summary>
    /// <param name="social"></param>
    public void Ana_Socials(Socials social)
    {
        Dictionary<string, object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"social", social}
        };

        AnalyticsEvent.Custom("Social", dict);
        UpdateDebug("Ana_Menus " + social);
    }

    /// <summary>
    /// Temps d'aspiration total sur toute la course
    /// </summary>
    /// <param name="time"></param>
    public void Ana_Aspiration_Total(float time)
    {
        time = Mathf.Round(time*10);
        time = time / 10;

        Dictionary<string, object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"aspiration_total", time}
        };

        AnalyticsEvent.Custom("Aspiration_Total", dict);
        UpdateDebug("Ana_Aspiration_Total " + time);
    }

    /// <summary>
    /// Temps d'aspiration sur un seul passage dans l'aspiration
    /// </summary>
    /// <param name="time"></param>
    public void Ana_Aspiration_Unique(float time)
    {
        time = Mathf.Round(time * 10);
        time = time / 10;

        Dictionary<string, object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"aspiration_unique", time}
        };

        AnalyticsEvent.Custom("Aspiration_Unique", dict);
        UpdateDebug("Ana_Aspiration_Unique " + time);
    }

    public void Ana_FPSinfos(float averageFps, string sceneName)
    {
        Dictionary<string, object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"race", sceneName},
            {"averageFps", averageFps}
        };

        AnalyticsEvent.Custom("FPS", dict);
        UpdateDebug("Ana_FPSinfos " + averageFps);
    }

    public void Ana_Pinginfos(float averagePing, string sceneName)
    {
        Dictionary<string, object> dict = new Dictionary<string, object>
        {
            {"date", (System.DateTime.Now).ToString()},
            {"race", sceneName},
            {"averagePing", averagePing}
        };

        AnalyticsEvent.Custom("PING", dict);
        UpdateDebug("Ana_Pinginfos " + averagePing);
    }
}
