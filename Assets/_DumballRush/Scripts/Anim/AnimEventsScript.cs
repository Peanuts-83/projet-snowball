﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class AnimEventsScript : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{
    AudioManager am;

    public Animator at;

    public GameObject chair;

    public GameObject armoire;

    private Selectable sel;

    public GameObject buttonManager;

    public bool NoSoundOnOverlap;

    private void Start()
    {
        at = gameObject.GetComponent<Animator>();

        am = FindObjectOfType<AudioManager>();

        sel = GetComponent<Selectable>(); 

        //Créé une snowSlideInstance et magma
        am.soundEffectConfiguration.InstanceSnowSlide = am.CreateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide,0f);
    }

    public void DisappearCustomPanelEvent()
    {
        CustomisationPlayer cp = FindObjectOfType<CustomisationPlayer>();

        if (cp.allChosePanelButtonsParent != null)
        {
            cp.allChosePanelButtonsParent.SetActive(true);

            cp.evt.SetSelectedGameObject(cp.allChosePanelButtons[0].gameObject);

            foreach (Button panelButton in cp.allChosePanelButtons)
            {
                panelButton.interactable = true;
            }
        }

        gameObject.SetActive(false);

    }

    public void AfterPressedButtonChosePanel()
    {
        Debug.Log("AfterPressedButtonChosePanel");
        
        
    }

    public void CloseArmoire()
    {
        if(armoire.GetComponent<Animator>().GetBool("IsNicknameMenu") == false)
        {
            armoire.GetComponent<Animator>().SetBool("DoorIsOpen",false);
        }     
    }

    public void DisappearButtonsChosePanel()
    {
        CustomisationPlayer cp = FindObjectOfType<CustomisationPlayer>();
        if (cp.allChosePanelButtonsParent != null)
            cp.allChosePanelButtonsParent.SetActive(false);
    }



    //Do this when the selectable UI object is selected.
    public void OnSelect(BaseEventData eventData)
    {
        if (FindObjectOfType<ButtonManager>().currentInputType == ButtonManager.InputType.Manette)
            am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventOverlappButtons);

    }


    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (NoSoundOnOverlap == false)
        {
            am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventOverlappButtons);
        }   
    }





    /// <summary>
    /// Joue un son quand le joueur de la scene slide sur la neige
    /// </summary>
    public void MakeSnowSlideGreatAgain()
    {
        if (at.GetCurrentAnimatorStateInfo(0).speed == 0.5f)
        {
            //Assigne la valeur
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, 0.5f);
        }
        else if(at.GetCurrentAnimatorStateInfo(0).speed == -0.5f)
        {
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, 0f);
        }
    }
    
    /// <summary>
    /// arrete le son de slide sur la neige pour le joueur du menu
    /// </summary>
    public void MakeSnowSlideNotGreatAgain()
    {
        if (at.GetCurrentAnimatorStateInfo(0).speed == 0.5f)
        {
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, 0f);
        }
        else if (at.GetCurrentAnimatorStateInfo(0).speed == -0.5f)
        {
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, 0.5f);
        }
    }

    public void OnEndEditNickname(GameObject InputFieldGO)
    {
        InputFieldGO.GetComponent<Animator>().Play("Pressed");
        
        //FindObjectOfType<CustomisationPlayer>().evt.SetSelectedGameObject(FindObjectOfType<CustomisationPlayer>().BackButton.gameObject);
    }

    public void BallInChair()
    {
        Debug.Log(at.GetCurrentAnimatorStateInfo(0).speed);

        if (at.GetCurrentAnimatorStateInfo(0).speed == 0.5f)
        {
            chair.GetComponent<Animator>().SetTrigger("Begin");
        }
        else if (at.GetCurrentAnimatorStateInfo(0).speed == -0.5f)
        {
            chair.GetComponent<Animator>().SetTrigger("End");
        }
    }

    public void TitleScreenCanClick()
    {
        buttonManager.GetComponent<ButtonManager>().isInTitle = true;
    }
}
