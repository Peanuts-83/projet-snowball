﻿using UnityEngine;

public class BalloonController : MonoBehaviour
{
    [Header("Movements")]
    [SerializeField]
    [Tooltip("Vitesse de la montgolfière sur un aller-retour")]
    private float frequence;
    [SerializeField]
    [Tooltip("Random de vitesse, ajoute une valeur entre x et y à la fréquence")]
    private Vector2 randomAddingToFrequence;

    [SerializeField]
    [Tooltip("Amplitude du mouvement en unité, permet de définir le déplacement maximale que prend la montgolfère lors d'une période")]
    private float amplitude;
    [SerializeField]
    [Tooltip("Random de l'amplitude, ajoute à l'amplitude une valeur entre x et y")]
    private Vector2 randomAddingToAmplitude;

    [SerializeField]
    [Tooltip("Direction dans laquelle la montgolfière se déplace")]
    private Vector3 movementDirection;

    [SerializeField]
    [Tooltip("Permet ou non, de créer du random sur la direction")]
    private bool enableRandomOnDirection;
    [SerializeField]
    [Tooltip("random de directipon, donne un vecteur compris entre 'movementDirection' et 'randomMovementDirection'")]
    private Vector3 randomMovementDirection;

    [Header("Rotations")]
    [SerializeField]
    [Tooltip("Vitesse de rotation")]
    private float rotationSpeed;
    [SerializeField]
    [Tooltip("Random sur la vitesse de rotation, ajoute une valeur entre x et y")]
    private Vector2 randomRotationSpeed;

    [SerializeField]
    [Tooltip("Utilisation de la direction locale")]
    private bool useLocalDirection;
    [SerializeField]
    [Tooltip("Utilisation de la rotation locale !!! Non testé !!!")]
    private bool useLocalRotation;

    private Vector3 initialPosition;
    private Vector3 initialPositionWithOffset;
    private float offset;
    private Vector3 localMovement;

    private void Start()
    {
        initialPosition = this.transform.position;

        frequence += Random.Range(randomAddingToFrequence.x, randomAddingToFrequence.y);
        amplitude += Random.Range(randomAddingToAmplitude.x, randomAddingToAmplitude.y);

        if (enableRandomOnDirection)
        {
            movementDirection = Vector3.Lerp(movementDirection, randomMovementDirection, Random.Range(0f, 1f));
        }

        rotationSpeed += Random.Range(randomRotationSpeed.x, randomRotationSpeed.y);

        if (useLocalDirection)
        {
            localMovement = new Vector3(transform.forward.x * movementDirection.normalized.x, transform.forward.y * movementDirection.normalized.y, transform.forward.z * movementDirection.normalized.z);
            initialPositionWithOffset = initialPosition + localMovement.normalized * amplitude;
        }
        else
        {
            initialPositionWithOffset = initialPosition + movementDirection.normalized * amplitude;
        }
    }

    void Update()
    {
        offset = Mathf.Sin(Time.time * frequence) * amplitude;
        
        if (useLocalDirection)
        {
            localMovement = new Vector3(transform.forward.x * movementDirection.normalized.x, transform.forward.y * movementDirection.normalized.y, transform.forward.z * movementDirection.normalized.z);
            
            initialPositionWithOffset = initialPosition + localMovement.normalized * amplitude;
            transform.position = initialPositionWithOffset + offset * localMovement.normalized;
        }
        else
        {
            initialPositionWithOffset = initialPosition + movementDirection.normalized * amplitude;
            transform.position = initialPositionWithOffset + offset * movementDirection.normalized;
        }

        if (useLocalRotation)
        {
            transform.Rotate(transform.localEulerAngles, Time.deltaTime * rotationSpeed);
        }
        else
        {
            transform.Rotate(Vector3.up, Time.deltaTime * rotationSpeed);
        }
    }
}
