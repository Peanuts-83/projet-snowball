﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNumberAnim : MonoBehaviour
{
    [SerializeField]
    private string parameterName;

    [SerializeField]
    private Vector2 randomBetween;

    [SerializeField]
    private float timeUpdate;

    [HideInInspector]
    public int voiceID;

    private AudioManager am;
    private ClassementManager cm;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChooseRandom());

        am = FindObjectOfType<AudioManager>();
        cm = FindObjectOfType<ClassementManager>();
    }

    private IEnumerator ChooseRandom()
    {
        yield return new WaitForSeconds(timeUpdate);

        GetComponent<Animator>().SetInteger($"{parameterName}", Random.Range((int)randomBetween.x, (int)randomBetween.y));

        StartCoroutine(ChooseRandom());
    }

    public void PlayCelebration()
    {
        if (cm.targetStep == 0)
            am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCelebration, this.gameObject, "ChoixVoix", voiceID, 0.24f);
    }
}
