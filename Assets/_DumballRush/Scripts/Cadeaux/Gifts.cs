﻿using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

using Photon.Pun;

/*
 * 
 * Contrôle la disparition et la réaparition des cadeaux + VFX
 * Le script doit se trouver sur le prefab du cadeau
 * 
 */

public class Gifts : MonoBehaviour
{
    [Tooltip("Temps de réapparition des cadeaux")]
    public float timeBeforeActive;

    [Tooltip("Les renderers des différents meshs du cadeau")]
    public SkinnedMeshRenderer[] allMeshRend;

    [Tooltip("VFX de luminescance du cadeau")]
    public GameObject goVfxIllu;

    //Fonction lancée à la collision d'un joueur avec le cadeau
    //Appellée à partir de PowerUpController
    [PunRPC]
    public void TakeGift()
    {
        //Lance la coroutine de désactivation
        StartCoroutine(DesactivateAndActivate()); 
    }

    //Coroutine permettant de désactiver pendant X secondes le cadeau récupéré
    private IEnumerator DesactivateAndActivate()
    {

        Debug.Log(allMeshRend.Length);
        //Récupère le collider de l'objet et le désactive
        gameObject.GetComponent<Collider>().enabled = false;
        //Désactive les particules d'illuminations
        PlayIllu(false);
        //Pour tous les meshs dans le cadeau
        foreach (SkinnedMeshRenderer mesh in allMeshRend)
        {
            //On les désactive
            mesh.enabled = false;
        }

        //On attend le temps nécéssaire pour réactivé le cadeau
        yield return new WaitForSeconds(timeBeforeActive);
        
        //On réactive le collider
        gameObject.GetComponent<Collider>().enabled = true;
        //On réactive l'illumination
        PlayIllu(true);
        //Pour tous les meshs
        foreach (SkinnedMeshRenderer mesh in allMeshRend)
        {
            //On les réactive
            mesh.enabled = true;
        }

        GetComponent<Animator>().SetTrigger("Appear");
    }

    //Joue ou non le VFX
    private void PlayIllu(bool playing)
    {
        //vfx true on joue false on arrete
        if (playing)
        {
            //On active le vfx
            goVfxIllu.GetComponent<VisualEffect>().enabled = true;
        }
        else
        {
            //On désactive le vfx
            goVfxIllu.GetComponent<VisualEffect>().enabled = false;
        }
    }
}