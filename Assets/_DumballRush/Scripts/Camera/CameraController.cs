﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/*
 *  Non retouché pdt la semaine de relecture car ce script va etre refait
 */

public class CameraController : MonoBehaviour
{
    [HideInInspector]
    public GameObject targetPosition;

    [Header("Mouvement et Rotations")]
    [Tooltip("Vitesse de déplacement de la camera vers le point de targetPosition")]
    public float speedMovement = 4;
    [Tooltip("Vitesse de rotation de la camera")]
    public float speedRotation = 4;

    [Header("Eloignement")]
    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetView (sans conditions)")]
    public float eloignementAmountTargetView_Normal;
    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetPosition (sans conditions)")]
    public float eloignementAmountTargetPosition_Normal;

    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetView (En BigBall)")]
    public float eloignementAmountTargetView_BigBall;
    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetPosition (En BigBall)")]
    public float eloignementAmountTargetPosition_BigBall;

    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetView (En Magma)")]
    public float eloignementAmountTargetView_Magma;
    [Tooltip("Eloignement gagné pour chaque unité de taille que gagne la boule pour le targetPosition (En Magma)")]
    public float eloignementAmountTargetPosition_Magma;

    [Header("Hauteur du point de focus")]
    [Tooltip("Objet parent permettant de calculer à quel point la pente est importante")]
    public GameObject penteAlignementParent;
    [Tooltip("Permet d'affecter plus ou moins d'offset en fonction de la pente, plus la valeur est élever, moins la pente aura d'effet")]
    public float divideAmountOffset;
    [Tooltip("Effecte un offset au point de target plus ou moins important en fonction de la pente (plus le chiffre est élevé, plus l'offset sera important)")]
    public float offsetAmountMultiplierTargetView;
    [Tooltip("Effecte un offset au point de position plus ou moins important en fonction de la pente (plus le chiffre est élevé, plus l'offset sera important)")]
    public float offsetAmountMultiplierTargetPosition;

    [Header("Hauteur minimal de la camera")]
    public float minimalHeightPosition;

    [Header("FOV")]
    [SerializeField]
    private Vector2 minMaxFOVAdder;
    [SerializeField]
    private float speedReturnToBaseFOV = 1;
    [SerializeField]
    private float speedUpToNewFOV = 2;

    [Header("Inclinaison")]
    public float speedToReachMaxRotateRight = 0.5f;
    public float speedToReachBaseRotateRight = 2f;
    public float maxRotateWithRight = 7f;

    [Header("Functionnal")]
    [Tooltip("profile du global volume")]
    public Volume vol;
    [Tooltip("Lens distortion / chromatic aberation / vignette / bloom du volume")]
    public LensDistortion lensD;
    public ChromaticAberration aberrChroma;
    public Vignette vignette;
    public Bloom bloom;
    public float LDSpeed;


    [HideInInspector]
    public PlayerController player;
    private Rigidbody playerRb;

    //Le deltaTime
    private float dt;

    private void Start()
    {
        //On récupère le global volume
        vol = FindObjectOfType<Volume>();

        //On récupère le composant LensDistortion du profile du global volume
        vol.profile.TryGet(out lensD);
        vol.profile.TryGet(out aberrChroma);
        vol.profile.TryGet(out vignette);
        vol.profile.TryGet(out bloom);

        //Distance de rendu
        if (PlayerPrefs.HasKey("renderDistance"))
        {
            GetComponent<Camera>().farClipPlane = PlayerPrefs.GetFloat("renderDistance") * 100;
        }
        else
        {
            PlayerPrefs.SetFloat("renderDistance", 10);
            GetComponent<Camera>().farClipPlane = PlayerPrefs.GetFloat("renderDistance") * 100;
        }
    }

    //Update is called once per frame
    void Update()
    {
        dt = Time.deltaTime;

        //Debug.Log(lensD.intensity.value);
        //Debug.Log(player.GetComponent<Rigidbody>().velocity.magnitude);


        //Update de la position
        if (targetPosition != null)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition.transform.position, speedMovement * dt);

            //Update de la rotation
            transform.rotation = Quaternion.Lerp(transform.rotation, targetPosition.transform.rotation, speedRotation * dt);
        }

        ////// Lens distortion
        //velocity du joueur entre 0 et 1
        if (player != null)
        {
            if (playerRb == null)
            {
                playerRb = player.GetComponent<Rigidbody>();
            }
            
            float unlerp = Mathf.InverseLerp(70f, 150f, (player.GetComponent<Rigidbody>().velocity.magnitude));

            //fov de la camera entre 80 et 120 selon la velocity
            float lerpLD = Mathf.Lerp(0f, -0.5f, unlerp);

        if (player.isBoosted)
        {
            //Debug.Log("BOOOOOOOOOOOOOOOOOOST");
            lensD.intensity.value = Mathf.Lerp(lensD.intensity.value, -1f, 4f * dt);
        }
        else if (!player.isBoosted)
        {
            lensD.intensity.value = Mathf.Lerp(lensD.intensity.value, lerpLD, 0.1f * dt);
        }
        lensD.intensity.value = Mathf.Lerp(lensD.intensity.value, lerpLD, 0.1f*dt);

            ///// FOV
            float unlerp2 = Mathf.InverseLerp(10f, 100f, (player.GetComponent<Rigidbody>().velocity.magnitude));

            //fov de la camera entre 80 et 120 selon la velocity
            float lerpfov = Mathf.Lerp(minMaxFOVAdder.x, minMaxFOVAdder.y, unlerp2);

            float actualFov = GetComponent<Camera>().fieldOfView;
            float newFov = 0;

            if (actualFov < lerpfov)
            {
                newFov = Mathf.Lerp(actualFov, lerpfov, speedUpToNewFOV * dt);
            }
            else
            {
                newFov = Mathf.Lerp(actualFov, lerpfov, speedReturnToBaseFOV * dt);
            }

            GetComponent<Camera>().fieldOfView = newFov;
        }

    }


    public void Shake(float time, float power)
    {
        StartCoroutine(Shaking(time, power));
    }

    private float remainingShakingTime;

    private IEnumerator Shaking(float time, float power)
    {
        if (remainingShakingTime < time)
        {
            remainingShakingTime += Time.deltaTime;

            transform.position = transform.position + new Vector3(Random.Range(-power, power), Random.Range(-power, power), Random.Range(-power, power));

            yield return new WaitForEndOfFrame();

            Shaking(time, power);
        }
        else
        {
            remainingShakingTime = 0;
        }
    }

    /// <summary>
    /// change l'intensité de la vignette et sa couleur (rond du global vvolume)
    /// </summary>
    /// <param name="colorVoid"></param>
    /// <param name="activated"></param>
    public void ChangeVignette(Color colorVoid , float valueInstensity)
    {
            vignette.intensity.value = valueInstensity;
            vignette.color.value = colorVoid;
        
    }

    /// <summary>
    /// Bloom + aberration chromatique
    /// 
    /// </summary>
    /// <param name="isConfu"></param>
    public void EffectPostProdConfu(bool isConfu)
    {
        //Debug.Log("test confusion pourquoi ça ne marche pas");

        //Debug.Log(bloom.intensity.value);

        if (isConfu)
        {
            bloom.threshold.value = 1f;
            bloom.intensity.value = 3f;
            aberrChroma.intensity.value = 1f;
        }
        else
        {
            bloom.threshold.value = 1f;
            bloom.intensity.value = 0.33f;
            aberrChroma.intensity.value = 0.02f;
        }
    }

}