﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

/*
 *  Ce script se trouve sur l'objet de positionnement de la camera
 *  
 *  Il permet de gérer tout ce qui est relatif au comportement de la camera
 */

public class CameraPositionAdaptater : MonoBehaviour
{
    [HideInInspector]
    public float eloignementAmountForTargetView_Normal;
    [HideInInspector]
    public float eloignementAmountForTargetPosition_Normal;

    [HideInInspector]
    public float eloignementAmountForTargetView_BigBall;
    [HideInInspector]
    public float eloignementAmountForTargetPosition_BigBall;

    [HideInInspector]
    public float eloignementAmountForTargetView_Magma;
    [HideInInspector]
    public float eloignementAmountForTargetPosition_Magma;

    private Vector3 calculatedEloignementForTargetViewVector;
    private Vector3 calculatedEloignementForTargetPositionVector;

    [HideInInspector]
    public PlayerController player;
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public GameObject penteAlignementTarget;

    private Vector3 startPos;
    private Vector3 startTargetPos;

    private Vector3 targetDirectionFromPlayer;

    //Calcule la direction
    private Vector3 directionFromPlayer;

    [HideInInspector]
    public float offsetAmountDivider;
    [HideInInspector]
    public float offsetAmountMultiplierTargetView;
    [HideInInspector]
    public float offsetAmountMultiplierTargetPosition;

    private GameObject detectWalls;

    private Quaternion stackedRotationHighAdapter;
    private float stackedCameraMovementLerping;

    //Calcule d'inclinaison de camera
    private float speedToReachMaxRotateRight = 0.5f;
    private float speedToReachBaseRotateRight = 2f;

    private float maxRotateWithRight = 7f;

    private float lerpRotateRight;
    private float rotateRightDir;

    private float minimumHeightPosition;

    private InputManager im;
    private CameraController cc;

    public void GetAllNecessary()
    {
        im = FindObjectOfType<InputManager>();
        cc = Camera.main.GetComponent<CameraController>();

        #region Récupération des variables
        eloignementAmountForTargetView_Normal = cc.eloignementAmountTargetView_Normal;
        eloignementAmountForTargetPosition_Normal = cc.eloignementAmountTargetPosition_Normal;

        eloignementAmountForTargetView_BigBall = cc.eloignementAmountTargetView_BigBall;
        eloignementAmountForTargetPosition_BigBall= cc.eloignementAmountTargetPosition_BigBall;

        eloignementAmountForTargetView_Magma = cc.eloignementAmountTargetView_Magma;
        eloignementAmountForTargetPosition_Magma = cc.eloignementAmountTargetPosition_Magma;


        penteAlignementTarget = cc.penteAlignementParent;
        offsetAmountDivider = cc.divideAmountOffset;
        offsetAmountMultiplierTargetView = cc.offsetAmountMultiplierTargetView;
        offsetAmountMultiplierTargetPosition = cc.offsetAmountMultiplierTargetPosition;

        speedToReachMaxRotateRight = cc.speedToReachMaxRotateRight;
        speedToReachBaseRotateRight = cc.speedToReachBaseRotateRight;

        maxRotateWithRight = cc.maxRotateWithRight;

        minimumHeightPosition = cc.minimalHeightPosition;
    #endregion

    startPos = transform.localPosition;
        startTargetPos = target.localPosition;

        directionFromPlayer = (transform.position - player.highAdapter.transform.position).normalized;
        targetDirectionFromPlayer = (target.position - player.highAdapter.transform.position).normalized;

        detectWalls = Instantiate(new GameObject());
        detectWalls.name = "detectWalls";
        detectWalls.transform.parent = this.transform.parent;

        stackedRotationHighAdapter = player.highAdapter.transform.localRotation;
        stackedCameraMovementLerping = cc.speedMovement;
    }

    private void Update()
    {
        #region ReverseCamera
        if (im.inputReverseCamera)
        {
            player.highAdapter.transform.localEulerAngles = new Vector3(stackedRotationHighAdapter.eulerAngles.x,stackedRotationHighAdapter.eulerAngles.y + 180, stackedRotationHighAdapter.eulerAngles.z);

            cc.speedMovement = 1000;
        }
        else
        {
            player.highAdapter.transform.localRotation = stackedRotationHighAdapter;

            cc.speedMovement = stackedCameraMovementLerping;
        }
        #endregion

        #region Calcul de la pente et offset
        //Calcul du vecteur de la pente

        Vector3 offsetTargetViewVector = Vector3.zero;
        Vector3 offsetTargetPositionVector = Vector3.zero;

        if (player != null && penteAlignementTarget != null)
        {
            Vector3 pente = (penteAlignementTarget.transform.position - player.transform.position).normalized;
            //calcul de l'angle de la pente
            float penteAngle = Vector3.Angle(pente, Vector3.up) - 90;

            //Division de la valeur de la pente pour un offset plus contrôlable
            float penteAngleAffectedByDivider = (penteAngle * -1) / offsetAmountDivider;

            //Calcul des différentes valeur d'offset passé dans un multiplicateur
            float offsetTargetView = penteAngleAffectedByDivider * offsetAmountMultiplierTargetView;
            float offsetTargetPosition = penteAngleAffectedByDivider * offsetAmountMultiplierTargetPosition;

            //Création des vecteurs associés à l'offset
            offsetTargetViewVector = Vector3.up * offsetTargetView;
            offsetTargetPositionVector = Vector3.up * offsetTargetPosition;
        }
        #endregion

        #region Calcul des éloignements

        if (player != null)
        {
            if (player.isBigBall)
            {
                EloignementCamera(eloignementAmountForTargetView_BigBall, eloignementAmountForTargetPosition_BigBall);
            }
            else if (player.isMagma)
            {
                EloignementCamera(eloignementAmountForTargetView_Magma, eloignementAmountForTargetPosition_Magma);
            }
            else
            {
                EloignementCamera(eloignementAmountForTargetView_Normal, eloignementAmountForTargetPosition_Normal);
            }
        }
        #endregion

        #region Placement des points
        //Initialise la position du targetView
        Vector3 newTargetViewPoint = startTargetPos + calculatedEloignementForTargetViewVector + offsetTargetViewVector;
        //Assigne la nouvelle position
        target.localPosition = newTargetViewPoint;


        //Nouvelle position du targetPosition
        Vector3 newTargetPositionPoint = startPos + calculatedEloignementForTargetPositionVector + offsetTargetPositionVector;

        //On vérifie qu'il est bien au dessus d'une certaine hauteur du sol
        RaycastHit hitGround;
        if (Physics.Raycast(newTargetPositionPoint, -Vector3.up * minimumHeightPosition, out hitGround))
        {
            if (hitGround.transform.tag == "Sol" || hitGround.transform.tag == "SolGlace")
            {
                float dist = Vector3.Distance(newTargetPositionPoint, hitGround.point);

                newTargetPositionPoint += Vector3.up * (minimumHeightPosition - dist);
            }
        }

        //Assigne la position de l'objet de détection de mur à la position souhaité
        detectWalls.transform.localPosition = newTargetPositionPoint;

        if (player != null)
        {
            //Créer le vecteur partant du joueur vers l'objet de détection de mur
            Vector3 directionFromPlayerDetectWalls = (player.transform.position - detectWalls.transform.position).normalized;

            //On regarde s'il y a un mur entre le point de détection du mur et le joueur
            RaycastHit hit;
            if (Physics.Linecast(player.transform.position, detectWalls.transform.position, out hit))
            {
                if (hit.collider.tag == "Sol" || hit.collider.tag == "SolGlace")
                    detectWalls.transform.position = hit.point + directionFromPlayerDetectWalls * 1f;
            }

            //Assigne la position du targetPosition
            transform.position = detectWalls.transform.position;
        }
        #endregion

        RotateCamera();
    }

    private void EloignementCamera(float targetViewAmount, float targetPositionAmount)
    {
        //Calcule l'éloignement
        float calculatedEloignementForTargetView = targetViewAmount * Mathf.Clamp(0, player.transform.localScale.x - 1, player.transform.localScale.x - 1);
        float calculatedEloignementForTargetPosition = targetPositionAmount * (player.transform.localScale.x - 1);

        //Création du vecteur
        calculatedEloignementForTargetViewVector = targetDirectionFromPlayer * calculatedEloignementForTargetView;

        calculatedEloignementForTargetPositionVector = directionFromPlayer * calculatedEloignementForTargetPosition;
    }

    private void RotateCamera()
    {
        transform.LookAt(target);

        if (im.inputMovement.x != 0)
        {
            rotateRightDir = -im.inputMovement.x / Mathf.Abs(im.inputMovement.x);

            lerpRotateRight = Mathf.Lerp(lerpRotateRight, maxRotateWithRight * rotateRightDir, speedToReachMaxRotateRight * Time.deltaTime);

        }
        else
        {
            lerpRotateRight = Mathf.Lerp(lerpRotateRight, 0, speedToReachBaseRotateRight * Time.deltaTime);
        }

        //transform.Rotate(transform.forward, lerpRotateRight * rotateRightDir);
        transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + lerpRotateRight);
    }
}