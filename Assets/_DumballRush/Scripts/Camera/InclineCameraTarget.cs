﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

/*
 * Ce script est placé sur le parent de la target de la camera
 * 
 * il permet de contrôler l'inclinaison de la camera
 */


public class InclineCameraTarget : MonoBehaviour
{
    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position,-Vector3.up,out hit))
        {
            transform.up = hit.normal;

            Debug.Log("Hello");
        }
    }
}
