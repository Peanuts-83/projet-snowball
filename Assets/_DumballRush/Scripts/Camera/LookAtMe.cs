﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;
using System.Linq;

public class LookAtMe : MonoBehaviour
{
    [SerializeField]
    private bool getAllAutomatic;
    [SerializeField]
    private string tagToCollect;
    [SerializeField]
    private GameObject[] allParentTrees;

    public GameObject parentTree;
    [SerializeField]
    private List<Transform> allObjectsToLookAtMe;

    [SerializeField]
    private Sprite[] allPineSprites;

    [SerializeField]
    private bool destroyPng = true;

    void Start()
    {
        if (getAllAutomatic)
        {
            allParentTrees = GameObject.FindGameObjectsWithTag(tagToCollect);

            foreach (GameObject parent in allParentTrees)
            {
                Transform[] trees = parent.GetComponentsInChildren<Transform>();
                trees[0] = trees[1];

                foreach (Transform t in trees)
                {
                    allObjectsToLookAtMe.Add(t);
                }
            }
        }
        else
        {
            allObjectsToLookAtMe = parentTree.GetComponentsInChildren<Transform>().ToList();
            allObjectsToLookAtMe[0] = allObjectsToLookAtMe[1];
        }

        //Assigne le nouveau sprite
        for (int i = 0; i < allObjectsToLookAtMe.Count; i++)
        {
            allObjectsToLookAtMe[i].GetComponent<SpriteRenderer>().sprite = allPineSprites[Random.Range(0, allPineSprites.Length)];
        }

        TurnTrees(-1);

        this.enabled = false;

        if (destroyPng)
        {
            foreach (Transform tr in allObjectsToLookAtMe)
            {
                Destroy(tr.gameObject);
            }
        }
    }

    void TurnTrees(float dist)
    {
        if (transform != null)
        {
            foreach (Transform tr in allObjectsToLookAtMe)
            {
                if (tr != null)
                {
                    if (dist < 0)
                    {
                        TurnTree(tr);
                    }
                    else
                    {
                        if (Vector3.Distance(tr.position, transform.position) < dist)
                        {
                            TurnTree(tr);
                        }
                    }
                }
            }
        }
    }

    void TurnTree(Transform tree)
    {
        //Calcule le vecteur pointant vers la camera
        Vector3 newForwardVector = this.transform.position - tree.position;
        //Supression de la dimension Y pour éviter la rotation sur x/z
        newForwardVector = new Vector3(newForwardVector.x, 0, newForwardVector.z);

        //assignation de la direction où regarde l'arbre
        tree.forward = newForwardVector;
    }
}
