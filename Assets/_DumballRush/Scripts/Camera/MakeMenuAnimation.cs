﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeMenuAnimation : MonoBehaviour
{
    public void playAnimCam(int destination)
    {
        GetComponent<Animator>().SetInteger("Destination", destination);
    }
}
