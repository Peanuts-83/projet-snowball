﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

public class PenteAlignement : MonoBehaviour
{
    [SerializeField]
    private float distOnGround;

    [SerializeField]
    private Transform transformToFollow;

    private float moveDirection;
    private float x;
    private float y;
    private float z;

    private void FixedUpdate()
    {
        if (transformToFollow == null)
            transformToFollow = this.transform;

        RaycastHit hit;

        if (Physics.Raycast(transformToFollow.position + 10*Vector3.up,-Vector3.up,out hit))
        {
            if (hit.collider.tag == "Sol" || hit.collider.tag == "SolGlace")
            {
                x = Mathf.Lerp(transform.position.x, hit.point.x, Time.fixedDeltaTime * 2);
                z = Mathf.Lerp(transform.position.z, hit.point.z, Time.fixedDeltaTime * 2);

                moveDirection = hit.point.y - transform.position.y;

                if (moveDirection < 0)
                    y = Mathf.Lerp(transform.position.y, hit.point.y + 2, Time.fixedDeltaTime * 0.8f);
                else
                    y = Mathf.Lerp(transform.position.y, hit.point.y + 2, Time.fixedDeltaTime * 2);

                transform.position = new Vector3(x, y, z);
            }
        }
    }
}
