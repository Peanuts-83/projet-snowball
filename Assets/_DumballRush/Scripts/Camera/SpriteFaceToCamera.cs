﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteFaceToCamera : MonoBehaviour
{
    //Tous les sprites possibles
    [SerializeField]
    private Sprite[] SapinsPNG;

    //ID de sprite choisit
    private int rand;

    void Start()
    {
        //Choisit l'ID du sprite
        rand = Random.Range(0, SapinsPNG.Length);

        //Assigne le nouveau sprite
        GetComponent<SpriteRenderer>().sprite = SapinsPNG[rand];
    }

    void Update()
    {
        //Calcule le vecteur pointant vers la camera
        Vector3 newForwardVector = Camera.main.transform.position - transform.position;
        //Supression de la dimension Y pour éviter la rotation sur x/z
        newForwardVector = new Vector3(newForwardVector.x, 0, newForwardVector.z);

        //assignation de la direction où regarde l'arbre
        transform.forward = newForwardVector;
    }
}
