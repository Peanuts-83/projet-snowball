﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class ConnectionDebugger : SingletonPUN<ConnectionDebugger>
{
    public bool displayPing;
    public bool displayFps;

    protected ConnectionDebugger() { }

    private float ping;
    private float fps = 0;

    [SerializeField]
    private Text pingText;

    [SerializeField]
    private Text fpsText;

    [SerializeField]
    private Color[] pingColors;

    [SerializeField]
    private Canvas canvasDisconnect;

    private bool localDisplPing;
    private bool localDisplFps;
    


    private void Start()
    {
        CheckPlayerPrefDisplay();

        StartCoroutine(SimiliUpdate());
    }

    private IEnumerator SimiliUpdate()
    {
        FPSPingDisplay();

        #region Analytics
        pingTotal += ping;
        fpsTotal += fps;

        iteration += 1;

        if (SceneChanged())
        {
            AverageFPS();
            AveragePing();

            pingTotal = 0;
            fpsTotal = 0;
            iteration = 0;

            previousSceneName = sceneName;
        }
        #endregion

        yield return new WaitForSeconds(1f);
        StartCoroutine(SimiliUpdate());
    }

    //affichage ping / fps
    public void FPSPingDisplay()
    {
        //ping calcul
        ping = PhotonNetwork.GetPing();

        if (localDisplPing)
        {
            pingText.gameObject.SetActive(true);

            pingText.text = $"Ping: {ping}";

            if (ping > 120)
            {
                pingText.color = pingColors[2];
            }
            else if (ping > 60)
            {
                pingText.color = pingColors[1];
            }
            else
            {
                pingText.color = pingColors[0];
            }
        }
        else 
        {
            pingText.gameObject.SetActive(false);
        }

        //fps calcul
        fps = (9.0f * fps + 1.0f / Time.deltaTime) / 10.0f;

        if (localDisplFps)
        {
            fpsText.gameObject.SetActive(true);

            fpsText.text = $"FPS: {(int)fps}";

            if (fps < 25)
            {
                fpsText.color = pingColors[2];
            }
            else if (fps < 50)
            {
                fpsText.color = pingColors[1];
            }
            else
            {
                fpsText.color = pingColors[0];
            }
        }
        else 
        {
            fpsText.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// vérifiaction des player pref dnas le script et affectation a des variables locales
    /// </summary>
    public void CheckPlayerPrefDisplay()
    {
        //récupération des player pref pour l'affichage du ping et des fps
        if (PlayerPrefs.GetInt("displPing") == 0 || !PlayerPrefs.HasKey("displPing"))
        {
            localDisplPing = false;
        }
        else if (PlayerPrefs.GetInt("displPing") == 1)
        {
            localDisplPing = true;
        }

        if (PlayerPrefs.GetInt("displFPS") == 0 || !PlayerPrefs.HasKey("displFPS"))
        {
            localDisplFps = false;
        }
        else if (PlayerPrefs.GetInt("displFPS") == 1)
        {
            localDisplFps = true;
        }
    }


    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnect cause : " + cause);
        base.OnDisconnected(cause);
        Canvas newCanvasDisconnect = Instantiate(canvasDisconnect);
        newCanvasDisconnect.GetComponentInChildren<Text>().text = "Sorry... you are disconnected, cause : " + cause;
        newCanvasDisconnect.GetComponentInChildren<Button>().Select();
        Cursor.lockState = CursorLockMode.None;
    }

    public void TryReconnect()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    #region Analytics

    string previousSceneName = "MainMenu";
    string sceneName = "MainMenu";

    float pingTotal;
    float fpsTotal;

    int iteration;

    AnalyticsDB ana;

    private void FindAnalytics()
    {
        if (ana == null)
        {
            ana = FindObjectOfType<AnalyticsDB>();
        }
    }

    private bool SceneChanged()
    {
        bool asChanged = false;

        sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

        if (sceneName != previousSceneName)
        {
            asChanged = true;
        }

        return asChanged;
    }

    private void AveragePing()
    {
        int averagePing = Mathf.RoundToInt(pingTotal) / iteration;

        FindAnalytics();
        ana.Ana_Pinginfos(averagePing, previousSceneName);
    }

    private void AverageFPS()
    {
        int averageFps = Mathf.RoundToInt(fpsTotal) / iteration;

        FindAnalytics();
        ana.Ana_Pinginfos(averageFps, previousSceneName);
    }

    #endregion
}
