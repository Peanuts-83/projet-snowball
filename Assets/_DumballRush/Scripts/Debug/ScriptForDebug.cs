﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

[Serializable]
public class VariableEvent : UnityEvent<string, GameObject>
{

}

public class ScriptForDebug : MonoBehaviour
{
    //Inputmanager
    private InputManager im;

    public GameObject goTextDebug;

    public VariableEvent VariableDebug;

    public GameObject playerPrefab;

    void Start()
    {
        //On récupère l'InputManager
        im = FindObjectOfType<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si on appui sur la touche de debug
        if (im.inputDebugFonction)
        {
            Debug.Log("instantiate");
            PhotonNetwork.InstantiateRoomObject(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            /*if (goTextDebug.activeSelf)
            {
                goTextDebug.SetActive(false);
            }
            else
            {
                goTextDebug.SetActive(true);
            }*/
        }

        //goTextDebug.GetComponent<Text>().text = ;
    }
}
