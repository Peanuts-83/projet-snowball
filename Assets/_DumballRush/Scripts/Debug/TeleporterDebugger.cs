﻿using UnityEngine;

/*
 *
 * Ce script permet de faire réapparaitre le joueur dans le cas d'une chute dans le vide
 * 
 * Ce script doit être placé sur l'objet Teleporter (collider en dessous de la map)
 * 
 */
public class TeleporterDebugger : MonoBehaviour
{
    //Quand un objet trigger cet objet
    private void OnTriggerEnter(Collider other)
    {
        //Si c'est le joueur 
        if (other.gameObject.GetComponent<PlayerController>())
        {
            //On renvoie le joueur au début de la map
            other.transform.position = Vector3.zero;
        }
    }
}