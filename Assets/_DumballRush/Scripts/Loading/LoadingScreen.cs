﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadingScreen : MonoBehaviour
{
    public Image background;
    public RawImage imageBackground;
    public Sprite[] allLoadingBackgrounds;

    public Text[] tipTexts;

    [SerializeField]
    private Lean.Localization.LeanToken pointPointPointToken;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadingText());

        imageBackground.texture = allLoadingBackgrounds[Random.Range(0, allLoadingBackgrounds.Length)].texture;

        foreach (Text tip in tipTexts)
            tip.gameObject.SetActive(false);

        tipTexts[Random.Range(0, tipTexts.Length)].gameObject.SetActive(true);
    }

    private IEnumerator LoadingText()
    {
        pointPointPointToken.SetValue("");
        yield return new WaitForSeconds(1f);
        pointPointPointToken.SetValue(".");
        yield return new WaitForSeconds(1f);
        pointPointPointToken.SetValue(". .");
        yield return new WaitForSeconds(1f);
        pointPointPointToken.SetValue(". . .");
        yield return new WaitForSeconds(1f);

        StartCoroutine(LoadingText());
    }
}
