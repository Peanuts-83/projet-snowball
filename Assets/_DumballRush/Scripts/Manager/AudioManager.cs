﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMOD.Studio;

public class AudioManager : Singleton<AudioManager>
{
    /*
     * Scipt qui est appelé pour changer les paramètres de la musique en jeu et dans les menus 
     * et aussi pour jouer les sound effect
     */

    #region Singleton
    //Pour être sur que le constructeur est un Singleton
    protected AudioManager() { }
    #endregion

    public enum SceneType
    {
        Menu,
        Lobby,
        Race,
        Classement
    }

    public SceneType actualScene;
    private SceneType tempActualScene;

    public enum InstanceSpacialization
    {
        Spacialization_2D,
        Spacialization_3D,
    }

    [Serializable]
    public struct FmodInstance
    {
        [HideInInspector]
        public EventInstance instance;
        [FMODUnity.EventRef]
        public string reference;

        public InstanceSpacialization spacialization;
    }

    #region Volume
    /*slider de volume dans le jeu
     */
    [System.Serializable]
    public struct Volume
    {
        [Header("Volume")]
        [Tooltip("Volume global du jeu")]
        [Range(0.0001f, 1)]
        public float volumeGlobal;

        [Tooltip("Volume de la musique du jeu")]
        [Range(0.0001f, 1)]
        public float volumeMusic;

        [Tooltip("Volume des soundEffect du jeu")]
        [Range(0.0001f, 1)]
        public float volumeSoundEffect;

        [Tooltip("Volume des voix du jeu")]
        [Range(0.0001f, 1)]
        public float volumeVoice;

        //Buses
        public string busMasterRef;
        public string busMusicRef;
        public string busSoundEffectRef;
        public string busVoiceRef;

        public FMOD.Studio.Bus busMaster;
        public FMOD.Studio.Bus busMusic;
        public FMOD.Studio.Bus busSoundEffect;
        public FMOD.Studio.Bus busVoice;

        [FMODUnity.EventRef]
        public string slider_soundEffect;
        [FMODUnity.EventRef]
        public string slider_voice;

    }

    public Volume volumeConfiguration;
    private Volume tempVolumeConfiguration;

    #endregion

    #region Music

    [System.Serializable]
    public struct Music
    {
        /* 
         * paramètres de base de FMOD
         */
        #region Configuration FMOD

        [Header("Musique")]
        public FmodInstance musicMenu;
        public FmodInstance musicLobby;
        public FmodInstance musicRace;
        public FmodInstance musicClassement;

        #endregion

        /*
         * implémentation des differents paramètres pour la musique
         */

        #region Parameters
        #region Structures
        [Serializable]
        public struct FmodParameter
        {
            public string name;
            public float value;
        }

        [Serializable]
        public struct MusicMenuParameters
        {
            [Tooltip("si on se trouve dans la personnalisation")]
            public FmodParameter inPersonalisation;

            [Tooltip("Si on se trouve dans les crédits")]
            public FmodParameter inCredits;
        }

        [Serializable]
        public struct MusicRaceParameters
        {
            [Tooltip("Depuis l'introduction de la course vers le 3, 2, 1")]
            public FmodParameter startDecompte;

            [Tooltip("A enclencher directement à la fin du décompte")]
            public FmodParameter startRaceAfterDecompte;

            [Tooltip("Si le joueur est dans une cave")]
            public FmodParameter cave;

            [Tooltip("La progression de la course en pourcentage")]
            public FmodParameter progression;

            [Tooltip("Vers la fin de la course")]
            public FmodParameter toFinish;

            [Tooltip("Quand le joueur est en power up gros")]
            public FmodParameter powerUpBig;

            [Tooltip("Quand le joueur est en pause")]
            public FmodParameter pause;

            [Tooltip("Quand le joueur est affecté par un malus qui ralentit")]
            public FmodParameter slowed;

            [Tooltip("Quand le joueur est en magma")]
            public FmodParameter magma;

            [Tooltip("Confusion du joueur")]
            public FmodParameter confus;

            [Tooltip("Position du joueur dans la course")]
            public FmodParameter positionInRace;
        }

        [Serializable]
        public struct MusicLobbyParameters
        {
            [Tooltip("Si c'est le foot")]
            public FmodParameter foot;

            [Tooltip("Si c'est le Sumo")]
            public FmodParameter sumo;
        }
        #endregion

        [Header("Paramètres")]

        public MusicMenuParameters parametersMusicMenu;
        public MusicRaceParameters parametersMusicRace;
        public MusicLobbyParameters parametersMusicLobby;
        #endregion
    }

    public Music musicConfiguration;
    private Music tempMusicConfiguration;

    #endregion

    #region Sound Effect

    /* 
     * implémentation des sound effect 
     */

    [System.Serializable]
    public struct SoundEffect
    {
        #region Structures
        [Serializable]
        public struct EventsCadeau
        {
            [Tooltip("Obtention du cadeau")]
            [FMODUnity.EventRef]
            public string fmodEventGift;
            [Tooltip("Utilisation d'un cadeau")]
            [FMODUnity.EventRef]
            public string fmodEventUseItem;
            [Tooltip("Roulement des cadeaux/power up")]
            [FMODUnity.EventRef]
            public string fmodEventRoulementPowerUp;
            [Tooltip("Fin du roulement, obtention du powerup")]
            [FMODUnity.EventRef]
            public string fmodEventObtentionPowerUp;
        }

        [Serializable]
        public struct EventsMouvement
        {
            [Tooltip("Saut du joueur")]
            [FMODUnity.EventRef]
            public string fmodEventSaut;

            [Tooltip("Si le joueur est glacé")]
            [FMODUnity.EventRef]
            public string fmodEventIsFrozen;
        }

        [Serializable]
        public struct EventsTurbo
        {
            [FMODUnity.EventRef]
            public string fmodEventTurbo; //event:/EffetsSonores/PowerUp/PUP_BoostSol

            [FMODUnity.EventRef]
            public string fmodEventTurboPowerUp; //event:/EffetsSonores/PowerUp/PUP_Boost
        }

        [Serializable]
        public struct EventsCollision
        {
            [FMODUnity.EventRef]
            public string fmodEventCollisionGeneric; //event:/EffetsSonores/Collisions/IC_CollisionSimple
            [FMODUnity.EventRef]
            public string fmodEventCollisionBois; //event:/EffetsSonores/Collisions/IC_CollisionBois
            [FMODUnity.EventRef]
            public string fmodEventCollisionCailloux;// event:/EffetsSonores/Collisions/IC_CollisionCailloux
            [FMODUnity.EventRef]
            public string fmodEventCollisionCascade; // event:/EffetsSonores/Collisions/IC_CascadeGelee
            [FMODUnity.EventRef]
            public string fmodEventBreakStalactite;//event:/EffetsSonores/Collisions/IC_CollisionStalactite
            [FMODUnity.EventRef]
            public string fmodEventCollisionFootBall;//event:/EffetsSonores/Collisions/IC_BallonFoot

            [FMODUnity.EventRef]
            public string fmodEventTailleMoins;//event:/NonUtilise/TailleMoins
        }

        [Serializable]
        public struct EventsTree
        {
            [FMODUnity.EventRef]
            public string fmodEventTreeDestruct;//event:/EffetsSonores/Collisions/IC_CollisionArbre

            [FMODUnity.EventRef]
            public string fmodEventBushSound;//event:/EffetsSonores/Collisions/IC_Buisson3D
        }

        [Serializable]
        public struct EventsClick
        {
            [FMODUnity.EventRef]
            public string fmodEventOverlappButtons;//event:/EffetsSonores/Menus/Curseurs/CurseurOver2

            [FMODUnity.EventRef]
            public string fmodEventClickOnButtons; //event:/EffetsSonores/Menus/Curseurs/CurseurPetitCheck

            [FMODUnity.EventRef]
            public string fmodEventClickBackButtons;// event:/EffetsSonores/Menus/Curseurs/CurseurBack

            [FMODUnity.EventRef]
            public string fmodEventClickCustomisation;//event:/EffetsSonores/Menus/Curseurs/CurseurCustomisation

            [FMODUnity.EventRef]
            public string fmodEventClickImportant;//event:/EffetsSonores/Menus/Curseurs/CurseurCheck

            [FMODUnity.EventRef]
            public string fmodEventWhooshMenu;//event:/EffetsSonores/Menus/M_WooshTransition
        }

        [Serializable]
        public struct EventsPowerUp
        {
            [FMODUnity.EventRef]
            public string fmodEventPUGrossissment;//event:/NonUtilise/PUP_Grossissement
            [FMODUnity.EventRef]
            public string fmodEventPUDegrossissment;//event:/NonUtilise/TailleMoins
            [FMODUnity.EventRef]
            public string fmodEventZoneConfusion;//event:/EffetsSonores/PowerUp/PUP_ZoneConfusion
            [FMODUnity.EventRef]
            public string fmodEventFondue;//event:/EffetsSonores/PowerUp/PUP_Fondue
        }

        [Serializable]
        public struct EventsVoix
        {
            [FMODUnity.EventRef]
            public string fmodEventTVoixCelebration;//event:/EffetsSonores/Voix/Voix_Celebration
            [FMODUnity.EventRef]
            public string fmodEventTVoixCollision;//event:/EffetsSonores/Voix/Voix_Collision
            [FMODUnity.EventRef]
            public string fmodEventTVoixTaunt;//event:/EffetsSonores/Voix/Voix_Provocation

            [HideInInspector]
            public int voiceID;

            public float cooldownCelebration;//1
            public float cooldownCollision;//1
            public float cooldownTaunt;//1.5
        }

        [Serializable]
        public struct EventsMenu
        {
            [Header("Lobby")]
            [FMODUnity.EventRef]
            public string enterLobby;//event:/EffetsSonores/Menus/Lobby/ML_EntréeJoueur
            [FMODUnity.EventRef]
            public string leaveLobby;//event:/EffetsSonores/Menus/Lobby/ML_SortieJoueur
            [FMODUnity.EventRef]
            public string but;//event:/EffetsSonores/Menus/Lobby/but

            [Header("Race")]
            [FMODUnity.EventRef]
            public string StartRace3;//event:/EffetsSonores/Course/C_FluteDecompte
            [FMODUnity.EventRef]
            public string StartRace2;//event:/EffetsSonores/Course/C_FluteDecompte
            [FMODUnity.EventRef]
            public string StartRace1;//event:/EffetsSonores/Course/C_FluteDecompte
            [FMODUnity.EventRef]
            public string StartRaceGO;//event:/EffetsSonores/Course/C_FluteStart

            [Header("Customisation")]
            [FMODUnity.EventRef]
            public string armoireOpen;//event:/EffetsSonores/Menus/Customisation/MC_OuvrirArmoire
            [FMODUnity.EventRef]
            public string armoireClose;//event:/EffetsSonores/Menus/Customisation/MC_FermerArmoire

            [Header("Classement")]
            [FMODUnity.EventRef]
            public string clochette; //event:/EffetsSonores/Menus/CLASSEMENT_Clochette
            [FMODUnity.EventRef]
            public string foule;//event:/EffetsSonores/Course/C_Arrivee
            [FMODUnity.EventRef]
            public string feuDArtifices;//A_FeuDartifice

            public FmodInstance fouleInstance;
            public FmodInstance feuDArtificesInstance;

        }

        [Serializable]
        public struct EventsEndRace
        {
            [FMODUnity.EventRef]
            public string arrivee;//event:/EffetsSonores/Course/C_Arrivee
        }
        #endregion

        //Cathégories
        public EventsCadeau cadeaux;
        public EventsMouvement mouvement;
        public EventsTurbo turbo;
        public EventsCollision collision;
        public EventsTree trees;
        public EventsClick click;
        public EventsPowerUp powerUp;
        public EventsVoix voix;
        public EventsMenu menu;
        public EventsEndRace endRace;

        //Instances
        [Header("Instances")]
        public FmodInstance InstanceSnowSlide; //event:/EffetsSonores/Ambiances/A_MultiGlisse
        public FmodInstance InstanceMagma;//event:/EffetsSonores/PowerUp/PUP_Magma
    }

    public SoundEffect soundEffectConfiguration;

    #endregion

    #region Ambiance
    [System.Serializable]
    public struct Ambiance
    {
        public FMOD.Studio.EventInstance ambianceInstance;

        [Header("Piste d'ambiance")]
        [FMODUnity.EventRef]
        public string fmodEventAmbiance;//event:/EffetsSonores/Ambiances/A_MultiAmbiance

        [FMODUnity.ParamRef]
        public string caveGlobal;//CAVE GLOBAL

        [Header("Parametres")]
        [Tooltip("Si le joueur se trouve dans une cave")]
        [Range(0, 1)]
        public float cave;
        [Tooltip("Si le joueur se trouve dans une forêt")]
        [Range(0, 1)]
        public float forest;
        [Tooltip("Si le joueur se trouve dans les airs depuis longtemps")]
        [Range(0, 1)]
        public float wind;

        public float lerpAmbiantSound;
    }

    [Tooltip("Paramètres des sons d'ambiance")]
    public Ambiance ambianceConfiguration;
    private Ambiance tempAmbianceConfiguration;

    #endregion

    #region Start & Update

    public override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        //Initialisation des buses et volumes
        volumeConfiguration.busMaster = FMODUnity.RuntimeManager.GetBus(volumeConfiguration.busMasterRef);
        volumeConfiguration.busMusic = FMODUnity.RuntimeManager.GetBus(volumeConfiguration.busMusicRef);
        volumeConfiguration.busSoundEffect = FMODUnity.RuntimeManager.GetBus(volumeConfiguration.busSoundEffectRef);
        volumeConfiguration.busVoice = FMODUnity.RuntimeManager.GetBus(volumeConfiguration.busVoiceRef);

        //Création de l'instance de la musique de menu
        musicConfiguration.musicMenu = CreateFMODInstance(musicConfiguration.musicMenu);

        //Création de l'instance de l'ambiance
        ambianceConfiguration.ambianceInstance = FMODUnity.RuntimeManager.CreateInstance(ambianceConfiguration.fmodEventAmbiance);
        ambianceConfiguration.ambianceInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(Camera.main.transform.position));
        ambianceConfiguration.ambianceInstance.start();

        //Sauvegardes volumeGlobal
        if (PlayerPrefs.HasKey("volumeGlobal"))
        {
            ModifyVolume(PlayerPrefs.GetFloat("volumeGlobal"), VolumeType.GlobalVolume);
        }

        //Sauvegardes volumeMusic
        if (PlayerPrefs.HasKey("volumeMusic"))
        {
            ModifyVolume(PlayerPrefs.GetFloat("volumeMusic"), VolumeType.MusicVolume);
        }

        //Sauvegardes volumeSoundEffect
        if (PlayerPrefs.HasKey("volumeSoundEffect"))
        {
            ModifyVolume(PlayerPrefs.GetFloat("volumeSoundEffect"), VolumeType.SoundEffectVolume);
        }

        //Sauvegardes volumeVoice
        if (PlayerPrefs.HasKey("volumeVoice"))
        {
            ModifyVolume(PlayerPrefs.GetFloat("volumeVoice"), VolumeType.VoiceVolume);
        }
    }

    private void Update()
    {
        #region parametres de la musique 
        /*
        * Affectation des paramètres de FMOD a ceux d'unity
        */
        if (TestVolumeChanged())
        {
            ModifyVolume(volumeConfiguration.volumeGlobal, VolumeType.GlobalVolume);
            ModifyVolume(volumeConfiguration.volumeMusic, VolumeType.MusicVolume);
            ModifyVolume(volumeConfiguration.volumeSoundEffect, VolumeType.SoundEffectVolume);
            ModifyVolume(volumeConfiguration.volumeVoice, VolumeType.VoiceVolume);

            tempVolumeConfiguration = volumeConfiguration;
        }

        #region Creation des instances
        if (TestActualSceneChanged())
        {
            switch (actualScene)
            {
                case SceneType.Menu:

                    //Créer l'instance de musique
                    musicConfiguration.musicMenu = CreateFMODInstance(musicConfiguration.musicMenu);

                    //Stop les autres musiques
                    StopFMODInstance(musicConfiguration.musicRace);
                    StopFMODInstance(musicConfiguration.musicLobby);
                    StopFMODInstance(musicConfiguration.musicClassement);

                    break;

                case SceneType.Lobby:

                    //Créer l'instance de musique
                    musicConfiguration.musicLobby = CreateFMODInstance(musicConfiguration.musicLobby);

                    //Stop les autres musiques
                    StopFMODInstance(musicConfiguration.musicMenu);
                    StopFMODInstance(musicConfiguration.musicRace);
                    StopFMODInstance(musicConfiguration.musicClassement);

                    break;

                case SceneType.Race:

                    //Créer l'instance de musique
                    musicConfiguration.musicRace = CreateFMODInstance(musicConfiguration.musicRace);

                    //Stop les autres musiques
                    StopFMODInstance(musicConfiguration.musicMenu);
                    StopFMODInstance(musicConfiguration.musicLobby);
                    StopFMODInstance(musicConfiguration.musicClassement);

                    break;

                case SceneType.Classement:

                    //Créer l'instance de musique
                    musicConfiguration.musicClassement = CreateFMODInstance(musicConfiguration.musicClassement);

                    //Stop les autres musiques
                    StopFMODInstance(musicConfiguration.musicMenu);
                    StopFMODInstance(musicConfiguration.musicLobby);
                    StopFMODInstance(musicConfiguration.musicRace);

                    break;
            }
        }
        #endregion

        if (TestMusicParametersChanged())
        {
            switch (actualScene)
            {
                case SceneType.Menu:

                    //Affectation des paramètres
                    UpdateFMODInstance(musicConfiguration.musicMenu, musicConfiguration.parametersMusicMenu.inPersonalisation);
                    UpdateFMODInstance(musicConfiguration.musicMenu, musicConfiguration.parametersMusicMenu.inCredits);

                    break;

                case SceneType.Lobby:

                    //Affectation des paramètres
                    //cave : paramètre > si le joueur est dans la cave
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicRace.cave);

                    //powerUpBig : paramètre > quand le joueur est gros
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicRace.powerUpBig);

                    //Magma
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicRace.magma);

                    //pause : parametre quand le joueur est en pause
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicRace.pause);

                    //Confusion
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicRace.confus);

                    //Foot
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicLobby.foot);

                    //Sumo
                    UpdateFMODInstance(musicConfiguration.musicLobby, musicConfiguration.parametersMusicLobby.sumo);
                    
                    break;

                case SceneType.Race:
                    
                    //raceGo : paramètre > Depuis l'introduction de la course vers le 3, 2, 1
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.startDecompte);

                    //S'enclenche juste après le décompte
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.startRaceAfterDecompte);

                    //Progression : paramètre > en pourcetage de la progression de la course
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.progression);

                    //cave : paramètre > si le joueur est dans la cave
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.cave);

                    //powerUpBig : paramètre > quand le joueur est gros
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.powerUpBig);

                    //toFinish : parametre > quand le joueu est orès de la fin de la course
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.toFinish);

                    //pause : parametre quand le joueur est en pause
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.pause);

                    //slowed : parametre quand le joueur est ralenti
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.slowed);

                    //Magma
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.magma);

                    //Confusion
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.confus);

                    //Placement des joueurs
                    UpdateFMODInstance(musicConfiguration.musicRace, musicConfiguration.parametersMusicRace.positionInRace);

                    break;

                case SceneType.Classement:



                    break;
            }
        }

        #endregion

        #region Parametres des ambiances
        //Spacialisation du son d'ambiance
        if (TestAmbianceParametersChanged() || TestActualSceneChanged())
        {
            switch (actualScene)
            {
                case SceneType.Menu:

                    //ambianceParameters.ambianceInstance.setParameterByName("IsMenu", 1);

                    if (musicConfiguration.parametersMusicMenu.inPersonalisation.value == 1)
                    {
                        ambianceConfiguration.lerpAmbiantSound = Mathf.Lerp(ambianceConfiguration.lerpAmbiantSound, 0, 2 * Time.deltaTime);
                    }
                    else
                    {
                        ambianceConfiguration.lerpAmbiantSound = Mathf.Lerp(ambianceConfiguration.lerpAmbiantSound, 1, 2 * Time.deltaTime);
                    }

                    ambianceConfiguration.wind = ambianceConfiguration.lerpAmbiantSound;
                    ambianceConfiguration.forest = ambianceConfiguration.lerpAmbiantSound;

                    //Réassigne les valeurs de l'ambiance du vent
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsWind", ambianceConfiguration.wind);

                    //Réassigne les valeurs de l'ambiance de la forêt
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsForest", ambianceConfiguration.forest);

                    break;

                case SceneType.Lobby:

                    //ambianceParameters.ambianceInstance.setParameterByName("IsMenu", 0);

                    //Réassigne les valeurs de l'ambiance de la cave
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsCave", ambianceConfiguration.cave);
                    //CaveGlobal
                    FMODUnity.RuntimeManager.StudioSystem.setParameterByName(ambianceConfiguration.caveGlobal, ambianceConfiguration.cave);

                    //Réassigne les valeurs de l'ambiance de la forêt
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsForest", ambianceConfiguration.forest);

                    //Réassigne les valeurs de l'ambiance du vent
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsWind", ambianceConfiguration.wind);

                    break;

                case SceneType.Race:

                    //Réassigne les valeurs de l'ambiance de la cave
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsCave", ambianceConfiguration.cave);
                    //CaveGlobal
                    FMODUnity.RuntimeManager.StudioSystem.setParameterByName(ambianceConfiguration.caveGlobal, ambianceConfiguration.cave);

                    //Réassigne les valeurs de l'ambiance de la forêt
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsForest", ambianceConfiguration.forest);

                    //Réassigne les valeurs de l'ambiance du vent
                    ambianceConfiguration.ambianceInstance.setParameterByName("IsWind", ambianceConfiguration.wind);

                    break;
            }

        }
        #endregion
    }
    #endregion

    #region Volume

    public enum VolumeType
    {
        GlobalVolume,
        MusicVolume,
        SoundEffectVolume,
        VoiceVolume
    }


    /// <summary>
    /// Permet de modifier le volume a l'aide du slider ou par SE
    /// </summary>
    /// <param name="value"></param>
    /// <param name="volType"></param>
    public void ModifyVolume(float value, VolumeType volType)
    {

        /*
         * Permet de modifier le volume a l'aide du slider ou par se
         * 
         * GlobalVolume : c'est le volume de tout en gros (musique + SE + Voices)
         * MusicVolume : le volume que de la musique
         * SoundEffectVolume : le volume de tt les SE
         * VoiceVolume : le volume des voix
         */

        switch (volType)
        {
            case VolumeType.GlobalVolume:

                if (value != tempVolumeConfiguration.volumeGlobal)
                {
                    //set de la valeur que l'on veut
                    volumeConfiguration.volumeGlobal = value;

                    //enregistrement dans le playerPrefs
                    PlayerPrefs.SetFloat("volumeGlobal", volumeConfiguration.volumeGlobal);

                    volumeConfiguration.busMaster.setVolume(volumeConfiguration.volumeGlobal);
                }

                break;



            case VolumeType.MusicVolume:

                if (value != tempVolumeConfiguration.volumeMusic)
                {
                    //set de la valeur que l'on veut
                    volumeConfiguration.volumeMusic = value;

                    //enregistrement dans le playerPrefs
                    PlayerPrefs.SetFloat("volumeMusic", volumeConfiguration.volumeMusic);

                    volumeConfiguration.busMusic.setVolume(volumeConfiguration.volumeMusic);
                    
                    
                    Debug.Log(volType + " " + value);
                }

                break;


            case VolumeType.SoundEffectVolume:

                if (value != tempVolumeConfiguration.volumeSoundEffect)
                {
                    //set de la valeur que l'on veut
                    volumeConfiguration.volumeSoundEffect = value;

                    //enregistrement dans le playerPrefs
                    PlayerPrefs.SetFloat("volumeSoundEffect", volumeConfiguration.volumeSoundEffect);

                    //volumeParameters.calculatedVolumeSoundEffect = volumeParameters.volumeSoundEffect * volumeParameters.volumeGlobal;

                    volumeConfiguration.busSoundEffect.setVolume(volumeConfiguration.volumeSoundEffect);
                }

                break;

            case VolumeType.VoiceVolume:

                if (value != tempVolumeConfiguration.volumeVoice)
                {
                    volumeConfiguration.volumeVoice = value;

                    PlayerPrefs.SetFloat("volumeVoice", volumeConfiguration.volumeVoice);

                    volumeConfiguration.busVoice.setVolume(volumeConfiguration.volumeVoice);
                }

                break;
        }
    }

    #endregion

    #region Functions

    /// <summary>
    /// Joue le son "fmodEvent" désiré. 
    ///  - Spécifier l'objet si le son doit être jouer en étant attaché à cet objet.
    ///  - Spécifier un nom de paramètre et sa valeur si le son doit changer un paramètre en jouant.
    ///  - Spécifier le volume du son
    /// </summary>
    /// <param name="fmodEvent"></param>
    /// <param name="go"></param>
    /// <param name="parameterName"></param>
    /// <param name="parameterValue"></param>
    /// <param name="volume"></param>
    public void PlaySoundEffect(string fmodEvent, GameObject go = null, string parameterName = "", float parameterValue = 0, float volume = 1)
    {
        if (go == null)
        {
            //Si le gameObject est vide, le son est joué sur la camera
            FMODUnity.RuntimeManager.PlayOneShotAttached(fmodEvent, Camera.main.gameObject, parameterName, parameterValue, volume);
        }
        else
        {
            //Si l'objet n'est pas null, le son se joue sur l'objet
            FMODUnity.RuntimeManager.PlayOneShotAttached(fmodEvent, go, parameterName, parameterValue, volume);
        }
    }

    /// <summary>
    /// Joue le son "fmodEvent" désiré à la position désiré.
    ///  - Spécifier un nom de paramètre et sa valeur si le son doit changer un paramètre en jouant.
    ///  - Spécifier le volume du son
    /// </summary>
    /// <param name="fmodEvent"></param>
    /// <param name="pos"></param>
    /// <param name="parameterName"></param>
    /// <param name="parameterValue"></param>
    /// <param name="volume"></param>
    public void PlaySoundEffectAtPosition(string fmodEvent, Vector3 pos, string parameterName = "", float parameterValue = 0, float volume = 1)
    {
        //Joue le son à la position souhaité
        FMODUnity.RuntimeManager.PlayOneShot(fmodEvent, pos, parameterName, parameterValue, volume);
    }

    /// <summary>
    /// Créer l'instance FMOD spécifiée. Si l'instance est déjà créer, update l'instance
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="volume"></param>
    /// <returns></returns>
    public FmodInstance CreateFMODInstance(FmodInstance instance, float volume = 1f, Vector3? position = null)
    {
        //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
        if (!(volume > 0))
        {
            volume = 0.001f;
        }

        //Si l'instance est déjà crée
        if (instance.instance.isValid())
        {
            //on Update le volume de l'instance
            UpdateFMODInstance(instance, position, volume);
        }
        else
        {
            //On créer l'instance
            Debug.Log($"Creating {instance.reference} at volume {volume}");



            instance.instance = FMODUnity.RuntimeManager.CreateInstance(instance.reference);

            if (instance.spacialization == InstanceSpacialization.Spacialization_3D)
            {
                if (position.HasValue)
                    instance.instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(position.Value));
            }

            instance.instance.setVolume(volume);

            instance.instance.start();

            instance.instance.release();
        }

        //On retourne l'instance pour l'assigné au fmod event
        return instance;
    }

    /// <summary>
    /// Update le volume de l'instance FMOD spécifiée.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="volume"></param>
    public void UpdateFMODInstance(FmodInstance instance, float volume)
    {
        //Si l'instance est valide
        if (instance.instance.isValid())
        {
            //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
            if (!(volume > 0))
            {
                volume = 0.001f;
            }

            //Change le volume
            instance.instance.setVolume(volume);
        }
    }

    /// <summary>
    /// Update le paramètre et le volume (non obligatoire) de l'instance FMOD spécifiée.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="paramName"></param>
    /// <param name="paramValue"></param>
    /// <param name="volume"></param>
    public void UpdateFMODInstance(FmodInstance instance, string paramName, float paramValue, float volume = -1f)
    {
        //Si l'instance existe
        if (instance.instance.isValid())
        {
            //On change le paramètre
            instance.instance.setParameterByName(paramName, paramValue);

            if (volume != -1f)
            {
                //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
                if (!(volume > 0))
                {
                    volume = 0.001f;
                }

                //On change le volume
                instance.instance.setVolume(volume);
            }
        }
    }

    /// <summary>
    /// Update le paramètre et le volume (non obligatoire) de l'instance FMOD spécifiée.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="parameter"></param>
    /// <param name="volume"></param>
    private void UpdateFMODInstance(FmodInstance instance, Music.FmodParameter parameter, float volume = -1f)
    {
        //Si l'instance existe
        if (instance.instance.isValid())
        {
            //On change le paramètre
            instance.instance.setParameterByName(parameter.name, parameter.value);

            if (volume != -1f)
            {
                //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
                if (!(volume > 0))
                {
                    volume = 0.001f;
                }

                //Change le volume
                instance.instance.setVolume(volume);
            }
        }
    }

    /// <summary>
    /// Update les paramètres et le volume (non obligatoire) de l'instance FMOD spécifiée.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="parameter"></param>
    /// <param name="volume"></param>
    private void UpdateFMODInstance(FmodInstance instance, Music.FmodParameter[] parameters, float volume = -1f)
    {
        //Si l'instance existe
        if (instance.instance.isValid())
        {
            //Change tous les paramètres spécifié
            foreach (Music.FmodParameter parameter in parameters)
            {
                instance.instance.setParameterByName(parameter.name, parameter.value);
            }

            if (volume != -1f)
            {
                //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
                if (!(volume > 0))
                {
                    volume = 0.001f;
                }

                //Change le volume
                instance.instance.setVolume(volume);
            }
        }
    }

    public void UpdateFMODInstance(FmodInstance instance, Vector3? position, float volume = -1f)
    {
        //Si l'instance existe
        if (instance.instance.isValid())
        {
            if (instance.spacialization == InstanceSpacialization.Spacialization_3D)
            {
                if (position.HasValue)
                {
                    instance.instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(position.Value));
                }
            }

            if (volume != -1f)
            {
                //Prévient les volume à 0 et évite que l'event ne diffuse pas de son
                if (!(volume > 0))
                {
                    volume = 0.001f;
                }

                //Change le volume
                instance.instance.setVolume(volume);
            }
        }
    }

    /// <summary>
    /// Arrête l'instance FMOD spécifiée.
    /// </summary>
    /// <param name="instance"></param>
    public void StopFMODInstance(FmodInstance instance)
    {
        //Si l'instance existe
        if (instance.instance.isValid())
        {
            Debug.Log($"Stop {instance.reference}");

            //On suprime l'instance
            instance.instance.stop(STOP_MODE.ALLOWFADEOUT);
        }
    }

    /// <summary>
    /// Change le type de sol sur lequel la boule de neige roule
    /// </summary>
    /// <param name="type"></param>
    public void SetSnowSlideType(float type)
    {
        UpdateFMODInstance(soundEffectConfiguration.InstanceSnowSlide, "Surface", type);
    }

    /// <summary>
    /// Regarde si les paramètres de la musique sont identique à la frame précédente.
    /// </summary>
    /// <returns></returns>
    private bool TestMusicParametersChanged()
    {
        bool asChange = false;

        if (!tempMusicConfiguration.Equals(musicConfiguration))
        {
            asChange = true;
            tempMusicConfiguration = musicConfiguration;
        }

        return asChange;
    }

    /// <summary>
    /// Regarde si les paramètres d'ambiances sont identique à la frame précédente.
    /// </summary>
    /// <returns></returns>
    private bool TestAmbianceParametersChanged()
    {
        bool asChange = false;

        if (!tempAmbianceConfiguration.Equals(ambianceConfiguration))
        {
            asChange = true;
            tempAmbianceConfiguration = ambianceConfiguration;
        }

        return asChange;
    }

    private bool TestActualSceneChanged()
    {
        bool asChange = false;

        if (tempActualScene != actualScene)
        {
            asChange = true;
            tempActualScene = actualScene;
        }

        return asChange;
    }

    private bool TestVolumeChanged()
    {
        bool asChange = false;

        if (!tempVolumeConfiguration.Equals(volumeConfiguration))
        {
            asChange = true;
        }

        return asChange;
    }
    #endregion
}
