﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Ce script permet de trouver l'audioManager (singleton) afin de le faire fonctionner
 * Ce script doit être placé sur l'objet ayant besoin de ce singleton (l'input field de SizeRace dans le menu de room)
 */

public class AudioManagerFinder : MonoBehaviour
{
    //L'audioManager
    private AudioManager am;

    //enum des différents Sliders
    public enum Sliders
    {
        globalVolume,
        musicVolume,
        soundEffectVolume,
        voiceVolume
    }

    //Les sliders de tous les volumes
    public Sliders typeOfVolume;

    private void Start()
    {
        //On récupère l'audioManager
        am = FindObjectOfType<AudioManager>();

        Slider slider = GetComponent<Slider>();

        switch (typeOfVolume)
        {
            //Si le slider est celui du Global Volume
            case Sliders.globalVolume:
                GlobalVolume(slider);
                break;

            //Si le slider est celui du Music Volume
            case Sliders.musicVolume:
                MusicVolume(slider);
                break;

            //Si le slider est celui du Sound Effect Volume
            case Sliders.soundEffectVolume:
                SoundEffectVolume(slider);
                break;

            case Sliders.voiceVolume:
                VoiceVolume(slider);
                break;
        }
    }

    //Changement du global Volume
    private void GlobalVolume(Slider slider)
    {
        //Changement de la valeur
        slider.onValueChanged.AddListener(delegate { am.ModifyVolume(slider.value, AudioManager.VolumeType.GlobalVolume); });
        //Récupération du bon paramètre
        slider.value = am.volumeConfiguration.volumeGlobal;
    }

    //Changement du Music Volume
    private void MusicVolume(Slider slider)
    {
        //Changement de la valeur
        slider.onValueChanged.AddListener(delegate { am.ModifyVolume(slider.value, AudioManager.VolumeType.MusicVolume); });
        //Récupération du bon paramètre
        slider.value = am.volumeConfiguration.volumeMusic;
    }

    //Changement du Sound Effect Volume
    private void SoundEffectVolume(Slider slider)
    {
        //Changement de la valeur
        slider.onValueChanged.AddListener(delegate
        {
            am.ModifyVolume(slider.value, AudioManager.VolumeType.SoundEffectVolume);

            am.PlaySoundEffect(am.volumeConfiguration.slider_soundEffect);
        });

        //Récupération du bon paramètre
        slider.value = am.volumeConfiguration.volumeSoundEffect;
    }

    //Changement du Voice Volume
    private void VoiceVolume(Slider slider)
    {
        //Changement de la valeur
        slider.onValueChanged.AddListener(delegate
        {
            am.ModifyVolume(slider.value, AudioManager.VolumeType.VoiceVolume);

            am.PlaySoundEffect(am.volumeConfiguration.slider_voice);
        });

        //Récupération du bon paramètre
        slider.value = am.volumeConfiguration.volumeVoice;
    }
}