﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Analytics;

public class ButtonManager : MonoBehaviour
{
    #region Panneau 

    [Header("Menus")]
    [SerializeField]
    private GameObject panneauCreateGame;

    [SerializeField]
    private GameObject panneauSearchGame;

    [SerializeField]
    private GameObject panneauOptions;

    [SerializeField]
    private GameObject panneauCredits;

    [SerializeField]
    private GameObject panneauQuit;

    [SerializeField]
    private GameObject titleScreen;

    [SerializeField]
    private GameObject panneauOncePseudo;

    [SerializeField]
    private GameObject panneauClufs;

    #endregion region

    #region Buttons

    [Header("Buttons")]
    [SerializeField]
    private Selectable[] allMenuPrincipalButtons;
    [SerializeField]
    private Button acceptClufBtn;
    [SerializeField]
    private Scrollbar clufScrollbar;
    #endregion

    #region Selectable target
    // premier truk sélectionner quand on change de menu (utile pour la manette)
    [SerializeField]
    private Selectable targetButtonMenuCreateGame;
    [SerializeField]
    private Selectable targetButtonMenuSearchGame;
    [SerializeField]
    private Selectable targetButtonMenuOptions;
    [SerializeField]
    private Selectable targetButtonMenuCredits;
    [SerializeField]
    private Selectable targetButtonMenuCustomisation;
    [SerializeField]
    private Selectable targetButtonMenuQuit;

    #endregion

    #region Functionnal

    [Header("Functionel")]
    [SerializeField]
    private GameObject playerMenu;

    public bool isInTitle = false;

    public GameObject UiControllerIcons;

    public Text txt_RaceSize;

    public GameObject RaceSizeGo;

    public Button btn_ChangeRoomName;

    public Button SearchByNameButton;
    public Button backBtnInSearchMenu;
    public List<GameObject> allInteractebleRoomButtons;

    [SerializeField]
    private GameObject gamePadInputInfo;
    #endregion

    #region Manager
    [SerializeField]
    private CustomMatchmackingLobbyController cmlc;

    private AudioManager am;

    private EventSystem evt;

    private GameObject sel;

    private InputManager im;

    private PreRaceSingleton prs;

    private AnalyticsDB ana;
    #endregion

    //Bouton back de l'écran en cours 
    public Button currentBackBtn;

    //id menus
    public enum TypeMenus
    {
        Principal = 0,
        CreateGame = 1,
        SearchGame = 2,
        Options = 3,
        Customisation = 4,
        Credits = 5
    }

    public enum TypesReseaux
    {
        TikTok,
        Insta,
        Youtube,
        Discord,
        Twitter,

    }

    public enum InputType
    {
        Manette,
        Mouse
    }

    public InputType currentInputType;

    // Start is called before the first frame update
    void Start()
    {
        evt = EventSystem.current;

        im = FindObjectOfType<InputManager>();

        prs = FindObjectOfType<PreRaceSingleton>();


        SearchAudioManagerAndAnalytics();
        

       //Debug.Log("Start Analytics : " + "userId" + AnalyticsSessionInfo.userId + " " + AnalyticsSessionInfo.sessionState + "sessionId " + AnalyticsSessionInfo.sessionId + " sessionElapsedTime" + AnalyticsSessionInfo.sessionElapsedTime);
            

    }

    // Update is called once per frame
    void Update()
    {

        /*
         * gère l'élément selectionné selon la navigation des boutons en mode souris ou manette / clavier 
         * enlève le selected au moindre mouvement suspect de la souris 
         * 
         *
         */


        if (evt.currentSelectedGameObject != null && evt.currentSelectedGameObject != sel)
        {
            sel = evt.currentSelectedGameObject;
        }
        else if (sel != null && evt.currentSelectedGameObject == null && im.inputMovement != Vector2.zero)
        {
            evt.SetSelectedGameObject(sel);
            gamePadInputInfo.SetActive(true);
            currentInputType = InputType.Manette;
        }
        else if (sel != null && im.inputMouse != Vector2.zero && sel.GetComponent<TMP_InputField>() == null && sel.GetComponent<InputField>() == null)
        {
            evt.SetSelectedGameObject(null);
            gamePadInputInfo.SetActive(false);
            currentInputType = InputType.Mouse;
        }

        if (im.controlType == InputManager.ControlsType.Gamepad)
        {
            Cursor.visible = false;
        }
        else if (im.controlType == InputManager.ControlsType.KeyboardAndMouse)
        {
            Cursor.visible = true;
        }

        //vérification de l'apuie de any key au star du jeu + si cest la premiere connection aussi
        if (im.inputAny && isInTitle)
        {

            ana.Ana_GameStart();

            if (PlayerPrefs.HasKey("Nickname") && PlayerPrefs.GetString("Nickname") != "")
            {
                foreach (Selectable button in allMenuPrincipalButtons)
                {
                    button.GetComponent<Button>().interactable = true;
                }

                evt.SetSelectedGameObject(allMenuPrincipalButtons[0].gameObject);

                isInTitle = false;

                Camera.main.GetComponent<MakeMenuAnimation>().playAnimCam(0);
                Debug.Log("j'etait title");
                UiControllerIcons.SetActive(true);
                StartCoroutine(WaitForDesactivateTitleScreen());
                panneauOncePseudo.SetActive(false);
            }
            else if (PlayerPrefs.HasKey("ClufAccepted") == false)
            {
                panneauClufs.SetActive(true);
                clufScrollbar.Select();
            }
            else if(PlayerPrefs.GetString("Nickname") == "")
            {
                panneauOncePseudo.SetActive(true);
            }
            
        }

        //Retour en utilisant le bouton B d'une manette
        if (im.menu_PreviousStep)
        {
            currentBackBtn.onClick.Invoke();
        }
    }

    private IEnumerator WaitForDesactivateTitleScreen()
    {
        yield return new WaitForSeconds(2);

        titleScreen.SetActive(false);
    }

    /// <summary>
    /// action a faire quand on reviens au menu principal depuis quelque pars + son des boutons
    /// </summary>
    /// <param name="from"></param>
    public void GoToMenuPrincipal(int from)
    {
        SearchAudioManagerAndAnalytics();

        ana.Ana_Menus(AnalyticsDB.Menus.principal);

        switch (from)
        {
            case (int)TypeMenus.CreateGame:

                allMenuPrincipalButtons[0].Select();
                cmlc.MatchmakingCancel();

                break;

            case (int)TypeMenus.SearchGame:

                allMenuPrincipalButtons[1].Select();
                cmlc.MatchmakingCancel();

                break;

            case (int)TypeMenus.Options:

                panneauOptions.SetActive(false);
                allMenuPrincipalButtons[2].Select();

                break;

            case (int)TypeMenus.Customisation:

                am.musicConfiguration.parametersMusicMenu.inPersonalisation.value = 0;

                break;

            case (int)TypeMenus.Credits:

                am.musicConfiguration.parametersMusicMenu.inCredits.value = 0;

                panneauCredits.SetActive(false);

                break;
        }

        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);
    }

    /// <summary>
    /// Go to l'url demandé 
    /// </summary>
    /// <param name="from"></param>
    public void GoReseau(int from)
    {
        SearchAudioManagerAndAnalytics();

        switch (from)
        {
            case (int)TypesReseaux.TikTok:

                Application.OpenURL("https://www.tiktok.com/@dumballrush");

                ana.Ana_Socials(AnalyticsDB.Socials.tiktok);

                break;

            case (int)TypesReseaux.Insta:
                
                Application.OpenURL("https://www.instagram.com/dumballrush/");

                ana.Ana_Socials(AnalyticsDB.Socials.instagram);

                break;

            case (int)TypesReseaux.Youtube:

                Application.OpenURL("https://www.youtube.com/channel/UCx8--tHujomVdl5a_oxBXBA");

                ana.Ana_Socials(AnalyticsDB.Socials.youtube);

                break;

            case (int)TypesReseaux.Discord:

                Application.OpenURL("https://discord.gg/wcPSHs2ZYp");

                ana.Ana_Socials(AnalyticsDB.Socials.discord);

                break;

            case (int)TypesReseaux.Twitter:

                Application.OpenURL("https://twitter.com/DumballRush");

                ana.Ana_Socials(AnalyticsDB.Socials.twitter);

                break;
        }

        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);
    }

    public void AcceptCluf()
    {
        PlayerPrefs.SetInt("ClufAccepted", 1);
        panneauClufs.SetActive(false);
    }

    public void ClufScrollbarEnd(Scrollbar scrollBar)
    {
        Debug.Log("cc" + scrollBar.value);

        if (scrollBar.value <= 0)
        {
            Debug.Log("cc2  " + scrollBar.value);
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnDown = acceptClufBtn;
            scrollBar.navigation = NewNav;
        }
        else
        {
            Debug.Log("cc3  " + scrollBar.value);
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnDown = null;
            scrollBar.navigation = NewNav;
        }
    }


    public void GoToCustomisation(float timeToStopSnowSlide)
    {
        SearchAudioManagerAndAnalytics();
        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);

        am.musicConfiguration.parametersMusicMenu.inPersonalisation.value = 1;

        ana.Ana_Menus(AnalyticsDB.Menus.custom);
    }

    public void GoToOptions()
    {
        panneauOptions.SetActive(true);
        targetButtonMenuOptions.Select();

        SearchAudioManagerAndAnalytics();
        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);

        ana.Ana_Menus(AnalyticsDB.Menus.option);
    }

    public void GoToCredits()
    {
        panneauCredits.SetActive(true);

        SearchAudioManagerAndAnalytics();
        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);
        am.musicConfiguration.parametersMusicMenu.inCredits.value = 1;

        ana.Ana_Menus(AnalyticsDB.Menus.credit);
    }

    public void GoToPlay()
    {
        panneauCreateGame.SetActive(true);
        panneauSearchGame.SetActive(false);

        targetButtonMenuCreateGame.Select();

        SearchAudioManagerAndAnalytics();
        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);

        ana.Ana_Menus(AnalyticsDB.Menus.play);

        cmlc.JoinLobbyOnClick();
    }

    public void GoToCreateRoom()
    {
        SearchAudioManagerAndAnalytics();
        ana.Ana_Menus(AnalyticsDB.Menus.create_room);
    }

    public void CreateRoom()
    {
        cmlc.CreateRoom();
    }

    public void GoToSearchRoom()
    {
        SearchAudioManagerAndAnalytics();
        ana.Ana_Menus(AnalyticsDB.Menus.search_room);
    }

    //Evite au paneau quit de disparaitre si on est trop rapide
    private bool inQuit;
    /// <summary>
    /// panneau de demande si on quitte le jeu
    /// </summary>
    public void AskQuit()
    {
        SearchAudioManagerAndAnalytics();
        //targetButtonMenuQuit.Select();

        panneauQuit.SetActive(true);
        inQuit = true;

        ana.Ana_Menus(AnalyticsDB.Menus.quit);
    }

    public void ReturnAskQuit()
    {
        //allMenuPrincipalButtons[5].Select();

        inQuit = false;
        StartCoroutine(WaitForDesactivateTheQuitPanel(1.5f));

        ana.Ana_Menus(AnalyticsDB.Menus.principal);
    }

    private IEnumerator WaitForDesactivateTheQuitPanel(float time)
    {
        yield return new WaitForSeconds(time);

        if (!inQuit)
            panneauQuit.SetActive(false);
    }

    public void QuitGame()
    {
        ana.Ana_GameStop();
        
        Application.Quit();
    }


    public void OnRoomNameChanged(Text txtInputName)
    {
        Debug.Log("Changed room name");
        cmlc.OnRoomNameChanged(txtInputName.text);
    }

    public void DoClickSound(int type)
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        switch (type)
        {
            case 0:

                am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventClickOnButtons);

                break;

            case 1:

                am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventClickBackButtons);

                break;

            case 2:

                am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventClickCustomisation);

                break;

            case 3:

                am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventClickImportant);

                break;
        }
    }

    public void TauntInPercent(float percent)
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        float theRandom = Random.Range(0, 100f);

        if (theRandom <= percent)
        {
            Debug.Log("Taunt");
            am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, playerMenu.gameObject);
        }
    }

    public void CelebrationInPercent(float percent)
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        float theRandom = Random.Range(0, 100f);

        if (theRandom <= percent)
        {
            Debug.Log("Celebration");
            am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCelebration, playerMenu.gameObject, "ChoixVoix", PlayerPrefs.GetInt("voice"));
        }
    }

    public void ChangeVoice(string input)
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        int voiceId;

        int.TryParse(input, out voiceId);
        am.soundEffectConfiguration.voix.voiceID = voiceId;

        PlayerPrefs.SetInt("voice", voiceId);

        am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, playerMenu.gameObject, "ChoixVoix", voiceId);
    }

    private void SearchAudioManagerAndAnalytics()
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        if (ana == null)
        {
            ana = FindObjectOfType<AnalyticsDB>();
        }
    }

    public void SettingResolution(Vector2 res)
    {
        Screen.SetResolution((int)res.x, (int)res.y, false);
    }

    public void OpenArmoireSound(bool open)
    {
        SearchAudioManagerAndAnalytics();

        if (open)
            am.PlaySoundEffect(am.soundEffectConfiguration.menu.armoireOpen);
        else
            am.PlaySoundEffect(am.soundEffectConfiguration.menu.armoireClose);
    }

    public void ChangeRaceSize(/*string sizeString*/ float size)
    {
        //int size;
        //int.TryParse(sizeString, out size);
        //int sizeInt = (int)size;
        txt_RaceSize.text = size.ToString();

        prs.ChangeSize((int)size);
    }

    public void ChangeRaceId(int race)
    {
        prs.ChangeRace(race);
    }

    public void ChangeCurrentBackButton(Button btn)
    {
        currentBackBtn = btn;
    }

    public void DisableSizeRaceWhenNoRandom()
    {
        if(prs.raceID == 0)
        {
            RaceSizeGo.SetActive(true);
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnDown = RaceSizeGo.GetComponentInChildren<Slider>();
            btn_ChangeRoomName.navigation = NewNav;

            /*Navigation NewNav2 = new Navigation();
            NewNav2.mode = Navigation.Mode.Explicit;

            NewNav2.selectOnUp = RaceSizeGo.GetComponentInChildren<Slider>();
            targetButtonMenuCreateGame.GetComponent<Button>().navigation = NewNav2;*/
        }
        else
        {
            RaceSizeGo.SetActive(false);
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnDown = targetButtonMenuCreateGame.GetComponent<Button>();
            btn_ChangeRoomName.navigation = NewNav;

            /*Navigation NewNav2 = new Navigation();
            NewNav2.mode = Navigation.Mode.Explicit;

            NewNav2.selectOnUp = btn_ChangeRoomName;
            targetButtonMenuCreateGame.GetComponent<Button>().navigation = NewNav2;*/
        }
    }

    public IEnumerator UiRoomNavigation(int idDisappearBtn)
    {
        yield return new WaitForSeconds(0.1f);

        allInteractebleRoomButtons.Clear();

        //allRoomButtons = new List<GameObject>();
        Transform roomsContainer = FindObjectOfType<CustomMatchmackingLobbyController>().roomsContainer;

        if (roomsContainer.childCount >= 1)
        {
            for (int i = 0; i < roomsContainer.childCount; i++)
            {
                if (roomsContainer.GetChild(i).GetComponent<Button>().interactable)
                {
                    allInteractebleRoomButtons.Add(roomsContainer.GetChild(i).gameObject);
                }
            }
        }

        if(evt.currentSelectedGameObject == null)
        {
            int idNewBtn = 0;

            if (allInteractebleRoomButtons.Count == 0)
            {
                SearchByNameButton.Select();
            }
            else if(allInteractebleRoomButtons.Count < idDisappearBtn)
            {
                idNewBtn = allInteractebleRoomButtons.Count - 1;
                allInteractebleRoomButtons[idNewBtn].GetComponent<Button>().Select();
            }
            else
            {
                allInteractebleRoomButtons[idNewBtn].GetComponent<Button>().Select();
            }
            
        }

        //Navigation du bouton de recherche de nom de la room
        //Si il y a au moins une room
        if(allInteractebleRoomButtons.Count >= 1)
        {
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnLeft = backBtnInSearchMenu;
            NewNav.selectOnDown = allInteractebleRoomButtons[0].GetComponent<Button>();
            SearchByNameButton.GetComponent<Button>().navigation = NewNav;
        }
        //S'il n'y a pas de room
        else
        {
            Navigation NewNav = new Navigation();
            NewNav.mode = Navigation.Mode.Explicit;

            NewNav.selectOnLeft = backBtnInSearchMenu;
            NewNav.selectOnDown = backBtnInSearchMenu;
            SearchByNameButton.GetComponent<Button>().navigation = NewNav;
        }

        for (int i = 0; i < allInteractebleRoomButtons.Count; i++)
        {
            //Dans le cas ou il n'y a qu'une room
            if(allInteractebleRoomButtons.Count == 1)
            {
                Navigation NewNav = new Navigation();
                NewNav.mode = Navigation.Mode.Explicit;

                NewNav.selectOnUp = SearchByNameButton;
                NewNav.selectOnLeft = backBtnInSearchMenu;
                allInteractebleRoomButtons[i].GetComponent<Button>().navigation = NewNav;
            }
            //s'il y a plus d'une room
            else if(allInteractebleRoomButtons.Count > 1)
            {
                //Navigation du premier bouton
                if(i == 0)
                {
                    Navigation NewNav = new Navigation();
                    NewNav.mode = Navigation.Mode.Explicit;

                    NewNav.selectOnUp = SearchByNameButton;
                    NewNav.selectOnLeft = backBtnInSearchMenu;
                    NewNav.selectOnDown = allInteractebleRoomButtons[i+1].GetComponent<Button>();
                    allInteractebleRoomButtons[i].GetComponent<Button>().navigation = NewNav;
                }
                //Navigation du dernier bouton
                else if (i == allInteractebleRoomButtons.Count - 1)
                {
                    Navigation NewNav = new Navigation();
                    NewNav.mode = Navigation.Mode.Explicit;

                    NewNav.selectOnUp = allInteractebleRoomButtons[i - 1].GetComponent<Button>();
                    NewNav.selectOnLeft = backBtnInSearchMenu;
                    allInteractebleRoomButtons[i].GetComponent<Button>().navigation = NewNav;
                }
                //Navigation des boutons au milieu
                else
                {
                    Navigation NewNav = new Navigation();
                    NewNav.mode = Navigation.Mode.Explicit;

                    NewNav.selectOnUp = allInteractebleRoomButtons[i - 1].GetComponent<Button>();
                    NewNav.selectOnLeft = backBtnInSearchMenu;
                    NewNav.selectOnDown = allInteractebleRoomButtons[i + 1].GetComponent<Button>();
                    allInteractebleRoomButtons[i].GetComponent<Button>().navigation = NewNav;
                }
            }
        }

        //FindObjectOfType<CustomMatchmackingLobbyController>().


    }
}
