﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Présent dans la scène de classement
 * 
 * Le script gère le classement de fin de partie
 * 
 */

public class ClassementManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Temps en seconde entre chaque ajout de barres")]
    private float timeBetweenClassementBarsSpawn;

    [SerializeField]
    [Tooltip("Temps en seconde avant de faire apparaitre les barres")]
    private float timeBeforeAppear;

    #region Functionnal

    //La liste des joueurs ayant terminé la course
    private List<PostGameInfo.PlayerInfo> playerInfos = new List<PostGameInfo.PlayerInfo>();

    [System.Serializable]
    private struct PlayerPodium
    {
        [Tooltip("L'objet du Player sur le podium")]
        public GameObject playerPodiumObject;

        [Tooltip("Mesh du player sur le podium")]
        public GameObject mesh;

        [Tooltip("Le visageController du joueur sur le podium")]
        public VisageController vc;
    }

    [Header("Functionnal")]
    [SerializeField]
    [Tooltip("Tous les joueurs présent sur le podium du 1er au 3ème")]
    private PlayerPodium[] playerPodiums;

    [SerializeField]
    [Tooltip("Bouton pour retourner dans la room")]
    private Selectable buttonReturnInRoom;
    [SerializeField]
    [Tooltip("Bouton pour retourner au menu principal")]
    private Selectable buttonReturnToMenu;
    [SerializeField]
    [Tooltip("Le parent des boutons")]
    private GameObject buttonsParent;
    [SerializeField]
    [Tooltip("Bouton retour au lobby")]
    private Button buttonBackToLobby;
    [SerializeField]
    [Tooltip("Text waiting master")]
    private GameObject txtWaitingMaster;
    [SerializeField]
    [Tooltip("Text Time auto back")]
    private Text txtAutoBack;

    //Création d'une liste pour acceuillir le bouton (C'était parceque la fonction AppearBar nécessite une liste)
    private List<GameObject> buttonsParentToList = new List<GameObject>();

    [Header("Classement bar")]
    [SerializeField]
    [Tooltip("Prefab d'une barre de classement")]
    private GameObject classementBar_Prefab;
    [SerializeField]
    [Tooltip("Parent des barres de classement")]
    private RectTransform classementBars_Parent;
    [SerializeField]
    [Tooltip("Points de spawn des barres de classement")]
    private RectTransform[] classementBars_SpawnPositions;

    //Liste des barres de classement
    private List<GameObject> allClassementBars = new List<GameObject>();

    [Header("Chronos bar")]
    [SerializeField]
    [Tooltip("Prefab d'une barres de chrono (celles qui affiche le schrono + le nom + la position")]
    private GameObject chronosBar_Prefab;
    [SerializeField]
    [Tooltip("Parent des barres de chronos")]
    private RectTransform chronosBars_Parent;
    [SerializeField]
    [Tooltip("Points de spawn des barres de chronos")]
    private RectTransform[] chronosBars_SpawnPositions;
    [SerializeField]
    [Tooltip("Offset pour chaque barres de chronos")]
    private Vector3 offsetBetweenChronosBars;

    //Liste des barres de chronos
    private List<GameObject> allChronosBars = new List<GameObject>();

    [SerializeField]
    [Tooltip("Texte affichant l'étape à laquelle se trouve le joueur")]
    private Text numberOfStep;

    [SerializeField]
    [Tooltip("Image du background")]
    private GameObject imageBackground;

    //Si le joueur peut changer d'étape
    private bool canSwitch = true;
    //etape visée
    [HideInInspector]
    public int targetStep = 0;

    //Le nombre de joueurs
    private int numberOfPlayers;

    [SerializeField]
    private GameObject hidingObject;

    [SerializeField]
    private int timeAutoBack;
    [SerializeField]
    private Lean.Localization.LeanToken timeAutoBackToken;
    #endregion

    #region Scripts et components
    //Photon view
    private PhotonView pv;
    //Input manager
    private InputManager im;
    //Audiomanager
    private AudioManager am;
    #endregion

    private void Start()
    {
        //Récupération du photon view
        pv = GetComponent<PhotonView>();
        //Récupération de l'input manager
        im = FindObjectOfType<InputManager>();
        //Récupération de l'AudioManager
        am = FindObjectOfType<AudioManager>();

        am.actualScene = AudioManager.SceneType.Classement;
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide);
        am.ambianceConfiguration.cave = 0;
        am.ambianceConfiguration.forest = 0;

        am.PlaySoundEffect(am.soundEffectConfiguration.menu.clochette);

        am.PlaySoundEffect(am.soundEffectConfiguration.menu.foule);
        am.soundEffectConfiguration.menu.feuDArtificesInstance = am.CreateFMODInstance(am.soundEffectConfiguration.menu.feuDArtificesInstance);

        SwitchMaster();

        //L'histoire de la fonction qui n'accepte que les listes (AppearBar)
        buttonsParentToList.Add(buttonsParent);
        buttonsParent.SetActive(false);

        //Récupère les informations de joueur
        GetPlayersPostGameInfosAndDestroyObjects();

        //DisAppear du cache misère
        StartCoroutine(DisappearHidingObject());

        //Initialise les joueurs sur le podium
        InitialisePlayerPodium();

        //Initialise les barres de classements
        InitialiseClassement();

        //Initialise les barres de chronos
        InitialiseChronosBar();

        //Fait apparaitre les barres de classement
        StartCoroutine(AppearBars(allClassementBars, 0));

        StartCoroutine(AutoBackToLobby());
    }

    private void Update()
    {
        //S'il peut changer l'écran
        if (canSwitch)
        {
            //S'il a appuyé sur la touche pour passer à l'écran suivant
            if (im.menu_NextStep)
            {
                //Si on ne va pas au delà de la limite d'écran
                if (targetStep < 2)
                {
                    //On change d'écran
                    targetStep++;

                    am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);

                    StartCoroutine(SwitchBetweenSteps(targetStep));
                    StartCoroutine(TimerCanSwitch());
                }
            }

            //S'il a appuyé sur la touche pour passer à l'écran précédant
            if (im.menu_PreviousStep)
            {
                //Si on ne va pas dans un écran négatif
                if (targetStep > 0)
                {
                    //On change d'écran
                    targetStep--;

                    am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventWhooshMenu);

                    StartCoroutine(SwitchBetweenSteps(targetStep));
                    StartCoroutine(TimerCanSwitch());
                }
            }
        }

        UpdateMaster();
    }

    private IEnumerator DisappearHidingObject()
    {
        hidingObject.GetComponent<Animator>().SetTrigger("Disappear");

        yield return new WaitForSeconds(1f);
    }

    /// <summary>
    /// Timer pour réautoriser le joueur à changer d'écran (ça évite de faire n'importe quoi en spammant)
    /// </summary>
    /// <returns></returns>
    private IEnumerator TimerCanSwitch()
    {
        canSwitch = false;

        yield return new WaitForSeconds(1.7f);

        canSwitch = true;
    }

    /// <summary>
    /// Récupère toutes les informations des joueurs ayant terminé la course
    /// </summary>
    private void GetPlayersPostGameInfosAndDestroyObjects()
    {
        PostGameInfo pgi = FindObjectOfType<PostGameInfo>();

        playerInfos = pgi.GetPlayerInfos();
    }

    /// <summary>
    /// Initialise les joueurs sur le podium
    /// </summary>
    private void InitialisePlayerPodium()
    {
        //Change l'apparence des 3 premier joueurs
        for(int i = 0; i < playerInfos.Count; i++)
        {
            if (playerInfos[i].position == 1 || 
                playerInfos[i].position == 2 || 
                playerInfos[i].position == 3)
            {
                int playerPodiumID = playerInfos[i].position - 1;

                playerPodiums[playerPodiumID].mesh.GetComponent<Renderer>().material.SetColor("Color_E797161B", playerInfos[i].color);
                playerPodiums[playerPodiumID].mesh.GetComponent<RandomNumberAnim>().voiceID = playerInfos[i].voiceId;
                playerPodiums[playerPodiumID].vc.UpdateEye(playerInfos[i].eyeId);
                playerPodiums[playerPodiumID].vc.UpdateMouth(playerInfos[i].mouthId);
            }
        }

        //Retire les joueurs du podium s'il y a moins de 3 joueurs
        if (playerInfos.Count < 3)
        {
            playerPodiums[2].playerPodiumObject.SetActive(false);

            if (playerInfos.Count < 2)
            {
                playerPodiums[1].playerPodiumObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Initialise les barres de classement
    /// </summary>
    private void InitialiseClassement()
    {
        //L'objet de la barre de classement
        GameObject classementBar;
        //Le RectTransform de la barre de classement
        RectTransform classementBarTrsfm;

        //Initialise seulement 8 barres de classement
        for (int i = 0; i < Mathf.Clamp(8,0,playerInfos.Count); i++)
        {
            //Instantiation
            classementBar = Instantiate(classementBar_Prefab);
            //Assignation du transform
            classementBarTrsfm = classementBar.GetComponent<RectTransform>();

            //Change le parent
            classementBarTrsfm.parent = classementBars_Parent;
            //Change la position
            classementBarTrsfm.position = classementBars_SpawnPositions[i].position;

            //Initialise la barre de classement
            classementBar.GetComponent<ClassementBarInitialiser>().Initialise(playerInfos[i].nickname, playerInfos[i].position);

            //Ajoute la barre dans la liste des barres
            allClassementBars.Add(classementBar);
        }
    }

    /// <summary>
    /// Initialise les barres de chronos
    /// </summary>
    private void InitialiseChronosBar()
    {
        //l'objet de la barre de chronos
        GameObject chronosBar;
        //Le RectTransform de la barre de chronos
        RectTransform chronosBarTrsfm;

        //Pour tous les joueurs
        for (int i = 0; i < playerInfos.Count; i++)
        {
            //Instantiation
            chronosBar = Instantiate(chronosBar_Prefab);
            //Récupération du transform
            chronosBarTrsfm = chronosBar.GetComponent<RectTransform>();

            //Change le parent
            chronosBarTrsfm.parent = chronosBars_Parent;

            //Choisit le point de spawn et change la position
            if (i < 8)
            {
                chronosBarTrsfm.position = chronosBars_SpawnPositions[0].position + i * offsetBetweenChronosBars;
            }
            else
            {
                chronosBarTrsfm.position = chronosBars_SpawnPositions[1].position + (i-8) * offsetBetweenChronosBars;
            }

            //Initialise la barre de chronos
            chronosBar.GetComponent<ClassementBarInitialiser>().Initialise(playerInfos[i].nickname, playerInfos[i].position, true, playerInfos[i].chrono);

            //Ajoute la barre de chronos
            allChronosBars.Add(chronosBar);
        }
    }

    /// <summary>
    /// Fait changer la valeur de la variable "Step" dans l'animator de chaque objet spécifié dans la liste
    /// </summary>
    /// <param name="bars"></param>
    /// <param name="newStep"></param>
    /// <returns></returns>
    private IEnumerator AppearBars(List<GameObject> bars, int newStep)
    {
        foreach (GameObject bar in bars)
        {
            bar.GetComponent<Animator>().SetInteger("Step",newStep);

            yield return new WaitForSeconds(timeBetweenClassementBarsSpawn);
        }
    }

    /// <summary>
    /// Switch entre les "steps"
    /// </summary>
    /// <param name="step"></param>
    /// <returns></returns>
    private IEnumerator SwitchBetweenSteps(int step)
    {
        //Change le step des animators de camera et de background + le texte qui montre à combien on est de step
        Camera.main.GetComponent<Animator>().SetInteger("Step", step);
        imageBackground.GetComponent<Animator>().SetInteger("Step", step);

        //numberOfStep.text = $"{step + 1}/3";

        //Défini l'action à faire selon le step
        switch (step)
        {
            case 0:

                StartCoroutine(AppearBars(allChronosBars, step));

                if (buttonsParent.activeSelf)
                {
                    StartCoroutine(AppearBars(buttonsParentToList, step));
                }

                yield return new WaitForSeconds(timeBeforeAppear);

                StartCoroutine(AppearBars(allClassementBars, step));

                buttonsParent.SetActive(false);

                break;


            case 1:

                StartCoroutine(AppearBars(allClassementBars, step));

                if (buttonsParent.activeSelf)
                {
                    StartCoroutine(AppearBars(buttonsParentToList, step));
                }
                

                yield return new WaitForSeconds(timeBeforeAppear);

                StartCoroutine(AppearBars(allChronosBars, step));

                buttonsParent.SetActive(false);

                break;

            case 2:

                StartCoroutine(AppearBars(allChronosBars, step));
                StartCoroutine(AppearBars(allClassementBars, step));

                yield return new WaitForSeconds(timeBeforeAppear);

                buttonsParent.SetActive(true);
                StartCoroutine(AppearBars(buttonsParentToList, step));

                buttonBackToLobby.Select();

                break;
        }

    }

    private IEnumerator AutoBackToLobby()
    {
        for (int i = timeAutoBack; i > 0; i--)
        {
            timeAutoBackToken.SetValue(i);

            if(i == timeAutoBack / 2 && Camera.main.GetComponent<Animator>().GetInteger("Step") == 0)
            {
                if (targetStep < 1)
                {
                    targetStep = 1;
                    StartCoroutine(SwitchBetweenSteps(1));
                }
            }

            yield return new WaitForSeconds(1f);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            GoToRoom();
        }        
    }

    /// <summary>
    /// Permet de retourner dans la room
    /// </summary>
    public void GoToRoom()
    {
        pv.RPC("FMODUpdateToMenu", RpcTarget.All, 1);

        //La room redevient ouverte pour les nouveaux joueurs
        PhotonNetwork.CurrentRoom.IsOpen = true;
        //On load la scène de room
        PhotonNetwork.LoadLevel(2);
    }

    /// <summary>
    /// Permet de retourner au menu principal
    /// </summary>
    public void GoToMainMenu()
    {
        //Fmod retourne au menu
        FMODUpdateToMenu(0);

        //si le joueur est le master
        if (PhotonNetwork.IsMasterClient)
        {
            //Si il reste au moins un autre joueur dans la room
            if (PhotonNetwork.PlayerList.Length > 1)
            {
                //choix d'un nouveau master parmis les joueurs restants puis le joueur part de la room
                int newIdMaster = Random.Range(0, PhotonNetwork.PlayerList.Length);
                if (!PhotonNetwork.PlayerList[newIdMaster].IsMasterClient)
                {
                    PhotonNetwork.SetMasterClient(PhotonNetwork.PlayerList[newIdMaster]);
                    SceneManager.LoadScene(1);
                    PhotonNetwork.LeaveRoom();
                }
                else
                {
                    GoToMainMenu();
                }
            }
            else
            {
                SceneManager.LoadScene(1);
                PhotonNetwork.LeaveRoom();
            }
        }
        else
        {
            SceneManager.LoadScene(1);
            PhotonNetwork.LeaveRoom();
        }
    }

    //Fonction de remise à 0 d'fmod quand on retourne au menu
    [PunRPC]
    private void FMODUpdateToMenu(int returnInRoom)
    {
        AudioManager am = FindObjectOfType<AudioManager>();

        am.musicConfiguration.parametersMusicRace.startDecompte.value = 0;
        am.musicConfiguration.parametersMusicRace.startRaceAfterDecompte.value = 0;
        am.musicConfiguration.parametersMusicRace.progression.value = 0;
        am.musicConfiguration.parametersMusicRace.toFinish.value = 0;

        am.actualScene = AudioManager.SceneType.Menu;
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide);

        am.StopFMODInstance(am.soundEffectConfiguration.menu.fouleInstance);
        am.StopFMODInstance(am.soundEffectConfiguration.menu.feuDArtificesInstance);

        if (returnInRoom == 1)
            am.actualScene = AudioManager.SceneType.Lobby;
    }

    private void UpdateMaster()
    {
        if (numberOfPlayers != PhotonNetwork.PlayerList.Length)
        {
            numberOfPlayers = PhotonNetwork.PlayerList.Length;

            SwitchMaster();
        }
    }

    private void SwitchMaster()
    {
        //Si c'est le master on active le bouton pour accéder à la room
        if (PhotonNetwork.IsMasterClient)
        {
            buttonReturnInRoom.interactable = true;
            txtWaitingMaster.SetActive(false);
        }
        else
        {
            buttonReturnInRoom.interactable = false;
            txtWaitingMaster.SetActive(true);
        }
    }
}
