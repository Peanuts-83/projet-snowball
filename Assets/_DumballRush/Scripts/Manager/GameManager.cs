﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class GameManager : MonoBehaviourPunCallbacks
{
    [Tooltip("Liste de tous les joueurs")]
    public List<PlayerController> AllPlayers;
    [Tooltip("Liste des valeurs de position pour chaque joueur")]
    public List<float> waypointsDistancePlayer;
    [Tooltip("Liste des valeurs de position triées pour avoir un classement")]
    public List<float> classement;

    [Tooltip("Player Controller du joueur (donné par le PositionInRace)")]
    public PlayerController myPlController;
    [Tooltip("Classement actuel du joueur")]
    public int Position;

    [Tooltip("Texte d'affichage du classement")]
    [SerializeField]
    private Text txtClassement2;
    [Tooltip("Texte d'affichage du countdown avant le début de la course")]
    [SerializeField]
    private Text txtCountdown;
    [SerializeField]
    private Text txtCountdownFinish;
    [SerializeField]
    private Image countdownImage;
    [SerializeField]
    private Sprite[] countdownSprites;

    [Tooltip("Objets d'ui")]
    public GameObject[] UiObjects;

    [Tooltip("Liste des chronos finaux")]
    public List<float> FinalChronos;
    [Tooltip("Liste des chronos finaux dans l'ordre")]
    public List<float> FinalChronosOrder;
    [Tooltip("Mon propre chrono")]
    public float myChrono;

    [Tooltip("l'UI d'affichage de fin")]
    public GameObject UiPositionFinale;

    [Tooltip("Temps entre 2 calculs de position")]
    public float MajRankTime;

    [Tooltip("Valeur du countdown")]
    public float countdownStart = 3;
    [Tooltip("Valeur du countdown de Fin")]
    public int countdownFinish = 5;


    //Si la course commence
    private bool startingRace = false;
    //Si la course à commencé
    private bool isStarted = false;
    [HideInInspector]
    public bool asFinishCountdown = false;

    [Tooltip("Le script du PositionAndTimerController de mon joueur")]
    public PositionAndTimerController myPositionAndTimerController;

    //L'heure de début
    private float startHour;
    [Tooltip("Ajout à l'heure du début pour être sur que tous les joueurs ont les informations")]
    public float addStartHour;

    //Si le joueur peut commencer la coroutine d'attente du classement
    private bool canWaitBeforeClassement = true;

    //Les textes de l'UiPositionFinale pour les nicknames
    public List<GameObject> UiPosFinaleTxt;

    //Les textes de l'UiPositionFinale pour les chrono
    public List<GameObject> UiChronoFinaleTxt;

    private AudioManager am;
    private PhotonView pv;
    [HideInInspector]
    public UiManager uiManager;
    private GameSetupController gameSController;
    private InputManager im;

    private int countDestroyingPlayer = 0;
    private Coroutine coroutine_startWaitBeforeClassement = null;


    [SerializeField]
    private GameObject GOTimelineCameraAnimStart;
    [SerializeField]
    private GameObject GOCameraStartAnim;
    [SerializeField]
    private int IntAllFinishAnim;
    private bool OnceAnimTrigger = true;
    #region

    #endregion


    // Start is called before the first frame update
    void Start()
    {
        uiManager = FindObjectOfType<UiManager>();

        gameSController = FindObjectOfType<GameSetupController>();
        im = GetComponent<InputManager>();

        pv = GetComponent<PhotonView>();

        //Calcul de classement du joueur
        StartCoroutine(ValuesPosition());

        countdownImage.GetComponent<CanvasRenderer>().SetAlpha(0);
        
        foreach (GameObject ui in UiObjects)
        {
            ui.GetComponent<CanvasRenderer>().SetAlpha(0);
        }
    }

    void Update()
    {
        //Si je suis le master
        if (PhotonNetwork.IsMasterClient)
        {
            if (startingRace == false)
            {
                if ((GOTimelineCameraAnimStart.GetComponent<PlayableDirector>().duration >= 7f && OnceAnimTrigger))
                {
                    EndOfAnimCamStart();
                    OnceAnimTrigger = false;
                }

                //Si tous les joueurs sont présents
                if (gameSController.PlayersInScene == PhotonNetwork.PlayerList.Length)
                {
                    if (IntAllFinishAnim >= PhotonNetwork.PlayerList.Length)
                    {
                        //La partie se lance
                        Debug.Log("Start Game");
                        startingRace = true;


                    }

                    //On créer une heure de début de course
                    startHour = PhotonNetwork.ServerTimestamp + addStartHour;
                        //On communique l'heure à tous les joueurs
                        pv.RPC("RPC_StartTheCount", RpcTarget.All, startHour);
                    
                   
                }
            }
        }
        else if (!PhotonNetwork.IsMasterClient)
        {
            if ((GOTimelineCameraAnimStart.GetComponent<PlayableDirector>().duration >= 7f && OnceAnimTrigger))
            {
                Debug.Log("+1 anim for complete");
                EndOfAnimCamStart();
                OnceAnimTrigger = false;
            }

           /* if (startingRace == false)
            {
                //Si tous les joueurs sont présents
                if (gameSController.PlayersInScene == PhotonNetwork.PlayerList.Length)
                {
                    textBeforeStart.text =
                      "" +
                     gameSController.PlayersInScene +
                      "/" +
                      PhotonNetwork.PlayerList.Length;

                }
                else if (gameSController.PlayersInScene != PhotonNetwork.PlayerList.Length)
                {
                    // affichage compteur de nombre d'arrivant sur total de joueur prévu pendant lattente des gens 
                    textBeforeStart.text =
                       "" +
                      gameSController.PlayersInScene +
                       "/" +
                       PhotonNetwork.PlayerList.Length;
                }
            }*/
        }

        //Si le nombre de joueur dans la liste de photon n'est pas égale au nombre de joueur dans ma liste de PlayerController
        if (PhotonNetwork.PlayerList.Length != AllPlayers.Count)
        {
            //On cherche tous les joueurs
            FindPlayers();

            Debug.Log("Player has left the game, remaining " + AllPlayers.Count);

            FinalChronos.Clear();

            foreach (PlayerController pc in AllPlayers)
            {
                Debug.Log(pc.GetComponent<PersonnalisationController>().playerNickName + " as finish race " + pc.GetComponent<PositionAndTimerController>().finishRace);

                if (pc.GetComponent<PositionAndTimerController>().finishRace)
                {
                    Debug.Log("Add chrono of " + pc.GetComponent<PersonnalisationController>().playerNickName);
                    AddMyChrono(pc.GetComponent<PositionAndTimerController>().elapsedTime);
                }
            }


            Debug.Log("Do once avec finalchrono" + FinalChronos.Count + "" + PhotonNetwork.PlayerList.Length + "" + canWaitBeforeClassement);

            //Lorsque les joueurs ont tous passé la ligne d'arrivé
            if (FinalChronos.Count == PhotonNetwork.PlayerList.Length && canWaitBeforeClassement == true)
            {
                //Je ne peut pas repasser dans le if
                canWaitBeforeClassement = false;

                if (FinalChronos.Count == AllPlayers.Count)
                {
                    Debug.Log("Start waitbeforeclassement");

                    //Je lance la coroutine d'attente de classement
                    if (coroutine_startWaitBeforeClassement != null)
                        StopCoroutine(coroutine_startWaitBeforeClassement);

                    coroutine_startWaitBeforeClassement = StartCoroutine(WaitBeforeClassement());
                }
            }
        }

        //Lorsque l'heure de départ calculée est atteinte, on lance la course
        if (PhotonNetwork.ServerTimestamp >=
            startHour &&
            isStarted == false &&
            startingRace == true)
        {
            ActivateCineCam(false);

            //Debug.Log("serv hour is : " + PhotonNetwork.ServerTimestamp + "and start hour was : " + startHour);
            isStarted = true;


            //On lance le countdown
            StartCoroutine(Countdown());

        }
    }

    //Calcul du classement actuel du joueur
    public IEnumerator ValuesPosition()
    {
        if (myPositionAndTimerController == null || myPlController == null)
        {
            Debug.Log($"FLORIAN: {myPositionAndTimerController != null} et {myPlController != null}");

            yield return new WaitForEndOfFrame();
            StartCoroutine(ValuesPosition());
        }

        //Pour tous les joueurs présents, je récupère dans une liste leur positionnement dans la course (nombre de waypoints franchis et celui en cours - Voir PositionInRace)
        for (int i = 0; i < AllPlayers.Count; i++)
        {
            if(AllPlayers[i] != null)
            {
                waypointsDistancePlayer[i] = AllPlayers[i].gameObject.GetComponent<PositionAndTimerController>().myDistance;
            }         
        }

        //Nouvelle liste avec les mêmes valeurs triées de la plus grande à la plus petite
        classement = waypointsDistancePlayer.OrderByDescending(x => x).ToList();

        //Le client retrouve son classement parmis la liste triée puis affichage de ce classement
        for (int i = 0; i < classement.Count; i++)
        {
            //Si le joueur n'est pas null
            if(myPlController != null)
            {
                //Si sa valeur de myDistance correspond à l'une des valeurs triées du classement
                if (myPlController.gameObject.GetComponent<PositionAndTimerController>().myDistance == classement[i])
                {
                    //On modifie la position du joueur
                    Position = i + 1;

                    //Trouve l'audioManager
                    if (am == null)
                        am = FindObjectOfType<AudioManager>();

                    am.musicConfiguration.parametersMusicRace.positionInRace.value = Position;

                    if (!myPositionAndTimerController.finishRace)
                    {
                        uiManager.SetPositionNumber(Position);
                    }
                    else
                    {
                        uiManager.ActivatePositionNumber(false);
                        //uiManager.ActivateCountdownFinish(false);
                    }
                }
            }           
        }

        //Attente avant nouveau calcul et relancement de la coroutine
        yield return new WaitForSeconds(MajRankTime);
        if (!myPositionAndTimerController.finishRace)
        {
            StartCoroutine(ValuesPosition());
        }       
    }

    //Lance le début de la course
    public IEnumerator Countdown()
    {
        FindObjectOfType<UiManager>().endCam.gameObject.SetActive(false);

        //Trouve l'audioManager
        if (am == null)
            am = FindObjectOfType<AudioManager>();

        //Change les stades dans la musique
        am.musicConfiguration.parametersMusicRace.startDecompte.value = 1;

        //Temps de battement avant lancement de la course
        yield return new WaitForSeconds(3f);

        //Fait défiler les nombre 3,2,1
        countdownImage.GetComponent<CanvasRenderer>().SetAlpha(1);

        countdownImage.sprite = countdownSprites[2];
        am.PlaySoundEffect(am.soundEffectConfiguration.menu.StartRace3);

        yield return new WaitForSeconds(1.5f);

        countdownImage.sprite = countdownSprites[1];
        am.PlaySoundEffect(am.soundEffectConfiguration.menu.StartRace2);
        yield return new WaitForSeconds(1.5f);

        countdownImage.sprite = countdownSprites[0];
        am.PlaySoundEffect(am.soundEffectConfiguration.menu.StartRace1);
        am.musicConfiguration.parametersMusicRace.startRaceAfterDecompte.value = 1;

        yield return new WaitForSeconds(1.5f);

        am.PlaySoundEffect(am.soundEffectConfiguration.menu.StartRaceGO);

        countdownImage.GetComponent<CanvasRenderer>().SetAlpha(0);

        asFinishCountdown = true;

        //Récupération des spawners
        List<GameObject> spawner = FindObjectOfType<GameSetupController>().spawnPoints;
        //Destruction des spawners
        foreach (GameObject spawn in spawner)
        {
            Destroy(spawn);
            spawn.GetComponentInParent<Animator>().SetTrigger("StartAnim");
        }

        foreach (GameObject ui in UiObjects)
        {
            ui.GetComponent<CanvasRenderer>().SetAlpha(1);
        }

        //Lancement du chrono
        myPlController.GetComponent<PositionAndTimerController>().BeginTimer();

        yield return new WaitForSeconds(1.5f);
        //animation du texte classement 
        uiManager.ActivatePositionNumber(true);
        //uiManager.ActivateChrono(true);
    }

    //Affichage dans le leaderboard des joueurs classés avec leur chrono
    /*public void UpdateFinalPosition()
    {
        //pour tous les chronos
        for (int i = 0; i < FinalChronosOrder.Count; i++)
        {
            //pour tous les joueurs
            for (int u = 0; u < AllPlayers.Count; u++)
            {
                //si le chrono du classement est égale au chrono du joueur et qu'il n'est pas dans le leaderboard alors c'est le sien
                if (AllPlayers[u].GetComponent<PositionAndTimerController>().elapsedTime == FinalChronosOrder[i] && 
                    AllPlayers[u].GetComponentInParent<PositionAndTimerController>().isInLeaderboard == false)
                {
                    anim.Play("Start",1 , -1f);

                    //Le joueur est donc dans le leaderboard (doOnce)
                    AllPlayers[u].GetComponentInParent<PositionAndTimerController>().isInLeaderboard = true;

                    //On affiche le nom du joueur au bon classement
                    UiPosFinaleTxt[i].GetComponent<Text>().text = AllPlayers[u].GetComponent<PersonnalisationController>().playerNickName;

                    //On affiche son chrono
                    UiChronoFinaleTxt[i].GetComponent<Text>().text = TimeSpan.FromSeconds(FinalChronosOrder[i] / 1000).ToString("mm':'ss'.'ff");

                    break;
                }
            }
        }
    }*/

    //envoie de l'heure de départ à tous les joueurs
    [PunRPC]
    void RPC_StartTheCount(float _startHour)
    {
        startingRace = true;
        startHour = _startHour;

        //txtCountdown.text = "Get Ready !";

        FindPlayers();
    }

    //Cherche les joueurs pour les ajouter dans une liste
    public void FindPlayers()
    {
        //Recherche tous les joueurs et set la liste
        AllPlayers = FindObjectsOfType<PlayerController>().ToList();
        if (myPlController != null)
            myPlController.gameObject.GetComponent<DetectForWarning>().CreateLists();
    }

    public void AddMyChrono(float chrono)
    {
        //Debug.Log("I add my chrono" + chrono);

        //On ajoute mon chrono à la liste des chronos finaux
        FinalChronos.Add(chrono);

        Debug.Log("Add My chrono");

        FindPlayers();

        Debug.Log("Final chrono = " + FinalChronos.Count + "allPlayer = " + AllPlayers.Count);

        if(FinalChronos.Count == 1)
        {
            StartCoroutine(CountdownToFinish());
        }

        //check si tous les joueurs ont fini en envoyant leur chronos
        if (FinalChronos.Count >= AllPlayers.Count)
        {
            //Classement des chronos dans l'ordre, le plus petit chrono etant le meilleur
            FinalChronosOrder = FinalChronos.OrderByDescending(x => x).ToList();
            FinalChronosOrder.Reverse();

            Debug.Log("final chronos de tous le monde " + FinalChronosOrder);

            //Lancement de l'affichage du classement
            if (coroutine_startWaitBeforeClassement != null)
                StopCoroutine(coroutine_startWaitBeforeClassement);

            Debug.Log("FinalChronos == allplayers.count");
            coroutine_startWaitBeforeClassement = StartCoroutine(WaitBeforeClassement());          
        }
    }

    public IEnumerator CountdownToFinish()
    {
        for (int i = 0; i < countdownFinish; i++)
        {
            if (myPositionAndTimerController.finishRace)
            {
                break;
            }
            else
            {
                int timeRemaining = countdownFinish - i;
                txtCountdownFinish.text = timeRemaining.ToString() + "'";
                uiManager.ActivateCountdownFinish();

                yield return new WaitForSeconds(1f);
            }           
        }

        if(myChrono == 0 && !myPositionAndTimerController.finishRace)
        {
            uiManager.ActivateGameOver(true);
            myPositionAndTimerController.EndTimer(999999);
        }
    }

    //Affichage du classement
    public IEnumerator WaitBeforeClassement()
    {
        Debug.Log("Start Wait before classement");

        FindPlayers();


        yield return new WaitForSeconds(5f);

        PostGameInfo pgi = FindObjectOfType<PostGameInfo>();

        if (pgi != null)
        {
            pgi.ClearPlayerList();

            //pour tous les chronos
            for (int i = 0; i < FinalChronosOrder.Count; i++)
            {
                //pour tous les joueurs
                for (int u = 0; u < AllPlayers.Count; u++)
                {
                    PositionAndTimerController patc = AllPlayers[u].GetComponent<PositionAndTimerController>();

                    //si le chrono du classement est égale au chrono du joueur et qu'il n'est pas dans le leaderboard alors c'est le sien
                    if (patc.elapsedTime == FinalChronosOrder[i] && patc.isInLeaderboard == false)
                    {
                        //(DoOnce)
                        patc.isInLeaderboard = true;

                        //Position du joueur
                        int finalPosition = i + 1;
                        //Chrono du joueur
                        float finalChrono = FinalChronosOrder[i];

                        //Récupération de la personnalisation du joueur
                        PersonnalisationController persoCtrl = AllPlayers[u].GetComponent<PersonnalisationController>();

                        string playerNickname = persoCtrl.playerNickName;
                        Debug.Log("Adding " + playerNickname + " in postGameInfo");

                        Color playerColor = persoCtrl.playerColor;
                        int playerEyeId = persoCtrl.eyeId;
                        int playerMouthId = persoCtrl.mouthId;
                        int playerVoiceId = persoCtrl.voiceId;

                        //Ajoute les caractéristiques du joueur au PostGameInfo
                        pgi.AddPlayerInfo(finalPosition, finalChrono, playerNickname, playerColor, playerEyeId, playerMouthId, playerVoiceId);

                        break;
                    }
                }
            }
        }

        Debug.Log("All players added" + pgi.GetPlayerInfos().Count + " first is " + pgi.GetPlayerInfos()[0].nickname);

        //Dit à tout le monde que je suis prêt
        FindPlayers();

        foreach (PlayerController player in AllPlayers)
        {
            if (player != null && player.pv.IsMine)
            {
                Debug.Log("Say ready " + player.GetComponent<PersonnalisationController>().playerNickName);
                player.pv.RPC("RPC_UpdateReadyClassement", RpcTarget.All, true);
            }
        }

        //Charge la scène de classement
        if (PhotonNetwork.IsMasterClient)
        {
            StartCoroutine(WaitOtherPlayersToClassement());
        }

        Debug.Log("Finish Wait before classement");
    }

    bool changeOnceScene = false;

    private IEnumerator WaitOtherPlayersToClassement()
    {
        yield return new WaitForEndOfFrame();

        int boolCount = 0;

        FindPlayers();

        foreach (PlayerController pl in AllPlayers)
        {
            if (pl.iAmReadyForClassement)
            {
                boolCount++;
            }
        }

        Debug.Log("player ready: " + boolCount);

        if (boolCount >= AllPlayers.Count)
        {
            //Dire à chaque joueur de supprimer son joueur pour empêcher les doubles
            /*
             * Chaque joueur doit suprimer son instance
             * Le Master doit attendre que chaque joueur ai suprimé son instance avant de changer de scène
             * 
             * L'instance du joueur ne peut être suprimé que dans la MainScene
             * L'instance du joueur ne peut être suprimé que par le joueur le controlant
             */

            Debug.Log("Kill all players");

            pv.RPC("RPC_KillMyPlayer", RpcTarget.All);

            if (countDestroyingPlayer >= PhotonNetwork.PlayerList.Length)
            {
                Debug.Log("As kill all players");

                if (changeOnceScene == false)
                {
                    changeOnceScene = true;

                    Debug.Log("Change Scene");

                    //Suprime tous les blocs
                    foreach (GameObject bloc in FindObjectOfType<GenProcedurale>().blockSpawned)
                    {
                        PhotonNetwork.Destroy(bloc);
                    }

                    PhotonNetwork.LoadLevel(5);
                }
            }
            else
            {
                StartCoroutine(WaitOtherPlayersToClassement());


                Debug.Log("Waiting other players to confirm update killing");
            }
        }
        else
        {


            Debug.Log("Waiting other players to confirm update classement");
            StartCoroutine(WaitOtherPlayersToClassement());

        }
    }

    public void ActivateCineCam(bool active)
    {
        GOCameraStartAnim.SetActive(active);
    }

    [PunRPC]
    void RPC_SendMyChrono(float chrono)
    {
        Debug.Log("RPC_SendMyChrono" + myPlController.name);

        //myChrono = chrono;
        AddMyChrono(chrono);
    }

    [PunRPC]
    void RPC_KillMyPlayer()
    {
        FindPlayers();

        foreach (PlayerController pc in AllPlayers)
        {
            if (pc != null)
            {
                if (pc.pv.IsMine)
                {
                    FindObjectOfType<UiManager>().uiGameOver.SetActive(false);
                    FindObjectOfType<UiManager>().HideView();

                    PhotonNetwork.Destroy(pc.gameObject);
                    pv.RPC("RPC_ConfirmKillingPlayer", RpcTarget.All);

                    Debug.Log("killing " + pc.GetComponent<PersonnalisationController>().playerNickName);
                }
            }
        }
    }

    [PunRPC]
    void RPC_ConfirmKillingPlayer()
    {
        countDestroyingPlayer++;
    }

    public void EndOfAnimCamStart()
    {
        pv.RPC("RPC_EndOfAnimCamStart", RpcTarget.MasterClient);
    }



    [PunRPC]
    void RPC_EndOfAnimCamStart()
    {
        IntAllFinishAnim++;
    }
}
