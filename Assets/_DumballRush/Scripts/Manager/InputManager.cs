﻿using System.Collections;
using UnityEngine;

/*
 * Ce script sert à assigner les inputs du jeu
 * Il doit être sur un objet InputManager dans la scene
 */

public class InputManager : MonoBehaviour
{
    //Configuration des touches player
    private PlayerInputActions _playerInputActions;

    #region Player
    //Input de mouvements
    [HideInInspector]
    public Vector2 inputMovement;
    //Input de saut
    [HideInInspector]
    public bool inputJump;
    //Input déplacement souris ingame (caméra)
    [HideInInspector]
    public Vector2 inputMouse;
    //Input debug perte de neige
    [HideInInspector]
    public bool inputDebugShrink;
    //Input utilisation du power Up
    [HideInInspector]
    public bool inputPowerUpForward;
    //Input utilisation du power Up
    [HideInInspector]
    public bool inputPowerUpBack;
    //Input debug récupération / changement de power up
    [HideInInspector]
    public bool inputDebugPowerUp;
    //Input deplacement souris menu
    [HideInInspector]
    public Vector2 inputMousePosition;
    //Input clic de souris
    [HideInInspector]
    public bool inputMouseClic;

    //Input debugFonction (ScriptForDebug)
    [HideInInspector]
    public bool inputDebugFonction;

    //Input de reverseCamera
    [HideInInspector]
    public bool inputReverseCamera;

    [HideInInspector]
    public bool inputOption;

    [HideInInspector]
    public bool inputAny;

    //Input de Chat
    [HideInInspector]
    public bool inputChatUp;
    [HideInInspector]
    public bool inputChatDown;
    [HideInInspector]
    public bool inputChatLeft;
    [HideInInspector]
    public bool inputChatRight;

    [HideInInspector]
    public bool inputCloseChat;

    [HideInInspector]
    public bool inputOpenChat;
    #endregion

    #region Menus
    [HideInInspector]
    public bool menu_NextStep;
    [HideInInspector]
    public bool menu_PreviousStep;
    [HideInInspector]
    public bool menu_SquareOrX;
    [HideInInspector]
    public bool menu_TriangleOrY;
    [HideInInspector]
    public bool menu_L2OrLT;

    [HideInInspector]
    public bool menu_Navigation;

    public enum ControlsType
    {
        Gamepad,
        KeyboardAndMouse
    }

    //[HideInInspector]
    public ControlsType controlType;

    #endregion

    private void Awake()
    {
        //Création de la configuration des touches
        _playerInputActions = new PlayerInputActions();

        //Assignation des différents inputs
        //Button Quit
        _playerInputActions.Player.DebugQuit.performed += context => Quit();

        //Button movement
        _playerInputActions.Player.Movement.performed += context => InputMovement(context.ReadValue<Vector2>());

        //Button Jump
        _playerInputActions.Player.Jump.performed += context => InputJump();

        //input Camera
        _playerInputActions.Player.Camera.performed += context => InputMouse(context.ReadValue<Vector2>());
        _playerInputActions.Player.Camera.canceled += context => InputMouse(context.ReadValue<Vector2>());

        //Button PowerUpForward
        _playerInputActions.Player.PowerUpForward.performed += context => InputPowerUpForward();

        //Button PowerUpBack
        _playerInputActions.Player.PowerUpBack.performed += context => InputPowerUpBack();

        //Debug Shrink
        _playerInputActions.Player.DebugShrink.performed += context => InputDebugShrink();

        //Debug PowerUp
        _playerInputActions.Player.DebugPowerUp.performed += context => InputDebugPowerUp();

        //Mouse position
        _playerInputActions.Player.MousePosition.performed += context => InputMousePosition(context.ReadValue<Vector2>());
        _playerInputActions.Player.MouseClic.performed += context => InputMouseClic();

        //Button DebugFonction
        _playerInputActions.Player.DebugFonction.performed += context => InputDebugFonction();

        //Reverse camera
        _playerInputActions.Player.ReverseCamera.performed += context => InputReverseCamera(true);
        _playerInputActions.Player.ReverseCamera.canceled += context => InputReverseCamera(false);

        //Debug Start Game
        _playerInputActions.Player.Option.performed += context => InputOption();

        //Any key
        _playerInputActions.Player.Any.performed += context => InputAny();

        _playerInputActions.Player.ChatUp.performed += context => InputChat(0);
        _playerInputActions.Player.ChatDown.performed += context => InputChat(1);
        _playerInputActions.Player.ChatLeft.performed += context => InputChat(2);
        _playerInputActions.Player.ChatRight.performed += context => InputChat(3);

        _playerInputActions.Player.CloseChat.performed += context => CloseChat();

        _playerInputActions.Player.OpenChat.performed += context => OpenChat();


        _playerInputActions.Menus.NextStep.performed += context => Menus_InputNextStep();
        _playerInputActions.Menus.PreviousStep.performed += context => Menus_InputPreviousStep();
        _playerInputActions.Menus.SquareOrX.performed += context => Menus_InputSquareOrX();
        _playerInputActions.Menus.TriangleOrY.performed += context => Menus_InputTriangleOrY();
        _playerInputActions.Menus.L2OrLT.performed += context => Menus_InputL2OrLT();

        _playerInputActions.Menus.Navigation.performed += context => Menus_InputNavigation();

        _playerInputActions.Menus.DetectGamepad.performed += context => Menus_ChangeControl(ControlsType.Gamepad);
        _playerInputActions.Menus.DetectGamepad.started += context => Menus_ChangeControl(ControlsType.Gamepad);
        _playerInputActions.Menus.DetectKeyboardAndMouse.performed += context => Menus_ChangeControl(ControlsType.KeyboardAndMouse);
        _playerInputActions.Menus.DetectKeyboardAndMouse.started += context => Menus_ChangeControl(ControlsType.KeyboardAndMouse);
    }

    //Fonction Quitter à partir d'une touche
    #region Functions Player
    private void Quit()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }
    

    //Gestion des inputs de mouvements
    public void InputMovement(Vector2 readedValue)
    {
        Vector2 recreatedVector = Vector2.zero;

        recreatedVector.x = Mathf.Clamp(readedValue.x * 2, -1, 1);

        recreatedVector.y = Mathf.Clamp(readedValue.y * 2, -1, 1);

        inputMovement = recreatedVector;
    }

    //Gestion de l'input du Jump
    private void InputJump()
    {
        inputJump = true;

        //Lancement de la coroutine pour réactiver la touche de saut (DoOnce)
        StartCoroutine(SetJumpFalse());

        //Réactivation de la touche de saut
        IEnumerator SetJumpFalse()
        {
            yield return new WaitForFixedUpdate();
            inputJump = false;
        }
    }

    //Gestion de la souris (déplacement cam)
    private void InputMouse(Vector2 readedValue)
    {
        inputMouse = readedValue;
    } 

    //Gestion de la souris (position du curseur pour le menu)
    private void InputMousePosition(Vector2 readedValue)
    {
       inputMousePosition = readedValue;
    }

    //Gestion du clic de la souris
    private void InputMouseClic()
    {
        inputMouseClic = true;

        //Lancement de la coroutine pour réactiver le clic (DoOnce)
        StartCoroutine(SetMouseClicFalse());

        //Réactivation du clic
        IEnumerator SetMouseClicFalse()
        {
            yield return new WaitForFixedUpdate();
            inputMouseClic = false;
        }
    }

    //Gestion de la touche de debug (perte de neige)
    private void InputDebugShrink()
    {
        inputDebugShrink = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetBoolFalse());

        //Réactivation de la touche
        IEnumerator SetBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            inputDebugShrink = false;
        }
    }

    //Gestion de la touche de power up
    private void InputPowerUpForward()
    {
        inputPowerUpForward = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetPowerUpForwardFalse());

        //Réactivation de la touche
        IEnumerator SetPowerUpForwardFalse()
        {
            yield return new WaitForEndOfFrame();
            inputPowerUpForward = false;
        }
    }

    //Gestion de la touche de power up
    private void InputPowerUpBack()
    {
        inputPowerUpBack = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetPowerUpBackFalse());

        //Réactivation de la touche
        IEnumerator SetPowerUpBackFalse()
        {
            yield return new WaitForEndOfFrame();
            inputPowerUpBack = false;
        }
    }

    //Gestion de la touche de power up (debug)
    private void InputDebugPowerUp()
    {
        inputDebugPowerUp = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputPowerUpFalse());

        //Réactivation de la touche
        IEnumerator SetInputPowerUpFalse()
        {
            yield return new WaitForEndOfFrame();
            inputDebugPowerUp = false;
        }
    }

    //Gestion de la touche debug fonction
    private void InputDebugFonction()
    {
        inputDebugFonction = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputDebugFonctionFalse());

        //Réactivation de la touche
        IEnumerator SetInputDebugFonctionFalse()
        {
            yield return new WaitForEndOfFrame();
            inputDebugFonction = false;
        }
    }

    private void InputReverseCamera(bool pressed)
    {
        inputReverseCamera = pressed;
    }

    private void InputOption()
    {
        inputOption = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputOptionFalse());

        //Réactivation de la touche
        IEnumerator SetInputOptionFalse()
        {
            yield return new WaitForEndOfFrame();
            inputOption = false;
        }
    }

    private void InputAny()
    {
        inputAny = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputAnyFalse());

        //Réactivation de la touche
        IEnumerator SetInputAnyFalse()
        {
            yield return new WaitForEndOfFrame();
            inputAny = false;
        }
    }

    private void InputChat(int dirId)
    {
        switch (dirId)
        {
            //Si c'est Up
            case 0:
                inputChatUp = true;

                //Lancement de la coroutine pour réactiver la touche (DoOnce)
                StartCoroutine(SetInputChatFalse());

                break;

            //Si c'est Down
            case 1:
                inputChatDown = true;

                //Lancement de la coroutine pour réactiver la touche (DoOnce)
                StartCoroutine(SetInputChatFalse());

                break;

            //Si c'est Left
            case 2:
                inputChatLeft = true;

                //Lancement de la coroutine pour réactiver la touche (DoOnce)
                StartCoroutine(SetInputChatFalse());

                break;

            //Si c'est Right
            case 3:
                inputChatRight = true;

                //Lancement de la coroutine pour réactiver la touche (DoOnce)
                StartCoroutine(SetInputChatFalse());

                break;
        }

        //Réactivation de la touche
        IEnumerator SetInputChatFalse()
        {
            yield return new WaitForEndOfFrame();
            inputChatUp = false;
            inputChatDown = false;
            inputChatLeft = false;
            inputChatRight = false;
        }
    }

    private void CloseChat()
    {
        inputCloseChat = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputCloseChatFalse());

        //Réactivation de la touche
        IEnumerator SetInputCloseChatFalse()
        {
            yield return new WaitForEndOfFrame();
            inputCloseChat = false;
        }
    }

    private void OpenChat()
    {
        inputOpenChat = true;

        //Lancement de la coroutine pour réactiver la touche (DoOnce)
        StartCoroutine(SetInputOpenChatFalse());

        //Réactivation de la touche
        IEnumerator SetInputOpenChatFalse()
        {
            yield return new WaitForEndOfFrame();
            inputOpenChat = false;
        }
    }

    #endregion

    #region Functions Menus
    private void Menus_InputNextStep()
    {
        menu_NextStep = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_NextStep = false;
        }
    }

    private void Menus_InputPreviousStep()
    {
        menu_PreviousStep = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_PreviousStep = false;
        }
    }

    private void Menus_InputSquareOrX()
    {
        menu_SquareOrX = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_SquareOrX = false;
        }
    }

    private void Menus_InputTriangleOrY()
    {
        menu_TriangleOrY = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_TriangleOrY = false;
        }
    }

    private void Menus_InputL2OrLT()
    {
        menu_L2OrLT = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_L2OrLT = false;
        }
    }

    private void Menus_InputNavigation()
    {
        menu_Navigation = true;

        StartCoroutine(setBoolFalse());

        IEnumerator setBoolFalse()
        {
            yield return new WaitForEndOfFrame();
            menu_Navigation = false;
        }
    }

    private void Menus_ChangeControl(ControlsType control)
    {
        controlType = control;
        UpdateControl();
    }

    private void UpdateControl()
    {
        Debug.Log("changeControl");
        switch (controlType)
        {
            case ControlsType.Gamepad:

                //Cursor.visible = false;

                break;
            case ControlsType.KeyboardAndMouse:

                /*Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;*/

                break;
            default:
                break;
        }

        UpdateControlForScripts();
    }

    public void UpdateControlForScripts()
    {
        if(FindObjectOfType<ChatManager>() != null)
        {
            FindObjectOfType<ChatManager>().ChangeControllerChat();
        }      
    }

    #endregion

    #region OnEnable / OnDisable
    //Activation des inputs
    private void OnEnable()
    {
        _playerInputActions.Enable();
    }

    //désactivation des inputs
    private void OnDisable()
    {
        _playerInputActions.Disable();
    }
    #endregion
}

