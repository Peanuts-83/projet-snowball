﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostGameInfo : Singleton<PostGameInfo>
{
    #region Singleton
    //Pour être sur que le constructeur est un Singleton
    protected PostGameInfo() { }
    #endregion

    [System.Serializable]
    public struct PlayerInfo
    {
        [Tooltip("Nickname du joueur")]
        public string nickname;

        [Tooltip("Position du joueur à la fin de la course")]
        public int position;
        [Tooltip("Chrono réalisé par le joueur à la fin de la course")]
        public float chrono;

        [Tooltip("Couleur du joueur")]
        public Color color;
        [Tooltip("L'ID des yeux du joueur")]
        public int eyeId;
        [Tooltip("L'ID de la mouche du joueur")]
        public int mouthId;
        [Tooltip("L'ID de la voix du joueur")]
        public int voiceId;
    }

    [SerializeField]
    [Tooltip("Liste de toutes les informations de tous les joueurs, ils doivent être ajouté dans l'ordre de position à la fin de la course (du premier et dernier)")]
    private List<PlayerInfo> playerInfos = new List<PlayerInfo>();

    /// <summary>
    /// Ajoute un joueur à la liste des joueurs à la fin de la course
    /// </summary>
    /// <param name="position"></param>
    /// <param name="chrono"></param>
    /// <param name="nickname"></param>
    /// <param name="color"></param>
    /// <param name="eyeId"></param>
    /// <param name="mouthId"></param>
    /// <param name="voiceId"></param>
    public void AddPlayerInfo(int position, float chrono, string nickname, Color color, int eyeId, int mouthId, int voiceId)
    {
        PlayerInfo newPlayer;

        newPlayer.position = position;
        newPlayer.chrono = chrono;

        newPlayer.nickname = nickname;
        newPlayer.color = color;
        newPlayer.eyeId = eyeId;
        newPlayer.mouthId = mouthId;
        newPlayer.voiceId = voiceId;

        playerInfos.Add(newPlayer);
    }

    /// <summary>
    /// Nettoie la liste des joueurs
    /// </summary>
    public void ClearPlayerList()
    {
        playerInfos.Clear();
    }

    /// <summary>
    /// Permet de récupérer la liste des joueurs
    /// </summary>
    /// <returns></returns>
    public List<PlayerInfo> GetPlayerInfos()
    {
        return playerInfos;
    }
}
