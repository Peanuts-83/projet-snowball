﻿using UnityEngine;

/*
 * Ce script permet de "sauvegarder" le dernier choix de taille de course grace à un singleton
 * 
 * Ce script doit être sur le SizeRaceManager du menu
 */
public class PreRaceSingleton : Singleton<PreRaceSingleton>
{
    #region Singleton
    //Pour être sur que le constructeur est un Singleton
    protected PreRaceSingleton() { }
    #endregion

    #region Variables
    [Tooltip ("Taille de la course de base")]
    public int sizeRace;

    //0: Semi-Procédural
    //1: Course 1
    //2: Course 2
    //x: Course x
    //...
    [Tooltip("L'id de la course")]
    public int raceID;
    [Tooltip("Le nom de la course")]
    public string raceName;

    public enum TypeOfLoad
    {
        Id,
        Name
    }

    [HideInInspector]
    public TypeOfLoad typeOfLoad;

    #endregion

    //Changement de la taille de la course (utilisée dans SizeRaceSingletonFinder)
    public void ChangeSize(int size)
    {
        sizeRace = size;
    }

    public void ChangeRace(int race)
    {
        raceID = race;
        typeOfLoad = TypeOfLoad.Id;
    }

    public void ChangeRace(string name)
    {
        raceName = name;
        typeOfLoad = TypeOfLoad.Name;
    }
}