﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

/*
 * Ce script permet à la musique de progresser en fonction de notre progression
 * 
 * Ce script doit être placé sur le GameObject GameManager de la MainScene (A changer peut etre ?)
 */

public class ProgressionSystem : MonoBehaviour
{
    [Tooltip("Période d'actualisation de la progression")]
    [SerializeField]
    private float periodUpdate;

    [Tooltip("Visualisation de la progression en pourcentage")]
    public float progressionVisualizer;

    //Le gamemanager
    private GameManager gm;
    //Le waypoint manager
    private WaypointsTracker wt;
    //l'audiomanager
    private AudioManager am;

    private void Start()
    {
        //On récupère le gameManager
        gm = GetComponent<GameManager>();
        //On récupère les waypoints
        wt = FindObjectOfType<WaypointsTracker>();

        //Start update
        StartCoroutine(UpdateProgression(periodUpdate));
    }

    //Update the progression
    IEnumerator UpdateProgression(float period)
    {
        //On récupère l'audioManager
        am = FindObjectOfType<AudioManager>();

        //Wait period time
        yield return new WaitForSeconds(period);

        #region Variables d'initialisation

        //Start position
        Vector3 startPosition = Vector3.zero;
        //End position
        Vector3 endPosition = Vector3.zero;
        //playerposition
        Vector3 playerPosition = Vector3.zero;

        if (wt.waypointsInOrder.Count > 0)
        {
            //Start position
            startPosition = wt.waypointsInOrder[0].transform.position;
            //End position
            endPosition = wt.waypointsInOrder[(int)Mathf.Clamp(wt.waypointsInOrder.Count - 1, 0, wt.waypointsInOrder.Count)].transform.position;
        }

        if (gm.myPlController != null)
        {
            //Player position
            playerPosition = gm.myPlController.transform.position;
        }

        #endregion

        #region Calculs
        //The totalDistance of the race
        float totalDistance = Vector3.Distance(startPosition, endPosition);
        //The actual distance of the player from de startPosition
        float actualDistance = Vector3.Distance(playerPosition, startPosition);

        //Unlerp de la distance
        float unlerpDist = Mathf.InverseLerp(0, totalDistance, actualDistance);
        #endregion

        //Update de la progression FMOD
        UpdateFMODProgression(unlerpDist * 100);

        //Restart coroutine
        if (gm.myPlController.GetComponent<PositionAndTimerController>().finishRace == false)
        {
            StartCoroutine(UpdateProgression(period));
        }     
    }

    //Update de la Progression
    void UpdateFMODProgression(float progressionPercent)
    {
        if (am.musicConfiguration.parametersMusicRace.toFinish.value != 1)
        {
            //Le parametre progression d'fmod est égal à la progression calculée
            am.musicConfiguration.parametersMusicRace.progression.value = progressionPercent;
            //On change la valeur du visualiseur
            progressionVisualizer = progressionPercent;
        }
    }
}
