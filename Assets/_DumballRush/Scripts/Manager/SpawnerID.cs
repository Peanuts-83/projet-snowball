﻿using UnityEngine;

/*
 * Ce script est utilisé pour donner un ID à chaque spawner
 * 
 * Ce script doit être placé sur chaque spawner
 */

public class SpawnerID : MonoBehaviour
{
    [Tooltip ("ID du Spawner")]
    public int ID;
}