﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.VFX;

public class UIPanneauManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    /*
     * Fonction du son sur le over d'un bouton
     * a placer sur les boutons en question
     */


    //public Material[] matPanneau;
    //public GameObject contour;

    private AudioManager am;

    private TextMeshProUGUI txtProTest;


    public GameObject goVfxLoss;



    public GameObject halo;

    public int id_Btn;



    void Start()
    {
        am = FindObjectOfType<AudioManager>();

        txtProTest = GetComponentInChildren<TextMeshProUGUI>();

    }



    public void OnPointerEnter(PointerEventData eventData)
    {
        txtProTest.fontMaterial.SetFloat("_GlowPower", 0.6f);
        goVfxLoss.GetComponent<VisualEffect>().Play();
        halo.SetActive(true);

        am.PlaySoundEffect(am.soundEffectConfiguration.click.fmodEventOverlappButtons);

    }





    public void OnPointerExit(PointerEventData eventData)
    {
        txtProTest.fontMaterial.SetFloat("_GlowPower", 0f);
        halo.SetActive(false);

    }
}

