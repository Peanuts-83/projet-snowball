﻿ using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;

/*
 * Ce script permet au joueur de se connecter à la scene de jeu
 * 
 * Il doit être placé sur SwitchScene (gameObject dans le Loading Scene)
 */

public class ConnectToGameScene : MonoBehaviour
{
    [Tooltip("ID de la scene ou le joueur doit se connecter")]
    [SerializeField]
    private int defaultSceneId;

    [SerializeField]
    private string predefinedRaceName;

    [SerializeField]
    private bool connectOnStart = true;

    private string sceneName;

    private PreRaceSingleton prs;

    [SerializeField]
    private bool inTesting;

    private void Start()
    {
        //Load la scene choisie
        if (inTesting == false)
        SearchPreRaceSingleton(connectOnStart);
    }

    public void LoadRaceById(int ID)
    {
        if (ID == 0)
        {
            PhotonNetwork.LoadLevel(defaultSceneId);
        }
        else
        {
            string raceName = ID.ToString();

            if (ID < 10)
            {
                raceName = "0" + raceName;
            }
            
            if (ID < 100)
            {
                raceName = "0" + raceName;
            }

            raceName = $"{predefinedRaceName}" + raceName;

            PhotonNetwork.LoadLevel(raceName);
        }
    }

    private void LoadByName(string name)
    {
        PhotonNetwork.LoadLevel(name);
    }

    private void SearchPreRaceSingleton(bool connect)
    {
        while (prs == null)
        {
            prs = FindObjectOfType<PreRaceSingleton>();
        }

        if (connect)
        {
            if (prs.typeOfLoad == PreRaceSingleton.TypeOfLoad.Id)
                LoadRaceById(prs.raceID);
            else if (prs.typeOfLoad == PreRaceSingleton.TypeOfLoad.Name)
                LoadByName(prs.raceName);
        }
    }
}