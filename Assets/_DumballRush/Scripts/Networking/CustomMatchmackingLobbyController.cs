﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CustomMatchmackingLobbyController : MonoBehaviourPunCallbacks
{
    #region Variables
    [SerializeField]
    private GameObject lobbyConnectButton; //button used for joining lobby
    [SerializeField]
    private GameObject lobbyPanel; //panel for displaying lobby
    [SerializeField]
    private GameObject mainPanel; //panel for displaying the main menu
    [SerializeField]
    private InputField playerNameInput; //input fild for changing nickname

    private string roomName; //name for saving room name
    public int roomSize = 8; //int for saving room size

    private List<RoomInfo> roomListings; //List of currents rooms
    [SerializeField]
    public Transform roomsContainer; //Container for all the room listing
    [SerializeField]
    private GameObject roomListingPrefab; //prefab for displayer each room in Lobby

    private string roomNameSearch;
    private List<RoomInfo> tempRoomList;

    private string roomSearchTag;

    private bool togglePrivate;

    [SerializeField]
    private int multiPlayerSceneIndex = 0; //scene index for loading multiplayer scene
    #endregion

    #region Unity Methods
    //Start
    private void Start()
    {
        //On désactive la permission au joueur de saisir son pseudo (jusqu'à ce qu'il soit connecter au serveur)
        playerNameInput.interactable = false;
    }


    #endregion

    #region Photon Methods
    //Lorsque le joueur se connecte au serveur
    public override void OnConnectedToMaster()
    {
        //Permet à tous les joueurs de rejoindre la même scène que le masterClient une fois dans une room
        PhotonNetwork.AutomaticallySyncScene = true;
        //On active le bonton de connection au Lobby
        lobbyConnectButton.SetActive(true);
        //On active l'inputField pour le pseudo du joueur
        playerNameInput.interactable = true;
        //On initialise la liste des rooms
        roomListings = new List<RoomInfo>();
        

        //Regarde si le joueur à déjà un Nickname
        //Si le joueur a un Nickname
        if (PlayerPrefs.HasKey("Nickname"))
        {
            //Indique que le joeuur à un pseudo
            Debug.Log("PlayerAsANickname");

            //Si le pseudo du joueur est vide
            if (PlayerPrefs.GetString("Nickname") == "")
            {
                //On génère un pseudo aléatoirement
               // PhotonNetwork.NickName = "Snowball_" + UnityEngine.Random.Range(0, 1000);
            }
            //Si le pseudo n'est pas vide
            else
            {
                //On défini le pseudo du joueur dans Photon comme étant celui qui est save
                PhotonNetwork.NickName = PlayerPrefs.GetString("Nickname");
            }
        }
        //Si le joueur n'a pas de pseudo
        else
        {
            //On dit que le joueur n'as pas de pseudo
            Debug.Log("Player n'as pas de nickname");
            //On défini un nouveau pseudo dans Photon
          //  PhotonNetwork.NickName = "Snowball_" + UnityEngine.Random.Range(0, 1000);
        }

        //On update le nom du joueur
        PlayerNameUpdate(PhotonNetwork.NickName);
    }

    //Update de la liste de room
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        tempRoomList = roomList;
        //Index de la room
        int tempIndex;

        //Pour toutes les room dans la liste de room
        foreach (RoomInfo room in roomList)
        {
            //Si la liste des room actuelle n'est pas null
            if (roomListings != null)
            {
                //tempIndex prend la valeur de l'index de la room actuelle de la boucle
                tempIndex = roomListings.FindIndex(ByName(room.Name));
            }
            //Si la liste est null
            else
            {
                //On fait en sorte que tempIndex n'affecte pas la suite du code
                tempIndex = -1;
            }

            //Si tempIndex est valide
            if (tempIndex != -1)
            {
                //On supprime la room de la liste
                roomListings.RemoveAt(tempIndex);

                //On détruit le boutton pour rejoindre la room
                Destroy(roomsContainer.GetChild(tempIndex).gameObject);

                StartCoroutine(FindObjectOfType<ButtonManager>().UiRoomNavigation(tempIndex));
                //FindObjectOfType<ButtonManager>().Invoke("UiRoomNavigation", 0.1f);
                //FindObjectOfType<ButtonManager>().UiRoomNavigation();           
            }

            //On regarde si la room contient au moins un joueur
            if (room.PlayerCount > 0)
            {
                //On regarde si la room comporte le nom spécifié
                if (roomNameSearch != null && roomNameSearch != "")
                {
                    if (room.Name.Contains(roomNameSearch))
                    {
                        //On ajoute la room à la liste des rooms
                        roomListings.Add(room);
                        //On créer un boutton pour la room
                        ListRoom(room);
                    }
                }
                else
                {
                    //On ajoute la room à la liste des rooms
                    roomListings.Add(room);
                    //On créer un boutton pour la room
                    ListRoom(room);
                }
            }
        }
    }

    //Lorsque le jeu n'arrive pas à créer de room
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room named like that");
    }

    //Lorsque le joueur rejoins la room
    public override void OnJoinedRoom()
    {
        //
        //PhotonNetwork.AutomaticallySyncScene = true;

        if (PhotonNetwork.IsMasterClient)
        {
            //On lance la scène de jeu
            PhotonNetwork.LoadLevel(multiPlayerSceneIndex);
        }
    }
    #endregion

    #region Methods
    //Update du pseudo
    public void PlayerNameUpdate(string nameInput)
    {
        //Change le pseudo photon
        PhotonNetwork.NickName = nameInput;
        //Sauvegarde le pseudo du joueur
        PlayerPrefs.SetString("Nickname", nameInput);
        //Update le nom affiché par le pseudo du joueur
        playerNameInput.text = nameInput;
    }

    //Quand le joueur click sur le boutton pour rejoindre le lobby
    public void JoinLobbyOnClick()
    {
        //On désactive le premier panneau
        mainPanel.SetActive(false);
        //On active le panneau de Lobby
        lobbyPanel.SetActive(true);
        //On rejoint le Lobby
        PhotonNetwork.JoinLobby();
    }


    //Recherche de room par nom
    static System.Predicate<RoomInfo> ByName(string name)
    {
        return delegate (RoomInfo room)
        {
            //Retourne le nom de la room
            return room.Name == name;
        };
    }

    //Création du boutton de la room
    void ListRoom(RoomInfo room)
    {
        //Si la room est visible
        if (room.IsVisible)
        {
            //On créer un boutton de room
            GameObject tempListing = Instantiate(roomListingPrefab, roomsContainer);
            //On récupère le script sur le bouton
            RoomButton tempButton = tempListing.GetComponent<RoomButton>();
            //On set les informations sur le boutton de la room
            tempButton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount, room.masterClientId, room.IsOpen);

            StartCoroutine(FindObjectOfType<ButtonManager>().UiRoomNavigation(0));
        }
    }

    //Changement du nom de la room
    public void OnRoomNameChanged(string nameIn)
    {
        roomName = nameIn;
    }

    //Changement de la taille de la room
    public void OnRoomSizeChanged(string sizeIn)
    {
        roomSize = int.Parse(sizeIn);
    }

    //Création d'une room
    public void CreateRoom()
    {
        //Message indiquant la création d'une room
        Debug.Log("Creating room now");
        //Création des options de la room
        RoomOptions roomOps = new RoomOptions() { IsVisible = !togglePrivate, IsOpen = true, MaxPlayers = (byte)roomSize };

        //Si le nom de la room est null
        if (roomName == null)
        {
            //On génère un nom de room
            roomName = "Dumball Rush " + Random.Range(0, 10000);
        }

        //On créer une room avec Photon et les options créée
        PhotonNetwork.CreateRoom(roomName, roomOps);
    }



    //Lorsque l'on quitte le panneau du lobby
    public void MatchmakingCancel()
    {
        //On active le panneau principal
        mainPanel.SetActive(true);
        //On désactive le panneau de lobby
        lobbyPanel.SetActive(false);
    }

    public void SetRoomNameSearch(Text nameTxt)
    {
        roomNameSearch = nameTxt.text;

        OnRoomListUpdate(tempRoomList);
    }

    public void UpdateRoomSearchTag(string tag)
    {
        roomSearchTag = tag;
    }

    public void JoinRoomWithTag()
    {
        PhotonNetwork.JoinRoom(roomSearchTag);
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void ModifyPrivacy(bool arg)
    {
        togglePrivate = arg;
    }

    #endregion
}
