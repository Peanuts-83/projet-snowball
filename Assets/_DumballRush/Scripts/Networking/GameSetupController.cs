﻿using Photon.Pun;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/*
 * Ce script permet au joueur de se connecter à la room de jeu (en passant par le menu puis lobby etc...)
 * 
 * Ce script doit être placé sur un objet dans la scène de jeu
 */

public class GameSetupController : MonoBehaviourPunCallbacks
{
    //Nom donné au futur joueur
    public string playerPrefabName = "PhotonPlayer";

    [Tooltip("Transform des points de spawn des joueurs")]
    private Transform spawnCoordinates;

    [Tooltip("Nombre de joueurs dans la scene")]
    public int PlayersInScene = 0;

    [Tooltip("Liste des SpawnPoints")]
    public List<GameObject> spawnPoints;
    [Tooltip("Liste des SpawnPoints une fois triés")]
    public List<SpawnerID> spawnPointsInOrder;

    //Récupération des spawnPoints puis remis dans l'ordre. Fonction faite après la génération procédurale (GenProcedural.cs)
    public void CalculateSpawners()
    {
        //Récupération de tous les objets SpawnPoints
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint").ToList();

        //Tri des spawnpoints par rapport à leur ID dans une nouvelle liste
        for (int i = 0; i < spawnPoints.Count + 1; i++)
        {
            foreach (GameObject spawnPoint in spawnPoints)
            {
                if (spawnPoint.GetComponent<SpawnerID>().ID == i)
                {
                    spawnPointsInOrder.Add(spawnPoint.GetComponent<SpawnerID>());
                }
            }
        }
    }

    //Création du joueur dans la scène. Fonction faite après la génération procédurale, juste après CalculateSpawners()
    public void CreatePlayer()
    {
        Debug.Log("Creating Player");     

        //Choix du spawn
        ChooseSpawn();

        //Création du joueur, dans le spawn choisi
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", playerPrefabName), spawnCoordinates.position, Quaternion.identity);

        //Envoie de l'info qu'il y a un joueur en plus dans la scene et qui est pret
        GetComponent<PhotonView>().RPC("RPC_HowMuchPlayersInScene", RpcTarget.AllBuffered, PlayersInScene);
    }

    //Fonction de placement du joueur sur les SpawnPoints
    private void ChooseSpawn()
    {
        if (spawnPoints != null)
        {
            //Vérification dans la liste des joueurs de notre propre joueur, quand notre joueur correspond à l'un de ceux dans la liste, on donne comme lieu de spawn le spawnpoint du meme ID
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                if(PhotonNetwork.LocalPlayer == PhotonNetwork.PlayerList[i])
                {
                    spawnCoordinates = spawnPoints[i].transform;
                }
            }
        }
        //Sinon le joueur spawn quand meme mais en 0.0.0 (ne doit pas arriver en principe)
        else
        {
            spawnCoordinates.position = new Vector3(0, 0, 0);
        }
    }

    //Combien de joueurs sont sur la scene et sont prets
    [PunRPC]
    void RPC_HowMuchPlayersInScene(int plInScene)
    {
        PlayersInScene ++;
    }
}