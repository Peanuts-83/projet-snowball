﻿using Photon.Pun;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

/*
 * Ce script permet de pouvoir tester le jeu sur une scene personnelle
 * ce script doit être placé sur la scene de jeu souhaitée
 */

public class GameSetupControllerTestMap : MonoBehaviourPunCallbacks
{
    //Nom de prefab
    public string playerPrefabName = "PhotonPlayer";

    private void Start()
    {
        //Connection aux serveurs photon
        PhotonNetwork.ConnectUsingSettings();
    }

    //Quand le joueur est connecté
    public override void OnConnectedToMaster()
    {
        //Il rentre dans le lobby
        PhotonNetwork.JoinLobby();
    }

    //Une fois qu'il est dans le lobby
    public override void OnJoinedLobby()
    {
        //Il crée une room
        PhotonNetwork.JoinOrCreateRoom("Testing" + Random.Range(0,999), new Photon.Realtime.RoomOptions() { IsVisible = false, IsOpen = false, MaxPlayers = (byte)1 }, PhotonNetwork.CurrentLobby);
    }

    //Quand il a rejoint la room de jeu
    public override void OnJoinedRoom()
    {
        //Il crée son player
        CreatePlayer();
    }

    //Création du joueur
    private void CreatePlayer()
    {
        Debug.Log("Creating Player");

        //Création du joueur en allant récupérer le prefab du joueur
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", playerPrefabName), Vector3.zero, Quaternion.identity);
    }
}
