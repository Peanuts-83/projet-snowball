﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkController : MonoBehaviourPunCallbacks
{
    /*
     * Documentation: https://doc.photonengine.com/en-us/pun/current/getting-started/pun-intro
     * Scripting API: https://doc-api.photonengine.com/en/pun/v2/index.html
     */

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.InRoom)
            {
                Debug.Log("LeavingRoom");
                PhotonNetwork.LeaveRoom();
            }
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings(); //connects photon master servers
        }

        //Other ways: https://doc-api.photonengine.com/en/pun/v2/class_photon_1_1_pun_1_1_photon_network.html
    }

    public override void OnConnectedToMaster()
    {
        //Quand le joueur se connecte au serveur
        Debug.Log("We are now connected to the " + PhotonNetwork.CloudRegion + " server !");
    }
}
