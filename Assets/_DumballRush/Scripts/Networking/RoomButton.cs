﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

/*
 * Ce script permet de créer les boutons de room (boutons d'affichage avec nom de la room / nombre de joueurs co / taille de la room
 * il permet également de rejoindre cette room
 * Ce script doit être placé sur l'objet RoomButton (Prefabs/Networking)
 */

public class RoomButton : MonoBehaviour
{
    [SerializeField]
    private Text nameText; //display for room name
    [SerializeField]
    private Text sizeText; //display for room size
    [SerializeField]
    private Text roomTagText;

    //Nom de la room
    private string roomName;
    //Taille de la room
    private int roomSize;
    //Nombre de joueurs
    private int playerCount;
    //# de la room
    private string roomTag;

    //Fonction pour rejoindre la room
    public void JoinRoomOnClick()
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    //Lors de la création d'une room, création du bouton pour les autres joueurs
    public void SetRoom(string nameInput, int sizeInput, int countInput, int masterClientID, bool isOpen)
    {
        //récupération du nom de la room
        roomName = nameInput;
        //Récupération de sa taille
        roomSize = sizeInput;
        //Récupération du nombre de joueurs
        playerCount = countInput;


        //Changement du tag
        if (masterClientID < 10)
        {
            roomTag = $"#000{masterClientID}";
        }
        else if (masterClientID < 100)
        {
            roomTag = $"#00{masterClientID}";
        }
        else if (masterClientID < 1000)
        {
            roomTag = $"#0{masterClientID}";
        }
        else if (masterClientID < 10000)
        {
            roomTag = $"#{masterClientID}";
        }
        else
        {
            roomTag = "Error";
        }


        //changement du texte du nom
        nameText.text = nameInput;
        //changement du texte du nombre de joueurs / taille de la room
        sizeText.text = countInput + "/" + sizeInput;
        //Assignatoon du tag
        roomTagText.text = roomTag;

        if (!isOpen)
        {
            GetComponent<Button>().interactable = false;
        }
    }
}
