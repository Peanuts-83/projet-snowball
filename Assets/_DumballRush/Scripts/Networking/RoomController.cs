﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Analytics;
using UnityEngine.EventSystems;
using UnityEngine.Timeline;
using UnityEngine.Playables;


public class RoomController : MonoBehaviourPunCallbacks
{
    #region Variables
    [Header("UI")]
    [SerializeField]
    [Tooltip("Prefab du playerUi")]
    private GameObject playerUiPrefab;
    //Liste de tous les playerUi
    private List<GameObject> playerUis = new List<GameObject>();
    //Liste de tous les joueurs
    private List<PersonnalisationController> players = new List<PersonnalisationController>();
    [SerializeField]
    private GameObject crosshair;

    [SerializeField]
    [Tooltip("Initial location reference for all the playerUi")]
    private RectTransform initialLocation;

    [SerializeField]
    [Tooltip("Espace entre les différents playerUi")]
    private float spaceBetweenPlayerUi;

    [SerializeField]
    [Tooltip("Speed for all playerUIs to go in her place when a player leave")]
    private float speedToRegainHisPlace;

    [Header("Options")]
    [SerializeField]
    [Tooltip("Option menu")]
    private GameObject roomStartMenu;
    [SerializeField]
    [Tooltip("Start game button")]
    private GameObject startGameButton;
    [SerializeField]
    [Tooltip("Room parameter button")]
    private Selectable roomParameterButton;
    [SerializeField]
    [Tooltip("Back")]
    private Selectable backButton;
    [SerializeField]
    public GameObject startButton;

    public Selectable[] allOptionsButtons; 
    [SerializeField]
    private Slider slider_SizeRace;


    [SerializeField]
    [Tooltip("Menu de configuration du jeu")]
    private GameObject roomOptionMenu;

    [Header("Functionnal")]
    [Tooltip("Nom donné au futur joueur")]
    [SerializeField]
    private string playerPrefabName = "PhotonPlayer";

    [SerializeField]
    [Tooltip("Point de spawn du joueur")]
    private Transform[] spawnPoints;

    [SerializeField]
    [Tooltip("ID de la scène de jeu (loading)")]
    private int sceneId = 1;

    [SerializeField]
    [Tooltip("Texte pour annoncé le nombre de joueur")]
    private Text playerNumberText;
    [SerializeField]
    [Tooltip("Texte pour le nom de la room")]
    private TextMeshProUGUI roomNameText;
    [SerializeField]
    private TextMeshProUGUI isMasterText;
    [SerializeField]
    private TextMeshProUGUI isNotMasterText;

    [SerializeField]
    [Tooltip("Parents des arbres png")]
    private GameObject parentTree;

    [SerializeField]
    [Tooltip("Input field pour la taille de la course")]
    //private InputField sizeRace_If;
    private Text txt_RaceSize;

    [SerializeField]
    [Tooltip("Dropdown pour la sélection de la course")]
    private Dropdown raceID_Dropdown;
    [SerializeField]
    private Dropdown raceSelectionDebug;

    private bool isInOption;

    public GameObject newPlayerPrefab;

    [SerializeField]
    private Color[] startGameButtonTextColors;
    [SerializeField]
    private Color[] roomOptionButtonTextColors;

    private System.DateTime ana_date_arrivée;

    [SerializeField]
    private GameObject[] allCheckmarks;

    //InputManager
    private InputManager im;
    //AudioManager
    private AudioManager am;
    //PhotonView
    private PhotonView pv;
    //Analytics
    private AnalyticsDB ana;

    private PreRaceSingleton prs;


    [SerializeField]
    private GameObject GOTimelineCameraAnimStart;
    [SerializeField]
    private GameObject GOCamAnim;

    #endregion

    #region Unity Methods
    // Start is called before the first frame update
    void Start()
    {
        //Trouve l'inputManager
        im = FindObjectOfType<InputManager>();
        //Trouve l'audio manager
        am = FindObjectOfType<AudioManager>();
        //Trouve le photonView
        pv = GetComponent<PhotonView>();
        //trouve les analytics
        ana = FindObjectOfType<AnalyticsDB>();

        //prs = FindObjectOfType<PreRaceSingleton>();

        //Changement de la musique
        am.actualScene = AudioManager.SceneType.Lobby;

        //Ajoute le joueur chez tout le monde
        StartCoroutine(UpdatePlayerList(2));

        //Désactivation du menu option
        roomStartMenu.GetComponent<Animator>().SetBool("isOpen", false);

        //Instantiation du player
        newPlayerPrefab = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", playerPrefabName), spawnPoints[ChooseSpawnpoint()].position, Quaternion.identity);
        newPlayerPrefab.GetComponentInChildren<LookAtMe>().parentTree = parentTree;

        //heure d'arrivéa
        ana_date_arrivée = System.DateTime.Now;


        Debug.Log($"{ PhotonNetwork.NickName} was instantiate");
        
        UpdatePlayerNumberAndRoomName();

        prs = FindObjectOfType<PreRaceSingleton>();

        SetSizeRace(prs.sizeRace);
        SetDropdownRaceID(prs.raceID);

        if(PlayerPrefs.GetInt("displCrosshair") == 1)
        {
            crosshair.SetActive(true);
        }
        else
        {
            crosshair.SetActive(false);
        }

        UpdateRaceId(FindObjectOfType<PreRaceSingleton>().raceID);

        SetTextMaster();
    }

    private void Update()
    {
        //Si le joueur ouvre les options
        if (im.inputOption)
        {
            if (isInOption)
            {
                isInOption = false;

                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SetRoomOptionMenu(false);
                SetRoomStartMenu(false);

                foreach (Selectable optionsBtn in allOptionsButtons)
                {
                    optionsBtn.interactable = false;
                }
            }
            else
            {
                isInOption = true;

                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                SetRoomStartMenu(true);
            }
        }
        //check si ya une anim de camera 
        if (GOTimelineCameraAnimStart.gameObject != null)
        {
            if (GOTimelineCameraAnimStart.GetComponent<PlayableDirector>().time >= 12.5f || im.inputAny)
            {
                GOCamAnim.SetActive(false);
            }

            if (!(GOTimelineCameraAnimStart == null) || !(GOCamAnim == null))
            {
                if (GOTimelineCameraAnimStart.GetComponent<PlayableDirector>().time >= 12.5f || im.inputAny)
                {
                    GOCamAnim.SetActive(false);
                }
            }




        }
    }
    #endregion

    #region Methods

    public void SetTextMaster()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            isNotMasterText.gameObject.SetActive(false);
            isMasterText.gameObject.SetActive(true);
        }
        else
        {
            isMasterText.gameObject.SetActive(false);
            isNotMasterText.gameObject.SetActive(true);
        }
    }

    public void SetRoomStartMenu(bool open)
    {
        if (roomOptionMenu.activeSelf)
        {
            roomOptionMenu.SetActive(false);
        }

        roomStartMenu.SetActive(open);

        foreach (Selectable optionsBtn in allOptionsButtons)
        {
            optionsBtn.interactable = true;
        }

        roomStartMenu.GetComponent<Animator>().SetBool("isOpen", open);

        bool master = PhotonNetwork.IsMasterClient;

        startGameButton.GetComponent<Button>().interactable = master;
        foreach (TextMeshProUGUI tmp in startGameButton.GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (master)
            {
                tmp.color = startGameButtonTextColors[0];
            }
            else
            {
                tmp.color = startGameButtonTextColors[1];
            }
        }
        
        roomParameterButton.interactable = master;
        foreach (TextMeshProUGUI tmp in roomParameterButton.GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (master)
            {
                tmp.color = roomOptionButtonTextColors[0];
            }
            else
            {
                tmp.color = roomOptionButtonTextColors[1];
            }
        }

        if (master)
        {
            roomParameterButton.Select();
        }
        else
        {
            //startGameButton.GetComponent<Button>().Select();
            backButton.Select();
        }

        Debug.Log(master);
    }

    public void SetRoomOptionMenu(bool open)
    {
        if (roomStartMenu.activeSelf)
        {
            roomStartMenu.SetActive(false);
        }
        roomOptionMenu.SetActive(open);

        //raceID_Dropdown.Select();

        roomOptionMenu.GetComponent<Animator>().SetBool("isOpen", open);

        if (open == false)
        {
            roomParameterButton.Select();
        }

        //SetRoomStartMenu(open);
    }

    public void ChangeRaceSize(/*string sizeString*/ float size)
    {
        //int size;
        //int.TryParse(sizeString, out size);
        //int sizeInt = (int)size;
        txt_RaceSize.text = size.ToString();

        prs.ChangeSize((int)size);
    }

    public void StartRace()
    {
        //On dit à tous les joueurs que la partie commence et on update le son
        pv.RPC("RPC_StartGameSound", RpcTarget.All);

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            PhotonNetwork.OpCleanActorRpcBuffer(i);

        //On désactive le fait que les joueurs entre dans la room après le lancement
        PhotonNetwork.CurrentRoom.IsOpen = false;




        #region ANALYTICS
        
        ana.Ana_StartRace(new Dictionary<string, object>
        {
            {"nb_joueur", PhotonNetwork.PlayerList.Length},
            {"date", (System.DateTime.Now).ToString() },
            {"course_id", sceneId },
            {"nb_blocs", txt_RaceSize.text }
        });
        ana.Ana_TimeInLobby(new Dictionary<string, object>
        {
            {"time", (System.DateTime.Now - ana_date_arrivée).ToString() }
        });

        #endregion 



        //Détruit le joueur avant de sortir
        pv.RPC("RPC_DestroyAllPlayers", RpcTarget.All);

        //On load la scène de jeu (Loading)
        PhotonNetwork.LoadLevel(sceneId);
    }

    public void BackToMenuPrincipal()
    {
        ResetParamsLobby();
        am.actualScene = AudioManager.SceneType.Menu;

        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// Création du player UI
    /// </summary>
    /// <param name="player"></param>
    private void CreatePlayerUi(PersonnalisationController player)
    {
        //Make the color of the player
        Color playerColor = player.playerColor;

        //Créer un nouveau gameObject playerUI
        GameObject newPlayerUI = Instantiate(playerUiPrefab);
        //Set le parent pour l'ajouter dans le canvas
        newPlayerUI.transform.parent = initialLocation;
        //Change le nom du playerUI
        newPlayerUI.name = $"playerUi_{player.playerNickName}";
        //Change la position de l'objet dans l'interface
        newPlayerUI.GetComponent<RectTransform>().position = initialLocation.position - Vector3.up * spaceBetweenPlayerUi * players.Count;

        newPlayerUI.GetComponent<PlayerRoomUiSetup>().SetupPlayer(player.playerNickName, playerColor, player.eyeId, player.mouthId);

        //Ajoute le playerUI dans la liste
        playerUis.Add(newPlayerUI);

        //players.Add(player);
        players.Add(player);

        //Update le nombre de joueur
        UpdatePlayerNumberAndRoomName();

        //Update de l'affichage du master
        UpdateMasterCrownUI();

        //Make sound
        if (!player.GetComponentInParent<PhotonView>().IsMine)
            am.PlaySoundEffect(am.soundEffectConfiguration.menu.enterLobby);
    }

    /// <summary>
    /// Retire un playerUI et update la position des autres joueurs
    /// </summary>
    /// <param name="index"></param>
    private void RemovePlayerUi(int index)
    {
        //Retire le player
        playerUis[index].GetComponent<Animator>().SetTrigger("Disapear");
        playerUis.RemoveAt(index);
        players.RemoveAt(index);

        //Update le nombre de joueur
        UpdatePlayerNumberAndRoomName();

        //Update de l'affichage du master
        UpdateMasterCrownUI();

        //Make sound
        am.PlaySoundEffect(am.soundEffectConfiguration.menu.leaveLobby);

        //Lance la coroutine pour remettre la position des autres joueurs
        Debug.Log("Start move");
        StartCoroutine(SetPositionOfAllPlayerUI());
    }

    /// <summary>
    /// Update du texte affichant le nombre de joueur dans la room
    /// </summary>
    private void UpdatePlayerNumberAndRoomName()
    {
        playerNumberText.text = $"{playerUis.Count}";
        roomNameText.text = $"{PhotonNetwork.CurrentRoom.Name}";
    }

    private void UpdateMasterCrownUI()
    {
        for (int i = 0; i < playerUis.Count; i++)
        {
            playerUis[i].GetComponent<PlayerRoomUiSetup>().MasterCrown.gameObject.SetActive(false);

            if(playerUis[i].GetComponent<PlayerRoomUiSetup>().nameInput.text == PhotonNetwork.MasterClient.NickName)
            {
                playerUis[i].GetComponent<PlayerRoomUiSetup>().MasterCrown.gameObject.SetActive(true);
            }
        }
    }

    public void UpdateSizeBlockRace(string size)
    {
        int realSize = 0;
        int.TryParse(size, out realSize);

        FindObjectOfType<PreRaceSingleton>().ChangeSize(realSize);
    }

    public void UpdateRaceId(int ID)
    {
        FindObjectOfType<PreRaceSingleton>().ChangeRace(ID);

        foreach (GameObject checkmark in allCheckmarks)
        {
            checkmark.SetActive(false);
        }
        allCheckmarks[ID].SetActive(true);

        /*if (ID != 0)
        {
            txt_RaceSize.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            txt_RaceSize.transform.parent.gameObject.SetActive(true);
        }*/
    }

    public void FindDropdownScene(int ID)
    {
        UpdateRaceName(raceSelectionDebug.options[ID].text);
    }

    public void UpdateRaceName(string name)
    {
        FindObjectOfType<PreRaceSingleton>().ChangeRace(name);

        foreach (GameObject checkmark in allCheckmarks)
        {
            checkmark.SetActive(false);
        }
    }

    private void SetSizeRace(int size)
    {
        slider_SizeRace.value = size;
    }

    private void SetDropdownRaceID(int ID)
    {
        raceID_Dropdown.value = ID;
    }

    private int ChooseSpawnpoint()
    {
        int index = 0;

        if (spawnPoints != null)
        {
            //Vérification dans la liste des joueurs de notre propre joueur, quand notre joueur correspond à l'un de ceux dans la liste, on donne comme lieu de spawn le spawnpoint du meme ID
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                if (PhotonNetwork.LocalPlayer == PhotonNetwork.PlayerList[i])
                {
                    index = i;
                }
            }
        }

        return index;
    }

    private void ResetParamsLobby()
    {
        am.ambianceConfiguration.cave = 0;
        am.ambianceConfiguration.forest = 0;
        am.ambianceConfiguration.wind = 1;
        am.musicConfiguration.parametersMusicLobby.foot.value = 0;
        am.musicConfiguration.parametersMusicLobby.sumo.value = 0;
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide);
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
    }

    #endregion

    #region Coroutine
    /// <summary>
    /// Détecte l'entrée et la sortie d'un joueur dans la room pour ajouter ou retirer les joueurs sur les UI.
    /// </summary>
    /// <param name="timeUpdate"></param>
    /// <returns></returns>
    private IEnumerator UpdatePlayerList(float timeUpdate)
    {
        PersonnalisationController[] allPlayerSpawnedInRoom = FindObjectsOfType<PersonnalisationController>();

        foreach (PersonnalisationController player in allPlayerSpawnedInRoom)
        {
            if (!players.Contains(player))
            {
                CreatePlayerUi(player);
            }
        }

        if (allPlayerSpawnedInRoom.Length < players.Count)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i] == null)
                {
                    RemovePlayerUi(i);
                }
            }
        }

        yield return new WaitForSeconds(timeUpdate);

        StartCoroutine(UpdatePlayerList(timeUpdate));
    }

    /// <summary>
    /// Change la position des playerUi lorsqu'un joueur s'en va
    /// </summary>
    /// <returns></returns>
    private IEnumerator SetPositionOfAllPlayerUI()
    {
        //On attend une frame
        yield return new WaitForEndOfFrame();

        //Déclaration des variables de targetPosition et de position actuelle
        Vector3 targetPosition = Vector3.zero;
        RectTransform playerUiRectTransform = null;

        //Pour tous les joueurs
        for (int i = 0; i < playerUis.Count; i++)
        {
            //On récupère le RectTransform
            playerUiRectTransform = playerUis[i].GetComponent<RectTransform>();
            //On récupère la target
            targetPosition = initialLocation.position - Vector3.up * spaceBetweenPlayerUi * i;

            //On monte ou on descend
            if (targetPosition.y < playerUiRectTransform.position.y)
            {
                playerUiRectTransform.position = playerUiRectTransform.position - Vector3.up * Time.deltaTime * speedToRegainHisPlace;
            }
            else
            {
                playerUiRectTransform.position = playerUiRectTransform.position + Vector3.up * Time.deltaTime * speedToRegainHisPlace;
            }
        }

        //On stop la coroutine quand l'objet est arrivé
        if (playerUiRectTransform != null && targetPosition != null)
        {

            if (playerUiRectTransform.position.y > targetPosition.y - 1 && playerUiRectTransform.position.y < targetPosition.y + 1)
            {
                Debug.Log("Stop The coroutine");
                playerUiRectTransform.position = targetPosition;
            }
            else
            {
                StartCoroutine(SetPositionOfAllPlayerUI());
            }
        }
    }

    #endregion

    #region Photon Methods  
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer == newMasterClient)
        {
            startGameButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            startGameButton.GetComponent<Button>().interactable = false;
        }

        SetTextMaster();
    }

    #endregion

    #region RPC Methods
    [PunRPC]
    private void RPC_StartGameSound()
    {

        ResetParamsLobby();
        am.actualScene = AudioManager.SceneType.Race;
    }

    [PunRPC]
    private void RPC_DestroyAllPlayers()
    {
        PhotonNetwork.Destroy(newPlayerPrefab);
    }


    #endregion
}
