﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Ce script sert à activer des objets (stalactite, pont, porte) à partir de levier ou autres (en trigger)
 * Il doit être placé sur les deux objets, activator et l'objet activable
 */

public class ActivableObstacles : MonoBehaviour
{
    //enum des styles d'objets
    public enum typeOfObject { Activator, Stalactite, Door};

    [Tooltip("Choix du style d'objet")]
    public typeOfObject objectType;

    [Header("Seulement pour l'Activator")]
    public GameObject objectToActivate;

    //Pour les objets en mode "Ouvert / Fermé"
    private bool boolDoorClose = true;

    [Header("Seulement si Stalactite")]
    public GameObject meshStalactiteBase;
    public GameObject meshStalactiteBroken;
    private bool asFall;

    //Fonction lancée par l'activator lorsqu'il est trigger par le joueur
    public void OnTriggerEnter(Collider other)
    {
        //vérification que c'est un activator et que l'objet trigger est le joueur
        if(objectType == typeOfObject.Activator && other.gameObject.tag == "Player")
        {
            //Activation de l'objet relié
            objectToActivate.GetComponentInChildren<ActivableObstacles>().ActivateObject(this.gameObject);
        }
    }

    //Fonction d'activation de l'objet
    public void ActivateObject(GameObject activatorGO)
    {
        //On vérifie le type d'objet activable
        switch (objectType)
        {
            //si l'objet est de type Stalactite
            case typeOfObject.Stalactite:
                //On désactive la contrainte de déplacement sur l'axe Y
                GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezePositionY;
                //On lance la coroutine qui va faire tomber la stalactite
                StartCoroutine(RecreatedGravityStalactite());
                //On détruit l'objet activator
                Destroy(activatorGO);
                break;
            
            //Si l'objet est de type porte / plateforme (ouvert / fermé)
            case typeOfObject.Door:
                //Si l'objet est fermé
                if (boolDoorClose)
                {
                    //disparition ou ouverture de l'objet
                    boolDoorClose = false;
                    GetComponent<Animator>().Play("Ouverture");
                }
                //Si l'objet est ouvert
                else
                {
                    //réapparation ou fermeture de l'objet
                    boolDoorClose = true;
                    GetComponent<Animator>().Play("Fermeture");
                }
                break;
        }       
    }

    //Coroutine pour faire tomber la stalactite
    IEnumerator RecreatedGravityStalactite()
    {
        //On ajoute une force sur l'axe Y
        GetComponent<ConstantForce>().force = new Vector3(0, -100000, 0);
        //On attend 5 secondes
        yield return new WaitForSeconds(5f);
        //On enlève la force
        GetComponent<ConstantForce>().force = new Vector3(0, 0, 0);
    }
}
