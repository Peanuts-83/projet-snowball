﻿using UnityEngine;
using System.Collections;

/*
 * Ce script permet de faire voler les objets en BigBall
 * Il doit être placé sur le parent de chaque objet en question
 */

public class GravityObstacles : MonoBehaviour
{
    [Tooltip("Est ce que cet objet est un arbre")]
    public bool isATree;

    [Tooltip("Est ce que cet objet est une stalactite")]
    public bool isAStalactite;

    [Tooltip("Est ce que l'objet vole")]
    public bool isFlying;

    private float timeToDie = 2;
    private float timeToShrink = 0.7f;

    private Vector3 initialLocalScale;
    float x;
    float y;
    float z;

    private GameManager gm;

    //Le Rumbler
    private Rumbler rumble;

    private void Awake()
    {
        initialLocalScale = transform.localScale;

        rumble = FindObjectOfType<Rumbler>();

        gm = FindObjectOfType<GameManager>();
    }

    //COLLISION ENTER (POUR LES AUTRES JOUEURS)
    public void OnCollisionEnter(Collision other)
    {
        //Si l'objet ne vole pas encore
        if (!isFlying)
        {
            //Que la collision s'effectue avec un joueur
            if (other.gameObject.tag == "Player")
            {
                PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();

                //Si le joueur est en bigball
                if (player.isBigBall)
                {
                    BigBallFlyingObject(player);
                }
            }
        }
    }

    //TRIGGER ENTER (POUR NOTRE JOUEUR)
    public void OnTriggerEnter(Collider other)
    {
        //Si l'objet ne vole pas encore
        if (!isFlying)
        {
            //Que la collision s'effectue avec un joueur
            if (other.gameObject.tag == "Player")
            {
                PlayerController player = other.GetComponentInParent<PlayerController>();

                //Si le joueur est en bigball
                if (player.isBigBall)
                {
                    BigBallFlyingObject(player);
                }
            }
        }       
    }

    private void BigBallFlyingObject(PlayerController playerInBigBall)
    {
        //désormais il vole (doOnce)
        isFlying = true;

        if(playerInBigBall == gm.myPlController)
        {
            rumble.RumbleConstant(.1f, .1f, .2f);
        }

        //Récupération des colliders
        Collider[] colList = GetComponentsInChildren<Collider>();

        //Suppression des colliders
        foreach (Collider col in colList)
        {
            if (!col.isTrigger)
            {
                Destroy(col);
            }
        }

        //destruction de l'objet
        Destroy(gameObject, timeToDie);

        //Mise en kinematic de l'objet
        GetComponentInParent<Rigidbody>().isKinematic = false;
        //Addforce pour faire voler l'objet
        //GetComponent<Rigidbody>().AddForce(Vector3.up * 100, ForceMode.Impulse);
        GetComponent<Rigidbody>().AddForce((playerInBigBall.GetComponent<Rigidbody>().velocity.normalized + Vector3.up).normalized * 100, ForceMode.Impulse);

        float[] numbers = new float[4];
        numbers[0] = -1;
        numbers[1] = -0.5f;
        numbers[2] = 0.5f;
        numbers[3] = 1f;

        GetComponent<Rigidbody>().AddTorque(new Vector3(numbers[Random.Range(0, numbers.Length)], numbers[Random.Range(0, numbers.Length)], numbers[Random.Range(0, numbers.Length)]).normalized*100f, ForceMode.Impulse);
        StartCoroutine(Gravity());
        StartCoroutine(Shrink(true));

        //Si c'est mon joueur
        if (playerInBigBall.GetComponentInParent<PlayerController>().pv.IsMine)
        {
            //Récupération de l'audioManager
            AudioManager am = FindObjectOfType<AudioManager>();

            am.PlaySoundEffect(am.soundEffectConfiguration.trees.fmodEventTreeDestruct);
            am.PlaySoundEffect(am.soundEffectConfiguration.trees.fmodEventBushSound, this.gameObject);

            Camera.main.GetComponent<CameraController>().Shake(playerInBigBall.GetComponentInParent<PlayerController>().shakeTime_BigBall_Tree, playerInBigBall.GetComponentInParent<PlayerController>().shakePower_BigBall_Tree);
        }
    }

    private IEnumerator Gravity()
    {
        yield return new WaitForFixedUpdate();

        GetComponent<Rigidbody>().AddForce(-Vector3.up * 65f, ForceMode.Acceleration);

        StartCoroutine(Gravity());
    }

    private IEnumerator Shrink(bool wait)
    {
        if (wait)
        {
            yield return new WaitForSeconds(timeToDie - timeToShrink);

            StartCoroutine(Shrink(false));
        }
        else
        {
            yield return new WaitForEndOfFrame();

            x = transform.localScale.x;
            y = transform.localScale.y;
            z = transform.localScale.z;

            x -= Time.deltaTime / timeToShrink * initialLocalScale.x;
            y -= Time.deltaTime / timeToShrink * initialLocalScale.y;
            z -= Time.deltaTime / timeToShrink * initialLocalScale.z;

            x = Mathf.Clamp(x, 0, initialLocalScale.x);
            y = Mathf.Clamp(y, 0, initialLocalScale.y);
            z = Mathf.Clamp(z, 0, initialLocalScale.z);

            transform.localScale = new Vector3(x, y, z);
            
            StartCoroutine(Shrink(false));
        }
    }
}
