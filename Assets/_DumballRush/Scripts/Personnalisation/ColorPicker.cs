﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;

/*
 * Ce Script permet de choisir sa couleur de boule de neige en cliquant sur une image 
 * Il doit se trouver sur l'objet qui a le sprite de couleurs
 */

//Fonction Event à gérer dans l'éditeur
[Serializable]
public class ColorEvent : UnityEvent<Color>
{

}

public class ColorPicker : MonoBehaviour
{
    //Le RectTransform de la texture
    RectTransform Rect;
    //Texture pour choisir sa couleur
    Texture2D ColorTexture;

    [Tooltip("Le joueur")]
    public GameObject player;
    [Tooltip("Texte de debug")]
    public TextMeshProUGUI DebugText;

    //Event OnColorPreview qui va gérer le choix de la couleur après un clic dans le colorTexture
    public ColorEvent OnColorPreview;

    //l'InputManager
    InputManager im;

    void Start()
    {
        //On récupère le Rect Transform
        Rect = GetComponent<RectTransform>();
        //Recupération de la texture
        ColorTexture = GetComponent<Image>().mainTexture as Texture2D;
        //On récupère l'input Manager
        im = FindObjectOfType<InputManager>();

        //Si le joueur a une couleur dans ses player pref
        if (PlayerPrefs.HasKey("ColorR"))
        {
            //on applique la couleur à la boule de neige
            player.GetComponent<Renderer>().material.SetColor("Color_E797161B", new Color(PlayerPrefs.GetFloat("ColorR"), PlayerPrefs.GetFloat("ColorG"), PlayerPrefs.GetFloat("ColorB")));
        }  
    }

    void Update()
    {
        //si la souris est sur la texture de couleur
        if (RectTransformUtility.RectangleContainsScreenPoint(Rect, im.inputMousePosition))
        {   
            //Calcule de position de la souris sur la texture
            Vector2 delta;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(Rect, im.inputMousePosition, null, out delta);

            string debug = "mousePosition =" + im.inputMousePosition;
            debug += "<br>delta" + delta;

            float width = Rect.rect.width;
            float height = Rect.rect.height;
            delta += new Vector2(width * 0.5f, height * 0.5f);

            float x = Mathf.Clamp( delta.x / width, 0f ,1f);
            float y = Mathf.Clamp( delta.y / height, 0f, 1f);

            int texX = Mathf.RoundToInt(x * ColorTexture.width);
            int texY = Mathf.RoundToInt(y * ColorTexture.height);

            
            Color color = ColorTexture.GetPixel(texX,texY);

            //Debug.Log(color);

            OnColorPreview?.Invoke(color);

            //Lors d'un clic
            if (im.inputMouseClic)
            {
                player.GetComponent<Renderer>().material.SetColor("Color_E797161B", color);

                PlayerPrefs.SetFloat("ColorR", color.r);
                PlayerPrefs.SetFloat("ColorG", color.g);
                PlayerPrefs.SetFloat("ColorB", color.b);
                PlayerPrefs.SetFloat("ColorA", color.a);
            }
        }
    }
}
