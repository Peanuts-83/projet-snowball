﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using UnityEngine.EventSystems;

public class CustomisationInventoryManager : MonoBehaviour
{
    #region Struct and Enum
    public enum TypeCustomisation
    {
        Eyes,
        Mouths,
        Colors,
        Motifs,
        Hats,
        Voices
    }

    [System.Serializable]
    public struct CustomColor
    {
        [Tooltip("Laisser vide si couleur simple")]
        public Texture texture;
        public Color color;

        public bool unlock;
    }
    [System.Serializable]
    public struct Custom2DSprite
    {
        public Sprite sprite;

        public bool unlock;
    }
    [System.Serializable]
    public struct Custom2DTexture
    {
        public Texture texture;

        public bool unlock;
    }
    [System.Serializable]
    public struct CustomVoices
    {
        public string leanLocalized_name;
        public string leanLocalized_description;

        [Tooltip("3 colors specific")]
        public Color[] color;

        public bool unlock;
    }
    [System.Serializable]
    public struct Custom3D
    {
        public Sprite objImage;
        public Mesh obj;

        public bool unlock;
    }

    [System.Serializable]
    public struct BasicInformations
    {
        public Vector2 spacingXY;
        public Vector2 columnAndRow;

        public GameObject buttonModel;
        public Transform pageContainer;
        public Transform pageGabarit;

        [HideInInspector]
        public List<GameObject> allPages;

        public Button nextButton;
        public Button previousButton;
        public Button validateButton;

        [HideInInspector]
        public GameObject tempSelectedSprite;
    }

    [System.Serializable]
    public struct Eyes_Customisation
    {
        public BasicInformations informations;

        public Custom2DSprite[] eyes;
    }

    [System.Serializable]
    public struct Mouths_Customisation
    {
        public BasicInformations informations;

        public Custom2DSprite[] mouths;
    }

    [System.Serializable]
    public struct Colors_Customisation
    {
        public BasicInformations informations;

        public CustomColor[] colors;
    }

    [System.Serializable]
    public struct Motifs_Customisation
    {
        public BasicInformations informations;

        public Custom2DTexture[] motifs;
    }

    [System.Serializable]
    public struct Hats_Customisation
    {
        public BasicInformations informations;

        public Custom3D[] hats;
    }

    [System.Serializable]
    public struct Voices_Customisation
    {
        public BasicInformations informations;

        public CustomVoices[] voices;
    }
    #endregion

    #region Customisation
    [Header("Customisation Inventory")]
    [SerializeField]
    private Eyes_Customisation eyes;
    [SerializeField]
    private Mouths_Customisation mouths;
    [SerializeField]
    private Colors_Customisation colors;
    [SerializeField]
    private Motifs_Customisation motifs;
    [SerializeField]
    private Hats_Customisation hats;
    [SerializeField]
    private Voices_Customisation voices;

    #endregion

    #region Navigation
    [Header("Navigation")]
    [SerializeField]
    private GameObject[] panels;

    [Header("Functional")]
    [SerializeField]
    private TMP_InputField playerNameInputField;

    [SerializeField]
    private GameObject armoireMesh;

    [SerializeField]
    private GameObject meshCustomisation;

    private int actualSelectedPage = 0;

    private TypeCustomisation selectedCathegory = TypeCustomisation.Eyes;

    private ButtonManager bm;

    #endregion

    private void Start()
    {
        GenerateInventory();
        InitializeArraysInCustomisationController();
    }

    public void GenerateInventory()
    {
        GenerateCategory(TypeCustomisation.Eyes);
        GenerateCategory(TypeCustomisation.Mouths);
        GenerateCategory(TypeCustomisation.Colors);
        GenerateCategory(TypeCustomisation.Voices);
    }

    private void GenerateCategory(TypeCustomisation type)
    {
        BasicInformations infos = new BasicInformations();
        int maxElements = 0;
        int spawnedElements = 0;
        Sprite[] buttonsSprites = new Sprite[0];
        Color[] buttonsColors1 = new Color[0];
        Color[] buttonsColors2 = new Color[0];
        Color[] buttonsColors3 = new Color[0];
        string[] buttonsNames = new string[0];
        string[] buttonsDescriptions = new string[0];

        List<GameObject> pages = new List<GameObject>();

        Debug.Log("Generate " + type);

        switch (type)
        {
            case TypeCustomisation.Eyes:

                infos = eyes.informations;

                maxElements = eyes.eyes.Length;

                buttonsSprites = new Sprite[maxElements];
                for (int i = 0; i < maxElements; i ++)
                {
                    buttonsSprites[i] = eyes.eyes[i].sprite;
                }

                //Clear list
                eyes.informations.allPages.Clear();

                break;

            case TypeCustomisation.Mouths:

                infos = mouths.informations;

                maxElements = mouths.mouths.Length;

                buttonsSprites = new Sprite[maxElements];
                for (int i = 0; i < maxElements; i++)
                {
                    buttonsSprites[i] = mouths.mouths[i].sprite;
                }

                mouths.informations.allPages.Clear();

                break;

            case TypeCustomisation.Colors:

                infos = colors.informations;

                maxElements = colors.colors.Length;

                buttonsColors1 = new Color[maxElements];
                for (int i = 0; i < maxElements; i++)
                {
                    buttonsColors1[i] = colors.colors[i].color;
                }

                colors.informations.allPages.Clear();

                break;

            case TypeCustomisation.Voices:

                infos = voices.informations;

                maxElements = voices.voices.Length;

                buttonsNames = new string[maxElements];
                buttonsDescriptions = new string[maxElements];

                buttonsColors1 = new Color[maxElements];
                buttonsColors2 = new Color[maxElements];
                buttonsColors3 = new Color[maxElements];

                for (int i = 0; i < maxElements; i++)
                {
                    buttonsNames[i] = voices.voices[i].leanLocalized_name;
                    buttonsDescriptions[i] = voices.voices[i].leanLocalized_description;

                    buttonsColors1[i] = voices.voices[i].color[0];
                    buttonsColors2[i] = voices.voices[i].color[1];
                    buttonsColors3[i] = voices.voices[i].color[2];
                }

                voices.informations.allPages.Clear();

                break;

        }

        #region Reset
        //Destroy pages
        for (int i = infos.pageContainer.childCount; i > 1; i--)
        {
            DestroyImmediate(infos.pageContainer.GetChild(i-1).gameObject);
        }

        #endregion

        while (spawnedElements < maxElements)
        {
            GameObject actualPage = Instantiate(infos.pageGabarit.gameObject,infos.pageContainer);
            actualPage.name = "Page_" + pages.Count;
            pages.Add(actualPage);

            switch (type)
            {
                case TypeCustomisation.Eyes:

                    eyes.informations.allPages.Add(actualPage);

                    break;

                case TypeCustomisation.Mouths:

                    mouths.informations.allPages.Add(actualPage);

                    break;

                case TypeCustomisation.Colors:

                    colors.informations.allPages.Add(actualPage);

                    break;

                case TypeCustomisation.Voices:

                    voices.informations.allPages.Add(actualPage);

                    break;
            }


            Button[,] coordButtons = new Button[(int)infos.columnAndRow.x+1, (int)infos.columnAndRow.y+1];

            for (int y = 0; y < infos.columnAndRow.y; y++)
            {
                for (int x = 0; x < infos.columnAndRow.x; x++)
                {
                    if (spawnedElements < maxElements)
                    {
                        spawnedElements++;

                        GameObject button = Instantiate(infos.buttonModel, actualPage.transform);

                        button.name = "ID_" + (spawnedElements - 1);

                        button.transform.localPosition = new Vector3(x * infos.spacingXY.x, -y * infos.spacingXY.y, 0);

                        button.GetComponent<ButtonCustomMenu>().ID = spawnedElements - 1;

                        switch (type)
                        {
                            case TypeCustomisation.Eyes:

                                button.transform.GetChild(2).GetComponent<Image>().sprite = buttonsSprites[spawnedElements - 1];

                                button.GetComponent<Button>().onClick.AddListener(delegate { Mesh_ChangeEyes(button.GetComponent<ButtonCustomMenu>().ID, button.GetComponent<ButtonCustomMenu>().onSelectedSprite); });

                                break;

                            case TypeCustomisation.Mouths:

                                button.transform.GetChild(2).GetComponent<Image>().sprite = buttonsSprites[spawnedElements - 1];

                                button.GetComponent<Button>().onClick.AddListener(delegate { Mesh_ChangeMouths(button.GetComponent<ButtonCustomMenu>().ID, button.GetComponent<ButtonCustomMenu>().onSelectedSprite); });

                                break;

                            case TypeCustomisation.Colors:

                                button.GetComponent<Image>().color = buttonsColors1[spawnedElements - 1];

                                button.GetComponent<Button>().onClick.AddListener(delegate { Mesh_ChangeColors(button.GetComponent<ButtonCustomMenu>().ID, button.GetComponent<ButtonCustomMenu>().onSelectedSprite); });

                                break;

                            case TypeCustomisation.Voices:

                                ButtonCustomMenu btnCM = button.GetComponent<ButtonCustomMenu>();

                                btnCM.txt_name.TranslationName = buttonsNames[spawnedElements - 1];
                                btnCM.txt_description.TranslationName = buttonsDescriptions[spawnedElements - 1];

                                btnCM.color1.color = buttonsColors1[spawnedElements - 1];
                                btnCM.color2.color = buttonsColors2[spawnedElements - 1];
                                btnCM.color3.color = buttonsColors3[spawnedElements - 1];

                                btnCM.txt_name.GetComponent<TMP_Text>().color = buttonsColors3[spawnedElements - 1];
                                btnCM.txt_description.GetComponent<TMP_Text>().color = buttonsColors2[spawnedElements - 1];

                                button.GetComponent<Button>().onClick.AddListener(delegate { Mesh_ChangeVoices(button.GetComponent<ButtonCustomMenu>().ID,button.GetComponent<ButtonCustomMenu>().onSelectedSprite); });

                                break;
                        }

                        coordButtons[x, y] = button.GetComponent<Button>();
                    }
                }
            }

            #region Navigation
            for (int y = 0; y < infos.columnAndRow.y; y++)
            {
                for (int x = 0; x < infos.columnAndRow.x; x++)
                {
                    Navigation newNav = new Navigation();
                    newNav.mode = Navigation.Mode.Explicit;

                    #region Setting coordinate
                    int coordYUp = y;
                    int coordXUp = x;
                    int coordYDown = y;
                    int coordXDown = x;

                    int coordXRight = x;
                    int coordXLeft = x;

                    bool isToPreviousOrNext = false;

                    #region selectOnUp

                    if (y-1 >= 0)
                    {
                        coordYUp = y-1;
                    }

                    #endregion

                    #region selectOnDown
                    if (coordButtons[x,y+1] == null)
                    {
                        Vector2 tempBtnCoord = Vector2.zero;

                        for (int i = 0; i < infos.columnAndRow.x; i++)
                        {
                            if (coordButtons[i,y+1] == null)
                            {
                                break;
                            }
                            else
                            {
                                tempBtnCoord = new Vector2(i, y + 1);
                            }
                        }

                        if (tempBtnCoord != Vector2.zero)
                        {
                            coordYDown = (int)tempBtnCoord.y;
                            coordXDown = (int)tempBtnCoord.x;
                        }
                        else
                        {
                            isToPreviousOrNext = true;
                        }
                    }
                    else
                    {
                        coordXDown = x;
                        coordYDown = y + 1;
                    }
                    #endregion

                    #region selectOnRight

                    if (coordButtons[coordXRight + 1,y] == null)
                    {
                        coordXRight = 0;
                    }
                    else
                    {
                        coordXRight = coordXRight + 1;
                    }

                    #endregion

                    #region selectOnLeft

                    if (coordXLeft-1 < 0)
                    {
                        if (coordButtons[(int)infos.columnAndRow.x,y] == null)
                        {
                            int tempX = 0;

                            for (int i = (int)infos.columnAndRow.x; i >= 0; i--)
                            {
                                if (coordButtons[i,y] != null)
                                {
                                    tempX = i;
                                    break;
                                }
                            }

                            coordXLeft = tempX;
                        }
                        else
                        {
                            coordXLeft = (int)infos.columnAndRow.x - 1;
                        }
                    }
                    else
                    {
                        coordXLeft = coordXLeft-1;
                    }

                    #endregion

                    #endregion

                    if (!isToPreviousOrNext)
                    {
                        newNav.selectOnDown = coordButtons[coordXDown, coordYDown];
                    }
                    else
                    {
                        if (x < infos.columnAndRow.x / 2)
                        {
                            newNav.selectOnDown = infos.previousButton;
                        }
                        else
                        {
                            newNav.selectOnDown = infos.nextButton;
                        }
                    }


                    if (y != coordYUp)
                    {
                        newNav.selectOnUp = coordButtons[x, coordYUp];
                    }

                    newNav.selectOnRight = coordButtons[coordXRight, y];

                    newNav.selectOnLeft = coordButtons[coordXLeft, y];

                    if (coordButtons[x,y] != null)
                        coordButtons[x, y].navigation = newNav;
                }
            }
            #endregion

        }

        foreach (GameObject page in pages)
        {
            page.SetActive(false);
        }
    }

    private void InitializeArraysInCustomisationController()
    {
        List<Sprite> spritesList = new List<Sprite>();

        foreach (Custom2DSprite eye in eyes.eyes)
        {
            spritesList.Add(eye.sprite);
        }

        meshCustomisation.GetComponent<CustomisationPlayer>().allEyes = spritesList.ToArray();
        spritesList.Clear();

        foreach (Custom2DSprite mouth in mouths.mouths)
        {
            spritesList.Add(mouth.sprite);
        }

        meshCustomisation.GetComponent<CustomisationPlayer>().allMouths = spritesList.ToArray();

        List<Color> colorList = new List<Color>();

        foreach (CustomColor color in colors.colors)
        {
            colorList.Add(color.color);
        }

        meshCustomisation.GetComponent<CustomisationPlayer>().allColors = colorList.ToArray();
    }

    #region Buttons functions

    public void ChangePage(bool next)
    {
        if (next)
        {
            actualSelectedPage += 1;
        }
        else
        {
            actualSelectedPage -= 1;
        }

        switch (selectedCathegory)
        {
            case TypeCustomisation.Eyes:

                SetPage(eyes.informations);

                SetNavigationForPreviousAndNextButton(eyes.informations);

                break;

            case TypeCustomisation.Mouths:

                SetPage(mouths.informations);

                SetNavigationForPreviousAndNextButton(mouths.informations);

                break;

            case TypeCustomisation.Colors:

                SetPage(colors.informations);

                SetNavigationForPreviousAndNextButton(colors.informations);

                break;

            case TypeCustomisation.Voices:

                SetPage(voices.informations);

                SetNavigationForPreviousAndNextButton(voices.informations);

                break;

            case TypeCustomisation.Hats:

                SetPage(hats.informations);

                SetNavigationForPreviousAndNextButton(hats.informations);

                break;

            case TypeCustomisation.Motifs:

                SetPage(motifs.informations);

                SetNavigationForPreviousAndNextButton(motifs.informations);

                break;
        }
    }

    private void SetNavigationForPreviousAndNextButton(BasicInformations infos)
    {
        Navigation buttonNav = new Navigation();
        buttonNav.mode = Navigation.Mode.Explicit;
        buttonNav.selectOnUp = infos.allPages[actualSelectedPage].transform.GetChild(0).GetComponent<Button>();
        buttonNav.selectOnDown = infos.nextButton.navigation.selectOnDown;
        buttonNav.selectOnRight = infos.nextButton.navigation.selectOnRight;
        buttonNav.selectOnLeft = infos.nextButton.navigation.selectOnLeft;

        infos.nextButton.navigation = buttonNav;

        buttonNav.selectOnDown = infos.previousButton.navigation.selectOnDown;
        buttonNav.selectOnRight = infos.previousButton.navigation.selectOnRight;
        buttonNav.selectOnLeft = infos.previousButton.navigation.selectOnLeft;

        infos.previousButton.navigation = buttonNav;

        buttonNav.selectOnDown = infos.validateButton.navigation.selectOnDown;
        buttonNav.selectOnRight = infos.validateButton.navigation.selectOnRight;
        buttonNav.selectOnLeft = infos.validateButton.navigation.selectOnLeft;

        infos.validateButton.navigation = buttonNav;
    }

    private void SetPage(BasicInformations infos)
    {
        if (actualSelectedPage < 0)
            actualSelectedPage = infos.allPages.Count - 1;
        else if (actualSelectedPage >= infos.allPages.Count)
            actualSelectedPage = 0;

        Debug.Log(actualSelectedPage);

        foreach (GameObject page in infos.allPages)
        {
            page.SetActive(false);
        }

        infos.allPages[actualSelectedPage].SetActive(true);
    }

    //Changement du panel de customisation à ouvrir
    public void ChangePanel(int panelID)
    {
        //On enleve l'interraction avec les boutons de choix de panel (pour éviter les doubles clics)
        foreach (Button ButtonChoosePanel in panels[0].GetComponentsInChildren<Button>())
        {
            ButtonChoosePanel.interactable = false;
        }

        if (panelID != 0)
        {
            //Les boutons de choix de panel disparaissent
            panels[0].GetComponent<Animator>().SetTrigger("OnDisappear");

            //Si le panel ouvert est celui du nickname
            if (panelID == 1)
            {
                //On change le texte de l'input field permettant de choisir son pseudo sur le dernier pseudo choisi
                playerNameInputField.text = PhotonNetwork.NickName;
            }
            
        }
        else
        {
            for (int i = 2; i < panels.Length; i++)
            {
                panels[i].GetComponent<Animator>().SetTrigger("OnDisappear");
            }
        }


        //Set cathegory
        switch (panelID)
        {
            case 2:

                selectedCathegory = TypeCustomisation.Eyes;

                break;

            case 3:

                selectedCathegory = TypeCustomisation.Mouths;

                break;

            case 4:

                selectedCathegory = TypeCustomisation.Colors;

                break;

            case 5:

                selectedCathegory = TypeCustomisation.Voices;

                break;
        }

        //Lancement de la coroutine de changement de panel
        StartCoroutine(CoroutineChangePanel(panelID));
    }

    //Coroutine de Changement de panel
    public IEnumerator CoroutineChangePanel(int panelID)
    {
        yield return new WaitForEndOfFrame();


        //On désactive tous les panels (pour etre sur)
        foreach (GameObject panel in panels)
        {
            panel.SetActive(false);
        }

        //Pour chaque bouton de choix de panel on les resize pour éviter tout bug d'anim
        foreach (Button panelButton in panels[0].GetComponentsInChildren<Button>())
        {
            panelButton.gameObject.transform.localScale = new Vector3(1, 1, 1);
        }

        //On active le panel choisi
        panels[panelID].SetActive(true);

        foreach (Button btn in panels[panelID].GetComponentsInChildren<Button>())
        {
            btn.interactable = true;
        }

        switch (panelID)
        {
            case 2:

                foreach (GameObject page in eyes.informations.allPages)
                {
                    page.SetActive(false);
                }
                eyes.informations.allPages[0].SetActive(true);

                SetNavigationForPreviousAndNextButton(eyes.informations);
                break;

            case 3:

                foreach (GameObject page in mouths.informations.allPages)
                {
                    page.SetActive(false);
                }
                mouths.informations.allPages[0].SetActive(true);

                SetNavigationForPreviousAndNextButton(mouths.informations);

                break;

            case 4:

                foreach (GameObject page in colors.informations.allPages)
                {
                    page.SetActive(false);
                }
                colors.informations.allPages[0].SetActive(true);

                SetNavigationForPreviousAndNextButton(colors.informations);

                break;

            case 5:

                foreach (GameObject page in voices.informations.allPages)
                {
                    page.SetActive(false);
                }
                voices.informations.allPages[0].SetActive(true);

                SetNavigationForPreviousAndNextButton(voices.informations);

                break;
        }

        if (panelID != 0)
            panels[panelID].GetComponent<Animator>().SetTrigger("OnAppear");

        if (panelID != 1)
        {
            //On sélectionne le 1er bouton du panel ouvert
            EventSystem.current.SetSelectedGameObject(panels[panelID].transform.GetChild(0).gameObject);
        }
    }

    public void SaveAndClosePanel()
    {
        //On désactive le panel ouvert
        foreach (GameObject panel in panels)
        {
            if (panel.activeSelf)
            {
                panel.GetComponent<Animator>().SetTrigger("OnDisappear");
            }
        }

        actualSelectedPage = 0;

        ChangePanel(0);
    }

    private void ActivateSelectedSpriteButton(GameObject sprite,TypeCustomisation type)
    {
        sprite.SetActive(true);

        switch (type)
        {
            case TypeCustomisation.Eyes:

                if (eyes.informations.tempSelectedSprite != null)
                    eyes.informations.tempSelectedSprite.SetActive(false);

                eyes.informations.tempSelectedSprite = sprite;

                break;

            case TypeCustomisation.Mouths:

                if (mouths.informations.tempSelectedSprite != null)
                    mouths.informations.tempSelectedSprite.SetActive(false);

                mouths.informations.tempSelectedSprite = sprite;

                break;

            case TypeCustomisation.Colors:

                if (colors.informations.tempSelectedSprite != null)
                    colors.informations.tempSelectedSprite.SetActive(false);

                colors.informations.tempSelectedSprite = sprite;

                break;

            case TypeCustomisation.Voices:

                if (voices.informations.tempSelectedSprite != null)
                    voices.informations.tempSelectedSprite.SetActive(false);

                voices.informations.tempSelectedSprite = sprite;

                break;
        }
    }

    private void Mesh_ChangeEyes(int ID, GameObject selectedSprite)
    {
        Debug.Log("Change eye " + ID);
        meshCustomisation.GetComponent<CustomisationPlayer>().ChangeEyes(ID);

        ActivateSelectedSpriteButton(selectedSprite,TypeCustomisation.Eyes);
    }

    private void Mesh_ChangeMouths(int ID, GameObject selectedSprite)
    {
        Debug.Log("Change mouth " + ID);
        meshCustomisation.GetComponent<CustomisationPlayer>().ChangeMouth(ID);

        ActivateSelectedSpriteButton(selectedSprite,TypeCustomisation.Mouths);
    }

    private void Mesh_ChangeColors(int ID, GameObject selectedSprite)
    {
        Debug.Log("Change color " + ID);
        meshCustomisation.GetComponent<CustomisationPlayer>().ChangeColor(ID);

        ActivateSelectedSpriteButton(selectedSprite,TypeCustomisation.Colors);
    }

    private void Mesh_ChangeVoices(int ID, GameObject selectedSprite)
    {
        Debug.Log("Change voices " + ID);
        meshCustomisation.GetComponent<CustomisationPlayer>().ChangeVoice(ID);

        ActivateSelectedSpriteButton(selectedSprite,TypeCustomisation.Voices);
    }

    private bool SearchButtonManager()
    {
        if (bm == null)
        {
            bm = FindObjectOfType<ButtonManager>();
        }

        return bm != null;
    }

    public void OpenArmoire(bool open)
    {
        if (SearchButtonManager())
        {
            bm.OpenArmoireSound(open);
        }

        armoireMesh.GetComponent<Animator>().SetBool("DoorIsOpen", open);
    }

    #endregion
}
