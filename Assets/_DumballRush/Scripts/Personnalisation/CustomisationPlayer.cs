﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class CustomisationPlayer : MonoBehaviour
{
    [HideInInspector]
    public Sprite[] allEyes;
    [HideInInspector]
    public Sprite[] allMouths;
    [HideInInspector]
    public Color[] allColors;

    [SerializeField]
    private Button[] allEyesButtons;
    [SerializeField]
    private Button[] allMouthsButtons;
    [SerializeField]
    private Button[] allColorsButtons;
    [SerializeField]
    private Button[] allVoicesButtons;

    public Button[] allChosePanelButtons;

    public GameObject allChosePanelButtonsParent;

    public Button SaveButton;

    public Button btn_keyboard_a;

    [SerializeField]
    private SpriteRenderer eye;
    private int eyeId = 0;

    [SerializeField]
    private SpriteRenderer mouth;
    private int mouthId = 0;

    private Color colorBall;

    public GameObject[] DifferentPanels;

    public EventSystem evt;

    GameObject sel;

    [SerializeField]
    private TMP_InputField playerNameInput;
    [SerializeField]
    private Text playerNickname;

    public GameObject armoire;

    private AudioManager am;

    void Start()
    {
        //On récupère l'event System
        evt = EventSystem.current;

        Color newCol = Random.ColorHSV(0f,1f,1f,1f,1f,1f);

        #region Prévention de customisation vide
        TestAndAssignPlayerPref("ColorR", newCol.r);
        TestAndAssignPlayerPref("ColorG", newCol.g);
        TestAndAssignPlayerPref("ColorB", newCol.b);
        TestAndAssignPlayerPref("ColorA", 1);
        TestAndAssignPlayerPref("eye", Random.Range(0,allEyes.Length), 1);
        TestAndAssignPlayerPref("mouth", Random.Range(0,allMouths.Length), 1);
        TestAndAssignPlayerPref("voice", Random.Range(0,6), 1);
        #endregion

        

        //Vérification des players prefs pour afficher la bonne customisation en entrant dans le menu
        //Yeux
        eye.sprite = allEyes[PlayerPrefs.GetInt("eye")];
        //Bouche
        mouth.sprite = allMouths[PlayerPrefs.GetInt("mouth")];
        //Couleur
        GetComponent<Renderer>().material.SetColor("Color_E797161B", new Color(PlayerPrefs.GetFloat("ColorR"),
                                                                                PlayerPrefs.GetFloat("ColorG"),
                                                                                PlayerPrefs.GetFloat("ColorB"),
                                                                                PlayerPrefs.GetFloat("ColorA")));
    }

    private void TestAndAssignPlayerPref(string pref, float value, int type = 0)
    {
        if (!PlayerPrefs.HasKey(pref))
        {
            switch (type)
            {
                case 0:

                    PlayerPrefs.SetFloat(pref, value);

                    break;

                case 1:

                    PlayerPrefs.SetInt(pref, (int)value);

                    break;
            }

            Debug.Log($"{pref} set to {value}");
        }
    }

    //Update du pseudo
    public void PlayerNameChange(/*string nameInput*/ Text nameInput)
    {
        Debug.Log(nameInput.text);
        if(nameInput.text.Length > 0)
        {
            //Change le pseudo photon
            PhotonNetwork.NickName = nameInput.text;
            //Sauvegarde le pseudo du joueur
            PlayerPrefs.SetString("Nickname", nameInput.text);
        }
    }

    //Changement du panel de customisation à ouvrir
    public void ChangePanel(int panelToEnable)
    {
        //On enleve l'interraction avec les boutons de choix de panel (pour éviter les doubles clics)
        foreach (Button ButtonChosePanel in allChosePanelButtons)
        {
            ButtonChosePanel.interactable = false;
        }

        //Les boutons de choix de panel disparaissent
        allChosePanelButtonsParent.GetComponent<Animator>().SetTrigger("OnDisappear");

        //Si le panel ouvert est celui du nickname
        if (panelToEnable == 4)
        {
            //On change le texte de l'input field permettant de choisir son pseudo sur le dernier pseudo choisi
            playerNameInput.text = PhotonNetwork.NickName;
            //playerNickname.text = PhotonNetwork.NickName;
        }

        //Lancement de la coroutine de changement de panel
        StartCoroutine(CoroutineChangePanel(panelToEnable));
    }

    //Coroutine de Changement de panel
    public IEnumerator CoroutineChangePanel(int panelToEnable)
    {
        //Booléen de l'animator de l'armoire, on réinitialise sur false (on n'est pas dans le panel nickname)
        armoire.GetComponent<Animator>().SetBool("IsNicknameMenu", false);

        //On attend un peu
        yield return new WaitForSeconds(.5f);

        //Si le panel ouvert n'est PAS celui du nickname
        if (panelToEnable != 4)
        {
            //trigger de l'animation "ouverture de porte" de l'armoire
            armoire.GetComponent<Animator>().SetTrigger("OpenDoor");
            //On active le bouton de validation
            SaveButton.gameObject.SetActive(true);
        }
        //si le panel est celui du nickname
        else
        {
            //On set le booléen (on est dans le menu de pseudo) à true
            armoire.GetComponent<Animator>().SetBool("IsNicknameMenu", true);
        }

        //On désactive tous les panels (pour etre sur)
        foreach (GameObject panel in DifferentPanels)
        {
            panel.SetActive(false);
        }

        //Pour chaque bouton de choix de panel on les resize pour éviter tout bug d'anim
        foreach (Button panelButton in allChosePanelButtons)
        {
            panelButton.gameObject.transform.localScale = new Vector3(1, 1, 1);
        }

        //On active le panel choisi
        DifferentPanels[panelToEnable].SetActive(true);
        DifferentPanels[panelToEnable].GetComponent<Animator>().SetTrigger("OnAppear");

        if(panelToEnable != 4)
        {
            //On sélectionne le 1er bouton du panel ouvert
            evt.SetSelectedGameObject(DifferentPanels[panelToEnable].transform.GetChild(0).gameObject);
        }         
    }

    public void FirstConnection()
    {
        //Booléen de l'animator de l'armoire, on réinitialise sur false (on n'est pas dans le panel nickname)
        armoire.GetComponent<Animator>().SetBool("IsNicknameMenu", true);
    }

    //Validation du choix de customisation
    public void Save()
    {
        //On désactive le panel ouvert
        foreach (GameObject panel in DifferentPanels)
        {
            if (panel.activeSelf)
            {
                panel.GetComponent<Animator>().SetTrigger("OnDisappear");
            }
        }

        //On désactive le bouton save
        SaveButton.gameObject.SetActive(false);
    }

    //CUSTOMISATION - Changement des yeux
    public void ChangeEyes(int ButtonId)
    {
        //On garde la valeur de l'id de customisation choisie
        eyeId = ButtonId;

        //Lancement de la fonction d'Update du visuel (pour les yeux)
        UpdateEyes(eyeId);

        //On change les player Prefs
        PlayerPrefs.SetInt("eye", eyeId);
    }

    //Update du visuel de la boule dans le menu
    public void UpdateEyes(int newEye)
    {
        eye.sprite = allEyes[newEye];
    }

    //CUSTOMISATION - Changement de la bouche
    public void ChangeMouth(int ButtonId)
    {
        //On garde la valeur de l'id de customisation choisie
        mouthId = ButtonId;

        //Lancement de la fonction d'Update du visuel (pour la bouche)
        UpdateMouth(mouthId);

        //On change les player Prefs
        PlayerPrefs.SetInt("mouth", mouthId);
    }

    //Update du visuel de la boule dans le menu
    public void UpdateMouth(int newMouth)
    {
        mouth.sprite = allMouths[newMouth];
    }

    public void ChangeColor(int ID)
    {
        Color newCol = allColors[ID];

        //On change le visuel de la boule du menu
        GetComponent<Renderer>().material.SetColor("Color_E797161B", newCol);

        //On change les player prefs
        PlayerPrefs.SetFloat("ColorR", newCol.r);
        PlayerPrefs.SetFloat("ColorG", newCol.g);
        PlayerPrefs.SetFloat("ColorB", newCol.b);
        PlayerPrefs.SetFloat("ColorA", newCol.a);
    }

    //CUSTOMISATION - Changement de la couleur
    public void ChangeColor(GameObject buttonClicked)
    {
        //On récupère la couleur du bouton de customisation choisie
        colorBall = buttonClicked.GetComponent<Image>().color;

        //On change le visuel de la boule du menu
        GetComponent<Renderer>().material.SetColor("Color_E797161B", colorBall);

        //On change les player prefs
        PlayerPrefs.SetFloat("ColorR", colorBall.r);
        PlayerPrefs.SetFloat("ColorG", colorBall.g);
        PlayerPrefs.SetFloat("ColorB", colorBall.b);
        PlayerPrefs.SetFloat("ColorA", colorBall.a);
    }

    public void ChangeVoice(int newVoice)
    {
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }

        am.soundEffectConfiguration.voix.voiceID = newVoice;

        PlayerPrefs.SetInt("voice", newVoice);

        am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, transform.parent.gameObject, "ChoixVoix", newVoice);
    }
}
