﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CustomisationInventoryManager))]
public class CustomisationInventoryManager_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        CustomisationInventoryManager script = (CustomisationInventoryManager)target;

        if (GUILayout.Button("Generate inventory"))
        {
            script.GenerateInventory();
        }

        DrawDefaultInspector();
    }
}
