﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class VisageController : MonoBehaviour
{
    [SerializeField]
    private Sprite[] allEyes;
    [SerializeField]
    private Sprite[] allMouths;

    [SerializeField]
    private SpriteRenderer eye;
    private int eyeId = 0;

    [SerializeField]
    private SpriteRenderer mouth;
    private int mouthId = 0;

    [SerializeField]
    private bool isInMenu = true;

    private void Start()
    {
        if (isInMenu && PlayerPrefs.HasKey("eye") && PlayerPrefs.HasKey("mouth"))
        {
            eye.sprite = allEyes[PlayerPrefs.GetInt("eye", eyeId)];
            mouth.sprite = allMouths[PlayerPrefs.GetInt("mouth", eyeId)];
        }
    }


    public void ChangeEyes(int dir)
    {
        if (eyeId + dir >= allEyes.Length)
        {
            eyeId = 0;
        }
        else if (eyeId + dir < 0)
        {
            eyeId = allEyes.Length -1 ;
        }
        else
        {
            eyeId += dir;
        }

        UpdateEye(eyeId);

        PlayerPrefs.SetInt("eye", eyeId);
    }

    public void ChangeMouth(int dir)
    {
        if (mouthId + dir >= allMouths.Length)
        {
            mouthId = 0;


        }
        else if (mouthId + dir < 0)
        {

            mouthId = allMouths.Length - 1;
        }
        else
        {
            mouthId += dir;
        }

        UpdateMouth(mouthId);

        PlayerPrefs.SetInt("mouth", mouthId);
    }

    public void UpdateEye(int newEye)
    {
        eye.sprite = allEyes[newEye];
    }

    public void UpdateMouth(int newMouth)
    {
        mouth.sprite = allMouths[newMouth];
    }
}
