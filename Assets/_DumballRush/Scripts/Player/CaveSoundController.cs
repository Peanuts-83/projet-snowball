﻿using UnityEngine;
using Photon.Pun;

/*
 * Ce script permet de changer le son du jeu quand on est dans une caverne
 * 
 * Ce script doit être placé sur le joueur
 */

public class CaveSoundController : MonoBehaviourPunCallbacks
{
    [Tooltip("Numéro du layer des Caves")]
    [SerializeField]
    private int layerOfCave;

    [Tooltip("Valeur rajoutée au parametres de son")]
    [SerializeField]
    private float addingAmount;

    [Tooltip("Est ce que le joueur est dans une Cave")]
    public bool inCave;
    [Tooltip("Parametre de son de Cave, quand on est dans une cave, il augmente, sinon il diminue")]
    public float CaveSoundParameter;

    //Le PhotonView
    private PhotonView pv;

    private void Start()
    {
        //On récupère le PhotonView
        pv = GetComponent<PhotonView>();
    }

    //Quand le joueur reste en trigger avec un objet
    private void OnTriggerStay(Collider other)
    {
        //Si ce n'est pas moi j'ignore
        if (!pv.IsMine)
        {
            return;
        }

        //Si l'objet trigger a comme layer de layer des caves
        if (other.gameObject.layer == layerOfCave)
        {
            //Alors le joueur est dans une Cave
            inCave = true;
        }
    }

    //Quand le joueur quitte un trigger
    private void OnTriggerExit(Collider other)
    {
        //Si ce n'est pas moi j'ignore
        if (!pv.IsMine)
        {
            return;
        }

        //Si l'objet que le joueur avait en trigger a comme layer de layer des caves
        if (other.gameObject.layer == layerOfCave)
        {
            //Alors il n'est plus dans une Cave
            inCave = false;
        }
    }

    private void Update()
    {
        if (!pv.IsMine)
        {
            return;
        }

        //Si le joueur est dans une Cave
        if (inCave)
        {
            //Le parametre augmente
            CaveSoundParameter += addingAmount*Time.deltaTime;
        }
        else
        {
            //Sinon il diminue
            CaveSoundParameter -= addingAmount*Time.deltaTime;
        }

        //Clamp de cette valeur entre 0 et 1
        CaveSoundParameter = Mathf.Clamp01(CaveSoundParameter);

        //On envoie la valeur calculée du parametre dans l'audio Manager
        FindObjectOfType<AudioManager>().musicConfiguration.parametersMusicRace.cave.value = CaveSoundParameter;
    } 
}
