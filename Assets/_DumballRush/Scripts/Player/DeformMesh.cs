﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeformMesh : MonoBehaviour
{
    private Rigidbody rb;
    private PlayerController pc;

    public GameObject parentPlayerMeshUp;
    public GameObject parentPlayerMeshForward;
    public GameObject playerMesh;

    public float speedBackToNormal;

    public float playerSpeedClamp = 0;

    public AnimationCurve jump;
    public float animSpeedJump;
    private float animJumpT;

    public AnimationCurve landing;
    public float animSpeedLanding;
    private float animLandingT;

    public bool stopCheckParentUp;

    private Vector3 jumpScaleCalculate;
    private Vector3 landingScaleCalculate;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pc = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
       CheckParentUp();

        //déformation de vitesse (se déforme quand je suis au sol)
        if (pc.IsGrounded())
        {
            playerSpeedClamp += speedBackToNormal * 3f * Time.deltaTime;
            playerSpeedClamp = Mathf.Clamp01(playerSpeedClamp);
            float speedUnlerp = Mathf.InverseLerp(0, 40, rb.velocity.magnitude);
            playerSpeedClamp = playerSpeedClamp * speedUnlerp;
        }
        //déformation de vitesse (se reforme quand je ne suis plus au sol)
        else
        {
            playerSpeedClamp -= speedBackToNormal * 5f * Time.deltaTime;
            playerSpeedClamp = Mathf.Clamp01(playerSpeedClamp);
        }    

        //déformation de vitesse (modifications du mesh après les calculs)
        if(rb.velocity.magnitude >= 0)
        {
            parentPlayerMeshForward.transform.localScale = new Vector3(1 - playerSpeedClamp / 10, parentPlayerMeshForward.transform.localScale.y, 1 + playerSpeedClamp / 3);
        }
    }

    //rotation du mesh de la Dumball
    public void FixedUpdate()
    {
        playerMesh.transform.rotation = transform.rotation;
    }

    //Vérification du Up du parent, replacement du up
    public void CheckParentUp()
    {
        if (parentPlayerMeshUp.transform.up != Vector3.up && stopCheckParentUp == false)
        {
            parentPlayerMeshUp.transform.up = Vector3.up;
        }
    }

    //Déformation en saut
    public IEnumerator JumpDeform()
    {
        if (animJumpT == 0)
        {
            playerMesh.transform.parent = null;
            parentPlayerMeshUp.transform.up = Vector3.up;
            playerMesh.transform.parent = parentPlayerMeshForward.transform;
            playerMesh.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            parentPlayerMeshUp.transform.up = Vector3.up;
        }

        parentPlayerMeshUp.transform.localScale = new Vector3(parentPlayerMeshUp.transform.localScale.x, 
                                                            jump.Evaluate(animJumpT),
                                                            parentPlayerMeshUp.transform.localScale.z);

        animJumpT += animSpeedJump * Time.deltaTime;
        animJumpT = Mathf.Clamp01(animJumpT);

        yield return new WaitForEndOfFrame();

        if(animJumpT < 1)
        {
            StartCoroutine(JumpDeform());
        }
        else
        {
            animJumpT = 0;
            jumpScaleCalculate = Vector3.zero;
        }
    }

    public IEnumerator LandingDeform(float force, Vector3 normalCollision)
    {
        stopCheckParentUp = true;

        if(animLandingT == 0) 
        {
            playerMesh.transform.parent = null;
            parentPlayerMeshUp.transform.up = normalCollision;
            playerMesh.transform.parent = parentPlayerMeshForward.transform;
            playerMesh.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            parentPlayerMeshUp.transform.up = normalCollision;
        }

        parentPlayerMeshUp.transform.localScale = new Vector3(parentPlayerMeshUp.transform.localScale.x,
                                                              1 + (landing.Evaluate(animLandingT) * force),
                                                              parentPlayerMeshUp.transform.localScale.z);

        playerMesh.transform.localPosition = Vector3.up * ((landing.Evaluate(animLandingT) * force) / 2);

        animLandingT += animSpeedLanding * Time.deltaTime;
        animLandingT = Mathf.Clamp01(animLandingT);

        yield return new WaitForEndOfFrame();

        if (animLandingT < 1)
        {
            StartCoroutine(LandingDeform(force, normalCollision));
        }
        else
        {
            animLandingT = 0;
            stopCheckParentUp = false;
            landingScaleCalculate = Vector3.zero;
        }
    }
}
