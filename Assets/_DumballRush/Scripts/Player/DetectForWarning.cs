﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class DetectForWarning : MonoBehaviour
{
    public GameObject warningGo;

    public GameObject UiParentWarnings;

    public List<PlayerController> ListOfPlayers;
    public bool[] areMagma;
    public bool[] areBigBall;

    private float distWrongDirection = 0;
    private Vector3 lastPosWrongDir = Vector3.zero;

    [SerializeField]
    private float distMinToWrongDir;

    // Start is called before the first frame update
    void Start()
    {
        UiParentWarnings = GameObject.Find("AllWarnings");
        if (GetComponent<PhotonView>().IsMine)
        {
            StartCoroutine(WrongDirection());
        }
        else
        {
            Destroy(this);
        }
    }

    public void CreateBigBallWarning()
    {
        for (int i = 0; i < ListOfPlayers.Count; i++)
        {
            if (ListOfPlayers[i].isBigBall == true && areBigBall[i] == false && ListOfPlayers[i].gameObject != FindObjectOfType<GameManager>().myPlController.gameObject)
            {
                GameObject newWarning = Instantiate(warningGo, UiParentWarnings.transform);
                newWarning.GetComponent<Warning>().otherPlayer = ListOfPlayers[i].gameObject;
                newWarning.GetComponent<Warning>().objectType = Warning.typeOfObject.BigBallWarning;

                newWarning.GetComponent<Image>().sprite = newWarning.GetComponent<Warning>().powerUpSprites[0];

                areBigBall[i] = true;
            }
        }        
    }

    public void CreateMagmaWarning()
    {
        for (int i = 0; i < ListOfPlayers.Count; i++)
        {
            if (ListOfPlayers[i].isMagma == true && areMagma[i] == false && ListOfPlayers[i].gameObject != FindObjectOfType<GameManager>().myPlController.gameObject)
            {
                GameObject newWarning = Instantiate(warningGo, UiParentWarnings.transform);
                newWarning.GetComponent<Warning>().otherPlayer = ListOfPlayers[i].gameObject;
                newWarning.GetComponent<Warning>().objectType = Warning.typeOfObject.MagmaWarning;

                newWarning.GetComponent<Image>().sprite = newWarning.GetComponent<Warning>().powerUpSprites[1];

                areMagma[i] = true;
            }
        }
    }

    public void ResetAfterWarning(GameObject otherPlayer, GameObject warningGo)
    {
        if(warningGo.GetComponent<Warning>().objectType == Warning.typeOfObject.BigBallWarning)
        {
            for (int i = 0; i < ListOfPlayers.Count; i++)
            {
                if (ListOfPlayers[i].gameObject == otherPlayer)
                {
                    areBigBall[i] = false;
                    Destroy(warningGo);
                }
            }
        }

        if (warningGo.GetComponent<Warning>().objectType == Warning.typeOfObject.MagmaWarning)
        {
            for (int i = 0; i < ListOfPlayers.Count; i++)
            {
                if (ListOfPlayers[i].gameObject == otherPlayer)
                {
                    areMagma[i] = false;
                    Destroy(warningGo);
                }
            }
        }
    }

    public IEnumerator WrongDirection()
    {        
        Vector3 myDir = GetComponent<Rigidbody>().velocity;

        Collider[] allColliders = GetComponent<PositionAndTimerController>().myWaypoints[GetComponent<PositionAndTimerController>().nextWaypoint].GetComponents<Collider>();
        float distNext = Mathf.Infinity;
        Vector3 nxtWaypoint = Vector3.zero;

        for (int i = 0; i < allColliders.Length; i++)
        {
            Vector3 closePoint = allColliders[i].ClosestPoint(transform.position);

            if (Vector3.Distance(transform.position, closePoint) < distNext)
            {
                distNext = Vector3.Distance(transform.position, closePoint);
                nxtWaypoint = closePoint;
            }
        }
        //Vector3 nxtWaypoint = GetComponent<PositionAndTimerController>().myWaypoints[GetComponent<PositionAndTimerController>().nextWaypoint].gameObject.transform.position;

        Vector3 meToWaypoint = new Vector3(nxtWaypoint.x - transform.position.x,
                                            0,
                                            nxtWaypoint.z - transform.position.z);

        if (Vector3.Angle(myDir, meToWaypoint) > 120 && GetComponent<PositionAndTimerController>().finishRace == false)
        {     
            if(lastPosWrongDir == Vector3.zero)
            {
                lastPosWrongDir = transform.position;
            }
            else
            {
                distWrongDirection += Vector3.Distance(lastPosWrongDir, transform.position);
                lastPosWrongDir = transform.position;

                if(distWrongDirection >= distMinToWrongDir)
                {
                    Debug.Log("mauvais sens");
                    FindObjectOfType<UiManager>().ActivateWrongDirection(true);
                }
                //Debug.Log(distWrongDirection);
            }
        }
        else
        {
            if(distWrongDirection > 0)
            {
                distWrongDirection = 0;
                lastPosWrongDir = Vector3.zero;
                FindObjectOfType<UiManager>().ActivateWrongDirection(false);
            }           
        }

        yield return new WaitForSeconds(0.2f);
        StartCoroutine(WrongDirection());
    }

    public void CreateLists()
    {
        ListOfPlayers = FindObjectOfType<GameManager>().AllPlayers;
        areMagma = new bool[ListOfPlayers.Count];
        areBigBall = new bool[ListOfPlayers.Count];
    }


}
