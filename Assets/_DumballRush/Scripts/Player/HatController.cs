﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatController : MonoBehaviour
{
    [SerializeField]
    private PlayerController pc;
    [SerializeField]
    private GameObject hat;
    [SerializeField]
    private float bumpForce = 200;
    [SerializeField]
    private float inclinaisonUpdateAmount = 10;

    private Vector3 tempScale;
    private InputManager im;

    private void Awake()
    {
        tempScale = transform.parent.localScale;
    }

    private void Update()
    {
        UpdateBump();

        UpdateInclinaison();

        UpdateScaling();

    }

    private void UpdateBump()
    {
        if (im != null)
        {
            if (im.inputJump)
            {

                Animator hatAnim = hat.GetComponent<Animator>();
                hatAnim.SetFloat("BumpForce", bumpForce * pc.GetComponent<Rigidbody>().velocity.magnitude);
                hatAnim.SetTrigger("Bump");
            }
        }
        else
        {
            im = FindObjectOfType<InputManager>();
        }
    }

    private void UpdateInclinaison()
    {
        Vector3 targetVectorUp = Vector3.up;

        if (pc.IsGrounded())
        {
            RaycastHit hit;
            if (Physics.Raycast(pc.transform.position, -Vector3.up, out hit))
            {
                targetVectorUp = Vector3.Lerp(transform.up, hit.normal, inclinaisonUpdateAmount * Time.deltaTime);
            }
        }
        else
        {
            targetVectorUp = Vector3.Lerp(transform.up, Vector3.up, inclinaisonUpdateAmount/4 * Time.deltaTime);
        }


        transform.up = targetVectorUp;
    }

    private void UpdateScaling()
    {
        if (transform.parent.localScale != tempScale)
        {
            tempScale = transform.parent.localScale;

            hat.transform.localScale = Vector3.one / transform.parent.localScale.x;

        }
    }
}
