﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/*
 * Ce script permet de voir les pseudos des autres joueurs
 * Il doit être sur l'objet NickName (en enfant du prefab de joueur)
 */

public class NicknameController : MonoBehaviour
{
    //Scale de base du pseudo
    public Vector3 baseScale;

    //Offset Position de l'objet 
    [HideInInspector]
    public Vector3 offsetPosition;

    //Le joueur à qui appartient le pseudo
    [HideInInspector]
    public GameObject player;

    [Tooltip("Distance avant de commencer à disparaître")]
    [SerializeField]
    private float distBeforeFade;

    [Tooltip("Distance où le nom n'apparrait plus")]
    [SerializeField]
    private float distMax;

    //Le playerLocal
    private GameObject localPlayer;

    //Si le nickname a déjà possédé un joueur ou non
    private bool asGetThePlayer;

    //Distance lerp
    private float unlerpDist;

    private TextMesh textComponent;

    //Les CameraController
    private CameraController[] ccs;

    private void Start()
    {
        if (PlayerPrefs.HasKey("distBF"))
        {
            distBeforeFade = PlayerPrefs.GetFloat("distBF");
        }
        else
        {
            distBeforeFade = 30f;
        }
        
        if (PlayerPrefs.HasKey("distMax"))
        {
            distMax = PlayerPrefs.GetFloat("distMax");
        }
        else
        {
            distMax = 120f;
        }

        //Récupère toutes les cameras
        ccs = FindObjectsOfType<CameraController>();

        //En théorie, il n'y en a qu'une seule
        //Pour toutes les cameras
        foreach (CameraController cc in ccs)
        {
            //Si elle est activée
            if (cc.isActiveAndEnabled)
            {
                //Le nickname regarde la camera
                transform.LookAt(cc.transform);
            }
        }

        SearchLocalPlayer();

        StartCoroutine(FakeUpdate());
    }

    void Update()
    {
        /*//Si les variables sont remplient
        if (player != null)
        {
            asGetThePlayer = true;

            if (offsetPosition != null)
            {
                //Met la position du Nickname au dessus du joueur qui lui correspond
                transform.position = player.transform.position + offsetPosition + Vector3.up * (player.transform.localScale.x / 2);
                //Scale le nickname par rapport à la taille de la boule
                transform.localScale = new Vector3(baseScale.x * player.transform.localScale.x, baseScale.y * player.transform.localScale.x, baseScale.z);
            }

            if (localPlayer != null)
            {
                if (Vector3.Distance(localPlayer.transform.position, this.transform.position) > distBeforeFade)
                {
                    unlerpDist = Mathf.InverseLerp(distMax, distBeforeFade, Vector3.Distance(localPlayer.transform.position, this.transform.position));

                    unlerpDist = Mathf.Clamp01(unlerpDist);

                    if (textComponent == null)
                        textComponent = GetComponent<TextMesh>();

                    textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, unlerpDist);
                }
            }
        }
        else if (asGetThePlayer)
        {
            Destroy(this.gameObject);
        }*/

        foreach (CameraController cc in ccs)
        {
            //Si elle est activée
            if (cc.isActiveAndEnabled)
            {
                //Le nickname regarde la camera
                transform.LookAt(cc.transform);
            }
        }
    }

    public IEnumerator FakeUpdate()
    {
        //Si les variables sont remplient
        if (player != null)
        {
            asGetThePlayer = true;

            if (offsetPosition != null)
            {
                //Met la position du Nickname au dessus du joueur qui lui correspond
                //transform.position = player.transform.position + offsetPosition + Vector3.up * (player.transform.localScale.x / 2);
                //Scale le nickname par rapport à la taille de la boule
                //transform.localScale = new Vector3(baseScale.x * player.transform.localScale.x, baseScale.y * player.transform.localScale.x, baseScale.z);
            }

            if (localPlayer != null)
            {
                if (Vector3.Distance(localPlayer.transform.position, this.transform.position) > distBeforeFade)
                {
                    unlerpDist = Mathf.InverseLerp(distMax, distBeforeFade, Vector3.Distance(localPlayer.transform.position, this.transform.position));

                    unlerpDist = Mathf.Clamp01(unlerpDist);

                    if (textComponent == null)
                        textComponent = GetComponent<TextMesh>();

                    textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, unlerpDist);
                }
            }
        }
        else if (asGetThePlayer)
        {
            Destroy(this.gameObject);
        }

        yield return new WaitForSeconds(0.2f);
        StartCoroutine(FakeUpdate());
    }

    private void SearchLocalPlayer()
    {
        if (localPlayer == null)
        {
            if (player.GetComponent<PlayerController>().isPlayerRoom)
            {
                PlayerController[] allPlayers = FindObjectsOfType<PlayerController>();

                foreach (PlayerController player in allPlayers)
                {
                    if (player.pv.IsMine)
                    {
                        localPlayer = player.gameObject;
                    }
                }
            }
            else
            {
                GameManager gm = FindObjectOfType<GameManager>();

                if (gm != null)
                {
                    if (gm.myPlController != null)
                    {
                        localPlayer = gm.myPlController.gameObject;
                    }
                }
            }
        }
    }
}