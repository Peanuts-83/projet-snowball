﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class ObstacleController : MonoBehaviour
{
    [Header("Indestructibles")]
    [Tooltip("Force d'explosion reçu par le joueur")]
    [SerializeField]
    private float repulsionForce = 500;
    
    [Header("Invincibilité")]
    [Tooltip("Le temps d'invincibilité")]
    [SerializeField]
    private float invincibilityDelay = 0;

    [SerializeField]
    private float clignotementDelay = 0;

    [Tooltip("Valeur de division de la taille")]
    public float damageAmount = 2;

    private bool isInvincible = false;

    //Le player Controller
    private PlayerController pc;
    //Le vfx controller
    private VfxController vc;
    //L'audio Manager
    private AudioManager am;
    //L'Input Manager
    private InputManager im;
    //Le Rumbler
    private Rumbler rumble;

    //La camera du joueur
    [Tooltip("Camera du joueur")]
    [SerializeField]
    private GameObject myCamera;

    [Tooltip("gameObject Bouclier")]
    public GameObject bouclier;
    [Tooltip("Renderer du bouclier")]
    [HideInInspector]
    public Renderer rendBoubou;

    private void Awake()
    {
        //Récupère le player controller
        pc = GetComponent<PlayerController>();
    }

    private void Start()
    {
        //Récupère le VFXController
        vc = GetComponent<VfxController>();
        //On récupère l'audio manager pour le son
        am = FindObjectOfType<AudioManager>();
        //On récupère l'input manager
        im = FindObjectOfType<InputManager>();
        //On récupère le Rumbler
        rumble = FindObjectOfType<Rumbler>();

        //Récupère le renderer
        rendBoubou = bouclier.GetComponent<Renderer>();



        //On récupère le script de la camera
        CameraController camController = myCamera.GetComponent<CameraController>();
    }

    //Quand le joueur entre dans un collider
    public void OnCollisionStay(Collision collision)
    {
        //Si le joueur n'est pas invincible
        if (!isInvincible && !pc.isBigBall)
        {
            //Si c'est un tag où le joueur peut collide et perdre de la neige
            if (collision.gameObject.tag == "DestructibleObstacle" || collision.gameObject.tag == "NoDestructibleObstacle")
            {
                //sfx
                if (canCollisionVoice)
                {
                    StartCoroutine(CooldownCollisionVoice(am.soundEffectConfiguration.voix.cooldownCollision));

                    am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCollision, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                }

                //On affecte les dégâts
                DamageObstacle(collision.collider);

                //Faire devenir le joueur invincible
                StartInvincibility();

                if (collision.gameObject.TryGetComponent(out GravityObstacles gravityObstaclesScript))
                {
                    if (gravityObstaclesScript.isAStalactite)
                    {
                        Destroy(collision.collider);
                        collision.gameObject.GetComponent<ActivableObstacles>().meshStalactiteBase.SetActive(false);
                        collision.gameObject.GetComponent<ActivableObstacles>().meshStalactiteBroken.SetActive(true);
                        collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                        collision.gameObject.GetComponent<Rigidbody>().AddExplosionForce(100, transform.position, 2);

                        am.PlaySoundEffect(am.soundEffectConfiguration.collision.fmodEventBreakStalactite, collision.gameObject);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Si le joueur entre en collision avec la chute d'eau glacée
        if (other.tag == "WaterfallIce")
        {
            //explosion
            other.GetComponent<Rigidbody>().isKinematic = false;
            other.GetComponent<Rigidbody>().AddExplosionForce(100, transform.position, transform.localScale.x * 3);
        }

        //Trigger avec le magma sans etre invincible ou soit meme magma
        if (other.gameObject.tag == "MagmaCollision" && !isInvincible && !pc.isMagma)
        {
            //Si je ne suis pas en bigBall
            if (!pc.isBigBall)
            {
                vc.PlayVFX(vc.powerUp.goVfxHitMagma2);

                if (pc.actualStep == 0)
                { 
                    //Damage
                    DamageBase();
                }else if(pc.actualStep != 0)
                {
                    //Damage
                    DamageBaseMagma();

                }





            }
            //Si je suis en bigBall
            else if (pc.isBigBall)
            {
                //Je deviens invincible
                StartInvincibility();
                //fin de bigball
                StartCoroutine(pc.ShrinkDuringBigBall());
            }
        }

        //Trigger avec la zone d'explosion du poxer up Onde de Confusion
        if(other.gameObject.tag == "ConfusionCollision")
        {
            if (!pc.isConfused)
            {
                //Lancement de la coroutine de confusion
                StartCoroutine(Confused());
            }
        }
    }

    //Dégat au joueur
    private void DamageBase()
    {
        //je vérifie si je suis pas invincible
        if (pc.pv.IsMine && isInvincible == false)
        {
            //je deviens invincible
            StartInvincibility();

            //Réduction de la taille du joueur
            pc.NewShrink();
           

            //vfx perte de neige
            vc.InstantiateParticles(vc.snow.goVfxLoseSnowPlayer, transform.position);
        }          
    }

    //Dégat au joueur
    private void DamageBaseMagma()
    {
        //je vérifie si je suis pas invincible
        if (pc.pv.IsMine && isInvincible == false)
        {
            Debug.Log("case 1");



            //je deviens invincible
            StartInvincibility();

            switch (pc.actualStep)
            { 
                case 0:
                    
                    pc.NewShrink();
                break;

                case 1:
                    Debug.Log("case 1");
                    pc.NewShrink(); 
                    pc.NewShrink();
                    break;

                case 2:
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    break;

                case 3:
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    break;

                case 4:
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    break;

                case 5:
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    pc.NewShrink();
                    break;


                default:
                    break;
            }

            //Réduction de la taille du joueur
            pc.NewShrink();


            //vfx perte de neige
            vc.InstantiateParticles(vc.snow.goVfxLoseSnowPlayer, transform.position);
        }
    }



    //Dégat au joueur (spéciaux pour les obstacles)
    private void DamageObstacle(Collider collider)
    {
        if (pc.pv.IsMine)
        {
            if (pc.isFrozen)
            {
                pc.ResetFrozen();
            }

            if(collider.gameObject.tag == "DestructibleObstacle")
            {
                GravityObstacles gravityObstacleScript;

                GravityObstacles[] allScriptsGravityObstaclesInParents = collider.transform.GetComponentsInParent<GravityObstacles>();
                

                //if (collider.TryGetComponent<GravityObstacles>(out gravityObstacleScript))
                if (allScriptsGravityObstaclesInParents.Length != 0)
                {
                    gravityObstacleScript = allScriptsGravityObstaclesInParents[0];

                    if (gravityObstacleScript.isATree)
                    {
                        am.PlaySoundEffect(am.soundEffectConfiguration.trees.fmodEventTreeDestruct);
                        am.PlaySoundEffect(am.soundEffectConfiguration.trees.fmodEventBushSound, collider.gameObject);

                        if (canCollisionVoice)
                        {
                            StartCoroutine(CooldownCollisionVoice(am.soundEffectConfiguration.voix.cooldownCollision));

                            am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCollision, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                        }
                    }
                }
            }

            Camera.main.GetComponent<CameraController>().Shake(1.5f, 8);

            rumble.RumbleConstant(0.5f, 0.5f, 0.25f);

            //Réduction de la taille du joueur
            pc.NewShrink();
        }

        //VFX Loss
        //selon la speed du joueur ?
        vc.InstantiateParticles(vc.snow.goVfxLoseSnowObstacle, collider.ClosestPointOnBounds(transform.position));

        //vfx perte de neige
        vc.InstantiateParticles(vc.snow.goVfxLoseSnowPlayer, transform.position);

        //Répulsion du joueur
        pc.RepulsePlayer(collider.ClosestPointOnBounds(transform.position), repulsionForce, transform.localScale.x);
    }

    //Débute l'invincibilité
    private void StartInvincibility()
    {
        //Dit qu'il est invincible
        isInvincible = true;

        //Lance la coroutine
        StartCoroutine(Invincibility());
        //Lance le clignotement
        StartCoroutine(Clignotement());
    }

    //Coroutine d'invincibilité
    private IEnumerator Invincibility()
    {
        //Attend une frame
        yield return new WaitForSeconds(invincibilityDelay);

        //Le joueur n'est plus invincible
        isInvincible = false;
    }

    //Fait clignoter le joueur
    private IEnumerator Clignotement()
    {
        //couleur du bouclier 
        rendBoubou.material.SetColor("Color_76274958", GetComponent<PersonnalisationController>().playerColor);

        //Si le joueur est invincible
        if (isInvincible)
        {
            //S'il est pas visible
            if (!rendBoubou.enabled)
            {
                //Il devient visible
                rendBoubou.enabled = true;
            }
            //Si oui
            else
            {
                //Il devient invisible
                rendBoubou.enabled = false;
            }

            //On attend le délai de clignotement
            yield return new WaitForSeconds(clignotementDelay);

            //Relance la coroutine pour clignoter
            StartCoroutine(Clignotement());
        }
        //S'il n'est pas invincible
        else
        {
            //Et que le mesh est activé
            if (rendBoubou.enabled)
                //On désactive le mesh
                rendBoubou.enabled = false;
        }
    }

    //Coroutine de confusion -> Inversion des controles
    private IEnumerator Confused()
    {
        if (pc.pv.IsMine)
        {
            myCamera.GetComponent<CameraController>().ChangeVignette(new Color(227, 0, 225), 0.06f);
            myCamera.GetComponent<CameraController>().EffectPostProdConfu(true);
            
            //j'active le parametres controles inversés du player Controller
            pc.isConfused = true;

            am.musicConfiguration.parametersMusicRace.confus.value = 1;
        }

        //Je lance le vfx de confusion même si ce n'est pas mon joueur
        vc.PlayVFX(vc.powerUp.gpVfxConfu);

        //j'attends le temps du power up
        yield return new WaitForSeconds(pc.timeConfused);

        //Désactivation du vfx
        vc.StopVFX(vc.powerUp.gpVfxConfu);

        if (pc.pv.IsMine)
        {
            //je désactive les controles inversés
            pc.isConfused = false; 
            
            myCamera.GetComponent<CameraController>().ChangeVignette(new Color(0, 240, 255), 0f);
            myCamera.GetComponent<CameraController>().EffectPostProdConfu(false);

            am.musicConfiguration.parametersMusicRace.confus.value = 0;
        }
    }

    private IEnumerator FondueSlow()
    {
        yield return new WaitForSeconds(.5f);
    }

    #region Cooldown voix
    private bool canCollisionVoice = true;

    private IEnumerator CooldownCollisionVoice(float time)
    {
        canCollisionVoice = false;

        yield return new WaitForSeconds(time);

        canCollisionVoice = true;
    }
    #endregion
}
