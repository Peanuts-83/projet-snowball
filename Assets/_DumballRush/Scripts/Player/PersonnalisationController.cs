﻿using Photon.Pun;
using UnityEngine;

/*
 * Ce script permet de gérer la personnalisation du joueur
 * 
 * Il doit se trouver sur le joueur
 */

public class PersonnalisationController : MonoBehaviour
{
    [Tooltip("Texte flottant au dessus du joueur pour le nickname")]
    public TextMesh nicknameText;

    [Tooltip("Liste des couleurs possibles pour le joueur")]
    public Color playerColor;

    [Tooltip("Pseudo du joueur")]
    public string playerNickName;

    [Tooltip("EyeId")]
    public int eyeId;

    [Tooltip("MouthId")]
    public int mouthId;

    [Tooltip("VoiceID")]
    public int voiceId;

    [Tooltip("Mesh de la boule de neige")]
    [SerializeField]
    private GameObject mesh;

    [Tooltip("Visage du joueur")]
    [SerializeField]
    private GameObject visage;

    //Le PhotonView
    private PhotonView pv;

    void Start()
    {
        //Récupération du photonView
        pv = GetComponent<PhotonView>();

        //Si il s'agit de mon joueur
        if (pv.IsMine)
        {
            //Change la couleur du joueur
            playerColor = new Color(PlayerPrefs.GetFloat("ColorR"), PlayerPrefs.GetFloat("ColorG"), PlayerPrefs.GetFloat("ColorB"), PlayerPrefs.GetFloat("ColorA"));

            //Transmet toutes les information
            pv.RPC("RPC_UpdatePlayer", RpcTarget.AllBuffered, PhotonNetwork.NickName, new Vector3(playerColor.r, playerColor.g, playerColor.b), PlayerPrefs.GetInt("eye"), PlayerPrefs.GetInt("mouth"), PlayerPrefs.GetInt("voice")) ;

            //Désactive la vision de mon propre pseudo (les autres joueurs verront quand meme mon pseudo)
            nicknameText.gameObject.SetActive(false);
        }
        else
        {
            //On récupère le nickname de ce joueur qu'on ne controlle pas
            NicknameController nc = nicknameText.GetComponent<NicknameController>();

            //On set la variable "player" du nicknameController étant ce joueur ci
            nc.player = this.gameObject;
            //On set la variable offsetPosition du nicknameController étant sa position par rapport au joueur
            nc.offsetPosition = nicknameText.transform.localPosition;
            nc.name = "PlayerNickname_" + nicknameText.text;
            //On déparente le nicknameText
            //nicknameText.transform.parent = null;
        }
    }

    #region RPC Fonction

    [PunRPC]
    private void RPC_UpdatePlayer(string newNickname, Vector3 col, int eye, int mouth, int voice)
    {
        #region Nickname
        //Change le nickname du joueur au dessus de sa tête
        nicknameText.text = newNickname;
        playerNickName = newNickname;
        #endregion

        #region Couleur
        //Reconstruit la couleur
        Color newCol = new Color(col.x, col.y, col.z, 1);

        //Modifie l'information de couleur
        playerColor = newCol;

        //Modifie la couleur du mesh
        mesh.GetComponent<Renderer>().material.SetColor("Color_E797161B", newCol);
        //Modifie la couleur du pseudo
        nicknameText.color = newCol;
        #endregion

        #region Visage
        eyeId = eye;
        mouthId = mouth;

        visage.GetComponent<VisageController>().UpdateEye(eye);
        visage.GetComponent<VisageController>().UpdateMouth(mouth);
        #endregion

        #region Voix
        voiceId = voice;
        #endregion

        Debug.Log("I send all data of player customisation");
    }
    #endregion
}