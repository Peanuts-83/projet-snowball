﻿using Photon.Pun;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

/*
 * Ce script permet de controller les mouvements, les rotations,
 * la gravité, le grossissement ou la rapetissement du joueurs.
 * 
 * On contrôle à partir de ce script les réactions des collisions 
 * et des triggers sur le mouvement du joueur.
 * 
 */

public class PlayerController : MonoBehaviour
{
    //Toutes les variables du PlayerController
    #region Variables
    //Toutes les variables pour contrôler les rotations du joueur
    #region Rotation variables
    [Header("Rotations")]
    [Tooltip("Vitesse de rotation du joueur")]
    public float rotationSpeedOnDirection = 100;
    public float rotationSpeedOnMouse = 150;
    [Tooltip("Lerping de rotation")]
    public float rotationLerping;

    //calcul de la rotation sur l'axe horizontal
    private float rotationMovementHorizontal;

    #endregion
    
    //Toutes les variables pour contrôler les déplacements du joueur
    #region Deplacement variables
    [Header("Déplacements")]
    [Tooltip("Vitesse max originale du joueur")]
    public float depSpeedMaxOriginal;
    [Tooltip("Paliers de vitesse en fonction de la taille")]
    public float[] speedSteps;
    //Vitesse actuelle du joueur
    private float deplacementSpeed;
    [HideInInspector]
    public Vector3 velocityWithoutGravity;

    [Header("Turbo")]
    [Tooltip("Vitesse maximale sur un turbo")]
    public float turboSpeedMax;
    [Tooltip("Vitesse maximale sur un tremplin")]
    public float turboTremplinSpeedMax;
    [Tooltip("Vitesse maximale BigBall")]
    public float turboBigBallSpeedMax;
    [Tooltip("Temps de Turbo")]
    public float timeTurbo;
    private bool turboEnd;

    [Tooltip("Le joueur ne peut plus bouger")]
    public bool inputMoveDisable;

    [Header("Accélération")]
    [Tooltip("Lerping des inputs")]
    public float inputLerping;
    [Tooltip("Accélération du joueur")]
    [SerializeField]
    private float acceleration = 2000;
    [Tooltip("Degré d'augmentation de la vitesse en fonction de la position dans la course")]
    [SerializeField]
    private float speedIncreaseByPosition = 2;
    [Tooltip("Accélération lors d'un changement de direction")]
    [SerializeField]
    private float accelerationSpeedWhileDirectionChange = 2300;

    [Header("Aspiration")]
    [Tooltip("Maximum de vitesse ajouter lors de l'aspiration")]
    [SerializeField]
    private float maxAddedSpeedInAspiration = 0;
    [SerializeField]
    private float speedToReachMaxAddedSpeedInAspiration = 0;
    [SerializeField]
    private float speedToReachBaseAddedSpeedInAspiration = 0;
    private float addedSpeedInAspiration;

    [HideInInspector]
    public bool isInAspiration = false;

    //Calcul du mouvement sur l'axe vertical
    private float verticalMovement;
    //Calcul du mouvement sur l'axe horizontal
    private float horizontalMovement;
    //Valeur utilisée à la place de im.inputMovement.x à cause de la confusion
    private float horizontalInput;
    #endregion

    //Toutes les variables en rapport avec la gravité et les sauts
    #region Gravity & Jump variables
    [Header("Gravité et sauts")]
    [Tooltip("Gravité")]
    public float gravity;
    [Tooltip("JumpForce")]
    public float jumpForce = 16;
    [Tooltip("Angle maximale du sol sur lequel le joueur peut sauter")]
    [SerializeField]
    private float maxJumpAngle = 60;

    [Tooltip("Temps en l'air avant le déclanchement des VFX")]
    [SerializeField]
    private float fallingDelay = 1;

    [Header("Angle du terrain")]
    [Tooltip("L'angle maximal sur lequel le joueur peut se mouvoir")]
    [SerializeField]
    private float maxAngleMovement = 60;
    [Tooltip("La force minimale et maximale ajouté à la gravité entre maxAngleMovement et 180°")]
    [SerializeField]
    private Vector2 minMaxAddedForceWith = new Vector2(0, 150);

    //Calcul de la gravité
    private float g;

    //Temps depuis la dernière collision avec le sol
    private float fallingTime = 0;
    //Si le timer de chute doit s'arrêter
    private bool stopFallingTimer = false;
    #endregion

    //Toutes les variables en rapport avec la taille du joueur
    #region Scale variables
    [Header("Taille")]
    [Tooltip("Taux d'augmentation de la taille de la boule")]
    public float magnificationAmount = 0.5f;
    [Tooltip("Taille maximale de la boule")]
    public float maxScale;
    [Tooltip("Taille calculée de la boule")]
    public float calculateScale;
    [Tooltip("Paliers de taille")]
    public float[] scaleSteps;
    [Tooltip("Palier Actuel")]
    public int actualStep;

    [Tooltip("Gain de masse en fonction du gain de taille (0 -> Ne gagne pas de masse; 1 -> Gagne autant de masse qu'en taille)")]
    [Range(0, 1)]
    public float massGainAmount = 0.5f;

    [Tooltip("Courbe de gain de taille (en fonction de la taille minimale et maximale")]
    [SerializeField]
    private AnimationCurve magnifyCurve;

    [Header("Interactions")]
    [Tooltip("Force minimale et maximale lors d'une collision entre joueur")]
    [SerializeField]
    private Vector2 playerContactForceMinMax = new Vector2(500, 3000);


    #endregion

    //Other Power Up Variables
    #region Power Up Variables

    [Header("BigBall")]
    [Tooltip("Taille de la boule pendant le power up")]
    [SerializeField]
    private float ballScalePU;
    private float ballScaleBase;
    [Tooltip("Durée du Power Up")]
    [SerializeField]
    private float timeBigBall;
    [Tooltip("Booléen - Le joueur utilise t-il ce pouvoir ?")]
    [SerializeField]
    public bool isBigBall;

    [Header("Magma")]
    public bool isMagma = false;
    public float timeMagma;

    [Header("Freeze")]
    [Tooltip("Quand le joueur est givré(il ne peut plus bouger)")]
    public bool isFrozen;
    private bool doOnceFrozen = true;
    [Tooltip("Temps de Freeze")]
    public float timeFreeze;

    [Header("Confusion (Contrôles inversés)")]
    [Tooltip("Temps de Confusion (contrôles inversés")]
    public float timeConfused;

    [HideInInspector]
    public bool isConfused = false;

    [HideInInspector]
    public bool isInFondue = false;

    private float speedSlowInFondue;
    #endregion

    //Collisions
    #region Collisions variables
    [Header("Collisions")]
    [SerializeField]
    [Tooltip("Layer du joueur")]
    int lyrPlayer;

    [SerializeField]
    [Tooltip("Layer de la collision non trigger des obstacles")]
    int lyrObstacleNoTrigger;

    [SerializeField]
    [Tooltip("Layer de la collision trigger des obstacles")]
    int lyrObstacleTrigger;
    #endregion

    //ScreenShake
    #region ScreenShake variables
    [Header("ScreenShake")]

    [SerializeField]
    [Tooltip("Temps du screenShake en BigBall lorsqu'il tombe")]
    public float shakeTime_BigBall_Fall = 2.7f;
    [SerializeField]
    [Tooltip("Puissance du screenShake en BigBall lorsqu'il tombe")]
    public float shakePower_BigBall_Fall = 8;

    [SerializeField]
    [Tooltip("Temps du screenShake en BigBall lorsqu'il percute un arbre")]
    public float shakeTime_BigBall_Tree = 2.3f;
    [SerializeField]
    [Tooltip("Puissance du screenShake en BigBall lorsqu'il percute un arbre")]
    public float shakePower_BigBall_Tree = 2;

    [SerializeField]
    [Tooltip("Temps du sceenShake lors des collisions entre joueur")]
    private float shakeTime_PlayerCollisions = 2f;
    [SerializeField]
    [Tooltip("Puissance du screenShake lors des collisions entre joueur")]
    private float shakePower_PlayerCollisions = 6;


    #endregion

    //Variables fonctionnelles (Camera, particules de neiges, etc...)
    #region Functionnal variables
    [Header("Fonctionnel")]
    [Tooltip("Objet contenant la camera, le point de target de la camera et le point de position de la camera. Permet d'orienter le joueur")]
    public GameObject directionRotator;

    [Tooltip("Objet qui a la rotation du joueur sans l'axe vertical (déplacements gauche/droite")]
    public GameObject velocityWithoutGravityForward;

    [Tooltip("Permet d'ajuster la hauteur de la camera en fonction de la taille de la boule")]
    [SerializeField]
    public GameObject highAdapter;

    //La camera du joueur
    [Tooltip("Camera du joueur")]
    [SerializeField]
    public GameObject myCamera;

    [Tooltip("Target pour la rotation de la camera, point de focus de la camera")]
    [SerializeField]
    private Transform cameraTarget;

    [Tooltip("prefab de position de la camera")]
    [SerializeField]
    private GameObject targetCameraPositionPrefab;

    [Tooltip("Les tags avec lesquels le joueur n'est pas considéré comme étant sur le sol")]
    [SerializeField]
    private string[] tagsIsNotGrounds;

    [Tooltip("Particules de neige qui tombent du ciel")]
    [SerializeField]
    private GameObject fallingSnowParticule;

    //Mesh du joueur
    public GameObject mesh;

    [SerializeField]
    private GameObject myAreaAspiration;

    //The angle of the terrain
    private float terrainAngle = 0;

    //Volume du snowSlide
    private float volumeSnowSlide;

    [SerializeField]
    [Tooltip("Si c'est le joueur 'Player_Room'")]
    public bool isPlayerRoom;

    public bool canJump;

    [HideInInspector]
    public bool iAmReadyForClassement;

    public bool isBoosted;

    //Prevent blocking in triggers
    private float timeBlocked = 0;
    private Vector3 previousPositionOfPlayer;
    #endregion

    //Variable de scripts et de components
    #region Scripts & Components variables
    //Ajout des inputActions
    private InputManager im;

    //Mon photonView
    [HideInInspector]
    public PhotonView pv;
    //Mon Rigidbody
    private Rigidbody rb;
    //Mon GameManager
    private GameManager gm;
    //VFX Controller
    private VfxController vc;
    //Global volume
    private Volume vol;
    //Analytics
    private AnalyticsDB ana;

    private AudioManager am;
    //Le Rumbler
    private Rumbler rumble;
    #endregion

    //DeltaTime
    #region DeltaTime variables
    //DeltaTime
    private float dt;
    //FixedDeltaTime
    private float fdt;
    #endregion

    //Analytics
    #region Analytics
    private enum States
    { 
        Boosted,
        Confused,
        Frozen,
        SlowedInFondu,
        None
    }

    private States state;
    private States previousState;

    private float aspirationTime_total;
    private float aspirationTime_unique;
    #endregion

    #endregion


    //Start, Update, FixedUpdate
    #region Unity Methods
    private void Awake()
    {
        //On récupère le photonView
        pv = GetComponent<PhotonView>();
        //On récupère le Rigidbody
        rb = GetComponent<Rigidbody>();
        //On récupère le vfx controller
        vc = GetComponent<VfxController>();

        //On récupère l'InputManager
        im = FindObjectOfType<InputManager>();
        //On récupère le gameManager
        gm = FindObjectOfType<GameManager>();
        //On récupère le globale volume
        vol = FindObjectOfType<Volume>();
        //On récupère l'audioManager
        am = FindObjectOfType<AudioManager>();
        //On récupère les Analytics
        ana = FindObjectOfType<AnalyticsDB>();
        //On récupère le Rumbler
        rumble = FindObjectOfType<Rumbler>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if(isPlayerRoom)
        {
            //Desactivate myAreaAspiration
            myAreaAspiration.SetActive(false);
        }

        //Si c'est mon joueur
        if (pv.IsMine)
        {
            //On désactive le curseur de la souris
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            #region Camera

            //On récupère le script de la camera
            CameraController camController = myCamera.GetComponent<CameraController>();

            //La position voulu par la camera
            //Création de la position à l'intérieur du directionRotator (pour que la position puisse s'adapté à l'orientation du joueur)
            GameObject cameraTargetPosition = Instantiate(targetCameraPositionPrefab);
            //Rename
            cameraTargetPosition.name = "targetCameraPosition_" + PhotonNetwork.NickName;
            //On met la position à l'emplacement actuelle de la camera
            cameraTargetPosition.transform.position = myCamera.transform.position;
            //On met la position en enfant du directionRotator
            cameraTargetPosition.transform.parent = highAdapter.transform;

            //Set les variables dans l'objet de positionnement de la camera
            CameraPositionAdaptater camPositionScript = cameraTargetPosition.GetComponent<CameraPositionAdaptater>();

            camPositionScript.player = this;
            camPositionScript.target = cameraTarget;

            camPositionScript.GetAllNecessary();

            //On assigne la position target par la camera dans la camera
            camController.targetPosition = cameraTargetPosition;
            camController.player = this;

            //Retire la camera du joueur
            myCamera.transform.parent = null;

            #endregion

            //Etablissement des layers de collisions
            Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleTrigger, true);
            Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleNoTrigger, false);

            //Créé une snowSlideInstance
            am.soundEffectConfiguration.InstanceSnowSlide = am.CreateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide,0f);

            //Desactivate myAreaAspiration
            myAreaAspiration.SetActive(false);
        }
        //Si ce n'est pas mon joueur
        else
        {
            //On désactive les cameras qui n'appartiennent pas à mon joueur
            myCamera.SetActive(false);

            //On désactive les particules de neiges des autres joueurs
            fallingSnowParticule.SetActive(false);
        }

        //Les vfx sont réinitialisé
        fallingTime = fallingDelay;

        //Récupération de la valeur de taille de base 
        calculateScale = transform.localScale.x;

        //Step (taille / vitesse) de base à 0
        actualStep = 0;

        transform.localScale = new Vector3(scaleSteps[actualStep], scaleSteps[actualStep], scaleSteps[actualStep]);
        //Speed de base
        ChangeSpeedBySize();

        NewShrink();

        //récupération des options de sensibilité de direction et de caméra
        if (PlayerPrefs.HasKey("sensiDir"))
        {
            rotationSpeedOnDirection = PlayerPrefs.GetFloat("sensiDir");
        }
        else
        {
            rotationSpeedOnDirection = 100;
        }

        if (PlayerPrefs.HasKey("sensiCam"))
        {
            rotationSpeedOnMouse = PlayerPrefs.GetFloat("sensiCam");
        }
        else
        {
            rotationSpeedOnMouse = 150;
        }
      

    }

    private void Update()
    {
        //Si le joueur n'est pas le mien, on effectue pas de Update
        if (!pv.IsMine)
        {
            return;
        }

        //DeltaTime
        dt = Time.deltaTime;

        //Fait grossir la boule
        Magnify();

        if (im.inputDebugShrink)
        {
            NewShrink();
        }

        //Effet vitesse caméra VFX
        vc.ChangeEffectSpeed(0, speedSteps[speedSteps.Length - 1], new Vector3(rb.velocity.x, 0, rb.velocity.z).magnitude, turboSpeedMax);


        //Effet de projection de neige derrière le joueur
        vc.SetLootSnowEnable(IsGrounded(), rb.velocity.magnitude);

        //Regarde à quel point le terrain est pentu
        terrainAngle = Vector3.Angle(Vector3.up, groundContactPointNormal);

        //Change le volume du snowSlide
        if (IsGrounded())
        {
            //Récupère une valeur entre 0 et 1
            volumeSnowSlide = Mathf.InverseLerp(0, deplacementSpeed, new Vector3(rb.velocity.x, 0, rb.velocity.z).magnitude);

            //Assigne la valeur
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, volumeSnowSlide);
            
        }
        else
        {
            //Fait descendre la rapidement la valeur de son du snowSlide
            volumeSnowSlide = Mathf.Lerp(volumeSnowSlide, 0, 5f * Time.deltaTime);

            //Assigne la valeur
            am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide, volumeSnowSlide);
        }

        //Vérification de Confusion ou non
        if (isConfused)
        {
            horizontalInput = -im.inputMovement.x;
        }
        else
        {
            horizontalInput = im.inputMovement.x;
        }

        //Rotations
        if (!Cursor.visible)
            Rotate();
        else if (!isPlayerRoom)
            Rotate();

        //Affecte la position au directionRotator
        AdaptHeightOfCamera();

        //Analytics
        #region Analytics
        if (isConfused)
            state = States.Confused;
        if (isFrozen)
            state = States.Frozen;
        if (isInFondue)
            state = States.SlowedInFondu;
        if (isBoosted)
            state = States.Boosted;
        if (!isConfused && !isFrozen && !isInFondue && !isBoosted)
            state = States.None;

        if (TestStateChange())
        {
            ChangePreviousState();

            SendStateAnalytics();
        }
        #endregion

        #region Debug
        /*if (isPlayerRoom || gm.asFinishCountdown)
        {
            if (Vector3.Distance(previousPositionOfPlayer, transform.position) >= 0.2f)
            {
                timeBlocked = 0;
                previousPositionOfPlayer = transform.position;
            }
            else
            {
                timeBlocked += Time.deltaTime;

                if (timeBlocked >= 2f)
                {
                    timeBlocked = 0;

                    if (actualStep > 0)
                    {

                        NewShrink();

                        Debug.Log("CoucouJESUIS COINCE et gros");
                    }
                    else
                    {

                        transform.position += Vector3.right * Random.Range(-5, 5) + Vector3.up * Random.Range(0, 5) + Vector3.forward * Random.Range(-5, 5);

                        Debug.Log("CoucouJESUIS COINCE et petit");
                    }

                }
            }
        }*/
        
        #endregion
    }

    void FixedUpdate()
    {
        //Si le joueur n'est pas le mien, on effectue pas de FixedUpdate
        if (!pv.IsMine)
        {
            return;
        }

        //Mise à jour du deltaTime
        fdt = Time.fixedDeltaTime;

        //Déplacement
        Movement();
    }
    #endregion

    //Fonctions de mouvements et de rotations
    #region Movements et rotations

    //Change la vitesse grâce à la taille
    private void ChangeSpeedBySize()
    {
        //S'il n'est pas en fin de turbo
        if (!turboEnd)
        {
            //Change la valeur de la vitesse
            deplacementSpeed = speedSteps[actualStep];
        }
    }

    private void Movement()
    {
        AddingSpeedWithAspiration();

        //Initialisation du vecteur de déplacement
        Vector3 motion = Vector3.zero;

        float speedIncreaseByPositionCalculate;

        if (!isPlayerRoom)
        {
            speedIncreaseByPositionCalculate = speedIncreaseByPosition * (gm.Position - 1);
        }
        else
        {
            speedIncreaseByPositionCalculate = 0;
        }

        //Si le turbo se termine
        if (turboEnd)
        {
            //S'il est en grosse boule
            if (isBigBall)
            {
                //Si sa vitesse est toujours supérieure
                if (deplacementSpeed > turboBigBallSpeedMax + speedIncreaseByPositionCalculate + addedSpeedInAspiration - speedSlowInFondue)
                {
                    //On baisse la vitesse
                    deplacementSpeed -= fdt * 50;
                }
                //Si la vitesse est bonne
                else
                {
                    //On dit qu'il a terminé la fin du turbo
                    turboEnd = false;
                    //On réassigne la vitesse
                    deplacementSpeed = turboBigBallSpeedMax;
                }
            }
            //Si ce n'est pas une grosse boule
            else
            {
                //Mais que la vitesse est toujours supérieure
                if (deplacementSpeed > speedSteps[actualStep] + speedIncreaseByPositionCalculate + addedSpeedInAspiration - speedSlowInFondue)
                {
                    //On baisse la vitesse
                    deplacementSpeed -= fdt * 50;
                }
                //Si la vitesse est bonne
                else
                {
                    //On dit qu'il a terminé la fin du turbo
                    turboEnd = false;
                    //On réassigne la vitesse
                    deplacementSpeed = speedSteps[actualStep];
                }
            }
        }

        //Calcul de l'ajout de vitesse, si le joueur n'a pas ses inputs désactivés
        if (!inputMoveDisable)
        {
            //Assignation du mouvement avec l'axe vertical
            verticalMovement = Mathf.Lerp(verticalMovement, im.inputMovement.y * acceleration, inputLerping * fdt);
            //Assignation du mouvement avec l'axe horizontal
            horizontalMovement = Mathf.Lerp(horizontalMovement, horizontalInput * acceleration, inputLerping * fdt);

            //donne la direction verticale au motion
            motion += verticalMovement * directionRotator.transform.forward;
            //donne la direction horizontale au motion
            motion += horizontalMovement * directionRotator.transform.right;

            #region Sensibilité horizontale
            //Variable pour détecter la direction vers laquelle se dirige le joueur
            int horizontalChangement = 0;

            //Si le joueur va vers la droite
            if (horizontalInput > 0)
            {
                //= 1
                horizontalChangement = 1;
            }
            //Si le joeuur va vers la gauche
            else if (horizontalInput < 0)
            {
                //= -1
                horizontalChangement = -1;
            }
            //Si le joueur ne va ni vers la gauche, ni vers la droite
            else
            {
                //= 0
                horizontalChangement = 0;
            }


            //Mouvement horizontaux plus réactif
            motion += horizontalChangement * accelerationSpeedWhileDirectionChange * directionRotator.transform.right;
            #endregion

            //Multiplication du vecteur par la fixedDeltaTime
            motion *= fdt;
        }

        //Déplacement du joueur
        //Ajoute la force du motion

        //S'il a été gelé
        if (isFrozen && doOnceFrozen)
        {
            //On ne repasse plus ici
            doOnceFrozen = false;

            if (!isMagma || !isBigBall)
            {
                //On lance la coroutine de freeze
                StartCoroutine(FrozenExplosion());
            }
          
        }
        //Si c'est normal
        else
        {
            //Si l'angle du terrain est bon, on ajoute la force de motion de manière classique
            if (terrainAngle < maxAngleMovement)
            {
                //On ajoute de la
                rb.AddForce(motion);
            }
            else
            {
                //On fait en sorte que le joueur soit attiré vers le bas
                float unlerpTerrainAngle = Mathf.InverseLerp(maxAngleMovement, 180, terrainAngle);
                float lerpForce = Mathf.Lerp(minMaxAddedForceWith.x, minMaxAddedForceWith.y, unlerpTerrainAngle);

                rb.AddForce(motion + (-Vector3.up * lerpForce));
            }

            //Clamp la vitesse maximale
            float tempVelocityY = rb.velocity.y;
            velocityWithoutGravity = new Vector3(rb.velocity.x, 0, rb.velocity.z);

            

            velocityWithoutGravity = Vector3.ClampMagnitude(velocityWithoutGravity, deplacementSpeed + speedIncreaseByPositionCalculate + addedSpeedInAspiration - speedSlowInFondue);
            velocityWithoutGravity += Vector3.up * tempVelocityY;

            //make aspiration
            Vector3 aspirationVector = directionRotator.transform.forward * addedSpeedInAspiration;

            //Réassignation de la vitesse
            rb.velocity = velocityWithoutGravity + aspirationVector;
        }


        #region Gravité et saut
        //Calcul de la gravité
        //S'il est sur le sol
        if (IsGrounded())
        {
            //S'il appuie sur le saut
            if (im.inputJump && !inputMoveDisable)
            {
                if (canJump)
                {
                    Jump();
                }
            }

            //la gravité
            g = gravity;

            //Affecte une gravité minime sur le sol
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y - 0.2f, rb.velocity.z);
        }
        else
        {
            //Augmentation de la gravité
            g += gravity * fdt;

            //S'il n'est pas sur le sol, le joueur tombe
            rb.AddForce(g * -Vector3.up, ForceMode.Acceleration);
        }


        #endregion
    }

    private void Jump()
    {
        //sfx de saut
        am.PlaySoundEffect(am.soundEffectConfiguration.mouvement.fmodEventSaut,null,"",0,0.7f);

        //Donne une valeur entre 0 et 1 en fonction de la pente et du maxJumpAngle
        float unlerpTerrainAngle = Mathf.Clamp01(Mathf.InverseLerp(0, maxJumpAngle, terrainAngle));
        //trouve la position adéquate pour déclancher l'explosion de saut --> Lerp en fonction de l'angle du terrain
        Vector3 jumpExplosionPosition = transform.position - Vector3.Lerp(Vector3.up, (groundContactPointNormal + (directionRotator.transform.forward / 2)).normalized, unlerpTerrainAngle) * (transform.localScale.x / 2);

        //Calcul de la force du saut
        float force = Mathf.Lerp(2, 0.5f, unlerpTerrainAngle);
        float jumpExplosionForce = transform.localScale.x * force;

        StartCoroutine(GetComponent<DeformMesh>().JumpDeform());
        //GetComponent<DeformMesh>().JumpDeform();

        //Ajout d'une force de saut
        rb.AddExplosionForce(jumpForce, jumpExplosionPosition, jumpExplosionForce);


        //Saut VFX
        vc.InstantiateParticles(vc.landHitExplosion.goVfxLanding, transform.position);
        //Attend avant de pouvoir ressauter
        StartCoroutine(TimeCanJump(0.12f));
    }

    private IEnumerator TimeCanJump(float time)
    {
        canJump = false;
        yield return new WaitForSeconds(time);
        canJump = true;
    }

    public IEnumerator FrozenExplosion()
    {
        //if en magma on reset le magma
        if (isMagma)
        {
            am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
            am.musicConfiguration.parametersMusicRace.magma.value = 0;

            isMagma = false;

            boucleMagmaSound = isMagma;

            pv.RPC("RPC_MajMagma", RpcTarget.All, isMagma);
            vc.GetComponent<PhotonView>().RPC("RPC_PlayMagma", RpcTarget.All, isMagma);
            pv.RPC("RPC_SyncMagmaSound", RpcTarget.Others, isMagma);

            myCamera.GetComponent<CameraController>().ChangeVignette(new Color(255, 90, 0), 0f);
        }


        //sfx 
        am.PlaySoundEffect(am.soundEffectConfiguration.mouvement.fmodEventIsFrozen);

        //vfx
        vc.GetComponent<PhotonView>().RPC("RPC_PlayFrozen", RpcTarget.All, true);

        //effet cam bleuté
        myCamera.GetComponent<CameraController>().ChangeVignette(new Color(0,240,255), 0.025f);

        //On attend quelques secondes avant de la freeze
        yield return new WaitForSeconds(1f);

        //On désactive l'utilisation des inputs
        inputMoveDisable = true;

        //Le joueur glisse : donc il ne tourne plus
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        //On attend la fin du freeze
        yield return new WaitForSeconds(timeFreeze);

        //On réactive les inputs
        inputMoveDisable = false;
        isFrozen = false;
        doOnceFrozen = true;

        //On enlève le glacon
        vc.GetComponent<PhotonView>().RPC("RPC_PlayFrozen", RpcTarget.All, false);

        //reset de leffet de cam
        myCamera.GetComponent<CameraController>().ChangeVignette(new Color(0, 240, 255), 0f);

        //On enlève les contraintes
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

    }


    public void ResetFrozen()
    {
        //On réactive les inputs
        inputMoveDisable = false;
        isFrozen = false;
        doOnceFrozen = true;

        //On enlève le glacon
        vc.GetComponent<PhotonView>().RPC("RPC_PlayFrozen", RpcTarget.All, false);

        //reset de leffet de cam
        myCamera.GetComponent<CameraController>().ChangeVignette(new Color(0, 240, 255), 0f);

        //On enlève les contraintes
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }

    void AddingSpeedWithAspiration()
    {
        float calculatedMaxSpeedInAspiration = maxAddedSpeedInAspiration * Mathf.InverseLerp(0, speedSteps[actualStep], Mathf.Clamp(velocityWithoutGravity.magnitude, 0, speedSteps[actualStep]));

        //If the player is in aspiration
        if (isInAspiration)
        {
            if (im.inputMovement.y <= 0 || GetComponent<PositionAndTimerController>().finishRace)
            {
                //On retire de l'aspiration
                addedSpeedInAspiration -= fdt * speedToReachBaseAddedSpeedInAspiration * 2;
            }
            else
            {
                //On ajoute de l'aspiration
                addedSpeedInAspiration += fdt * speedToReachMaxAddedSpeedInAspiration;
            }


            //Analytics
            CalculateAnalyticsAspiration();
        }
        else
        {
            //On retire de l'aspiration
            addedSpeedInAspiration -= fdt * speedToReachBaseAddedSpeedInAspiration;

            //Analytics
            StopAnalyticsAspiration();
        }

        //On clamp l'aspiration
        addedSpeedInAspiration = Mathf.Clamp(addedSpeedInAspiration, 0, calculatedMaxSpeedInAspiration);
    }

    void SlowSpeedWithFondue()
    {
        if (isInFondue)
        {
           speedSlowInFondue = speedSteps[actualStep] * 0.8f;
        }
        else
        {
            speedSlowInFondue = 0;
        }
    }

    private void Rotate()
    {
        //On calcule la rotation
        rotationMovementHorizontal = Mathf.Lerp(rotationMovementHorizontal, horizontalInput * rotationSpeedOnDirection + im.inputMouse.x * rotationSpeedOnMouse, rotationLerping * dt);
        //rotationMovementHorizontal = im.inputMouse.x * rotationSpeed;

        //Rotation du joueur en fonction des inputs horizontal
        //On tourne l'objet de direction pour orienter le joueur
        directionRotator.transform.Rotate(rotationMovementHorizontal * Vector3.up * dt);
    }

    //Change la heuteur de la camera
    private void AdaptHeightOfCamera()
    {
        //Augmente la hauteur du position rotator en fonction de la taille
        //directionRotator.transform.position = this.transform.localPosition + (transform.localScale.x*0.7f) * Vector3.up;
        highAdapter.transform.position = this.transform.localPosition + (transform.localScale.x * 0.7f) * Vector3.up;
    }

    //Repulse le joueur
    public void RepulsePlayer(Vector3 from, float repulseForce, float radius)
    {
        float angle = Vector3.Angle((from - transform.position).normalized, rb.velocity.normalized);
        angle = Mathf.Clamp(angle, -90, 90);

        angle = Mathf.InverseLerp(-90, 90, angle);
        float forceAmount = Mathf.Lerp(1f, 0.05f, angle);

        //Debug.Log($"Angle: {angle}, ClampedAngle: {clampedAngle},UnlerpAngle: {unlerpAngle},ForceAmount: {forceAmount},Velocity: {rb.velocity.magnitude},Force: {repulseForce * forceAmount}");

        rb.velocity = Vector3.Lerp(Vector3.zero, rb.velocity, angle);

        rb.AddExplosionForce(repulseForce * forceAmount, from, radius);
    }

    #endregion

    //Fonctions de grossissement
    #region Grossiement
    //Fait grossir la boule
    void Magnify()
    {
        //Si le joueur est sur le sol
        if (IsGrounded())
        {
            //Qu'il avance
            if (rb.velocity.magnitude > 1)
            {
                //augmente la taille de la boule
                if (transform.localScale.x < maxScale)
                {
                    //Calcul d'une valeur entre 0 et 1 avec la taille minimale et maximale
                    float unlerpScale = Mathf.InverseLerp(1, maxScale, transform.localScale.x);

                    //Calcul de la prise de masse
                    float magnification = magnifyCurve.Evaluate(unlerpScale) * rb.velocity.magnitude * magnificationAmount * 0.1f * dt;

                    //Ajout à la taille
                    calculateScale += magnification;

                    //On adapte la taille et la vitesse de la boule en fonction de l'étape de grossissement actuelle
                    if (actualStep < scaleSteps.Length - 1)
                    {
                        if (calculateScale > scaleSteps[actualStep + 1])
                        {
                            actualStep++;

                            vc.GetComponent<PhotonView>().RPC("RPC_PlayWinSnow", RpcTarget.All);

                            transform.localScale = new Vector3(scaleSteps[actualStep], scaleSteps[actualStep], scaleSteps[actualStep]);
                            ChangeSpeedBySize();

                            //Ajout à la masse
                            rb.mass += magnification * massGainAmount;
                        }
                    }
                }
            }
        }
    }

    public void PU_BigBall()
    {
        if (isFrozen)
        {
            ResetFrozen();
        }

        isBigBall = true;

        pv.RPC("RPC_MajBigBall", RpcTarget.Others, isBigBall);

        Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleNoTrigger, true);
        Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleTrigger, false);

        deplacementSpeed = turboBigBallSpeedMax;

        am.musicConfiguration.parametersMusicRace.powerUpBig.value = 1;
        //Son de Celebration
        am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
        pv.RPC("RPC_Celebration", RpcTarget.Others, transform.position.x, transform.position.y, transform.position.z, GetComponent<PersonnalisationController>().voiceId);

        StartCoroutine(BigBallTime());
    }

    [PunRPC]
    void RPC_MajBigBall(bool bigballOrNot)
    {
        isBigBall = bigballOrNot;
        if (bigballOrNot)
        {
            FindObjectOfType<GameManager>().myPlController.GetComponent<DetectForWarning>().CreateBigBallWarning();
        }      
    }

    private int countBigBallTimeCouroutine = 0;
    private bool isGrowing;

    public IEnumerator BigBallTime()
    {
        countBigBallTimeCouroutine++;

        if (countBigBallTimeCouroutine == 1)
        {
            isGrowing = true;

            ballScaleBase = scaleSteps[actualStep];

            //vfx de grossisssmenet
            

            vc.GetComponent<PhotonView>().RPC("RPC_PlayBig", RpcTarget.All);

            vc.GetComponent<PhotonView>().RPC("RPC_PlayStartBig", RpcTarget.All);


            yield return new WaitForSeconds(0.6f);

            //sfx 
            am.PlaySoundEffect(am.soundEffectConfiguration.powerUp.fmodEventPUGrossissment);
            

            transform.localScale = new Vector3(ballScalePU / 3f, ballScalePU / 3f, ballScalePU / 3f);
            yield return new WaitForSeconds(0.1f);
            transform.localScale = new Vector3(ballScalePU / 2f, ballScalePU / 2f, ballScalePU / 2f);
            yield return new WaitForSeconds(0.1f);
            transform.localScale = new Vector3(ballScalePU / 1.5f, ballScalePU / 1.5f, ballScalePU / 1.5f);
            yield return new WaitForSeconds(0.1f);
            transform.localScale = new Vector3(ballScalePU, ballScalePU, ballScalePU);

            isGrowing = false;
        }
        
        //attente durée du power up
        yield return new WaitForSeconds(timeBigBall);

        //LABOULE DéGrossit
        countBigBallTimeCouroutine--;

        if (countBigBallTimeCouroutine <= 0)
        {
            if (isBigBall)
            {
                StartCoroutine(ShrinkDuringBigBall());
            }
        }
    }

    private int countShrinkDuringBigBallCouroutine = 0;

    public IEnumerator ShrinkDuringBigBall()
    {
        if (!isGrowing)
        {

            countShrinkDuringBigBallCouroutine++;

            if (countShrinkDuringBigBallCouroutine == 1)
            {
                vc.GetComponent<PhotonView>().RPC("RPC_PlayStartBig", RpcTarget.All);

                vc.GetComponent<PhotonView>().RPC("RPC_StopBig", RpcTarget.All);

                //sfx
                am.PlaySoundEffect(am.soundEffectConfiguration.powerUp.fmodEventPUDegrossissment);

                yield return new WaitForSeconds(0.1f);

                transform.localScale = new Vector3(ballScaleBase * 3, ballScaleBase * 3, ballScaleBase * 3);

                yield return new WaitForSeconds(0.1f);

                transform.localScale = new Vector3(ballScaleBase * 2, ballScaleBase * 2, ballScaleBase * 2);

                yield return new WaitForSeconds(0.1f);

                transform.localScale = new Vector3(ballScaleBase, ballScaleBase, ballScaleBase);
            }


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            countShrinkDuringBigBallCouroutine--;

            if (countShrinkDuringBigBallCouroutine <= 0)
            {
                deplacementSpeed = speedSteps[actualStep];

                isBigBall = false;

                pv.RPC("RPC_MajBigBall", RpcTarget.All, isBigBall);

                Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleNoTrigger, false);
                Physics.IgnoreLayerCollision(lyrPlayer, lyrObstacleTrigger, true);

                am.musicConfiguration.parametersMusicRace.powerUpBig.value = 0;
            }
        }
        else
        {
            yield return new WaitForSeconds(0.2f);

            StartCoroutine(ShrinkDuringBigBall());
        }
    }

    /// <summary>
    /// permet de réduire d'une taille le joueur
    /// </summary>
    public void NewShrink()
    {
        if (actualStep > 0)
        {
            //Le joueur repasse au palier de taille inférieur
            actualStep--;

            //sfx
            am.PlaySoundEffect(am.soundEffectConfiguration.collision.fmodEventTailleMoins, this.gameObject);

            //Changement de taille
            transform.localScale = new Vector3(scaleSteps[actualStep], scaleSteps[actualStep], scaleSteps[actualStep]);

            ChangeSpeedBySize();

            //Mise à jour de la taille calculée
            calculateScale = transform.localScale.x;
        }
        else if (actualStep == 0)
        {
            //Changement de taille
            transform.localScale = new Vector3(scaleSteps[actualStep], scaleSteps[actualStep], scaleSteps[actualStep]);

            //Mise à jour de la taille calculée
            calculateScale = transform.localScale.x;
        }
    }

    /// <summary>
    /// Réduit totalement les tailles du joueurs
    /// </summary>
    public void ResetShrink()
    {
            //Le joueur repasse au palier de taille inférieur
            actualStep = 0;

            //sfx
            am.PlaySoundEffect(am.soundEffectConfiguration.collision.fmodEventTailleMoins, this.gameObject);

            //Changement de taille
            transform.localScale = new Vector3(scaleSteps[actualStep], scaleSteps[actualStep], scaleSteps[actualStep]);

            ChangeSpeedBySize();

            //Mise à jour de la taille calculée
            calculateScale = transform.localScale.x;
       
    }

    #endregion

    //Magma
    #region Magma

    int magmaCoroutineCount = 0;

    public IEnumerator MagmaTime()
    {
        if (isFrozen)
        {
            ResetFrozen();
        }

        magmaCoroutineCount++;

        if (magmaCoroutineCount <= 1)
        {
            am.soundEffectConfiguration.InstanceMagma = am.CreateFMODInstance(am.soundEffectConfiguration.InstanceMagma,1f,transform.position);
            am.musicConfiguration.parametersMusicRace.magma.value = 1;

            isMagma = true;

            boucleMagmaSound = isMagma;
            StartCoroutine(UpdateMagmaSoundPosition());

            pv.RPC("RPC_MajMagma", RpcTarget.All, isMagma);
            vc.GetComponent<PhotonView>().RPC("RPC_PlayMagma", RpcTarget.All, isMagma);
            pv.RPC("RPC_SyncMagmaSound", RpcTarget.Others, isMagma);

            myCamera.GetComponent<CameraController>().ChangeVignette(new Color(255, 90, 0), 0.025f);
        }

        yield return new WaitForSeconds(timeMagma);

        magmaCoroutineCount--;

        if (magmaCoroutineCount <= 0)
        {
            am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
            am.musicConfiguration.parametersMusicRace.magma.value = 0;

            isMagma = false;

            boucleMagmaSound = isMagma;

            pv.RPC("RPC_MajMagma", RpcTarget.All, isMagma);
            vc.GetComponent<PhotonView>().RPC("RPC_PlayMagma", RpcTarget.All, isMagma);
            pv.RPC("RPC_SyncMagmaSound", RpcTarget.Others, isMagma);

            myCamera.GetComponent<CameraController>().ChangeVignette(new Color(255, 90, 0), 0f);
        }
    }


    bool boucleMagmaSound = false;

    private IEnumerator UpdateMagmaSoundPosition()
    {
        yield return new WaitForSeconds(0.2f);

        am.UpdateFMODInstance(am.soundEffectConfiguration.InstanceMagma, transform.position);

        if (boucleMagmaSound)
            StartCoroutine(UpdateMagmaSoundPosition());
    }

    [PunRPC]
    void RPC_MajMagma(bool magmaOrNot)
    {
        isMagma = magmaOrNot;
        
        if (magmaOrNot)
        {
            FindObjectOfType<GameManager>().myPlController.GetComponent<DetectForWarning>().CreateMagmaWarning();
        }
    }

    [PunRPC]
    void RPC_SyncMagmaSound(bool magmaOrNot)
    {
        if (magmaOrNot)
        {
            am.soundEffectConfiguration.InstanceMagma = am.CreateFMODInstance(am.soundEffectConfiguration.InstanceMagma, 1f, transform.position);

            boucleMagmaSound = true;

            StartCoroutine(UpdateMagmaSoundPosition());
        }
        else
        {
            boucleMagmaSound = false;
            am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
        }
    }

    #endregion

    //Fonctions de collisions
    #region Collisions
    private int countColliders = 0;
    private Vector3 groundContactPointNormal = Vector3.zero;
    [HideInInspector]
    public List<string> tagsInContact = new List<string>();

    //Lorsque le joueur entre en collision
    private void OnCollisionEnter(Collision collision)
    {
        //Ajout dans la liste du tag de cette collision
        tagsInContact.Add(collision.gameObject.tag);

        //Si la collision est avec un tag valide
        if (collision.gameObject.tag == "Sol" || collision.gameObject.tag == "SolGlace")
        {

            #region Son de la boule qui roule
            switch (collision.gameObject.tag)
            {
                case "Sol":

                    am.SetSnowSlideType(0);

                    break;

                case "SolGlace":

                    am.SetSnowSlideType(1);

                    break;
            }

            #endregion

            //On compte qu'il est en collision avec le sol
            countColliders++;
            //On permet au joueur de sauter
            canJump = true;

            //Lancement d'un timer (pour le Landing)
            if (fallingTime >= fallingDelay)
            {
                vc.InstantiateParticles(vc.landHitExplosion.goVfxLanding, collision.contacts[0].point);
            }

            if (fallingTime >= fallingDelay / 2)
            {
                float unlerpFalling = Mathf.InverseLerp(0.2f, fallingDelay, Mathf.Clamp(fallingTime, 0.2f, fallingDelay));

                StartCoroutine(GetComponent<DeformMesh>().LandingDeform(unlerpFalling, collision.GetContact(0).normal));
                //GetComponent<DeformMesh>().LandingDeform(unlerpFalling, collision.GetContact(0).normal);

                if (pv.IsMine)
                {
                    if (isBigBall)
                        myCamera.GetComponent<CameraController>().Shake(shakeTime_BigBall_Fall, shakePower_BigBall_Fall);

                    am.PlaySoundEffect(am.soundEffectConfiguration.collision.fmodEventCollisionGeneric, this.gameObject, "", 0, unlerpFalling);
                }
            }
            else
            {
                GetComponent<DeformMesh>().CheckParentUp();
            }

            fallingTime = 0;
            //am.ambianceConfiguration.wind = 0.1f;
        }
        else
        {
            if (pv.IsMine)
            {
                float unlerpSpeed = Mathf.InverseLerp(0.1f, speedSteps[actualStep], velocityWithoutGravity.magnitude);

                am.PlaySoundEffect(am.soundEffectConfiguration.collision.fmodEventCollisionGeneric, this.gameObject, "", 0, unlerpSpeed);
            }
        }

        //Si la collision se fait avec un autre joueur
        if (collision.gameObject.tag == "Player")
        {
            if (pv.IsMine)
            {
                if (canCollisionVoice)
                {
                    StartCoroutine(CooldownCollisionVoice(am.soundEffectConfiguration.voix.cooldownCollision));
                    am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCollision, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                }

                rumble.RumbleConstant(.1f, .1f, .1f);  

                //Récupère l'autre joueur
                PlayerController otherPlayer = collision.gameObject.GetComponent<PlayerController>();

                //VFX hit
                vc.InstantiateHit(collision.contacts[0].point, otherPlayer.gameObject);

                //VFX explo
                vc.InstantiateParticles(vc.landHitExplosion.goVfxExplo, collision.contacts[0].point);

                PlayerContact(collision.contacts[0].point, otherPlayer.transform.localScale.x, transform.localScale.x, playerContactForceMinMax);

                myCamera.GetComponent<CameraController>().Shake(shakeTime_PlayerCollisions, shakePower_PlayerCollisions);

                if (isFrozen)
                {
                    ResetFrozen();
                }
            }
        }
    }

    #region Fonction de contact entre les joueurs
    public void PlayerContact(Vector3 from, float otherPlayerScaleX, float myPlayerScaleX, Vector2 repulseForceMinMax)
    {
        //Calcule de la différence
        float diffPlayerScale = (otherPlayerScaleX - myPlayerScaleX); //-7 > 7
        //Remise de la différence de 0 à 1
        float unlerpDiffPlayerScale = Mathf.InverseLerp(-maxScale + 1, maxScale - 1, diffPlayerScale);

        float lerpRepulseForce = 0;
        //float repulseForce = unlerpDiffPlayerScale * repulseForceMinMax.x;

        //Si mon joueur est plus petit que l'adversaire
        if (myPlayerScaleX < otherPlayerScaleX)
        {
            //Calcule de la force en fonction de la différence
            lerpRepulseForce = Mathf.Lerp(repulseForceMinMax.x, repulseForceMinMax.y, unlerpDiffPlayerScale);
            float repulseForce = unlerpDiffPlayerScale * repulseForceMinMax.y;

            //Ajoute la force d'explosion
            rb.AddExplosionForce(repulseForce, from, otherPlayerScaleX + 1);
        }
        else
        {
            //Calcule de la force en fonction de la différence
            lerpRepulseForce = Mathf.Lerp(repulseForceMinMax.y, repulseForceMinMax.x, unlerpDiffPlayerScale);
            float repulseForce = unlerpDiffPlayerScale * repulseForceMinMax.x;

            //Ajoute la force d'explosion
            rb.AddExplosionForce(repulseForce, from, otherPlayerScaleX + 1);
        }
    }
    #endregion

    //Lorsque le joueur sort de la collision
    private void OnCollisionExit(Collision collision)
    {
        //On retire de la liste le tag
        tagsInContact.Remove(collision.gameObject.tag);

        //Si la collision est avec un tag valide
        if (collision.gameObject.tag == "Sol" || collision.gameObject.tag == "SolGlace")
        {

            //On retire la collision du compte
            StartCoroutine(RemoveCollider());

            if (!tagsInContact.Contains("Sol") && !tagsInContact.Contains("SolGlace"))
            {
                //launch timer 
                StartCoroutine(Timer(fallingTime, true));
            }
        }
    }


    //couroutine qui lance un timer pour le landing
    public IEnumerator Timer(float timer, bool firstTime)
    {
        if (firstTime)
        {
            fallingTime = 0;
        }

        fallingTime = timer + Time.deltaTime;


        yield return new WaitForEndOfFrame();

        if (!tagsInContact.Contains("Sol") && !tagsInContact.Contains("SolGlace"))
        {
            //Scale du volume du vent
            /*if (pv.IsMine)
            {
                float volumeOfWind = Mathf.InverseLerp(0f, fallingDelay, fallingTime);
                am.ambianceConfiguration.wind = Mathf.Clamp(volumeOfWind, 0.1f, 1);
            }*/


            //Lancement du timer pour le Landing
            StartCoroutine(Timer(fallingTime, false));
        }
        else
        {
            fallingTime = 0;
        }
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Sol" || collision.gameObject.tag == "SolGlace")
        {
            groundContactPointNormal = collision.GetContact(0).normal;
        }
    }

    //Retire 1 au compte de collision
    public IEnumerator RemoveCollider()
    {
        //Retire une collision après avoir attendu un certain temps
        yield return new WaitForSecondsRealtime(0.12f);
        countColliders--;
    }

    //Vérifie que le joueur est sur le sol
    public bool IsGrounded()
    {
        //Vérifie que le compte est supérieur à 0
        return countColliders > 0;
    }
    #endregion

    //Fonctions de triggers
    #region Triggers
    public void OnTriggerEnter(Collider other)
    {
        if (!pv.IsMine)
        {
            return;
        }

        if (other.gameObject.tag == "Turbo")
        {
            Turbo(turboSpeedMax, "TurboSol", other.transform);
        }

        if (other.gameObject.tag == "TurboTremplin")
        {
            Turbo(turboTremplinSpeedMax, "Tremplin", other.transform);
        }

        if (other.gameObject.tag == "Aspiration")
        {
            if (other.gameObject.transform.parent.GetComponent<Orientator>().rb != this.rb)
                isInAspiration = true;
        }
        //Trigger avec la zone de fondue
        if (other.gameObject.tag == "FondueTrigger" && other.GetComponentInParent<ProjectilePowerUp>().fondueCanStopMe)
        {
            isInFondue = true;
            state = States.SlowedInFondu;

            am.PlaySoundEffect(am.soundEffectConfiguration.powerUp.fmodEventFondue);

            pv.RPC("RPC_FondueSound", RpcTarget.Others, transform.position.x, transform.position.y, transform.position.z);

            SlowSpeedWithFondue();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (!pv.IsMine)
        {
            return;
        }

        if (other.gameObject.tag == "Aspiration")
        {
            if (other.gameObject.transform.parent.GetComponent<Orientator>().rb != this.rb)
                isInAspiration = false;
        }

        //Trigger avec la zone de fondue
        if (other.gameObject.tag == "FondueTrigger")
        {
            isInFondue = false;
            SlowSpeedWithFondue();
        }

        timeBlocked = 0;
    }

    public void OnTriggerStay(Collider other)
    {
        //Trigger avec la zone de fondue
        if (other.gameObject.tag == "FondueTrigger" && other.GetComponentInParent<ProjectilePowerUp>().fondueCanStopMe == false)
        {
            isInFondue = false;
            SlowSpeedWithFondue();
        }
    }

    public void Turbo(float speedMaxTurbo, string playSound, Transform collidedObject = null)
    {
        isBoosted = true;

        if (playSound == "TurboSol")
        {
            if (canCelebration)
            {
                StartCoroutine(CooldownCelebration(am.soundEffectConfiguration.voix.cooldownCelebration));

                //myCamera.GetComponent<CameraController>().ChangeVignette(new Color(0, 240, 255), 0.025f);

                am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCelebration, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                pv.RPC("RPC_Celebration", RpcTarget.Others, transform.position.x, transform.position.y, transform.position.z, GetComponent<PersonnalisationController>().voiceId);
            }

            am.PlaySoundEffect(am.soundEffectConfiguration.turbo.fmodEventTurbo);

        }
        else if (playSound == "TurboPowerup")
        {
            if (canCelebration)
            {
                StartCoroutine(CooldownCelebration(am.soundEffectConfiguration.voix.cooldownCelebration));

                am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixCelebration, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                pv.RPC("RPC_Celebration", RpcTarget.Others, transform.position.x, transform.position.y, transform.position.z, GetComponent<PersonnalisationController>().voiceId);
            }

            am.PlaySoundEffect(am.soundEffectConfiguration.turbo.fmodEventTurboPowerUp);

        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (speedMaxTurbo > deplacementSpeed)
        {
            deplacementSpeed = speedMaxTurbo;
        }

        if (collidedObject == null)
            rb.velocity = rb.velocity.normalized * speedMaxTurbo;
        else
            rb.velocity = new Vector3(rb.velocity.normalized.x, collidedObject.forward.y, rb.velocity.normalized.z) * speedMaxTurbo;

        StartCoroutine(vc.ChangeColorVFXCamSpeed());

        vc.GetComponent<PhotonView>().RPC("SpeedMax", RpcTarget.All);

        StartCoroutine(TurboTime());
    }

    public IEnumerator TurboTime()
    {
        yield return new WaitForSeconds(timeTurbo);

        isBoosted = false;
        turboEnd = true;
    }

    #region CooldownVoice
    private bool canCelebration = true;

    private IEnumerator CooldownCelebration(float time)
    {
        canCelebration = false;

        yield return new WaitForSeconds(time);

        canCelebration = true;
    }

    private bool canCollisionVoice = true;

    private IEnumerator CooldownCollisionVoice(float time)
    {
        canCollisionVoice = false;

        yield return new WaitForSeconds(time);

        canCollisionVoice = true;
    }
    #endregion

    [PunRPC]
    void RPC_Celebration(float x, float y, float z, int voiceId)
    {
        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.voix.fmodEventTVoixCelebration, new Vector3(x,y,z), "ChoixVoix", voiceId);
    }

    [PunRPC]
    private void RPC_FondueSound(float x, float y, float z)
    {
        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.powerUp.fmodEventFondue,new Vector3(x,y,z));
    }
    #endregion

    //RPC PHOTON
    #region RPC
    [PunRPC]
    public void RPC_UpdateReadyClassement(bool ready)
    {
        iAmReadyForClassement = true;
    }
    #endregion

    //Fonctions Analytics
    #region Analytics
    /// <summary>
    /// Test le changement de statut du joueur
    /// </summary>
    /// <returns></returns>
    private bool TestStateChange()
    {
        bool change = false;

        if (previousState != state)
        {
            change = true;
        }

        return change;
    }

    /// <summary>
    /// Change l'acien statut du joueur
    /// </summary>
    private void ChangePreviousState()
    {
        previousState = state;
    }

    /// <summary>
    /// Envoie les donnée de statut du joueur
    /// </summary>
    private void SendStateAnalytics()
    {
        ana.Ana_PlayerState(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"state", state}
                    });
    }

    //Permet d'envoyer une info une seule fois
    private bool sendOnce = true;

    /// <summary>
    /// Calcule les analytics d'aspiration
    /// </summary>
    private void CalculateAnalyticsAspiration()
    {
        if (GetComponent<PositionAndTimerController>().finishRace)
        {
            if (sendOnce)
            {
                sendOnce = false;

                Debug.Log("AspirationTotal PC " + aspirationTime_total);

                StopAnalyticsAspiration();

                ana.Ana_Aspiration_Total(aspirationTime_total);
            }
        }
        else
        {
            stopOnce = true;

            aspirationTime_total += fdt;
            aspirationTime_unique += fdt;
        }
    }

    //Permet de ne faire qu'une fois l'action
    private bool stopOnce = true;
    /// <summary>
    /// Permet d'envoyer la durée de l'aspiration
    /// </summary>
    private void StopAnalyticsAspiration()
    {
        if (stopOnce)
        {
            stopOnce = false;

            if (aspirationTime_unique > 0)
            {
                ana.Ana_Aspiration_Unique(aspirationTime_unique);
                aspirationTime_unique = 0;
            }
        }
    }
    #endregion
}
