﻿using UnityEngine;

/*
 * Ce script permet d'avoir un objet qui suit le joueur sans etre en enfant
 * il doit etre placé sur l'objet du meme nom (en enfant du joueur)
 */

public class PlayerPositionAndRotationIndicator : MonoBehaviour
{
    [Header("Taking")]
    [SerializeField]
    private bool takePosition = true;
    [SerializeField]
    private bool takeRotation = false;
    [SerializeField]
    private bool takeScale = false;

    [Header("Offset")]
    [SerializeField]
    private bool useOffsetPosition = false;
    [SerializeField]
    private bool useOffsetRotation = false;
    [SerializeField]
    private bool useOffsetScale = false;

    [Header("IsForCam")]
    [SerializeField]
    private bool isForCam;

    private Vector3 offsetPos = Vector3.one;
    private Quaternion offsetRot = Quaternion.identity;
    private Vector3 offsetScale = Vector3.one;

    //Le player
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        //Récupération du player
        player = transform.parent.gameObject;

        if (useOffsetPosition)
            offsetPos = transform.localPosition;

        if (useOffsetRotation)
            offsetRot = transform.localRotation;

        if (useOffsetScale)
            offsetScale = transform.localScale;

        //On déparente du player
        transform.parent = null;

        if (takePosition)
            //On met la position de l'objet sur le player
            transform.position = player.transform.position;

        if (takeRotation)
            //On set la rotation de l'objet comme étant celle du joueur
            transform.rotation = player.transform.rotation;

        if (takeScale)
            transform.localScale = new Vector3(player.transform.localScale.x * offsetScale.x, player.transform.localScale.y * offsetScale.y, player.transform.localScale.z * offsetScale.z);

        //update du nom de l'objet
        gameObject.name = gameObject.name + "_" + player.GetComponent<PersonnalisationController>().nicknameText.text;
    }

    private void Update()
    {
        if (isForCam)
        {
            return;
        }

        //Si le joueur lié a ce script n'existe plus
        if (player == null)
        {
            //Destruction de cet objet
            Destroy(this.gameObject);
        }
        else
        {
            if (takePosition)
            {
                if (useOffsetPosition)
                {
                    //On met la position de l'objet sur le player
                    transform.position = player.transform.position + offsetPos * player.transform.localScale.x;
                }
                else
                {
                    //On met la position de l'objet sur le player
                    transform.position = player.transform.position;
                }
            }

            if (takeRotation)
            {
                if (useOffsetRotation)
                {
                    //On set la rotation de l'objet comme étant celle du joueur
                    transform.rotation = player.transform.rotation * offsetRot;
                }
                else
                {
                    //On set la rotation de l'objet comme étant celle du joueur
                    transform.rotation = player.transform.rotation;
                }
            }

            if (takeScale)
            {
                if (useOffsetScale)
                {
                    transform.localScale = new Vector3(player.transform.localScale.x * offsetScale.x, player.transform.localScale.y * offsetScale.y, player.transform.localScale.z * offsetScale.z);
                }
                else
                {
                    transform.localScale = player.transform.localScale;
                }
            }


        }
    }

    void FixedUpdate()
    {
        if (!isForCam)
        {
            return;
        }

        //Si le joueur lié a ce script n'existe plus
        if (player == null)
        {
            //Destruction de cet objet
            Destroy(this.gameObject);
        }
        else
        {
            if (takePosition)
            {
                if (useOffsetPosition)
                {
                    //On met la position de l'objet sur le player
                    transform.position = player.transform.position + offsetPos * player.transform.localScale.x;
                }
                else
                {
                    //On met la position de l'objet sur le player
                    transform.position = player.transform.position;
                }
            }

            if (takeRotation)
            {
                if (useOffsetRotation)
                {
                    //On set la rotation de l'objet comme étant celle du joueur
                    transform.rotation = player.transform.rotation * offsetRot;
                }
                else
                {
                    //On set la rotation de l'objet comme étant celle du joueur
                    transform.rotation = player.transform.rotation;
                }
            }

            if (takeScale)
            {
                if (useOffsetScale)
                {
                    transform.localScale = new Vector3(player.transform.localScale.x * offsetScale.x, player.transform.localScale.y * offsetScale.y, player.transform.localScale.z * offsetScale.z);
                }
                else
                {
                    transform.localScale = player.transform.localScale;
                }
            }


        }
    }
}
