﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

using Photon.Pun;

/*
 * Calcule la position du joueur dans la course grâce aux waypoints
 * 
 * Engage le calcul de position du joueur par rapport aux autres joueurs.
 * 
 * 
 * Le script se trouve sur le joueur.
 * 
 */
public class PositionAndTimerController : MonoBehaviour
{
    #region Variables
    //Si le joueur est dans le leaderboard
    [HideInInspector]
    public bool isInLeaderboard = false;

    [Tooltip("Distance parcourue en waypoints par le joueur (= nombre de waypoints + waypoint en cours)")]
    public float distValue;

    [Tooltip("Distance parcourue en metres par le joueur (= distvalue * distance entre les waypoints franchis)")]
    public float myDistance;

    [Tooltip("Distances entre chaque Waypoints")]
    public List<float> waypointsDistances;

    [Tooltip("Distance avec le joueur 1er")]
    public float distWithFirst;

    [SerializeField]
    [Tooltip("Valeur du prochain waypoint à franchir")]
    public int nextWaypoint = 1;

    [Tooltip("Liste des waypoints une fois rangée (voir WaypointsTracker)")]
    public List<GameObject> myWaypoints;

    //somme de tt la distance entre tt les waypoints
    public float distanceAllWaypoints;

    //Si le joueur a terminé la course
    [HideInInspector]
    public bool finishRace = false;

    //Distance entre le waypoint actuel et le prochain
    private float distWaypoints;
    //Distance entre le joueur et le prochain waypoint (donnée entre 0 et 1)
    private float distNextClamp;

    //Temps entre 2 calculs de position (voir GameManager)
    private float MajClassTime;

    //Mon photonView
    private PhotonView pv;
    //Le gameManager
    private GameManager gm;

    //Le texte du debug de timer en jeu
    //private Text timeCounter;

    //Si le timer est en cours
    public bool timerIsActive = false;

    //Le temps écoulé depuis le début de la course
    public float elapsedTime;

    //L'heure de départ d'après photon
    public float photonTimeBeginning;

    //Le temps passé convertit en minutes/secondes/...
    private TimeSpan timePlaying;

    private AnalyticsDB ana;
    #endregion

    #region DATA
    [SerializeField]
    [Header("DEBUG")]
    private string pathToWrite = @"C:\Users\Florian\Desktop\";
    [SerializeField]
    private bool enableWriting = false;

    PositionAndTimerController[] patcs;
    private int numberOfPlayers;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        //On récupère le photonView
        pv = GetComponent<PhotonView>();
    }

    void Start()
    {
        //On récupère le gameManager
        gm = FindObjectOfType<GameManager>();
        //on find  analytics
        ana = FindObjectOfType<AnalyticsDB>();

        //On récupère tous les waypoints qui ont été mis dans l'ordre
        myWaypoints = FindObjectOfType<WaypointsTracker>().waypointsInOrder;

        for (int i = 0; i < myWaypoints.Count - 1 ; i++)
        {
            waypointsDistances.Add(Vector3.Distance(myWaypoints[i + 1].transform.position, myWaypoints[i].transform.position));
            distanceAllWaypoints += Vector3.Distance(myWaypoints[i + 1].transform.position, myWaypoints[i].transform.position);
        }

        //On récupère le temps de mise à jour des infos de classement (temps entre 2 calculs)
        MajClassTime = gm.MajRankTime;

        //On envoie l'information au GameManager que mon joueur est celui sur lequel est le script PositionInRace
        if (pv.IsMine)
        {
            //Mise à jour de mon player controller dans le gamemanager
            gm.myPlController = GetComponent<PlayerController>();
            //Mise à jour de ce script dans le gameManager
            gm.myPositionAndTimerController = this;

            /*//Lancement de la coroutine de calcul de position
            StartCoroutine(MajDistValue());*/

            //Initialisation du timer
            //Récupère le texte de débug du timer
            //timeCounter = GameObject.FindGameObjectWithTag("Chrono").GetComponent<Text>();

            //Set le texte de base
            //timeCounter.text = "Chrono: 00:00.00";
            //Met que le timer n'est pas actif
            timerIsActive = false;
        }

        //Lancement de la coroutine de calcul de position
        StartCoroutine(MajDistValue());

        if (pv.IsMine)
            ClearMyCSVdata();
    }

    //Calcul du nombre de waypoints franchis
    public void OnTriggerEnter(Collider other)
    {
        //Si le joueur passe dans un waypoint
        if (other.gameObject.tag == "Waypoint")
        {
            if (other.gameObject == myWaypoints[nextWaypoint] && other.gameObject != myWaypoints[myWaypoints.Count - 1])
            {
                nextWaypoint++;
            }
            else if (nextWaypoint < myWaypoints.Count - 1)
            {
                for (int i = nextWaypoint + 1; i < myWaypoints.Count - 1; i++)
                {
                    if (other.gameObject == myWaypoints[i])
                    {
                        nextWaypoint = i + 1;
                    }
                }
            }
            else if (other.gameObject == myWaypoints[myWaypoints.Count - 1])
            {
                //La distance est égale a la distance totale
                myDistance = distanceAllWaypoints;

                //Si c'est mon joueur et que je n'ai pas terminé la course
                if (!finishRace && pv.IsMine)
                {
                    FindObjectOfType<UiManager>().ActivateFinish(true);

                    //On dit qu'il termine la course
                    pv.RPC("RPC_SetFinishRace", RpcTarget.All);

                    //On met fin au timer
                    EndTimer(PhotonNetwork.ServerTimestamp - photonTimeBeginning);
                    Debug.Log("Serv : " + PhotonNetwork.ServerTimestamp + " / timeBegin : " + photonTimeBeginning + " / TIME : " + (PhotonNetwork.ServerTimestamp - photonTimeBeginning));

                    //On dit à la musique que la course est terminée
                    AudioManager am = FindObjectOfType<AudioManager>();
                    am.musicConfiguration.parametersMusicRace.toFinish.value = 1;
                    am.musicConfiguration.parametersMusicRace.progression.value = 0;
                    am.PlaySoundEffect(am.soundEffectConfiguration.endRace.arrivee);

                    //On lance la coroutine pour activer le bouton de retour au lobby
                    StartCoroutine(FindObjectOfType<UiManager>().ChangeViewEndRace());
                }
            }
        }

        if (other.gameObject.tag == "TriggerHideUI")
        {
            gm.uiManager.positionNumber.GetComponent<Animator>().SetBool("Bool", false);
        }
    }
    #endregion

    #region Methods
    //Coroutine de calcul de position
    public IEnumerator MajDistValue()
    {
        //Calcul de la distance entre le waypoint actuel et le prochain
        if (nextWaypoint > 0)
        {
            distWaypoints = Vector3.Distance(myWaypoints[nextWaypoint].transform.position, myWaypoints[nextWaypoint - 1].transform.position);
        }

        //Récupération du point du waypoint le plus proche du joueur 
        //afin de calculer la distance entre le joueur et ce waypoint, 
        //la valeur est ensuite réduite entre 0 et 1
        Collider[] allColliders = myWaypoints[nextWaypoint].GetComponents<Collider>();
        float distNext = Mathf.Infinity;
        for (int i = 0; i < allColliders.Length; i++)
        {
            Vector3 closePoint = allColliders[i].ClosestPoint(transform.position);

            if(Vector3.Distance(transform.position, closePoint) < distNext)
            {
                distNext = Vector3.Distance(transform.position, closePoint);
            }
        } 

        distNextClamp = (distWaypoints - distNext) / distWaypoints;

        //Calcul du positionnement du joueur dans la course étant le nombre de waypoints franchis plus la valeur entre 0 et 1 précédemment calculée
        distValue = (nextWaypoint - 1) + distNextClamp;

        //calcul de ma distance parcourue en metres
        if(distValue >= 1)
        {
            //myDistance = Mathf.A(waypointsDistances[0, Mathf.Floor(distValue)] ;
            float sum = 0;
            for (int i = 0; i < Mathf.Floor(distValue); i++)
            {
                sum += waypointsDistances[i];
            }

            myDistance = sum + (distValue - Mathf.Floor(distValue)) * waypointsDistances[nextWaypoint - 1];
        }
        else
        {
            myDistance = distValue * waypointsDistances[nextWaypoint - 1];
        }

        //Calcul de l'ecart de distance entre le premier et moi
        if(gm.Position != 1)
        {
            if (gm.classement.Count > 0)
                distWithFirst = gm.classement[0] - myDistance;
        }
        else
        {
            distWithFirst = 0;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////UNIQUEMENT POUR DU DEBUG --> A SUPRIMER DU JEU
        #region DATA
        if (pv.IsMine)
        {
            PlayerController[] pcs = FindObjectOfType<GameManager>().AllPlayers.ToArray() ;

            patcs = new PositionAndTimerController[pcs.Length];

            for (int i = 0; i < pcs.Length; i++)
            {
                patcs[i] = pcs[i].GetComponent<PositionAndTimerController>();
            }

            numberOfPlayers = patcs.Length;

            string messageToPrint = "";

            for (int i = 0; i < numberOfPlayers; i++)
            {
                if (patcs[i] != null)
                {

                    if (gm.classement.Count > 0)
                        messageToPrint += $"{Mathf.Clamp(Mathf.Floor(gm.classement[0] - patcs[i].myDistance),0,Mathf.Infinity)},";
                }
                else
                {
                    messageToPrint += $"{0},";
                }
            }

            if (!finishRace && gm.asFinishCountdown)
            {
                DistanceWithFirstInLogFile(messageToPrint);
            }
            else if (finishRace)
            {
                DistanceWithFirstInLogFile("AsFinishRace,");
            }
            else
            {
                DistanceWithFirstInLogFile("AsntStartRace,");
            }
        }
        #endregion

        //Envoie de cette valeur aux autres joueurs
        //pv.RPC("RPC_UpdateMyDistance", RpcTarget.Others, myDistance);

        //Attente avant le prochain calcul puis relancement de la coroutine
        yield return new WaitForSeconds(MajClassTime);
        if (nextWaypoint < myWaypoints.Count - 1)
        {
            StartCoroutine(MajDistValue());
        }                    
    }

    //Fait commencer le timer
    public void BeginTimer()
    {
        if (!pv.IsMine)
        {
            return;
        }

        //Rend le timer actif
        timerIsActive = true;

        //Si c'est le masterclient
        if (PhotonNetwork.IsMasterClient)
        {
            //Le master récupère le temps du serveur
            photonTimeBeginning = PhotonNetwork.ServerTimestamp;

            //Envoie de l'info à tous les joueurs
            pv.RPC("RPC_SendBeginTime", RpcTarget.OthersBuffered, photonTimeBeginning);
        }

        //Reset la variable du temps déjà écoulé
        elapsedTime = 0f;

        //Lance la coroutine pour update le timer
        //StartCoroutine(UpdateTimer());
    }

    //Fait terminer le timer
    public void EndTimer(float _elapsedTime)
    {
        if (!pv.IsMine)
        {
            return;
        }

        finishRace = true;

        //Set le timer non actif
        timerIsActive = false;

        //On remet le curseur visible et bougeable
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        elapsedTime = _elapsedTime;
        gm.myChrono = _elapsedTime;

        //Transmet le chrono de ce joueur à tous les joueurs
        pv.RPC("RPC_UpdateOtherTimerControllerElapsedTime", RpcTarget.Others, _elapsedTime);

        //Analytics
        ana.Ana_TimerRace(new Dictionary<string, object>
        {
            {"course_id", SceneManager.GetActiveScene().name },
            {"timer" , _elapsedTime}
            /*{"position",  }*/
        }); 

        //Envoie mon chrono au gameManager pour le trier
        gm.GetComponent<PhotonView>().RPC("RPC_SendMyChrono", RpcTarget.All, _elapsedTime);
    }

    /// <summary>
    /// Ecrit la distance entre le joueur actuel et le premier
    /// </summary>
    /// <param name="distance"></param>
    private void DistanceWithFirstInLogFile(string message)
    {
#if UNITY_EDITOR
        if (enableWriting)
        {
            using (StreamWriter logFileDistanceFromFirst = new StreamWriter(pathToWrite + $"Data_{PhotonNetwork.NickName}.csv", true))
            {
                logFileDistanceFromFirst.WriteLine($"{message}");
            }
        }
#endif
    }

    private void ClearMyCSVdata()
    {
#if UNITY_EDITOR
        if (enableWriting)
        {
            if (Directory.GetFiles(pathToWrite, $"Data_{PhotonNetwork.NickName}.csv").Length != 0)
            {
                File.Delete(pathToWrite + $"Data_{PhotonNetwork.NickName}.csv");
            }
        }
#endif
    }
    #endregion

    #region Coroutine

    //Update du chrono/elapsedTime
    /*private IEnumerator UpdateTimer()
    {
        //Si le timer est en cours
        if (timerIsActive)
        {
            //On update le temps
            elapsedTime = PhotonNetwork.ServerTimestamp - photonTimeBeginning;

            //Mise à jour du timePlaying avec elapsedTime
            timePlaying = TimeSpan.FromSeconds(elapsedTime / 1000);
            //Mise à jour du texte de temps
            string timePlayingStr = "" + timePlaying.ToString("mm':'ss'.'ff");
            //Mise à jour de l'objet de texte du temps
            timeCounter.text = timePlayingStr;

            //Debug.Log(timePlayingStr);

            //Attente d'une frame
            yield return new WaitForEndOfFrame();

            //Relance la coroutine
            StartCoroutine(UpdateTimer());
        }
    }*/
#endregion

    #region RPC
    //Fonction de l'envoie du positionnement du joueur dans la course
    [PunRPC]
    void RPC_UpdateMyDistance(float distanceValue)
    {
        //myDistance = distanceValue;
    }

    [PunRPC]
    void RPC_UpdateOtherTimerControllerElapsedTime(float chrono)
    {
        //On remet elapsedTime pour tous les TimerController
        elapsedTime = chrono;
    }

    [PunRPC]
    void RPC_SendBeginTime(float beginTime)
    {
        //Change la variable dans le script de chaque joueur
        gm.myPositionAndTimerController.photonTimeBeginning = beginTime;
    }

    [PunRPC]
    void RPC_SetFinishRace()
    {
        finishRace = true;
    }
#endregion
}
