﻿using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;

/*
 * 
 * Permet de générer un power up lorsque le cadeau est récupérée
 * Permet de déclencher les utilisations de powerUps
 * 
 * Le script est sur le joueur
 * 
 */

public class PowerUpController : MonoBehaviour
{
    #region Public variables
    //Est ce que le joueur a le cadeau
    //[HideInInspector]
    public bool hasGift;

    //Est ce que le joueur peut utiliser le cadeau (une fois le roulement dans l'ui terminé)
    //[HideInInspector]
    public bool canUseGift;
    #endregion

    #region Private variables

    //L'id du powerUp
    private int PowerUpRandom;
    //Permet de changer le power up
    private int debugSelectedPowerUp = 0;

    [Tooltip("Le nombre de power up dans le jeu")]
    [SerializeField]
    private int maxPowerUpCount = 7;

    public List<PowerUpClass> pucs;

    public List<GameObject> confWaveGoList;
    public List<GameObject> FondueGoList;

    public GameObject aimLine;

    private string[] datas;

    [SerializeField]
    private int whichProbas;

    //Inputmanager
    private InputManager im;
    //PlayerController
    private PlayerController pc;
    //PhotonView
    private PhotonView pv;
    //AudioManager
    private AudioManager am;
    //GameManager
    private GameManager gm;
    //VFX Controller
    private VfxController vc;
    //Position & Timer Controller
    private PositionAndTimerController ptc;
    //annalytics
    private AnalyticsDB ana;
    #endregion

    #region Unity Methods
    void Start()
    {
        //On récupère l'InputManager
        im = FindObjectOfType<InputManager>();
        //On récupère le PlayerController
        pc = GetComponent<PlayerController>();
        //On récupère le PhotonView
        pv = GetComponent<PhotonView>();
        //On récupère le GameManager
        gm = FindObjectOfType<GameManager>();
        //On récupère le vfx controller
        vc = GetComponent<VfxController>();
        //On récupère le PositionAndTimerController
        ptc = GetComponent<PositionAndTimerController>();
        //On récup les analytics
        ana = FindObjectOfType<AnalyticsDB>();

        //On Load le CSV de proba de récupération des power up
        TextAsset probaPowerUp = Resources.Load<TextAsset>("CSV/probaPowerUp");

        //On split le CSV en lignes a chaque retour à la ligne (rappel : ligne = horizontal)
        datas = probaPowerUp.text.Split(new char[] { '\n' });

        for (int i = 1; i < datas.Length - 1; i++)
        {
            //Split des colonnes (Rappel : colonne = vertical) + On relie les variables à chaque colonne
            string[] row = datas[i].Split(new char[] { ',' });

            //Nouvelle classe powerUpClass
            PowerUpClass puc = new PowerUpClass();

            //On lui ajoute les bonnes valeurs
            for (int j = 1; j < maxPowerUpCount + 1; j++)
            {
                int newProba;
                int.TryParse(row[j], out newProba);
                puc.probasPu.Add(newProba);
            }

            //On ajoute cette classe dans la liste de toutes les classes
            pucs.Add(puc);
        }

        foreach (GameObject go in confWaveGoList)
        {
            go.transform.position = go.transform.position - Vector3.up * 10;
        }
        foreach (GameObject go in FondueGoList)
        {
            go.transform.position = go.transform.position - Vector3.up * 10;
        }
    }

    void Update()
    {
        /*//Si le joueur n'est pas le mien, j'y touche po
        if (!pv.IsMine)
        {
            Destroy(this);
            return;
        }*/

        //Si on appui sur la touche de debug
        if (im.inputDebugPowerUp)
        {
            //On change le power up obtenu fictif
            ChangePowerUpByDebug();
        }

        //Si j'appuie sur la touche de powerUp
        if (im.inputPowerUpForward)
        {
            //Lance le powerup
            LaunchPowerUp(PowerUpRandom, false);
        }

        //Si j'appuie sur la touche de powerUp
        if (im.inputPowerUpBack)
        {
            //Lance le powerup
            LaunchPowerUp(PowerUpRandom, true);
        }
    }

    // Si le joueur entre en trigger avec un objet
    public void OnTriggerEnter(Collider other)
    {
        /*// Si cet objet est un cadeau
        if (other.gameObject.tag == "Gift")
        {
            // Vérification que c'est bien notre joueur qui a activé la collision
            if (pv.IsMine)
            {
                //Si l'audiomanager est null
                if (am == null)
                {
                    //Trouve l'audiomanager
                    am = FindObjectOfType<AudioManager>();
                }

                //Fait le son de récupération de cadeau
                am.PlaySoundEffect(am.soundEffectParameters.fmodEventGift);

                // Fonction de destruction du cadeau
                other.gameObject.GetComponent<PhotonView>().RPC("TakeGift", RpcTarget.All);

                //Fonction de choix du power up
                NewPowerUp();
            }

            //Activation vfx confetti + collect
            vc.PlayVFX(vc.cadeau.goVfxCollecte);
            vc.InstantiateParticles(vc.cadeau.goVfxConfetti, transform.position);
        }*/

        //CADEAU V2
        // Si cet objet est un cadeau
        if (other.gameObject.tag == "Gift")
        {
            // Vérification que c'est bien notre joueur qui a activé la collision
            if (pv.IsMine)
            {
                //Si l'audiomanager est null
                if (am == null)
                {
                    //Trouve l'audiomanager
                    am = FindObjectOfType<AudioManager>();
                }

                //Fait le son de récupération de cadeau
                am.PlaySoundEffect(am.soundEffectConfiguration.cadeaux.fmodEventGift);

                // Fonction de destruction du cadeau
                other.gameObject.GetComponent<PhotonView>().RPC("TakeGift", RpcTarget.All);

                StartCoroutine(FindObjectOfType<UiManager>().RandomGiftOpen());

                //Fonction de choix du power up
                NewPowerUp();
            }

            //Activation vfx confetti + collect
            vc.PlayVFX(vc.cadeau.goVfxCollecte);
            vc.InstantiateParticles(vc.cadeau.goVfxConfetti, transform.position);
        }
    }
    #endregion

    #region Methods

    #region UsePowerUp
    private void LaunchPowerUp(int Id, bool isBack)
    {
        //S'il n'y a pas d'AudioManager
        if (am == null)
        {
            //On récupère l'AudioManager
            am = FindObjectOfType<AudioManager>();
        }

        if (Id == 0)
        {
            if (canTaunt)
            {
                StartCoroutine(CooldownTaunt(am.soundEffectConfiguration.voix.cooldownTaunt));

                //Son de taunt
                am.PlaySoundEffect(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, this.gameObject, "ChoixVoix", GetComponent<PersonnalisationController>().voiceId);
                pv.RPC("RPC_Taunt", RpcTarget.Others, transform.position.x, transform.position.y, transform.position.z, GetComponent<PersonnalisationController>().voiceId);
            }
        }


        //Change en fonction du powerUp
        if (hasGift && canUseGift)
        {
            //Déclanche le son de déclenchement du cadeau
            am.PlaySoundEffect(am.soundEffectConfiguration.cadeaux.fmodEventUseItem);

            //Reset du booléen de possibilité d'utiliser le cadeau
            canUseGift = false;
            hasGift = false;

            switch (Id)
            {
                //Si c'est le turbo
                case 1:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    Turbo();
                    break;

                //Si c'est le Bigball
                case 2:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    BigBall();
                    break;

                //Si c'est le Glacon
                case 3:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    Glacon();                    
                    break;

                //Si c'est le Magma
                case 4:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    Magma();
                    break;
                //Si c'est l'onde de Confusion
                case 5:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    ConfusionWave(isBack);
                    break;

                //Si c'est la fondue
                case 6:
                    ana.Ana_PowerUpTaken(new Dictionary<string, object>
                    {
                        {"date", (System.DateTime.Now).ToString()},
                        {"course_id", SceneManager.GetActiveScene().name},
                        {"poweruptaken", Id}
                    });
                    Fondue(isBack);
                    break;
            }

            //Remet l'ID de powerUp à 0
            PowerUpRandom = 0;

            //Change le powerUp
            UiManager uiManager = FindObjectOfType<UiManager>();

            if (uiManager != null)
                StartCoroutine(FindObjectOfType<UiManager>().OnChangePowerUp(PowerUpRandom));
        }
    }

    //Fonction de déclenchement du Turbo
    private void Turbo()
    {
        //On appelle la fonction Turbo du playerController
        pc.Turbo(pc.turboSpeedMax, "TurboPowerup");
    }

    //Fonction de déclenchement du BigBall
    private void BigBall()
    {
        //On appelle la fonction BigBall du playerController
        pc.PU_BigBall();
    }

    //Fonction de déclenchement du Glacon
    private void Glacon()
    {
        //On lance la fonction de glaçon en RPC
        pv.RPC("RPC_Glacon", RpcTarget.All);

        //pv.RPC("RPC_DebugGlacon", RpcTarget.All);
    }

    //Fonction de déclenchement du Magma
    private void Magma()
    {
        //Déclenchement de la coroutine de Magma dans le playerController
        StartCoroutine(pc.MagmaTime());
    }

    //Fonction de déclenchement de l'onde de confusion
    private void ConfusionWave(bool isBack)
    {
        //Récupération du 1er objet de la liste
        GameObject nextConfWave = confWaveGoList[0];
        //on met l'objet au bon scale
        nextConfWave.transform.localScale = new Vector3(2, 2, 2);

        //on l'enleve de la liste des projectiles utilisables
        confWaveGoList.Remove(nextConfWave);
        //on le met à sa position initiale de lancer (sur le joueur)
        nextConfWave.transform.position = transform.position;

        //on le rotate en fonction d'ou va le joueur
        //nextConfWave.transform.rotation = pc.velocityWithoutGravityForward.transform.rotation;
        //TEST TIR CAMERA
        nextConfWave.transform.localRotation = new Quaternion(0, pc.myCamera.transform.rotation.y, pc.myCamera.transform.rotation.z, pc.myCamera.transform.rotation.w);
        nextConfWave.transform.eulerAngles = new Vector3(0, nextConfWave.transform.eulerAngles.y, nextConfWave.transform.eulerAngles.z);
        //on tire
        nextConfWave.GetComponent<ProjectilePowerUp>().LaunchProjectile(isBack);
        //on n'a plus de projectile, désactivation de la visée
        aimLine.GetComponent<LineAimProjectile>().haveAProjectile = false;
    }

    //Fonction de déclenchement de la fondue
    private void Fondue(bool isBack)
    {
        //Récupération du 1er objet de la liste
        GameObject nextFondue = FondueGoList[0];
        //on met l'objet au bon scale
        nextFondue.transform.localScale = new Vector3(2, 2, 2);
        //on l'enleve de la liste des projectiles utilisables
        FondueGoList.Remove(nextFondue);
        //on le met à sa position initiale de lancer (sur le joueur)
        nextFondue.transform.position = transform.position;
        //on le rotate en fonction d'ou va le joueur
        //nextFondue.transform.rotation = pc.velocityWithoutGravityForward.transform.rotation;
        //TEST TIR CAMERA
        nextFondue.transform.localRotation = new Quaternion(0, pc.myCamera.transform.rotation.y, pc.myCamera.transform.rotation.z, pc.myCamera.transform.rotation.w);
        nextFondue.transform.eulerAngles = new Vector3(0, nextFondue.transform.eulerAngles.y, nextFondue.transform.eulerAngles.z);
        //on tire
        nextFondue.GetComponent<ProjectilePowerUp>().LaunchProjectile(isBack);
        //on n'a plus de projectile, désactivation de la visée
        aimLine.GetComponent<LineAimProjectile>().haveAProjectile = false;
    }

    [PunRPC]
    void RPC_Glacon()
    {
        Debug.Log("Je recois le rpc glacon");

        //Si je suis premier
        if (gm.Position == 1)
        {
            Debug.Log("je suis 1er");
            //Set le joueur frozen
            gm.myPlController.isFrozen = true;
        }
    }

    [PunRPC]
    void RPC_DebugGlacon()
    {
        Debug.Log("Je recois le rpc debug glacon");

        /*//Si je suis premier
        if (gm.Position == 1)
        {
            Debug.Log("je suis 1er");
            //Set le joueur frozen
            gm.myPlController.isFrozen = true;
        }*/
    }

    [PunRPC]
    void RPC_Taunt(float x, float y, float z, int voiceID)
    {
        //S'il n'y a pas d'AudioManager
        if (am == null)
        {
            //On récupère l'AudioManager
            am = FindObjectOfType<AudioManager>();
        }

        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.voix.fmodEventTVoixTaunt, new Vector3(x, y, z), "ChoixVoix", voiceID);
    }

    #endregion

    #region GeneratePowerUp
    //Fonction de choix aléatoire du power up
    private void NewPowerUp()
    {
        //Si je n'ai pas de cadeaux
        if (!hasGift)
        {
            //Génération d'un id aléatoire
            PowerUpRandom = Random.Range(1, 101);
            Debug.Log(PowerUpRandom);
            //int whichProbas;

            //Quel tableau de proba à utiliser
            if (gm.Position == 1)
            {
                whichProbas = 0;
            }
            else if (ptc.distWithFirst <= 65f)
            {
                whichProbas = 1;
            }
            else if (ptc.distWithFirst <= 150f)
            {
                whichProbas = 2;
            }
            else if (ptc.distWithFirst <= 400f)
            {
                whichProbas = 3;
            }
            else if (ptc.distWithFirst > 400f)
            {
                whichProbas = 4;
            }

            //Vérification des probas en fonction de la position du joueur
            for (int i = 0; i < pucs[whichProbas].probasPu.Count; i++)
            {
                //Si la proba est plus grande que la valeur random tirée
                if (pucs[whichProbas].probasPu[i] >= PowerUpRandom)
                {
                    //on set le power up à l'index de cette proba
                    SetPowerUp(i + 1);
                    break;
                }
            }
        }
    }

    //Envoie l'information du powerUp généré
    private void SetPowerUp(int ID)
    {
        //Seulement pour le débug
        PowerUpRandom = ID;

        //On change le power up dans l'affichage (UI)
        StartCoroutine(FindObjectOfType<UiManager>().OnChangePowerUp(ID));

        //On dit qu'on a un cadeau
        hasGift = true;
    }

    //Permet de changer le powerup avec la touche de débug
    private void ChangePowerUpByDebug()
    {
        //On augmente l'ID du powerup de débug
        debugSelectedPowerUp++;

        //Si l'ID dépasse la liste, on le remet à 0
        if (debugSelectedPowerUp > maxPowerUpCount)
        {
            debugSelectedPowerUp = 0;
        }

        //On set le powerUpID
        SetPowerUp(debugSelectedPowerUp);
    }
    #endregion

    #region Cooldown Voix
    private bool canTaunt = true;

    private IEnumerator CooldownTaunt(float time)
    {
        canTaunt = false;

        yield return new WaitForSeconds(time);

        canTaunt = true;
    }
    #endregion

    #endregion
}
