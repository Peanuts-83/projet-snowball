﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Ce script permet de générer les trails des joueurs
 * Il doit être sur le TrailContainer (enfant du prefab de joueur)
 */

public class TrailAdapter : MonoBehaviour
{
    [Tooltip("Hauteur de la trail au dessus du sol")]
    [SerializeField]
    private float heightTrail = 0.075f;

    [Tooltip("Le prefab de la ligne")]
    [SerializeField]
    private GameObject linePrefabs;

    [Tooltip("Distance entre 2 points de la trail")]
    [SerializeField]
    private float distBetweenPoints = 0.3f;

    [Tooltip("Le nombre de points où les collisions sont testées; -1 pour tous tester")]
    [SerializeField]
    private float nbOfCollisionPoints;

    //Le joueur
    private GameObject player;

    //line actuelle utilisé dans le code
    private GameObject currentLineObject;
    //La line renderer
    private LineRenderer currentLineRenderer;

    //Poubelle pour les trails
    private GameObject trailPoubelle;

    private PlayerController tempMyPC;

    void Start()
    {
        //On récupère le joueur
        player = transform.parent.gameObject;
        //On détache la trail du joueur
        transform.parent = null;

        trailPoubelle = Instantiate(new GameObject());
        trailPoubelle.name = "trailPoubelle";
        trailPoubelle.transform.parent = null;
    }
    
    void Update()
    {
        //Si il n'y a plus de joueur, on détruit cet objet
        if (player == null)
        {
            Destroy(this.gameObject);

            return;
        }


        #region Position de la trail
        //Raycast
        RaycastHit hit;

        //Si le raycast rencontre le sol (ce qui est généralement le cas mdr)
        if (Physics.Raycast(player.transform.position, -Vector3.up, out hit))
        {
            //La position de l'objet devient le point de collision du raycast
            transform.position = hit.point + Vector3.up * heightTrail;

            //Oriente la trail
            if (currentLineObject != null)
                currentLineObject.transform.forward = -hit.normal;
        }
        #endregion

        //Si le joueur est au sol
        if (player.GetComponent<PlayerController>().IsGrounded())
        {
            if (hit.collider.tag == "Sol")
            {
                //S'il n'existe pas de ligne
                if (currentLineObject == null)
                {
                    //Je créer une ligne
                    CreateLine();
                }
                else
                {
                    //Si la distance entre la position du joueur actuelle et le dernier point de la trail est supérieur au nombre spécifié
                    if (Vector3.Distance(currentLineRenderer.GetPosition(currentLineRenderer.positionCount - 1),
                        currentLineRenderer.GetPosition(currentLineRenderer.positionCount - 2))
                        > distBetweenPoints)
                    {
                        //On ajoute un point dans la ligne
                        AddPointToLine(transform.position);
                    }
                    else
                    {
                        //met la position du dernier point sur la position de l'objet
                        currentLineRenderer.SetPosition(currentLineRenderer.positionCount - 1, transform.position);
                    }
                }
            }
            else if (currentLineObject != null)
            {
                //On dissocie la ligne de cette objet
                DissociateLine(currentLineObject);
            }
        }
        //Si le joueur n'est pas au sol
        else if (currentLineObject != null)
        {
            //On dissocie la ligne de cette objet
            DissociateLine(currentLineObject);
        }

        //Update de la taille de cet objet
        transform.localScale = player.transform.localScale;
    }

    //Création d'une ligne
    private void CreateLine()
    {
        //instantiation de la line
        currentLineObject = Instantiate(linePrefabs);

        //Met la ligne dans cet objet
        currentLineObject.transform.parent = this.gameObject.transform;
        //Met la ligne à la position 0
        currentLineObject.transform.localPosition = Vector3.zero;
        //Mise à jour du scale
        currentLineObject.transform.localScale = Vector3.one;

        //recuperation du component
        currentLineRenderer = currentLineObject.GetComponent<LineRenderer>();
        //Changement de couleur
        currentLineObject.GetComponent<Renderer>().material.SetColor("_Color", player.GetComponent<PersonnalisationController>().playerColor);
        
        //Pour deux points
        for (int i = 0; i < 2; i++)
        {
            //Rempli les points comme étant 0
            currentLineRenderer.SetPosition(i, transform.position);
        }
    }

    //Ajoute un point à la ligne
    private void AddPointToLine(Vector3 newPos)
    {
        //ajout du max de points dans la ligne
        currentLineRenderer.positionCount++;

        //on ajoute dans la liste
        currentLineRenderer.SetPosition(currentLineRenderer.positionCount - 1, newPos);
    }

    //Discotie la trailRenderer de cet objet
    private void DissociateLine(GameObject go)
    {
        //On change le parent de la trail pour la mettre à la poubelle
        go.transform.parent = trailPoubelle.transform;
        //On détruit la trail après 10s
        Destroy(go, 10);

        //On reset les variables
        currentLineObject = null;
        currentLineRenderer = null;
    }
}
