﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using System.Collections.Generic;



/*
 * _____________________________
 * 
 * 
 * 
 * Trigger & Instantiate des vfx
 * souvent appelé vc dans les autres scripts
 * 
 * Le script se trouve sur le joueur
 * 
 * _____________________________
 */

public class VfxController : MonoBehaviourPunCallbacks
{
    #region variable
    public GameObject joueur;

    #region Snow

    [System.Serializable]
    public struct Struct_Snow
    {
        [Tooltip("VFX de perte de neige obstacle")]
        public GameObject goVfxLoseSnowObstacle;
        [Tooltip("VFX de perte de neige du joueur")]
        public GameObject goVfxLoseSnowPlayer;
        [Tooltip("VFX de gain de neige du player")]
        public GameObject goVfxWinSnowPlayer;
    }


    #endregion Snow

    #region landing hit explosion

    [System.Serializable]
    public struct Struct_LandHitExplosion
    {
        [Tooltip("VFX d'atterrisage")]
        public GameObject goVfxLanding;
        [Tooltip("VFX de collision avec les joueurs")]
        public GameObject goVfxHit;
        [Tooltip("VFX d'explosion lors des collisions")]
        public GameObject goVfxExplo;
    }

    #endregion landing hit explo

    #region vitese

    [System.Serializable]
    public struct Struct_Vitesse
    {
        [Tooltip("VFX des traits de vitesse")]
        public GameObject goVfxCamSpeed;

        [Tooltip("Le minimum de trait de vitesse")]
        public float minParticulesCamSpeed;
        [Tooltip("Le maximum de trait de vitesse")]
        public float maxParticulesCamSpeed;

        [Tooltip("Gradient des traits de vitesse lorsque le joueur prend un boost")]
        public Gradient gradientTurbo;
        [Tooltip("Gradient des traits de vitesse de base")]
        public Gradient gradientBase;

        [Tooltip("VFX de la neige que le joueur projette lorsqu'il va vite")]
        public GameObject goVfxLootSnow;

        [Tooltip("Minimum de vitesse pour que le loot de neige fonctionne")]
        public float minSpeedForLootSnow;

        [Tooltip("VFX de la neige que le joueur projette lorsqu'il a ateint la vitesse max")]
        public GameObject goVFXSpeedMaxSnow;

        [Tooltip("Rond Speed")]
        public GameObject goVfxSpeed;

        [Tooltip("Temps entre spanw rond speed")]
        public float timeSpeed;

        [Tooltip("Nb spanw rond speed")]
        public int nbSpeed;

        [Tooltip("go vfx pour l'aspiration")]
        public GameObject goVfxAspiration;

        [Tooltip("Nb spawn pour particules d'aspiration")]
        public float nbAspi;


    }
    #endregion vitesse

    #region cadeau

    [System.Serializable]
    public struct Struct_Cadeau
    {
        [Tooltip("VFX sur le joueur lorsqu'il obtient un cadeau")]
        public GameObject goVfxCollecte;
        [Tooltip("VFX de confettis lorsque le cadeau est cassé")]
        public GameObject goVfxConfetti;
    }

    #endregion cadeau 

    #region PowerUp
    [System.Serializable]
    public struct Struct_PowerUp
    {
        [Tooltip("VFX du power up de magma")]
        public GameObject goVfxMagma;
        [Tooltip("VFX du power up de magma")]
        public GameObject goRocheMagma;
        [Tooltip("VFX du power up de magma")]
        public GameObject goVfxHitMagma;
        [Tooltip("VFX du power up de magma")]
        public GameObject goVfxHitMagma2;
        [SerializeField, Tooltip("taille max de la sphère power up de magma")]
        public float maxSphereMagma;
        [SerializeField, Tooltip("taille min de la sphère power up de magma")]
        public float minSphereMagma;
        [SerializeField, Tooltip("vitesse du lerp de la sphère power up de magma")]
        public float speedLerpMagmaUp;
        [SerializeField, Tooltip("vitesse du lerp de la sphère power up de magma")]
        public float speedLerpMagmaDown;
        [SerializeField, Tooltip("temps avant le lancement de la zone de magma")]
        public float waitBeforeMagma;
        public float objectifLerp;
        public int count;
        public float newLerpLava;
        [SerializeField]
        public float speedLerpLava;


        [HideInInspector]
        public List<Coroutine> magmaCoroutine;


        [Tooltip("VFX du powerUp de grossissement")]
        public GameObject goVfxStartBig;
        [Tooltip("VfX du power quand il est gros")]
        public GameObject goVfxBig;

        [Tooltip("VFX du power up de glacon")]
        public GameObject goVfxGlacon;
        [Tooltip("Model du glacon")]
        public GameObject goBlockGlacon;
        [Tooltip("vfx de craquement du glacon")]
        public GameObject goVfxICeCrack;

        [Tooltip("VFX feedback sur le joueur confu")]
        public GameObject gpVfxConfu;
    }
    #endregion

    #region RightLeftEffects
    [System.Serializable]
    public struct RightLeftEffect
    {
        public bool activateEffect;

        [Header("Scale")]
        public Vector2 minMaxScale;
        public float speedDecreaseInAir;
        public float multiplierWhenNotMaxScale;
        public float multiplierWhenMaxScale;

        [HideInInspector]
        public float scale;

        [Header("Proportion")]
        [Range(-1,1)]
        public float proportion;
        public float speedProportionUpdate;
        public float speedReturnToNeutralProportion;

        [Header("Functionnal")]
        public GameObject parentObject;
        public GameObject rightEffect;
        public GameObject leftEffect;

    }

    #endregion

    #region struct
    [Tooltip("Variables en rapport avec la neige")]
    public Struct_Snow snow;
    [Tooltip("Variables de collisions entre joueurs et avec le sol")]
    public Struct_LandHitExplosion landHitExplosion;
    [Tooltip("Variables de vitesse")]
    public Struct_Vitesse vitesse;
    [Tooltip("Variables des cadeaux")]
    public Struct_Cadeau cadeau;
    [Tooltip("Variables des powerUps")]
    public Struct_PowerUp powerUp;
    [Tooltip("Variables de l'effet 'RightLeft'")]
    public RightLeftEffect rightLeftEffect;
    #endregion

    private PlayerController player;
    private InputManager im;

    #endregion

    #region Unity Methods
    private void Start()
    {
        player = GetComponent<PlayerController>();
        powerUp.magmaCoroutine = new List<Coroutine>();

        im = FindObjectOfType<InputManager>();

        powerUp.count = 0;
        powerUp.objectifLerp = -3;
        powerUp.newLerpLava = -3;
    }

    private void Update()
    {
        if (rightLeftEffect.activateEffect)
            DoRightLeftEffect();
    }
    #endregion

    #region Vitesse

    //Déclenche les vfx de loot de neige si le joueur est au sol. 
    public void SetLootSnowEnable(bool isGrounded, float speed)
    {
        //On récupère le particule system de l'objet
        ParticleSystem ps = vitesse.goVFXSpeedMaxSnow.GetComponentInChildren<ParticleSystem>();

        //Si le joueur est sur le sol
        if (isGrounded && speed >= vitesse.minSpeedForLootSnow)
        {
            //Si le vfx n'est pas encore joué 
            if (!ps.isPlaying)
                //On joue le vfx
                ps.Play();
        }
        else
        {
            //Si le joueur joue le vfx
            if (ps.isPlaying)
                //On stop le vfx
                ps.Stop();
        }
    }


    //Traits de vitesses et lensdistortion
    public void ChangeEffectSpeed(float minSpeedPlayer, float maxSpeedPlayer, float velocityMagnitude, float maxTurbo)
    {

        #region NbParticulesVitesse
        //Remet la vitesse du joueur entre 0 et 1
        float unlerpDiffSpeed = Mathf.InverseLerp(minSpeedPlayer + 10f, maxSpeedPlayer, velocityMagnitude);

        //Défini le nombre de particules en fonction de la vitesse du joueur
        float lerpNbParticules = Mathf.Lerp(vitesse.minParticulesCamSpeed, vitesse.maxParticulesCamSpeed, unlerpDiffSpeed);

        //Assigne le nouveau nombre de particules au VFX
        vitesse.goVfxCamSpeed.GetComponent<VisualEffect>().SetFloat("NbParticules", lerpNbParticules);

        #endregion


    }

    public IEnumerator SpeedMaxCoroutine()
    {
        for (int i = 0; i < vitesse.nbSpeed; i++)
        {
            InstantiateParticles(vitesse.goVfxSpeed, transform.position);
            yield return new WaitForSeconds(vitesse.timeSpeed);
        }


    }

    //Change la couleur des traits lorsque qu'un turbo est pris
    public IEnumerator ChangeColorVFXCamSpeed()
    {
        /*
         * couleur des vfx vitesse quand on prend un trubo 
         * 
         */

        //Met du bleu dans les traits
        vitesse.goVfxCamSpeed.GetComponent<VisualEffect>().SetGradient("GradientColor", vitesse.gradientTurbo);

        //temps de turbo 
        yield return new WaitForSeconds(1f);

        //remet la couleur de base des traits
        vitesse.goVfxCamSpeed.GetComponent<VisualEffect>().SetGradient("GradientColor", vitesse.gradientBase);
    }


    #endregion

    #region instanciate



    //Instantiation de particules
    public GameObject InstantiateParticles(GameObject vfx, Vector3 position)
    {
        //Créer une nouvelle particule
        GameObject newParticle = Instantiate(vfx);

        //Change la position, le parent et le scale de la particule
        newParticle.transform.position = position;
        newParticle.transform.parent = null;


        newParticle.transform.localScale = transform.localScale;

        //Détruit la particule
        Destroy(newParticle, 3f);

        return newParticle;
    }

    //Joue les particules de collision avec un autre joueur
    public void InstantiateHit(Vector3 position, GameObject otherPlayer)
    {
        #region Gradient
        //Création d'un gradient
        Gradient newGradient = new Gradient();

        //Prend la couleur de l'autre joueur
        GradientColorKey[] colorKey = new GradientColorKey[1];
        colorKey[0].color = otherPlayer.GetComponentInChildren<PersonnalisationController>().playerColor;
        colorKey[0].time = 0.0f;

        GradientAlphaKey[] alphaKey = new GradientAlphaKey[1];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;

        //Affecte la couleur au gradient
        newGradient.SetKeys(colorKey, alphaKey);
        #endregion

        //Instantie le vfx de collision
        GameObject goHit = InstantiateParticles(landHitExplosion.goVfxHit, position);
        //Change la couleur du nouveau vfx
        goHit.GetComponentInChildren<VisualEffect>().SetGradient("Gradient", newGradient);
    }

    //Joue la collecte de cadeau

    #endregion instanciate 

    #region Play VFX
    public void PlayVFX(GameObject goVFX)
    {
        goVFX.GetComponent<VisualEffect>().Play();
    }

    [PunRPC]
    public void RPC_PlayVFX(GameObject goVFX)
    {
        goVFX.GetComponent<VisualEffect>().Play();
    }


    [PunRPC]
    public void RPC_StopVFX(GameObject goVFX)
    {
        goVFX.GetComponent<VisualEffect>().Stop();
    }

    public void StopVFX(GameObject goVFX)
    {
        goVFX.GetComponent<VisualEffect>().Stop();
    }

    #endregion

    #region frozen
    /// <summary>
    /// controle des vfx + appartition du glaocn (le mesh)
    /// </summary>
    /// <param name="activate"></param>
    /// <returns></returns>
    private IEnumerator PlayFrozen(bool activate)
    {

        //Si le glaçon doit s'activer
        if (activate)
        {
            //On joue le vfx
            powerUp.goVfxGlacon.GetComponent<VisualEffect>().Play();

            yield return new WaitForSeconds(1f);

            //On active le glaçon
            powerUp.goBlockGlacon.SetActive(true);
            //cc.GetComponent<CameraController>().ChangeVignette(new Color(0, 240, 255), 0.03f);

            yield return new WaitForSeconds(0.4f);

            StartCoroutine(PlayClignotementFrozen());

        }
        else
        {
            //On attend x temps
            yield return new WaitForSeconds(0.1f);

            //On désactive le block de glaçon
            powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Play();
            powerUp.goBlockGlacon.GetComponentsInChildren<Renderer>()[1].material.SetFloat("Vector1_750057DF", 0f);
            powerUp.goVfxGlacon.GetComponent<VisualEffect>().Stop();
            powerUp.goBlockGlacon.SetActive(false);


            yield return new WaitForSeconds(0.1f);
            powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Play();

        }
    }

    /// <summary>
    /// Clignotement des crack du shader + vfx 
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlayClignotementFrozen()
    {

        yield return new WaitForSeconds(0.6f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Play();
        powerUp.goBlockGlacon.GetComponentsInChildren<Renderer>()[1].material.SetFloat("Vector1_750057DF", 0.05f);
        yield return new WaitForSeconds(0.1f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Stop();

        yield return new WaitForSeconds(0.3f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Play();
        powerUp.goBlockGlacon.GetComponentsInChildren<Renderer>()[1].material.SetFloat("Vector1_750057DF", 0.2f);
        yield return new WaitForSeconds(0.1f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Stop();

        yield return new WaitForSeconds(0.1f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Play();
        powerUp.goBlockGlacon.GetComponentsInChildren<Renderer>()[1].material.SetFloat("Vector1_750057DF", 1f);
        yield return new WaitForSeconds(0.1f);
        powerUp.goVfxICeCrack.GetComponent<VisualEffect>().Stop();
    }
    #endregion

    #region Right Left Effect

    private void DoRightLeftEffect()
    {
        RLE_Scale();
        RLE_Turn();
    }

    private void RLE_Scale()
    {
        float scale1 = 0;
        float scale2 = 0;

        if (player.tagsInContact.Contains("Sol"))
        {
            //Calcul du scale de base
            if (player.IsGrounded())
            {
                float unlerpSpeed = Mathf.InverseLerp(0, player.speedSteps[player.actualStep], player.velocityWithoutGravity.magnitude);

                rightLeftEffect.scale = Mathf.Lerp(0, rightLeftEffect.minMaxScale.y, unlerpSpeed);
            }
            else
            {
                rightLeftEffect.scale = Mathf.Lerp(rightLeftEffect.scale, 0, rightLeftEffect.speedDecreaseInAir * Time.deltaTime);
            }

            //Ponctuation du scale par la direction du joueur
            if (im.inputMovement.x != 0)
            {
                float dir = -im.inputMovement.x / Mathf.Abs(im.inputMovement.x);
                if (player.isConfused)
                {
                    dir *= -1;
                }

                rightLeftEffect.proportion = Mathf.Lerp(rightLeftEffect.proportion, dir, rightLeftEffect.speedProportionUpdate * Time.deltaTime);
            }
            else
            {
                rightLeftEffect.proportion = Mathf.Lerp(rightLeftEffect.proportion, 0, rightLeftEffect.speedReturnToNeutralProportion * Time.deltaTime);
            }

            //Recalcul du scale en fonction des situations
            float calculatedProportion = Mathf.InverseLerp(-1, 1, rightLeftEffect.proportion);


            if (im.inputMovement.magnitude != 0 && player.IsGrounded())
            {
                scale1 = Mathf.Lerp(rightLeftEffect.minMaxScale.x, rightLeftEffect.minMaxScale.y, rightLeftEffect.scale * calculatedProportion);
                scale2 = Mathf.Lerp(rightLeftEffect.minMaxScale.x, rightLeftEffect.minMaxScale.y, rightLeftEffect.scale * (1 - calculatedProportion));
            }
            else
            {
                scale1 = Mathf.Lerp(0, rightLeftEffect.minMaxScale.y, rightLeftEffect.scale * calculatedProportion);
                scale2 = Mathf.Lerp(0, rightLeftEffect.minMaxScale.y, rightLeftEffect.scale * (1 - calculatedProportion));
            }

            if (!(player.actualStep == player.scaleSteps.Length - 1))
            {
                scale1 *= rightLeftEffect.multiplierWhenNotMaxScale;
                scale2 *= rightLeftEffect.multiplierWhenNotMaxScale;
            }
            else
            {
                scale1 *= rightLeftEffect.multiplierWhenMaxScale;
                scale2 *= rightLeftEffect.multiplierWhenMaxScale;
            }
        }

        //Application du scale
        rightLeftEffect.leftEffect.transform.localScale = Vector3.one * scale1;
        rightLeftEffect.rightEffect.transform.localScale = (Vector3.right + Vector3.up - Vector3.forward) * scale2;
    }

    private void RLE_Turn()
    {
        float angle = 0;
        RaycastHit hit;

        if (Physics.Raycast(transform.position,-Vector3.up,out hit))
        {
            if (hit.collider.gameObject.tag == "Sol" || hit.collider.tag == "SolGlace")
            {
                //rightLeftEffect.parentObject.transform.up = Vector3.Lerp(rightLeftEffect.parentObject.transform.up, hit.normal, 10 * Time.deltaTime);
                angle = Vector3.Angle(Vector3.up, hit.normal);
            }
        }

        rightLeftEffect.parentObject.transform.parent.up = Vector3.up;

        if (player.velocityWithoutGravity != Vector3.zero)
            rightLeftEffect.parentObject.transform.forward = player.velocityWithoutGravity.normalized;

        rightLeftEffect.parentObject.transform.parent.up = hit.normal;
    }

    #endregion




    #region Fonction RPC

    [PunRPC]
    /// <summary>
    /// effet d'aspiration vfx  
    /// </summary>
    public void RPC_ChangeEffectAspiration(float amount)
    {
        vitesse.goVfxAspiration.GetComponent<VisualEffect>().SetFloat("Delay", Mathf.Lerp(1f, 0.6f, amount));

        if (amount > 0)
        {
            vitesse.goVfxAspiration.GetComponent<VisualEffect>().SetFloat("Count", 1);

        }
        else
        {
            vitesse.goVfxAspiration.GetComponent<VisualEffect>().SetFloat("Count", 0);
        }
    }

    /// <summary>
    /// Rond de vitesse a l'activation de turbo
    /// </summary>
    [PunRPC]
    public void SpeedMax()
    {
        //Debug.Log("speedmax ");
        StartCoroutine(SpeedMaxCoroutine());


    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// vfx magma
    /// </summary>
    /// <param name="active"></param>
    [PunRPC]
    public void RPC_PlayMagma(bool active)
    {
        if (active)
        {
            powerUp.goVfxMagma.SetActive(true);
            powerUp.goRocheMagma.SetActive(true);



            foreach (Coroutine co in powerUp.magmaCoroutine)
            {
                StopCoroutine(co);
            }

            powerUp.magmaCoroutine.Clear();

            powerUp.magmaCoroutine.Add(StartCoroutine(VFXStartMagma()));
            
            //powerUp.magmaCoroutine.Add(StartCoroutine(LerpMagmaSphere(powerUp.maxSphereMagma)));
        }
        else
        {
            foreach (Coroutine co in powerUp.magmaCoroutine)
            {
                StopCoroutine(co);
            }

            powerUp.magmaCoroutine.Clear();

            powerUp.magmaCoroutine.Add(StartCoroutine(LerpMagmaSphere(powerUp.minSphereMagma)));
           
        }
    }

    private IEnumerator VFXStartMagma()
    {
        yield return new WaitForEndOfFrame();
        RPC_PlayVFX(powerUp.goVfxHitMagma);
        powerUp.magmaCoroutine.Add(StartCoroutine(LerpMaterialsMagma()));

        yield return new WaitForSeconds(powerUp.waitBeforeMagma);

        powerUp.magmaCoroutine.Add(StartCoroutine(LerpMagmaSphere(powerUp.maxSphereMagma)));
        
    }



    private IEnumerator LerpMagmaSphere(float sizeWanted)
    {
        

        //récupération du composant 
        Transform trfsSphereMagma = powerUp.goVfxMagma.transform.GetChild(1).GetComponent<Transform>();

        //activation ou desac selon si en magma ou pas 
        if (sizeWanted < trfsSphereMagma.localScale.x)
        {
            trfsSphereMagma.GetComponent<Collider>().enabled = false;
        }
        else
        {
            trfsSphereMagma.GetComponent<Collider>().enabled = true;
        }

        Vector3 newScale = Vector3.one;

        if (trfsSphereMagma.localScale.x > sizeWanted)
        {
            newScale = Vector3.Lerp(trfsSphereMagma.localScale, Vector3.one * sizeWanted, powerUp.speedLerpMagmaUp * Time.deltaTime);
        }
        else if(trfsSphereMagma.localScale.x < sizeWanted)
        {
            newScale = Vector3.Lerp(trfsSphereMagma.localScale, Vector3.one * sizeWanted, powerUp.speedLerpMagmaDown * Time.deltaTime);
        }


        //affectation
        trfsSphereMagma.localScale = newScale;

        yield return new WaitForEndOfFrame();


        //si c'est pas fini on continu sinon on arrete
        if (!(trfsSphereMagma.localScale.x > sizeWanted - 0.1f && trfsSphereMagma.localScale.x < sizeWanted + 0.1f))
        {
            powerUp.magmaCoroutine.Add(StartCoroutine(LerpMagmaSphere(sizeWanted)));
        }
        else
        {
            if (!player.isMagma)
            { 
                powerUp.goVfxMagma.SetActive(false);
                powerUp.goRocheMagma.SetActive(false);

                RPC_StopVFX(powerUp.goVfxHitMagma);

               

                foreach (Coroutine co in powerUp.magmaCoroutine)
                {
                    StopCoroutine(co);
                }

                powerUp.magmaCoroutine.Clear();
            }
        }
    }
    private IEnumerator LerpMaterialsMagma()
    {
        Material matLavaMgma = powerUp.goRocheMagma.GetComponent<Renderer>().material;

        yield return new WaitForSeconds(0.5f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2);
        yield return new WaitForSeconds(0.5f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2);
        yield return new WaitForSeconds(0.3f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2);
        yield return new WaitForSeconds(0.3f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2); 
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2); 
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2); 
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", 2);
        yield return new WaitForSeconds(0.1f);
        matLavaMgma.SetFloat("Vector1_51BA7403", -2);
        yield return new WaitForSeconds(0.1f);




        //Debug.Log(matLavaMgma);

        //float currentLerp = matLavaMgma.GetFloat("Vector1_51BA7403");

        //Debug.Log("currentLERRRRPP  " + currentLerp);
        //Debug.Log("LERRRRPP  "+ powerUp.newLerpLava);
        //Debug.Log("obj  "+ powerUp.objectifLerp);
        /*
        //lerp
        if (currentLerp > powerUp.objectifLerp)
        {
            //Debug.Log("condition");
            powerUp.newLerpLava = Mathf.Lerp(currentLerp, powerUp.objectifLerp, powerUp.speedLerpLava * Time.deltaTime);
            
        }


        //affectation
        matLavaMgma.SetFloat("Vector1_51BA7403", powerUp.newLerpLava);

        yield return new WaitForEndOfFrame();*/

        /*
        
        //si on est a lobjectif on arrete sinon on continu 
        if(currentLerp > powerUp.objectifLerp - 0.3f  &&
            currentLerp < powerUp.objectifLerp + 0.3f)
        {
            
            //powerUp.magmaCoroutine.Add(StartCoroutine(LerpMaterialsMagma()));
            yield return new WaitForSeconds(0.1f);
            matLavaMgma.SetFloat("Vector1_51BA7403", 2.5f);
            /*  yield return new WaitForSeconds(0.2f);
              matLavaMgma.SetFloat("Vector1_51BA7403", -3);
              yield return new WaitForSeconds(0.2f);
              matLavaMgma.SetFloat("Vector1_51BA7403", 3);
              yield return new WaitForSeconds(0.2f);
              matLavaMgma.SetFloat("Vector1_51BA7403", -3);
              yield return new WaitForSeconds(0.2f);
              matLavaMgma.SetFloat("Vector1_51BA7403", 3);*/
        /*
        if (!player.isMagma)
        {



            powerUp.goVfxMagma.SetActive(false);
            powerUp.goRocheMagma.SetActive(false);

            RPC_StopVFX(powerUp.goVfxHitMagma);



            foreach (Coroutine co in powerUp.magmaCoroutine)
            {
                StopCoroutine(co);
            }

            powerUp.magmaCoroutine.Clear();
        }


    }
    else
    {
        Debug.Log("pas finiiiiiii ");
        //matLavaMgma.SetFloat("Vector1_51BA7403", 0);
        powerUp.magmaCoroutine.Add(StartCoroutine(LerpMaterialsMagma()));
    }


    //si c'est pas fini on continu sinon on arrete
    if (!(trfsSphereMagma.localScale.x > sizeWanted - 0.1f && trfsSphereMagma.localScale.x < sizeWanted + 0.1f))
    {
        powerUp.magmaCoroutine.Add(StartCoroutine(LerpMaterialsMagma(sizeWanted)));
    }
    else
    {
        if (!player.isMagma)
        { 
            powerUp.goVfxMagma.SetActive(false);
            powerUp.goRocheMagma.SetActive(false);

            RPC_StopVFX(powerUp.goVfxHitMagma);



            foreach (Coroutine co in powerUp.magmaCoroutine)
            {
                StopCoroutine(co);
            }

            powerUp.magmaCoroutine.Clear();
        }
    }*/
    }

        /// <summary>
        /// vfx gain de neige quand gain de masse
        /// </summary>
        [PunRPC]
    public void RPC_PlayWinSnow()
    {
        //Instantie le vfx de gain de neige
        InstantiateParticles(snow.goVfxWinSnowPlayer, transform.position);
    }

    /// <summary>
    /// vfx grossismeent quand on est en train de grossir
    /// </summary>
    [PunRPC]
    public void RPC_PlayStartBig()
    {
        #region Gradient
        Gradient newGradient = new Gradient();

        //Récupère la couleur du joueur
        GradientColorKey[] colorKey = new GradientColorKey[1];
        colorKey[0].color = joueur.GetComponent<MeshRenderer>().material.GetColor("Color_E797161B");
        colorKey[0].time = 0.0f;

        GradientAlphaKey[] alphaKey = new GradientAlphaKey[1];
        alphaKey[0].alpha = 0.4f;
        alphaKey[0].time = 0.0f;


        //Affecte la couleur au gradient
        newGradient.SetKeys(colorKey, alphaKey);
        #endregion

        //Joue le vfx
        powerUp.goVfxStartBig.GetComponentInChildren<VisualEffect>().SetGradient("Gradient", newGradient);
        powerUp.goVfxStartBig.GetComponent<VisualEffect>().Play();
    }

    /// <summary>
    /// vfx grossismeent quan don est deja gros
    /// </summary>
    [PunRPC]
    public void RPC_PlayBig()
    {
        #region Gradient
        Gradient newGradient = new Gradient();

        //Récupère la couleur du joueur
        GradientColorKey[] colorKey = new GradientColorKey[1];
        colorKey[0].color = joueur.GetComponent<MeshRenderer>().material.GetColor("Color_E797161B");
        colorKey[0].time = 0.0f;

        GradientAlphaKey[] alphaKey = new GradientAlphaKey[1];
        alphaKey[0].alpha = 0.4f;
        alphaKey[0].time = 0.0f;


        //Affecte la couleur au gradient
        newGradient.SetKeys(colorKey, alphaKey);
        #endregion

        //Joue le vfx
        powerUp.goVfxBig.GetComponentInChildren<VisualEffect>().SetGradient("Gradient", newGradient);
        powerUp.goVfxBig.GetComponent<VisualEffect>().Play();
    }

    /// <summary>
    /// vfx grossismeent quan don est deja gros
    /// </summary>
    [PunRPC]
    public void RPC_StopBig()
    {
        powerUp.goVfxBig.GetComponent<VisualEffect>().Stop();
    }


    /// <summary>
    /// vfx glacon / frozen
    /// </summary>
    /// <param name="active"></param>
    [PunRPC]
    public void RPC_PlayFrozen(bool active)
    {
        //Lance la coroutine de frozen
        StartCoroutine(PlayFrozen(active));
    }

    #endregion

}
