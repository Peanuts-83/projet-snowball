﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardWithoutGravity : MonoBehaviour
{
    public PlayerController pc;
    public Rigidbody rb;

    void Update()
    {
        if(rb != null)
        {
            Vector3 newForward = new Vector3(rb.velocity.x, 0, rb.velocity.z);

            if (newForward != Vector3.zero)
                transform.forward = newForward;
        }      
    }
}
