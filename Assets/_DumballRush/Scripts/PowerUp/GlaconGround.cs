﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

public class GlaconGround : MonoBehaviour
{
    [SerializeField]
    private float speedUpdate = 10;

    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position,-Vector3.up,out hit))
        {
            if ((hit.collider.gameObject.tag == "Sol" || hit.collider.tag == "SolGlace") && FindObjectOfType<GameManager>().myPlController.IsGrounded())
            {
                transform.up = Vector3.Lerp(transform.up, hit.normal,speedUpdate* Time.deltaTime);
            }
        }
    }
}
