﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineAimProjectile : MonoBehaviour
{
    [SerializeField]
    private LineRenderer aimLine;

    [SerializeField]
    private int vertexNumber;

    [SerializeField]
    private List<Vector3> VectLerp;

    [SerializeField]
    private GameObject aimSphere;
    [SerializeField]
    private GameObject aimSphere2;

    private LayerMask layerMask;

    public Rigidbody rb;

    //PlayerController
    public PlayerController pc;

    public bool haveAProjectile;

    public float maxScaleSphere= 2f;

    // Start is called before the first frame update
    void Start()
    {
        layerMask = 1 << 0;

        aimLine.positionCount = vertexNumber;

        for (int i = 0; i < vertexNumber; i++)
        {
            float t = (1f / (vertexNumber / 2f)) * i;
            Vector3 curveLerp = QuadraticCurve(VectLerp[0], VectLerp[1], VectLerp[2], t);
            aimLine.SetPosition(i, curveLerp);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (haveAProjectile)
        {
            CheckZoneShot();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void CheckZoneShot()
    {
        transform.localRotation = new Quaternion(0, pc.myCamera.transform.rotation.y, pc.myCamera.transform.rotation.z, pc.myCamera.transform.rotation.w);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);

        RaycastHit hit;

        bool doOnce = false;

        Vector3 nextStart = transform.position;

        for (int i = 0; i < vertexNumber - 1; i++)
        {
            Vector3 dirWithoutRotation = new Vector3(aimLine.GetPosition(i + 1).x - aimLine.GetPosition(i).x, 
                                                     aimLine.GetPosition(i + 1).y - aimLine.GetPosition(i).y,
                                                     aimLine.GetPosition(i + 1).z - aimLine.GetPosition(i).z);

            float diffYandZ = dirWithoutRotation.y / dirWithoutRotation.z;

            //Vector3 rbNormalizedWithoutY = new Vector3(rb.velocity.x, 0, rb.velocity.z).normalized;
            Vector3 rbNormalizedWithoutY = new Vector3(pc.myCamera.transform.forward.x, pc.myCamera.transform.forward.y, pc.myCamera.transform.forward.z).normalized;

            Vector3 dir = new Vector3(rbNormalizedWithoutY.x, diffYandZ, rbNormalizedWithoutY.z).normalized * dirWithoutRotation.magnitude;

            Debug.DrawRay(nextStart, dir, Color.yellow);

            if (Physics.Raycast(nextStart, dir, out hit, Vector3.Distance(aimLine.GetPosition(i+1), aimLine.GetPosition(i)), layerMask) && doOnce == false)
            {
                
                //Debug.Log(dir);

                aimSphere.transform.position = hit.point;
                aimSphere2.transform.position = hit.point;

                aimSphere.transform.localScale = new Vector3(-dir.y * maxScaleSphere, -dir.y * maxScaleSphere, -dir.y * maxScaleSphere) ;
                aimSphere2.transform.localScale = new Vector3(-dir.y * maxScaleSphere / 2f, -dir.y * maxScaleSphere / 2f, -dir.y * maxScaleSphere / 2f) ;

                doOnce = true;
            }

            nextStart += dir;
        }
    }

    //Fonctions lerp permattant de calculer la position du projectile

    public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
    {
        return a + (b - a) * t;
    }

    public static Vector3 QuadraticCurve(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        Vector3 p0 = Lerp(a, b, t);
        Vector3 p1 = Lerp(b, c, t);
        return Lerp(p0, p1, t);
    }
}
