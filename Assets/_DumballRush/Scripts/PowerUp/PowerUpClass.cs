﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerUpClass
{
    //public int id;

    /*public int probaPu1;
    public int probaPu2;
    public int probaPu3;
    public int probaPu4;
    public int probaPu5;
    public int probaPu6;
    public int probaPu7;*/

    public List<int> probasPu = new List<int>();
}