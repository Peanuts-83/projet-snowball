﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;

/// <summary>
/// Fonction de lancer des projectiles 
/// 
/// se trouve sur les projectiles dans le joueur
/// 
/// </summary>
public class ProjectilePowerUp : MonoBehaviour
{

    #region variables

    public enum typeOfObject { Confusion, Fondue };

    [Tooltip("Choix du style d'objet")]
    public typeOfObject objectType;

    [Tooltip("Le joueur auquel je suis associé")]
    [SerializeField]
    private GameObject myPlayer;

    [Tooltip("transform du 1er point du lerp")]
    public Transform transfVectA;
    [Tooltip("transform du 2eme point du lerp")]
    public Transform transfVectB;
    [Tooltip("transform du 3eme point du lerp")]
    public Transform transfVectC;

    [Tooltip("transform du 2eme point du lerp si tir arrière")]
    public Transform transfBackVectB;
    [Tooltip("transform du 3eme point du lerp si tir arrière")]
    public Transform transfBackVectC;

    //position du 1er point du lerp au moment du lancer
    private Vector3 VectA;
    //position du 2eme point du lerp au moment du lancer
    private Vector3 VectB;
    //position du 3eme point du lerp au moment du lancer
    private Vector3 VectC;

    //valeur T du lerp de position du projectile
    private float tProjectile;

    [Tooltip("Vitesse de déplacement du projectile")]
    public float speedProjectile;

    //Position à la frame précédente pour des calculs de collision
    private Vector3 lastPosition;

    [Tooltip("Objet de l'explosion")]
    public GameObject explosionGo;

    [Tooltip("Objet de lancer")]
    public GameObject lancerGo;

    [Tooltip("Taille maximum de la zone d'explosion")]
    public Vector3 expansionSizeMax;

    //Valeur T du lerp de taille de la zone d'explosion
    private float tSizeExpan;

    [Tooltip("Vitesse d'agrandissement de la zone d'explosion")]
    public float speedExplo;

    [Tooltip("Temps durant lequel l'explosion reste à sa taille max")]
    public float timeExpansionStay;

    [Tooltip("Objet de l'explosion")]
    public GameObject splashFondueGo;

    public bool fondueCanStopMe;

    //DoOnce pour arreter les déplacements une fois la collision faite
    private bool doOnce;

    //Est ce que le projectile a été tiré par moi
    private bool isLaunchedByMe;

    //Est ce que c'est un tir arrière ou non
    public bool isBackFire;

    [SerializeField, Tooltip(" MAX confusion particules attractionSpeed, nombre de particules, alpha de la sphère et nombre de particuyles du brouilllard")]
    private Vector4 paramConfusionLerpMax = new Vector4(5f,500f,0.9f,50f);
    [SerializeField, Tooltip("MAX confusion particules attractionSpeed, nombre de particules, alpha de la sphère et nombre de particuyles du brouilllard")]
    private Vector4 paramConfusionLerpMin = new Vector4(-3f,0f,1.2f,0f);
    [SerializeField, Tooltip("MAX confusion particules attractionSpeed, nombre de particules, alpha de la sphère et nombre de particuyles du brouilllard")]
    private Vector4 paramConfusionLerpSpeed = new Vector4(1f,1f,1f,1f);

    [Tooltip("Mesh du fromage")]
    public GameObject fromageGO;
    #endregion

    //Audiomanager
    private AudioManager am;

    private void Start()
    {
        myPlayer = GetComponentInParent<PowerUpController>().gameObject;
        //On sort les projectiles du joueur
        transform.parent = null;
    }

    void Update()
    {
        //On vérifie que le projectile a été lancé par moi même, et si le projectile n'a pas encore touché le sol
        if(doOnce == false && isLaunchedByMe == true)
        {
            if(lancerGo != null)
            {
                //activation du mesh de la popo into les vfx aussi 
                lancerGo.SetActive(true);
            }
            


            //on déplace le projectile par rapport au lerp
            transform.position = QuadraticCurve(VectA, VectB, VectC, tProjectile);

            //Vérification de collision au sol
            RaycastHit hit;

            if (Physics.Linecast(lastPosition, transform.position, out hit))
            {
                //si le projectile touche le sol
                if ((hit.transform.gameObject.tag == "Sol" || hit.transform.gameObject.tag == "SolGlace") && doOnce == false)
                {
                    if (objectType == typeOfObject.Confusion)
                    {
                        //doOnce pour le faire qu'une fois
                        doOnce = true;
                        //on met le projectile à l'endroit de la collision
                        transform.position = hit.point;

                        

                        //on active l'objet explosion 
                        explosionGo.SetActive(true);
                        // reset des paramètre des effet de la zone de confusion
                        ResetZoneConfu();
                        //on déplace l'objet explosion sur le point de collision
                        explosionGo.transform.position = hit.point;

                        //Joue le son
                        if (am == null)
                            am = FindObjectOfType<AudioManager>();

                        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.powerUp.fmodEventZoneConfusion, hit.point);

                        //On envoie les informations du point de collision aux autres joueurs
                        GetComponent<PhotonView>().RPC("RPC_Explosion", RpcTarget.Others, hit.point);
                        

                        //lancement de la fonction d'explosion
                        StartCoroutine(ExplosionExpansion());
                    }

                else if(objectType == typeOfObject.Fondue)
                    {
                        //doOnce pour le faire qu'une fois
                        doOnce = true;
                        //on met le projectile à l'endroit de la collision
                        transform.position = hit.point;
                        //on active l'objet explosion 
                        splashFondueGo.SetActive(true);
                        //on déplace l'objet explosion sur le point de collision
                        splashFondueGo.transform.position = hit.point;

                        splashFondueGo.transform.forward = hit.normal;

                        //Joue le son
                        if (am == null)
                            am = FindObjectOfType<AudioManager>();
                        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.powerUp.fmodEventFondue, hit.point);

                        //On envoie les informations du point de collision aux autres joueurs
                        GetComponent<PhotonView>().RPC("RPC_SplashFondue", RpcTarget.Others, hit.point, hit.normal);
                        //lancement de la fonction d'explosion
                        StartCoroutine(SplashExpansion());
                    }
                }
            }

            //Si il n'y a pas de collision, on enregistre ma position pour vérifier la collision à la frame suivante
            lastPosition = transform.position;
            //on avance dans le lerp
            tProjectile += speedProjectile * Time.deltaTime;
        }     
    }

    //Fonction de lancement du projectile
    public void LaunchProjectile(bool isBack)
    {
        isBackFire = isBack;

        //Récupération de la position des 3 points du lerp au moment du lancement
        VectA = transfVectA.position;
        if (isBackFire)
        {
            VectB = transfBackVectB.position;
            VectC = transfBackVectC.position;
        }
        else
        {
            VectB = transfVectB.position;
            VectC = transfVectC.position;
        }       

        //on récupère la 1ere position du joueur (pour la vérification de collision)
        lastPosition = transform.position;

        //ce projectile a été lancé par moi
        isLaunchedByMe = true;
    }

    //Fonction RPC d'avant explosion pour les autres joueurs
    [PunRPC]
    void RPC_Explosion(Vector3 hit)
    {
        //on change la position du projectile
        transform.position = hit;
        //on active le gameobject explosion
        explosionGo.SetActive(true);
        // reset des paramètre des effet de la zone de confusion
        ResetZoneConfu();
        //on place l'explosion au point de collision
        explosionGo.transform.position = hit;
        //on lance la fonction d'explosion
        StartCoroutine(ExplosionExpansion());

        //Joue le son
        if (am == null)
            am = FindObjectOfType<AudioManager>();

        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.powerUp.fmodEventZoneConfusion, hit);
    }

    //Fonction d'explosion (pour tous les joueurs)
    private IEnumerator ExplosionExpansion()
    {
        //augmentation de la valeur T du lerp d'agrandissement de la zone d'explosion, entre 0 et 1
        tSizeExpan += Mathf.Clamp01(speedExplo * Time.deltaTime);

        //changement de la taille de la zone d'explosion en fonction du nouveau T
        explosionGo.transform.localScale = Lerp(Vector3.zero, expansionSizeMax, tSizeExpan);

        //on attend la frame
        yield return new WaitForEndOfFrame();       

        //si T est inferieur à 1
        if (tSizeExpan < 1)
        {
            //on continue l'expansion de la zone
            StartCoroutine(ExplosionExpansion());
        }
        //si T a passé 1
        else if(tSizeExpan >= 1)
        {
            //on passe à la deuxieme phase de l'explosion
            StartCoroutine(ExpansionStay());
            
        }
        
    }

    //Fonction de 2eme phase de l'explosion, la zone reste à la meme taille
    private IEnumerator ExpansionStay()
    {
        //on attend le temps que doit durer cette explosion
        yield return new WaitForSeconds(timeExpansionStay);
        
        //on détruit l'explosion et on reset les valeurs
        DestroyAndRestart();
    }

    //fonction de disparition de l'explosion et de reset des valeurs
    private void DestroyAndRestart()
    {
        if (objectType == typeOfObject.Confusion)
        {
            StartCoroutine(LerpAllEffectConfusion());
        }
        else if(objectType == typeOfObject.Fondue)
        {
            StartCoroutine(FinVfxFondue());
        }   
    }


    /// <summary>
    /// vfx de confuion on joue sur l'attractiojn speed et le nombre de particuyles des points extérieur
    /// l'alpha de la sphère
    /// et le nombres de particules du brouillar inside la sphère
    /// </summary>
    /// <returns></returns>
    private IEnumerator LerpAllEffectConfusion()
    {
        //récupération des composants
        VisualEffect confuParticules = explosionGo.GetComponent<VisualEffect>();
       // MeshRenderer confuSphereRenderer = explosionGo.GetComponentsInChildren<MeshRenderer>()[0];
        VisualEffect brouillardConfu = explosionGo.GetComponentsInChildren<VisualEffect>()[1];


        if(lancerGo != null)
        {
            //désactivation du vfx de lancer
            lancerGo.SetActive(false);
        }



        float newScaleZone = Mathf.Lerp(this.transform.localScale.x, 0.1f, paramConfusionLerpSpeed[1] * Time.deltaTime);


        this.transform.localScale = new Vector3(newScaleZone, newScaleZone, newScaleZone);



        // confusion Attraction speed
        float newCPAT = Mathf.Lerp(confuParticules.GetFloat("AttractionSpeed"),
                                    paramConfusionLerpMin[0],
                                    paramConfusionLerpSpeed[0] * Time.deltaTime);

        confuParticules.SetFloat("AttractionSpeed", newCPAT);


        //Debug.Log("oui" + confuParticules.GetFloat("AttractionSpeed"));


        // confusion nb particules
        float newCPNbP = Mathf.Lerp(confuParticules.GetFloat("NbParticules"),
                                    paramConfusionLerpMin[1],
                                    paramConfusionLerpSpeed[1] * Time.deltaTime);

        confuParticules.SetFloat("NbParticules", newCPNbP);

        /*
        // alpha de la shpere
        float newCS = Mathf.Lerp(confuSphereRenderer.material.GetFloat("Alpha"),
                                   paramConfusionLerpMin[2],
                                   paramConfusionLerpSpeed[2] * Time.deltaTime);

        confuSphereRenderer.material.SetFloat("Alpha", newCS);*/



        //nb partiliues brouillard
        float newBC = Mathf.Lerp(brouillardConfu.GetFloat("NbParticules"),
                                    paramConfusionLerpMin[3],
                                    paramConfusionLerpSpeed[3] * Time.deltaTime);

        brouillardConfu.SetFloat("NbParticules", newBC);



        yield return new WaitForEndOfFrame();

        

        if ((!(confuParticules.GetFloat("AttractionSpeed") > paramConfusionLerpMin[0] - 0.1f 
            && confuParticules.GetFloat("AttractionSpeed") < paramConfusionLerpMin[0] + 0.1f))
            &&
            (!(confuParticules.GetFloat("NbParticules") > paramConfusionLerpMin[1] - 0.1f
            && confuParticules.GetFloat("NbParticules") < paramConfusionLerpMin[1] + 0.1f))
            &&/*
            (!(confuSphereRenderer.material.GetFloat("AlphaShader") > paramConfusionLerpMin[2] - 0.1f
            && confuSphereRenderer.material.GetFloat("AlphaShader") < paramConfusionLerpMin[2] + 0.1f))
            &&*/
            (!(brouillardConfu.GetFloat("NbParticules") > paramConfusionLerpMin[3] - 0.1f
            && brouillardConfu.GetFloat("NbParticules") < paramConfusionLerpMin[3] + 0.1f))
            )
        {
            //Debug.Log("again");

            StartCoroutine(LerpAllEffectConfusion());
        }
        else
        {

            Debug.Log("end");

            confuParticules.SetFloat("AttractionSpeed", paramConfusionLerpMax[0]);

            confuParticules.SetFloat("NbParticules", paramConfusionLerpMax[1]);

           /* confuSphereRenderer.material.SetFloat("AlphaShader", paramConfusionLerpMax[2]);*/

            brouillardConfu.SetFloat("NbParticules", paramConfusionLerpMax[3]);


            ResetConfu();
        }

    }

    private void ResetConfu()
    {

        //désactivation de l'objet explosion
        explosionGo.SetActive(false);


        //reset de variable
        isLaunchedByMe = false;
        doOnce = false;
        tProjectile = 0;
        tSizeExpan = 0;

        //On remet le projectile dans la liste des projectiles utilisables
        myPlayer.GetComponent<PowerUpController>().confWaveGoList.Add(this.gameObject);

        //On rétrécit la taille du projectile
        this.transform.localScale = new Vector3(.1f, .1f, .1f);

        

    }


    public IEnumerator FinVfxFondue()
    {
        Debug.Log("in la couroutine");

        if (lancerGo != null)
        {
            //désactivation du vfx de lancer
            lancerGo.SetActive(false);
        }

        yield return new WaitForEndOfFrame();


        float newScaleZone = Mathf.Lerp(this.transform.localScale.x, 0.1f, paramConfusionLerpSpeed[1] * Time.deltaTime);

        this.GetComponent<Transform>().localScale = new Vector3(newScaleZone, newScaleZone, newScaleZone);

        Debug.Log(newScaleZone);
        Debug.Log(this.GetComponent<Transform>().localScale);

        if ((this.transform.localScale.x > 0f) && (this.transform.localScale.x > 0.3f))
        {
            Debug.Log("Coutouine ça fond");
            StartCoroutine(FinVfxFondue());
        }
        else
        {
            Debug.Log("avant reset fondu");
            ResetFondu();
        }

        
       


        
    }

    private void ResetFondu()
    {
        this.GetComponent<Transform>().Translate(new Vector3(transform.position.x, transform.position.y - 100, transform.position.x));
        fondueCanStopMe = false;

        //désactivation de l'objet explosion
        splashFondueGo.SetActive(false);

        //reset de variable
        isLaunchedByMe = false;
        doOnce = false;
        tProjectile = 0;
        tSizeExpan = 0;
        fondueCanStopMe = true;

        //On remet le projectile dans la liste des projectiles utilisables
        myPlayer.GetComponent<PowerUpController>().FondueGoList.Add(this.gameObject);

        //On rétrécit la taille du projectile
        this.transform.localScale = new Vector3(.1f, .1f, .1f);
    }




    private void ResetZoneConfu()
    {
        explosionGo.GetComponent<VisualEffect>().SetFloat("AttractionSpeed", 5f);
        explosionGo.GetComponent<VisualEffect>().SetFloat("NbParticules", 500f);
        explosionGo.GetComponentsInChildren<VisualEffect>()[1].SetFloat("NbParticules", 70f);
        explosionGo.GetComponentsInChildren<MeshRenderer>()[0].material.SetFloat("AlphaShader", 0.6f);
    }

    //Fonction RPC d'avant explosion pour les autres joueurs
    [PunRPC]
    void RPC_SplashFondue(Vector3 hit, Vector3 normal)
    {
        //on change la position du projectile
        transform.position = hit;
        //on active le gameobject explosion
        splashFondueGo.SetActive(true);
        //on place l'explosion au point de collision
        splashFondueGo.transform.position = hit;

        splashFondueGo.transform.forward = normal;
        //on lance la fonction d'explosion
        StartCoroutine(SplashExpansion());
    }

    //Fonction de splash (pour tous les joueurs)
    private IEnumerator SplashExpansion()
    {
        //augmentation de la valeur T du lerp d'agrandissement de la zone d'explosion, entre 0 et 1
        tSizeExpan += Mathf.Clamp01(speedExplo * Time.deltaTime);

        //changement de la taille de la zone d'explosion en fonction du nouveau T
        splashFondueGo.transform.localScale = Lerp(Vector3.zero, expansionSizeMax, tSizeExpan);

        //on attend la frame
        yield return new WaitForEndOfFrame();

        //si T est inferieur à 1
        if (tSizeExpan < 1)
        {
            //on continue l'expansion de la zone
            StartCoroutine(SplashExpansion());
        }
        //si T a passé 1
        else if (tSizeExpan >= 1)
        {
            //on passe à la deuxieme phase de l'explosion
            StartCoroutine(ExpansionStay());
        }
        
    }

    //Fonctions lerp permattant de calculer la position du projectile

    public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
    {
        return a + (b - a) * t;
    }

    public static Vector3 QuadraticCurve(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        Vector3 p0 = Lerp(a, b, t);
        Vector3 p1 = Lerp(b, c, t);
        return Lerp(p0, p1, t);
    }
}
