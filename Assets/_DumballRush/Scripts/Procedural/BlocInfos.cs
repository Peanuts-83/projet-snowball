﻿using UnityEngine;

/*
 * Ce script permet d'indiquer les points de départ / d'arrivée du bloc (pour leur génération)
 * Il indique également si le bloc est un bloc normal ou miroir
 * 
 * Ce script doit être placé sur le parent de chaque bloc
 */

public class BlocInfos : MonoBehaviour
{
    [Tooltip("Point de départ du bloc : mettre le parent")]
    public Transform StartPoint;
    [Tooltip("Point de fin du bloc : voir dans les objets enfants du bloc")]
    public Transform EndPoint;
    [Tooltip("Cocher si le bloc est un bloc miroir")]
    public bool isMirror;
    [Tooltip("Waypoints de ce bloc")]
    public GameObject[] waypointsBloc;

    private void Awake()
    {
        transform.parent = FindObjectOfType<GenProcedurale>().terrain;
    }
}