﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

/*
 * Ce script permet de générer la map juste avant la course, d'une façon semi-procédurale
 * il doit être placé sur l'objet ProceduralManager dans la MainScene
 */

public class GenProcedurale : MonoBehaviour
{
    [Header("Blocs de course")]

    [Tooltip("Liste des blocs normaux")]
    public List<GameObject> originalBlocks;
    [Tooltip("Liste des blocs miroirs")]
    public List<GameObject> mirorBlocks;

    private int totalOfBlocks;

    [Tooltip("Bloc de départ")]
    public GameObject blockStart;
    [Tooltip("Bloc d'arrivée")]
    public GameObject blockFinish;

    [Tooltip("Bloc transfert de miroir à normal")]
    public GameObject transfToNormal;
    [Tooltip("Bloc transfert de normal à miroir")]
    public GameObject transfToMiroir;

    [Header("Génération")]
    [SerializeField]
    private bool activeGeneration = true;
    //Taille de la course (en lien avec le PreRaceSingleton)
    private int raceSize;

    //Liste des blocs créés
    public List<GameObject> blockSpawned;

    [Tooltip("Objet Parent")]
    public Transform terrain;

    [Header("Fin")]
    [Tooltip("Est ce que la génération est terminée ?")]
    public bool GenFinish = false;

    private PreRaceSingleton prs;
    private PhotonView pv;

    void Start()
    {
        //On retrouve le masterClient
        if (PhotonNetwork.IsMasterClient)
        {
            pv = GetComponent<PhotonView>();

            prs = FindObjectOfType<PreRaceSingleton>();

            totalOfBlocks = originalBlocks.Count;

            //Récupération de la taille de la course choisie + clamp pour éviter d'avoir un bug pour cause d'un nombre trop élevé
            raceSize = Mathf.Clamp(prs.sizeRace,0,totalOfBlocks);

            if (activeGeneration)
            {
                //Lancement de la génération (bloc de départ)
                ProcCreateStart();
            }
            else
            {
                //Envoie en RPC du lancement de plusieurs autres fonctions qui devaient attendre la génération
                pv.RPC("RPC_AcceptCreatePlayer", RpcTarget.AllBuffered);
            }
        }       
    }

    //Fonction de génération du bloc de départ
    public void ProcCreateStart()
    {
        //On instancie le 1er, celui du départ 
        GameObject newBlock = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", blockStart.gameObject.name), Vector3.zero, Quaternion.identity);
        //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
        newBlock.transform.parent = terrain;
        //On l'ajoute à la liste des blocs créés
        blockSpawned.Add(newBlock);


        //Lancement de la suite de la génération (les blocs de courses) en aléatoire
        ProcCreateRace();
    }

    //Fonction de génération des blocs de courses
    public void ProcCreateRace()
    {
        //Pour le nombre choisi comme taille de course
        for (int i = 0; i < raceSize; i++)
        {
            //Aléatoire : bloc normal ou miroir
            int oriOrMir = Random.Range(0, 2);

            //Si c'est un bloc normal
            if(oriOrMir == 0)
            {
                //Aléatoire : un bloc parmis les bloc normaux restants
                int u = Random.Range(0, originalBlocks.Count);

                //Si le dernier bloc était un bloc miroir
                if (blockSpawned[blockSpawned.Count - 1].GetComponent<BlocInfos>().isMirror)
                {
                    //On instancie un bloc transfert miroir vers normal avant d'instancier le bloc
                    GameObject newTransfert = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", transfToNormal.gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
                    //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
                    newTransfert.transform.parent = terrain;
                    //On l'ajoute dans la liste des blocs créés
                    blockSpawned.Add(newTransfert);
                }

                //On peut ensuite instancier le bloc choisi
                GameObject newBlock3 = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", originalBlocks[u].gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
                //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
                newBlock3.transform.parent = terrain;

                //On l'ajoute à la liste des blocs créés
                blockSpawned.Add(newBlock3);
                //On l'enlève lui et sa version miroir des blocs de base pour ne pas retomber 2x sur le même bloc dans la course
                originalBlocks.RemoveAt(u);
                mirorBlocks.RemoveAt(u);
            }

            //Si c'est un bloc miroir
            else
            {
                //Aléatoire : un bloc parmis les bloc miroirs restants
                int u = Random.Range(0, mirorBlocks.Count);

                //Si le dernier bloc était un bloc normal
                if (blockSpawned[blockSpawned.Count - 1].GetComponent<BlocInfos>().isMirror == false)
                {
                    //On instancie un bloc transfert normal vers miroir avant d'instancier le bloc
                    GameObject newTransfert = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", transfToMiroir.gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
                    //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
                    newTransfert.transform.parent = terrain;
                    //On l'ajoute dans la liste des blocs créés
                    blockSpawned.Add(newTransfert);
                }

                //On peut ensuite instancier le bloc choisi
                GameObject newBlock3 = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", mirorBlocks[u].gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
                //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
                newBlock3.transform.parent = terrain;

                //On l'ajoute à la liste des blocs créés
                blockSpawned.Add(newBlock3);
                //On l'enlève lui et sa version normale des blocs de base pour ne pas retomber 2x sur le même bloc dans la course
                originalBlocks.RemoveAt(u);
                mirorBlocks.RemoveAt(u);
            }           
        }

        //Lancement de la génération finale (le bloc d'arrivée)
        ProcCreateFinish();
    }

    //Fonction de génération du bloc d'arrivée
    public void ProcCreateFinish()
    {
        //Le bloc arrivée étant un bloc normal, on vérifie si le bloc précédent est miroir
        if (blockSpawned[blockSpawned.Count - 1].GetComponent<BlocInfos>().isMirror)
        {
            //On instancie un transfert miroir vers normal
            GameObject newTransfert = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", transfToNormal.gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
            //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
            newTransfert.transform.parent = terrain;
            //On l'ajoute à la liste des blocs créés
            blockSpawned.Add(newTransfert);
        }

        //On peut ensuite instancer le bloc d'arrivée
        GameObject newBlock3 = PhotonNetwork.InstantiateRoomObject(Path.Combine("Blocs", blockFinish.gameObject.name), blockSpawned[blockSpawned.Count - 1].GetComponentInChildren<BlocInfos>().EndPoint.position, Quaternion.identity);
        //On change son parent (surtout pour simplifier la visibilité de la hierarchy)
        newBlock3.transform.parent = terrain;

        //On l'ajoute à la liste des blocs créés
        blockSpawned.Add(newBlock3);

        //La génération est terminée
        GenFinish = true;

        //Envoie en RPC du lancement de plusieurs autres fonctions qui devaient attendre la génération
        pv.RPC("RPC_AcceptCreatePlayer", RpcTarget.AllBuffered);
    }

    //Lnacement de plusieurs fonctions après la génération
    [PunRPC]
    void RPC_AcceptCreatePlayer()
    {
        //Gestion des Waypoints
        FindObjectOfType<WaypointsTracker>().StartWhenRaceIsGen();
        /*//Calcule des spawn du joueur sur tel ou tel spawner
        FindObjectOfType<GameSetupController>().CalculateSpawners();*/
        /*//Création du joueur
        FindObjectOfType<GameSetupController>().CreatePlayer();*/
    }
}