﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RandomizeHouseColor))]
public class Editor_RandomizeHouseColor : Editor
{
    RandomizeHouseColor myScript;

    RandomizeHouseColor.APossibleColor[] myColors;

    private void Awake()
    {
        myScript = (RandomizeHouseColor)target;
    }

    public override void OnInspectorGUI()
    {
        if (myScript.houseRender != null)
        {
            if (GUILayout.Button("RandomColor"))
            {
                myScript.SetColor("Random");
            }

            GUILayout.BeginHorizontal();
            //GUILayout.FlexibleSpace();
            int countCol = -1;

            foreach (RandomizeHouseColor.APossibleColor col in myScript.allPossibleColors)
            {
                countCol++;
                if (countCol % 6 == 0)
                {
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }

                if (GUILayout.Button(col.colorName))
                {
                    myScript.SetColor(col);
                }
            }

            //GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        
        base.OnInspectorGUI();
    }
}
