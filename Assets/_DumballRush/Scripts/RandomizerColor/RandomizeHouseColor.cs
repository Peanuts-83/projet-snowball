﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RandomizeHouseColor : MonoBehaviour
{
    public Renderer houseRender;

    [System.Serializable]
    public struct APossibleColor
    {
        public string colorName;
        public Color color;
    }

    public APossibleColor[] allPossibleColors;

    public void SetColor(APossibleColor col)
    {
        houseRender.material.SetColor("_RoofColor", col.color);
    }

    public void SetColor(string random)
    {
        StartCoroutine(Randomize());
    }

    private IEnumerator Randomize()
    {
        for (int i = 0; i < allPossibleColors.Length; i++)
        {
            houseRender.material.SetColor("_RoofColor", allPossibleColors[i].color);
            yield return new WaitForSeconds(0.1f);
        }

        int ID = Random.Range(0, allPossibleColors.Length);
        houseRender.material.SetColor("_RoofColor", allPossibleColors[ID].color);
    }
}
