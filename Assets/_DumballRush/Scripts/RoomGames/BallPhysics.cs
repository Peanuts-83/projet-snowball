﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.VFX;

public class BallPhysics : MonoBehaviour
{
    private Rigidbody rb;
    private PhotonView pv;

    public float gravityBall;

    public Transform EngagePoint;

    public GameObject exploBall;
    public GameObject[] exploGoal;

    [SerializeField]
    private float maxSpeedCalculated = 10;

    private AudioManager am;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pv = GetComponent<PhotonView>();
        am = FindObjectOfType<AudioManager>();

        foreach (GameObject vfxGoal in exploGoal)
        {
            vfxGoal.GetComponent<VisualEffect>().SetFloat("Spawn", 0);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(Vector3.down * gravityBall, ForceMode.Acceleration);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject == FindObjectOfType<RoomController>().newPlayerPrefab/* && PhotonNetwork.IsMasterClient*/)
        {
            pv.TransferOwnership(PhotonNetwork.LocalPlayer);
            rb.AddExplosionForce(200, collision.contacts[0].point, 5);
        }

        //Son de ballon
        float unlerpSpeed = Mathf.InverseLerp(0, maxSpeedCalculated, Mathf.Clamp(rb.velocity.magnitude,0,maxSpeedCalculated));
        float lerpVol = Mathf.Lerp(0, 1, unlerpSpeed);

        am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.collision.fmodEventCollisionFootBall, collision.contacts[0].point, "", 0, lerpVol);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "GoalsTrigger")
        {
            am.PlaySoundEffectAtPosition(am.soundEffectConfiguration.menu.but, transform.position);

            StartCoroutine(explosionVFXGoal());

            if (pv.Owner == PhotonNetwork.LocalPlayer)
            {
                rb.velocity = Vector3.zero;
                transform.position = EngagePoint.position;
            }
        }
    }

    private IEnumerator explosionVFXGoal()
    {
        foreach (GameObject vfxGoal in exploGoal)
        {
            vfxGoal.GetComponent<VisualEffect>().SetFloat("Spawn", 100);
        }
        
        exploBall.transform.parent = null;
        exploBall.gameObject.SetActive(true);

        yield return new WaitForSeconds(1.4f);

        foreach (GameObject vfxGoal in exploGoal)
        {
            vfxGoal.GetComponent<VisualEffect>().SetFloat("Spawn", 0);
        }

        exploBall.gameObject.SetActive(false);
        exploBall.transform.parent = this.gameObject.transform;
        exploBall.transform.localPosition = Vector3.zero;
    }
}
