﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;

public class SoundAmbianceController : MonoBehaviour
{
    [Header("Type d'ambiance")]
    [Tooltip("Si l'ambiance cave doit être joué")]
    [SerializeField]
    private bool isCave;
    [Tooltip("Si l'ambiance forêt doit être joué")]
    [SerializeField]
    private bool isForest;
    [Tooltip("Si l'ambiance foot doit être joué (seulement dans le lobby)")]
    [SerializeField]
    private bool isFoot;
    [Tooltip("Si l'ambiance sumo doit être joué (seulement dans le lobby")]
    [SerializeField]
    private bool isSumo;

    private AudioManager am;

    //Si quelque chose entre dans le trigger
    private void OnTriggerEnter(Collider other)
    {
        //On créer une variable de playerController
        PlayerController pc = null;

        //Si l'objet entrant possède un playerController
        if (other.TryGetComponent(out pc))
        {
            //Si c'est mon joueur
            if (pc.pv.IsMine)
            {
                FindAudioManagerIfEmpty();

                //On fait jouer les ambiances et les variations de musique en fonction du lieu
                Cave(true);
                Forest(true);
                Foot(true);
                Sumo(true);
            }
        }
    }

    //Si quelque chose sort du trigger
    private void OnTriggerExit(Collider other)
    {
        //On créer une variable de playerController
        PlayerController pc = null;

        //Si l'objet sortant possède un playerController
        if (other.TryGetComponent(out pc))
        {
            //Si c'est mon joueur
            if (pc.pv.IsMine)
            {
                FindAudioManagerIfEmpty();

                //On fait arrêter de jouer les ambiances et les variations de musique
                Cave(false);
                Forest(false);
                Foot(false);
                Sumo(false);
            }
        }
    }

    private void FindAudioManagerIfEmpty()
    {
        //On cherche l'audio manager si on ne l'a pas et on l'assigne
        if (am == null)
        {
            am = FindObjectOfType<AudioManager>();
        }
    }

    /// <summary>
    /// Fait jouer ou non les ambiances de caves
    /// </summary>
    /// <param name="play"></param>
    private void Cave(bool play)
    {
        if (!isCave)
            return;

        if (play)
        {
            am.musicConfiguration.parametersMusicRace.cave.value = 1;
            am.ambianceConfiguration.cave = 1;
        }
        else
        {
            am.musicConfiguration.parametersMusicRace.cave.value = 0;
            am.ambianceConfiguration.cave = 0;
        }
    }

    /// <summary>
    /// Fait jouer ou non les ambiances de forêts
    /// </summary>
    /// <param name="play"></param>
    private void Forest(bool play)
    {
        if (!isForest)
            return;

        if (play)
        {
            am.ambianceConfiguration.forest = 1;
        }
        else
        {
            am.ambianceConfiguration.forest = 0;
        }
    }

    private void Foot(bool play)
    {
        if (!isFoot)
            return;

        if (play)
        {
            am.musicConfiguration.parametersMusicLobby.foot.value = 1;
        }
        else
        {
            am.musicConfiguration.parametersMusicLobby.foot.value = 0;
        }
    }

    private void Sumo(bool play)
    {
        if (!isSumo)
            return;

        if (play)
        {
            am.musicConfiguration.parametersMusicLobby.sumo.value = 1;
        }
        else
        {
            am.musicConfiguration.parametersMusicLobby.sumo.value = 0;
        }
    }
}
