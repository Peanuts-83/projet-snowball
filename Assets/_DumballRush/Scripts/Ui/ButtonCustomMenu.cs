﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCustomMenu : MonoBehaviour
{
    public GameObject onSelectedSprite;

    [Header("Button Text")]
    public Lean.Localization.LeanLocalizedTextMeshPro txt_name;
    public Lean.Localization.LeanLocalizedTextMeshPro txt_description;

    [Header("Button ColoredObjects")]
    public Image color1;
    public Image color2;
    public Image color3;

    [HideInInspector]
    public int ID;
    

}
