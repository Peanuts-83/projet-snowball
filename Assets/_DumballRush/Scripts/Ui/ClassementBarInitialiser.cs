﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ClassementBarInitialiser : MonoBehaviour
{
    [System.Serializable]
    private struct ColorsTaking
    {
        [Tooltip("Couleur de background, la couleur plus clair")]
        public Color pale;
        [Tooltip("Couleur de la première ombre, la couleur la plus saturé")]
        public Color ombre1;
        [Tooltip("Couleur de la deuxième ombre, la couleur la plus sombre")]
        public Color ombre2;
    }

    [SerializeField]
    [Tooltip("Les couleurs des barres selon les positions")]
    private ColorsTaking[] colorsByPosition;

    [System.Serializable]
    private struct PositionSprite
    {
        [Tooltip("Contour de la position")]
        public Sprite ext;
        [Tooltip("Intérieur de la position")]
        public Sprite fill;
    }

    [SerializeField]
    [Tooltip("Tous les chiffres possible (de 0 à 9)")]
    private PositionSprite[] allPositionSprites;

    [SerializeField]
    [Tooltip("Scale de l'objet en fonction de la position du joueur à la fin de la course")]
    private Vector3[] scaleByPosition;

    [SerializeField]
    [Tooltip("Les images du background, des ombres1 et des ombres2")]
    private Image background, ombre1, ombre2;

    [System.Serializable]
    private struct Chiffre
    {
        [Tooltip("Contour du chiffre de position")]
        public Image ext;
        [Tooltip("Intérieur du chiffre de position")]
        public Image fill;
    }

    [SerializeField]
    [Tooltip("Sprite de position")]
    private Chiffre[] positionSpriteToModify;

    [SerializeField]
    [Tooltip("Le chiffre ou le nombre")]
    private GameObject chiffre, nombre;

    [SerializeField]
    [Tooltip("Texte pour le nickname")]
    private Text nicknameTxt;

    [SerializeField]
    [Tooltip("Texte pour le chrono")]
    private Text chronosTxt;

    /// <summary>
    /// Initialise la barre
    /// </summary>
    /// <param name="nickname"></param>
    /// <param name="position"></param>
    /// <param name="chronosBar"></param>
    /// <param name="chronos"></param>
    public void Initialise(string nickname, int position, bool chronosBar = false, float chronos = 0)
    {
        //Change le nom
        transform.name = $"ClassementBar_{nickname}";
        //Change le texte du nickname
        nicknameTxt.text = nickname;

        //Sélection des couleurs et assignation
        ColorsTaking colors = colorsByPosition[Mathf.Clamp(position - 1, 0, 3)];

        background.color = colors.pale;
        ombre1.color = colors.ombre1;
        ombre2.color = colors.ombre2;
        nicknameTxt.color = colors.ombre2;

        foreach (Chiffre c in positionSpriteToModify)
        {
            c.fill.color = colors.ombre1;
            c.ext.color = colors.ombre2;
        }

        //Choix du chiffre ou du nombre
        if (position < 10)
        {
            positionSpriteToModify[0].fill.sprite = allPositionSprites[position].fill;
            positionSpriteToModify[0].ext.sprite = allPositionSprites[position].ext;
        }
        else
        {
            Debug.Log(Mathf.FloorToInt(position / 10));
            positionSpriteToModify[1].fill.sprite = allPositionSprites[Mathf.FloorToInt(position/10)].fill;
            positionSpriteToModify[1].ext.sprite = allPositionSprites[Mathf.FloorToInt(position/10)].ext;

            positionSpriteToModify[2].fill.sprite = allPositionSprites[position % 10].fill;
            positionSpriteToModify[2].ext.sprite = allPositionSprites[position % 10].ext;
        }
        
        //Si c'est la barre de chronos
        if (chronosBar)
        {
            //Change le nom
            transform.name = $"ChronosBar_{nickname}";

            //Change le chiffre
            chiffre.SetActive(!(position > 9));
            nombre.SetActive(position > 9);

            //Change le texte de chrono
            string chronosText = TimeSpan.FromSeconds(chronos / 1000).ToString("mm':'ss'.'ff");

            chronosTxt.text = $" {chronosText}";
            chronosTxt.color = colors.ombre2;
        }
        //Si c'est la barre de classement
        else
        {
            //Change le scale et l'int de position
            GetComponent<RectTransform>().localScale = scaleByPosition[Mathf.Clamp(position - 1, 0, 3)];
            GetComponent<Animator>().SetInteger("Position", position);
        }
    }
}
