﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Ce script permet d'avoir une boule de neige qui tourne dans un coin de l'ecran de chargement
 * Il doit etre placé sur le LoadingImage dans la scene de chargement
 */

public class LoadingBall : MonoBehaviour
{
    [Tooltip("Toutes les images de boule de Neige")]
    [SerializeField]
    private Sprite[] loadingImages;

    [Tooltip("Vitesse de rotation")]
    [SerializeField]
    private float speedRotation;

    //transform d'un rect dans l'ui
    private RectTransform rt;
    //Image de l'ui
    private Image img;

    private void Start()
    {
        //Récupération du RectTransform
        rt = GetComponent<RectTransform>();
        //Récupération de l'image
        img = GetComponent<Image>();

        //Changement du sprite de l'image
        img.sprite = loadingImages[Random.Range(0, loadingImages.Length)];
    }

    private void Update()
    {
        //Rotation de l'image
        rt.Rotate(Vector3.forward * speedRotation * Time.deltaTime);
    }
}