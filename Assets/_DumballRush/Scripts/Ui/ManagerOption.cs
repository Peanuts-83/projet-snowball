﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Rendering;
using Lean.Localization;

public class ManagerOption : MonoBehaviour
{
    #region go panneau
    [Header("Panneau ameObject")]
    [SerializeField]
    private GameObject pnSon;

    [SerializeField]
    private GameObject pnGraphisme;

    [SerializeField]
    private GameObject pnGameplay;

    [SerializeField]
    private GameObject pnControls;
    #endregion

    #region btn Titre
    [Header("Button titre")]
    [SerializeField]
    private GameObject btnSon;

    [SerializeField]
    private GameObject btnGraphisme;

    [SerializeField]
    private GameObject btnGameplay;

    [SerializeField]
    private GameObject btnControls;


    #endregion

    #region idMenu

    public enum TypeMenusOption
    {
        Son = 0,
        Graphisme = 1,
        Gameplay = 2,
        Controls = 3

    }

    #endregion

    #region Manager
    /*
    private AudioManager am;

    private EventSystem evt;

    private GameObject sel;

    private InputManager im;

    private PreRaceSingleton prs;*/

    private ConnectionDebugger cd;


    #endregion

    #region input
    public enum InputType
    {
        Manette,
        Mouse

    }

    public InputType currentInputType;
    #endregion

    #region graphism

    [Header("Graphisme")]
    public RenderPipelineAsset[] qualityURPLevel;

    [SerializeField]
    private Resolution[] resolutions;

    public Dropdown resolutionDropdown;

    public Toggle toggleRenderShadows;

    #endregion

    #region gameplay

    [Header("Gameplay"), SerializeField]
    private Slider sldSensibilityDir;
    [SerializeField]
    private Slider sldSensibilityCam;
    [SerializeField]
    private Slider sldDeadZoneJoystick;
    [SerializeField]
    private Slider sldRenderDistance;
    [SerializeField]
    private Text txtRenderDistValue;
    [SerializeField]
    private GameObject leanTrad;
    [SerializeField]
    private Button[] languagesButtons;
    #endregion

    #region toggle ping n fps

    [SerializeField]
    private GameObject togglePing;

    [SerializeField]
    private GameObject toggleFps;

    [SerializeField]
    private GameObject toggleCrosshair;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        /*evt = EventSystem.current;

        im = FindObjectOfType<InputManager>();

        prs = FindObjectOfType<PreRaceSingleton>();*/


        //récupération des autres scripts
        cd = FindObjectOfType<ConnectionDebugger>();


        //création des résolutions

        resolutions = new Resolution[4];

        resolutions[0].width = 1920;
        resolutions[0].height = 1080;
        
        resolutions[1].width = 1366;
        resolutions[1].height = 768;
        
        resolutions[2].width = 1536;
        resolutions[2].height = 864;
        
        resolutions[3].width = 2560;
        resolutions[3].height = 1440;

        //Debug.Log(resolutions);

        //récupération des resolutions présentes sur l'ordinateur 
        //resolutions = Screen.resolutions;

        //création de la liste des résolutions dans le dropdomw truc de l'ui 
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option); 

            if(resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                //index de la résolution voulue
                currentResolutionIndex = i;
            }
        }

        //maj des résolutions ui
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();


        //cahregment des parametre connu dans les composants
        LoadParameter();
        LoadSlider();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// active /désactive les pannel et les interraction des button de navigation dans le menu option
    /// </summary>
    /// <param name="ToLeBonPannel"></param>
    public void PannelMenuOption(int ToLeBonPannel)
    {
        Button btn_son = btnSon.GetComponent<Button>();
        Button btn_grap = btnGraphisme.GetComponent<Button>();
        Button btn_gamep = btnGameplay.GetComponent<Button>();
        Button btn_Contr = btnControls.GetComponent<Button>();


        switch (ToLeBonPannel)
        {
            case (int)TypeMenusOption.Son:

                pnSon.SetActive(true);
                //btn_son.interactable = false;

                pnGraphisme.SetActive(false);
                //btn_grap.interactable = true;

                pnGameplay.SetActive(false);
                //btn_gamep.interactable = true;

                pnControls.SetActive(false);
                //btn_Contr.interactable = true;


                break; 
            
            case (int)TypeMenusOption.Graphisme:

                pnSon.SetActive(false);
                //btn_son.interactable = true;

                pnGraphisme.SetActive(true);
                //btn_grap.interactable = false;

                pnGameplay.SetActive(false);
                //btn_gamep.interactable = true;

                pnControls.SetActive(false);
                //btn_Contr.interactable = true;

                break; 
            
            case (int)TypeMenusOption.Gameplay:

                pnSon.SetActive(false);
                //btn_son.interactable = true;

                pnGraphisme.SetActive(false);
                //btn_grap.interactable = true;

                pnGameplay.SetActive(true);
                //btn_gamep.interactable = false;

                pnControls.SetActive(false);
                //btn_Contr.interactable = true;


                break;

            case (int)TypeMenusOption.Controls:

                pnSon.SetActive(false);
                //btn_son.interactable = true;

                pnGraphisme.SetActive(false);
                //btn_grap.interactable = true;

                pnGameplay.SetActive(false);
                //btn_gamep.interactable = true;

                pnControls.SetActive(true);
                //btn_Contr.interactable = false;


                break;

        }

    }


    #region graphisme
    /// <summary>
    /// changement de la qualité du jeu low / médium / high
    /// </summary>
    /// <param name="qualityWanted"></param>
    public void ChangeQuality(int qualityWanted)
    {
        QualitySettings.SetQualityLevel(qualityWanted);
        QualitySettings.renderPipeline = qualityURPLevel[qualityWanted];

        PlayerPrefs.SetFloat("QualityLvl", qualityWanted);

    }

    /// <summary>
    /// paramétrage Affichage Ombres
    /// </summary>
    /// <param name="RenderShadows"></param>
    public void SetRenderShadows(bool RenderShadows)
    {
        if (RenderShadows)
        {
            PlayerPrefs.SetInt("setRenderShadows", 1);
        }
        else
        {
            PlayerPrefs.SetInt("setRenderShadows", 0);
        }

    }

    /// <summary>
    /// paramétrage fentre / plein ecran
    /// </summary>
    /// <param name="isWindowed"></param>
    public void SetWindowed(bool isWindowed)
    {
        Screen.fullScreen = !isWindowed;

        if (isWindowed)
        {
            PlayerPrefs.SetInt("isWindowed", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isWindowed", 0);
        }
        
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetFloat("resolutionIndex", resolutionIndex);

    }

    #endregion

    /// <summary>
    /// affichage du ping 
    /// </summary>
    /// <param name="display"></param>
    #region gameplay
    public void DisplayPing(bool display)
    {
        cd.displayPing = display;
        //Debug.Log(togglePing.GetComponent<Toggle>().isOn);
        
        if (display)
        {
            PlayerPrefs.SetInt("displPing", 1);
            cd.CheckPlayerPrefDisplay();
        }
        else
        {
            PlayerPrefs.SetInt("displPing", 0);
            cd.CheckPlayerPrefDisplay();
        }
    }
    
    /// <summary>
    /// affichage des fps
    /// </summary>
    /// <param name="display"></param>
    public void DisplayFPS(bool display)
    {
        cd.displayFps = display;
        
        if (display)
        {
            PlayerPrefs.SetInt("displFPS", 1);
            cd.CheckPlayerPrefDisplay();
        }
        else
        {
            PlayerPrefs.SetInt("displFPS", 0);
            cd.CheckPlayerPrefDisplay();
        }
    }

    /// <summary>
    /// affichage de la réticule
    /// </summary>
    public void DisplayCrosshair()
    {
        Debug.Log("coucou");
        if (PlayerPrefs.GetInt("displCrosshair") == 0 || !PlayerPrefs.HasKey("displCrosshair"))
        {
            PlayerPrefs.SetInt("displCrosshair", 1);
            Debug.Log(PlayerPrefs.GetInt("displCrosshair"));
        }
        else
        {
            PlayerPrefs.SetInt("displCrosshair", 0);
            Debug.Log(PlayerPrefs.GetInt("displCrosshair"));
        }
    }

    public void ChangeSensibilityDirection(float sensi)
    {
        PlayerPrefs.SetFloat("sensiDir", sensi);
    }

    public void ChangeSensibilityCamera(float sensi)
    {
        PlayerPrefs.SetFloat("sensiCam", sensi);
    }

    public void ChangeDeadZoneJoystick(float dead)
    {
        PlayerPrefs.SetFloat("deadZone", dead);
    }

    public void ChangeRenderDistance(float renderDistance)
    {
        txtRenderDistValue.text = renderDistance.ToString();
        PlayerPrefs.SetFloat("renderDistance", renderDistance);
        Camera[] allCams = null;
        Camera.GetAllCameras(allCams);
        foreach (Camera cam in allCams)
        {
            GetComponent<Camera>().farClipPlane = PlayerPrefs.GetFloat("renderDistance") * 100;
        }       
    }


    public void ChangeDisplayNamePlayers(bool display)
    {
        if (display)
        {
            PlayerPrefs.SetFloat("distBF", 0f);
            PlayerPrefs.SetFloat("distMax", 0f);
        }
        else
        {
            PlayerPrefs.DeleteKey("distBF");
            PlayerPrefs.DeleteKey("distMax");
        }
    }

    public void ChangeLanguage(int idLanguage)
    {
        leanTrad.GetComponent<LeanLocalization>().SetCurrentLanguage(idLanguage);
        PlayerPrefs.SetInt("idLanguage", idLanguage);
        foreach (Button languageBtn in languagesButtons)
        {
            languageBtn.transform.GetChild(0).gameObject.SetActive(false);
        }
        languagesButtons[idLanguage].transform.GetChild(0).gameObject.SetActive(true);
    }

    #endregion region

    public void LoadParameter()
    {
        if (PlayerPrefs.GetInt("displFPS") == 1)
        {
            toggleFps.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            toggleFps.GetComponent<Toggle>().isOn = false;
        }

        if (PlayerPrefs.GetInt("displPing") == 1)
        {
            togglePing.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            togglePing.GetComponent<Toggle>().isOn = false;
        }

        if (PlayerPrefs.GetInt("displCrosshair") == 1)
        {
            PlayerPrefs.SetInt("displCrosshair", 0);
            toggleCrosshair.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            toggleCrosshair.GetComponent<Toggle>().isOn = false;
        }

        /*if (PlayerPrefs.GetInt("setRenderShadows") == 1)
        {
            toggleRenderShadows.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            toggleRenderShadows.GetComponent<Toggle>().isOn = false;
        }*/
    }

    public void LoadSlider()
    {
        if (PlayerPrefs.HasKey("sensiDir"))
        {
            sldSensibilityDir.value = PlayerPrefs.GetFloat("sensiDir");
        }

        if (PlayerPrefs.HasKey("sensiCam"))
        {

            sldSensibilityCam.value = PlayerPrefs.GetFloat("sensiCam");
        }

        if (PlayerPrefs.HasKey("deadZone"))
        {
            sldDeadZoneJoystick.value = PlayerPrefs.GetFloat("deadZone");
        }

        if (PlayerPrefs.HasKey("idLanguage"))
        {
            leanTrad.GetComponent<LeanLocalization>().SetCurrentLanguage(PlayerPrefs.GetInt("idLanguage"));
            languagesButtons[PlayerPrefs.GetInt("idLanguage")].transform.GetChild(0).gameObject.SetActive(true);
        }

        if (PlayerPrefs.HasKey("renderDistance"))
        {
            sldRenderDistance.value = PlayerPrefs.GetFloat("renderDistance");
            txtRenderDistValue.text = PlayerPrefs.GetFloat("renderDistance").ToString();
        }
    }

}
