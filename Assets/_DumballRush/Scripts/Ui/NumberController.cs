﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberController : MonoBehaviour
{
    [System.Serializable]
    private struct Number
    {
        public Sprite number_Cont;
        public Sprite number_Int;
    }

    [SerializeField]
    private Number[] numbers;

    [System.Serializable]
    private struct Exponent
    {
        public Sprite expo_Cont;
        public Sprite expo_Int;
    }

    [SerializeField]
    private Exponent[] exponents;

    [SerializeField]
    private bool forPositionInRace;

    [Header("Chiffres")]
    [SerializeField]
    private Image[] chiffre;
    [SerializeField]
    private Image[] exponent;

    [Header("Nombres")]
    [SerializeField]
    private Image[] number1;
    [SerializeField]
    private Image[] number2;
    [SerializeField]
    private Image[] exponentNumber;


    [System.Serializable]
    private struct Colors
    {
        public Color contour;
        public Color interieur;
    }

    [Header("Colors")]
    [SerializeField]
    private Colors[] colors;

    int fakePos = 1;

    int contSetNumber = 0;

    public void SetNumber(int number)
    {
        contSetNumber++;
        if (contSetNumber <= 1)
        {
            Debug.Log("FLORIAN: " + "SetNumber() PREMIERE FOIS");
        }

        Colors assignedColor = colors[Mathf.Clamp(number - 1, 0, colors.Length - 1)];

        if (number < 10)
        {
            ActivateArray(number1, false);
            ActivateArray(number2, false);
            ActivateArray(exponent, false);
            ActivateArray(exponentNumber, false);

            ActivateArray(chiffre);

            chiffre[0].sprite = numbers[number].number_Cont;
            chiffre[1].sprite = numbers[number].number_Int;

            chiffre[0].color = assignedColor.contour;
            chiffre[1].color = assignedColor.interieur;

            //Exposant
            if (forPositionInRace)
            {
                if (number > 0)
                {
                    number = Mathf.Clamp(number, 0, exponents.Length);

                    ActivateArray(exponent);

                    exponent[0].sprite = exponents[number - 1].expo_Cont;
                    exponent[1].sprite = exponents[number - 1].expo_Int;

                    exponent[0].color = assignedColor.contour;
                    exponent[1].color = assignedColor.interieur;
                }
            }
        }
        else
        {
            ActivateArray(chiffre, false);
            ActivateArray(exponent, false);
            ActivateArray(number1);
            ActivateArray(number2);
            ActivateArray(exponentNumber);

            int firstNumber = Mathf.FloorToInt(number / 10);
            int secondNumber = number - 10 * firstNumber;

            number1[0].sprite = numbers[firstNumber].number_Cont;
            number1[1].sprite = numbers[firstNumber].number_Int;

            number1[0].color = assignedColor.contour;
            number1[1].color = assignedColor.interieur;

            number2[0].sprite = numbers[secondNumber].number_Cont;
            number2[1].sprite = numbers[secondNumber].number_Int;

            number2[0].color = assignedColor.contour;
            number2[1].color = assignedColor.interieur;

            //Exposant
            if (forPositionInRace)
            {
                number = Mathf.Clamp(number, 0, exponents.Length);

                Debug.Log("EXPOSANT NUMBER="+number);

                ActivateArray(exponentNumber);

                exponentNumber[0].sprite = exponents[number - 1].expo_Cont;
                exponentNumber[1].sprite = exponents[number - 1].expo_Int;

                exponentNumber[0].color = assignedColor.contour;
                exponentNumber[1].color = assignedColor.interieur;
            }
        }
    }

    private void ActivateArray(Image[] array, bool activate = true)
    {
        foreach (Image img in array)
        {
            img.gameObject.SetActive(activate);
        }
    }
}
