﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberRace : MonoBehaviour
{
    [System.Serializable]
    private struct PositionIndicator
    {
        [Tooltip("Parent du chiffre ou du nombre")]
        public GameObject parent;

        [Tooltip("Le texte du chiffre/nombre")]
        public Text numberText;
        [Tooltip("Tous les exposants dans le cas d'un chiffre. Ne mettre qu'un exposant si c'est un nombre")]
        public GameObject[] exponent;

        [HideInInspector]
        public GameObject activeExponent;
    }

    [Header("Functionnal")]
    [SerializeField]
    private PositionIndicator chiffre;

    [SerializeField]
    private PositionIndicator nombres;


    [System.Serializable]
    private struct Colors
    {
        public Color interieur;
        public Color contour;
    }

    [Header("Colors")]
    [SerializeField]
    private Colors[] colors;
    
    /// <summary>
    /// Permet de changer la position du joueur dans l'UI
    /// </summary>
    /// <param name="position"></param>
    public void SetPosition(int position)
    {
        if (position <= 0)
        {
            Debug.LogError($"Position not valid : {position}");
            return;
        }

        //Est ce que le joueur est en position 1 à 9 ou 10 à 16 ?
        //C'est un chiffre entre 1 et 9 et un nombre entre 10 et 16.
        bool isChiffre = position <= 9;

        //Désactive et active selon si la position est un chiffre ou un nombre
        nombres.parent.SetActive(!isChiffre);
        chiffre.parent.SetActive(isChiffre);

        //Sélection du chiffre ou du nombre
        PositionIndicator pos;

        if (isChiffre)
        {
            pos = chiffre;

            //Sélection de l'exposant
            foreach (GameObject go in pos.exponent)
                go.SetActive(false);

            pos.activeExponent = pos.exponent[Mathf.Clamp(position - 1, 0, 3)];
            pos.activeExponent.SetActive(true);
        }
        else
        {
            pos = nombres;

            pos.activeExponent = pos.exponent[0];
        }

        //Affectation du texte de numero
        pos.numberText.text = $"{position}";


        //Sélection de la couleur
        Colors color = colors[Mathf.Clamp(position - 1, 0, 3)];

        pos.numberText.color = color.interieur;
        pos.numberText.GetComponent<Outline>().effectColor = color.contour;

        pos.activeExponent.GetComponent<Text>().color = color.interieur;
        pos.activeExponent.GetComponent<Outline>().effectColor = color.contour;

    }
}
