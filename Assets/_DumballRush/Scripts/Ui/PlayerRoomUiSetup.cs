﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class PlayerRoomUiSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    public TextMeshProUGUI nameInput;

    [Header("Master")]
    public Image MasterCrown;

    [Header("Eye and mouth")]
    [SerializeField]
    private Image eye;
    [SerializeField]
    private Image mouth;

    [SerializeField]
    private Sprite[] eyes;
    [SerializeField]
    private Sprite[] mouths;

    [Header("Colors")]
    [SerializeField]
    private Image ball_Color0;
    [SerializeField]
    private Image ball_Color1;

    [Header("Nickname background colors")]
    [SerializeField]
    private Image nameBG_ColorNormal;
    [SerializeField]
    private Image NameBG_ColorDark;
    [SerializeField]
    private Image NameBG_ColorLight;

    [SerializeField]
    private Sprite[] ball_Colors0;
    [SerializeField]
    private Sprite[] ball_Colors1;

    [SerializeField]
    [Range(0, 1)]
    private float addDarkColor;
    [SerializeField]
    [Range(0, 1)]
    private float addLightColor;

    public void SetupPlayer(string playerName, Color playerColor, int selectedEye, int selectedMouth)
    {
        //Change le nom
        nameInput.text = playerName;

        //Change les sprites de la boule
        int randomBall = Random.Range(0, ball_Colors0.Length);

        ball_Color0.sprite = ball_Colors0[randomBall];
        ball_Color1.sprite = ball_Colors1[randomBall];

        //Change la couleur de la boule
        ball_Color1.color = playerColor;
        ball_Color0.color = Color.Lerp(playerColor, Color.black, addDarkColor);

        //Change les yeux et la bouche
        eye.sprite = eyes[selectedEye];
        mouth.sprite = mouths[selectedMouth];

        //Change la couleur du background du pseudo
        nameBG_ColorNormal.color = playerColor;
        NameBG_ColorDark.color = Color.Lerp(playerColor, Color.black, addDarkColor);
        NameBG_ColorLight.color = Color.Lerp(playerColor, Color.white, addLightColor);
    }

    [PunRPC]
    public void MakeMyPlayer(string playerName, Color playerColor, int selectedEye, int selectedMouth)
    {

    }

    public void KillPlayerUi()
    {
        Destroy(this.gameObject);
    }
}
