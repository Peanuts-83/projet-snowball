﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Ce script permet de gérer l'interface inGame du jeu
 * Il doit se trouver sur le CanvasUI de la mainScene
 */

public class UiManager : MonoBehaviour
{
    [Tooltip("Temps qu'il reste avant de retourner dans la room")]
    public int timeBeforeRoom;

    [Tooltip("Image de l'ui du power up")]
    public Image powerUpImg;
    [Tooltip("Liste de sprites à mettre dans powerUpImg")]
    public Sprite[] allPowerUps;

    public Camera endCam;
    private Transform transfEndCam;

    [SerializeField]
    private GameObject hideViewObject;

    public GameObject positionNumber;

    [SerializeField]
    //private GameObject chrono;

    public GameObject uiFinish;

    public GameObject uiGameOver;

    public GameObject uiCountdownFinish;

    public GameObject uiWrongDirection;

    [SerializeField]
    private GameObject crosshair;


    private void Start()
    {
        if (PlayerPrefs.GetInt("displCrosshair") == 1)
        {
            crosshair.SetActive(true);
        }
        else
        {
            crosshair.SetActive(false);
        }
    }

    //Fonction d'activation du bouton retour au lobby (PositionAndTimerController)
    public IEnumerator ChangeViewEndRace()
    {
        yield return new WaitForSeconds(5f);

        transfEndCam = GameObject.FindGameObjectWithTag("TransformCamEnd").transform;
        endCam.transform.position = transfEndCam.position;
        endCam.transform.forward = transfEndCam.forward;
        uiFinish.SetActive(false);
        endCam.gameObject.SetActive(true);
    }

    //A la fin du compte a rebours : retour dans la room
    public void ReturnInRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            foreach(GameObject bloc in FindObjectOfType<GenProcedurale>().blockSpawned)
            {
                PhotonNetwork.Destroy(bloc);
            }
        }

        foreach (PlayerController player in FindObjectOfType<GameManager>().AllPlayers)
        {
            if (player != null && player.gameObject.GetComponent<PhotonView>().IsMine)
            {
                PhotonNetwork.Destroy(player.gameObject);
            }
        }

        //Fmod retourne au menu
        FMODUpdateToMenu(true);
        

        //La room redevient ouverte pour les nouveaux joueurs
        PhotonNetwork.CurrentRoom.IsOpen = true;

        
        //On load la scene de room
        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.LoadLevel(2);
    }

    //Utilisation du bouton retour menu
    public void ReturnMenu()
    {
        //Fmod retourne au menu
        FMODUpdateToMenu(false);

        //si le joueur est le master
        if (PhotonNetwork.IsMasterClient)
        {   
            //Si il reste au moins un autre joueur dans la room
            if (PhotonNetwork.PlayerList.Length > 1)
            {
                //choix d'un nouveau master parmis les joueurs restants puis le joueur part de la room
                int newIdMaster = Random.Range(0, PhotonNetwork.PlayerList.Length);
                if (!PhotonNetwork.PlayerList[newIdMaster].IsMasterClient)
                {
                    PhotonNetwork.SetMasterClient(PhotonNetwork.PlayerList[newIdMaster]);
                    SceneManager.LoadScene(1);
                    PhotonNetwork.LeaveRoom();
                }
                else
                {
                    ReturnMenu();
                }
            }
            else
            {
                SceneManager.LoadScene(1);
                PhotonNetwork.LeaveRoom();
            }
        }
        else
        {
            SceneManager.LoadScene(1);
            PhotonNetwork.LeaveRoom();
        }
    }

    //Fonction de changement de l'ui quand un powerUp est récupéré
    public IEnumerator OnChangePowerUp(int powerUpId)
    {
        //FindObjectOfType<GameManager>().myPlController.GetComponent<PowerUpController>().hasGift = true;

        if (powerUpId == 0)
        {
            powerUpImg.GetComponent<Animator>().SetBool("Appear", false);
            powerUpImg.GetComponent<Animator>().SetBool("Disappear", true);
        }
        else
        {
            powerUpImg.GetComponent<Animator>().SetBool("Disappear", false);
            powerUpImg.GetComponent<Animator>().SetBool("Appear", true);

            int randomInt = 0;

            AudioManager am = FindObjectOfType<AudioManager>();

            for (int i = 0; i < 10; i++)
            {
                am.PlaySoundEffect(am.soundEffectConfiguration.cadeaux.fmodEventRoulementPowerUp);

                int randomInt2 = Random.Range(1, allPowerUps.Length);
                if(randomInt2 != randomInt)
                {
                    powerUpImg.sprite = allPowerUps[randomInt2];
                    randomInt = randomInt2;
                }
                else if(randomInt2 != allPowerUps.Length - 1)
                {
                    powerUpImg.sprite = allPowerUps[randomInt2 + 1];
                    randomInt = randomInt2 + 1;
                }
                else
                {
                    powerUpImg.sprite = allPowerUps[randomInt2 - 1];
                    randomInt = randomInt2 - 1;
                }

                yield return new WaitForSeconds(0.1f);
            }

            am.PlaySoundEffect(am.soundEffectConfiguration.cadeaux.fmodEventObtentionPowerUp);

            //Changement du sprite en fonction du powerUp
            powerUpImg.sprite = allPowerUps[powerUpId];

            FindObjectOfType<GameManager>().myPlController.GetComponent<PowerUpController>().canUseGift = true;


            //Si le power up est un projectile, affichage de la zone de visée
            if (powerUpId == 5 || powerUpId == 6)
            {
                FindObjectOfType<GameManager>().myPlController.GetComponent<PowerUpController>().aimLine.GetComponent<LineAimProjectile>().haveAProjectile = true;
                FindObjectOfType<GameManager>().myPlController.GetComponent<PowerUpController>().aimLine.SetActive(true);
            }
        }
    }

    public IEnumerator RandomGiftOpen()
    {

        yield return new WaitForSeconds(1f);
    }

    //Fonction de remise à 0 d'fmod quand on retourne au menu
    private void FMODUpdateToMenu(bool returnInRoom)
    {
        AudioManager am = FindObjectOfType<AudioManager>();
        am.musicConfiguration.parametersMusicRace.startDecompte.value = 0;
        am.musicConfiguration.parametersMusicRace.startRaceAfterDecompte.value = 0;
        am.actualScene = AudioManager.SceneType.Menu;
        am.musicConfiguration.parametersMusicRace.toFinish.value = 0;
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceMagma);
        am.StopFMODInstance(am.soundEffectConfiguration.InstanceSnowSlide);

        if (returnInRoom)
            am.actualScene = AudioManager.SceneType.Lobby;
    }

    public void HideView()
    {
        hideViewObject.GetComponent<Animator>().SetTrigger("Appear");
    }

    public void SetPositionNumber(int position)
    {
        positionNumber.GetComponent<NumberRace>().SetPosition(position);
    }

    public void ActivatePositionNumber(bool active)
    {
        if (active == true)
            positionNumber.SetActive(active);

        positionNumber.GetComponent<Animator>().SetBool("Bool", active);
    }

    /*public void ActivateChrono(bool active)
    {
        chrono.GetComponent<Animator>().SetBool("Bool", active);
    }*/

    public void ActivateCountdownFinish()
    {
        uiCountdownFinish.GetComponent<Animator>().SetTrigger("AnimTrigger");
    }

    public void ActivateWrongDirection(bool active)
    {
        uiWrongDirection.GetComponent<Animator>().SetBool("Bool", active);
    }

    public void ActivateFinish(bool active)
    {
        uiFinish.GetComponent<Animator>().SetBool("Bool", active);
    }

    public void ActivateGameOver(bool active)
    {
        uiGameOver.GetComponent<Animator>().SetBool("Bool", active);
    }
}