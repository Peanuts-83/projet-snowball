﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Ce script permet de voir la version actuelle du jeu sur le menu principal
 * Il doit être placé sur le GameObject Version dans le canvas du menu
 */

public class UiVersion : MonoBehaviour
{
    void Start()
    {
        //Récupération de la version du jeu et changement du texte du canvas
        GetComponent<Text>().text = Application.productName + "_v" + Application.version;
    }
}
