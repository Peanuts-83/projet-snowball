﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Warning : MonoBehaviour
{
    public Transform PosGauche;
    public Transform PosCentre;
    public Transform PosDroite;

    public Quaternion AngleGauche;
    public Quaternion AngleDroite;

    public float maxAngleToSee;

    //position du 1er point du lerp au moment du lancer
    private Vector3 VectGauche;
    //position du 2eme point du lerp au moment du lancer
    private Vector3 VectCentre;
    //position du 3eme point du lerp au moment du lancer
    private Vector3 VectDroite;

    private GameObject myPlayer;
    public GameObject otherPlayer;

    public float maxDistToSee;

    private float tOpacity;

    public Sprite[] powerUpSprites;
    public Image powerUpImage;

    public enum typeOfObject { BigBallWarning, MagmaWarning};

    [Tooltip("Choix du style d'objet")]
    public typeOfObject objectType;

    [Range(0, 1)]
    public float tLerp;

    private PositionAndTimerController posAndTimer;

    // Start is called before the first frame update
    void Start()
    {
        myPlayer = FindObjectOfType<GameManager>().myPlController.gameObject;

        posAndTimer = myPlayer.GetComponent<PositionAndTimerController>();

        //A enlever, pour eviter les bugs tant que c'est pas coder
        //otherPlayer = myPlayer;
        //

        VectGauche = PosGauche.position;
        VectCentre = PosCentre.position;
        VectDroite = PosDroite.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(otherPlayer == null)
        {
            otherPlayer = myPlayer;
        }
        else
        {
            CheckEndWarning();

            FindPosition();

            GetComponent<RectTransform>().position = QuadraticCurve(VectGauche, VectCentre, VectDroite, tLerp);

            GetComponent<RectTransform>().rotation = Quaternion.Lerp(AngleGauche, AngleDroite, tLerp);

            FindDistance();

            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r,
                                                    GetComponent<Image>().color.g,
                                                    GetComponent<Image>().color.b,
                                                    tOpacity);

            if(tLerp == 0 || tLerp == 1)
            {
                GetComponent<Image>().color = new Color(GetComponent<Image>().color.r,
                                                    GetComponent<Image>().color.g,
                                                    GetComponent<Image>().color.b,
                                                    0);
            }
        }
    }

    //Fonctions lerp permattant de calculer la position de l'ui
    public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
    {
        return a + (b - a) * t;
    }

    public static Vector3 QuadraticCurve(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        Vector3 p0 = Lerp(a, b, t);
        Vector3 p1 = Lerp(b, c, t);
        return Lerp(p0, p1, t);
    }

    public void FindPosition()
    {
        Vector3 meToCam = new Vector3(Camera.main.transform.position.x - myPlayer.transform.position.x, 0, Camera.main.transform.position.z - myPlayer.transform.position.z);
        Vector3 meToOther = new Vector3(otherPlayer.transform.position.x - myPlayer.transform.position.x, 0, otherPlayer.transform.position.z - myPlayer.transform.position.z);
        Vector3 CamForward = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);

        float angle = Mathf.Clamp(Vector3.Angle(-CamForward, meToOther), -maxAngleToSee, maxAngleToSee);

        if(Vector3.Cross(CamForward, meToOther).y < 0)
        {
            angle = -angle;
        }

        tLerp = Mathf.InverseLerp(-maxAngleToSee, maxAngleToSee, angle);
    }

    public void FindDistance()
    {
        float distWithoutLerp = Vector3.Distance(myPlayer.transform.position, otherPlayer.transform.position);

        tOpacity = Mathf.InverseLerp(maxDistToSee, 0, distWithoutLerp);
    }

    void CheckEndWarning()
    {
        if(objectType == typeOfObject.BigBallWarning)
        {
            if(otherPlayer.GetComponent<PlayerController>().isBigBall == false)
            {
                myPlayer.GetComponent<DetectForWarning>().ResetAfterWarning(otherPlayer, this.gameObject);
            }
        }

        if (objectType == typeOfObject.MagmaWarning)
        {
            if (otherPlayer.GetComponent<PlayerController>().isMagma == false)
            {
                myPlayer.GetComponent<DetectForWarning>().ResetAfterWarning(otherPlayer, this.gameObject);
            }
        }
    }

    
}
