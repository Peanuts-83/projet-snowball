﻿using UnityEngine;

/*
 * Ce script permet de savoir la direction du joueur
 * Il doit être sur l'objet Orientator, qui est enfant de TrailContainer dans le prefab du joueur
 */

public class Orientator : MonoBehaviour
{
    [Tooltip("Le rigidbody du joueur")]
    public Rigidbody rb;

    void Update()
    {
        //Si la vitesse du joueur est différente de 0
        if (rb.velocity != Vector3.zero)
            //alors le forward de l'objet orientator est dans la direction du vecteur velocity
            transform.forward = rb.velocity;
    }
}