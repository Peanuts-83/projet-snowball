﻿using UnityEngine;
using System.Collections;
using UnityEngine.VFX;

/*
 * Ce script permet de lancer les vfx de perte de neige des props quand on les collisionne
 * et du magma
 * 
 * Il doit etre sur tous l'objet parent des props qui ont besoin de ces vfx
 */

public class TriggerVfxProps : MonoBehaviour
{
    [Tooltip("Prefabs du vfx de snow")]
    public GameObject goVfxLossSnow;
    [Tooltip("Prefabs du vfx de snow pour les chalet")]
    public GameObject goVfxLossChalet;
    [Tooltip("Prefabs du vfx de Feuu")]
    public GameObject goVfxFire;


    [Tooltip("Durée de vie de loss")]
    public float timeToLoss = 10f;

    [Tooltip("Durée de vie du feu minimum 2 !")]
    [SerializeField]
    private float timeToFire = 2f;
    [SerializeField]
    private float randomTimeFire = 1f;


    [Tooltip("Est ce qu'il y a de la neige sur l'arbre de base ?")]
    public bool isSnowed = false;
    public bool isTree = false;
    public bool isBush = false;
    [Tooltip("est ce c'est un chalet ?")]
    public bool isChalet = false;

    [Tooltip("Est ce que l'objet est brûlé")]
    private bool isBurn;

    [System.Serializable]
    private struct DissolveMeshes
    {
        public GameObject objectToDissolve;
        public Material[] mat;
        //public int matIndexToReplace;
        public bool desactivateCollider;

        [HideInInspector]
        public Renderer rend;
    }

    [Header("Dissolve")]
    [Tooltip("Array contenant les dissolve et les meshs")]
    [SerializeField]
    private DissolveMeshes[] allMeshesToDissolve;

    [SerializeField]
    private float speedToDissolve = 0.2f;

    /// <summary>
    /// Lorsque le props entre en trigger avec un objet = lance le vfx perte de neige 
    /// </summary>
    /// <param name="collision"></param>
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && isSnowed && isTree)
        {
            //intanciation des particules de perte de neige
            GameObject newParticle = Instantiate(goVfxLossSnow, this.transform);
            newParticle.transform.parent = null;

            newParticle.transform.localScale = Vector3.one;

            Destroy(newParticle, timeToLoss);
        }
    }

    /// <summary>
    /// trigger inchallllet
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //Si l'objet est le joueur
        if (other.gameObject.tag == "Player")
        {
            if (isChalet)
            {
                Debug.Log("trigger chalet");


                GameObject newParticle = Instantiate(goVfxLossChalet, this.transform);
                newParticle.transform.parent = null;

                newParticle.transform.localScale = Vector3.one;

                Destroy(newParticle, timeToLoss);
            }
        }
        //Si ce n'est pas un joueur
        else if(other.gameObject.tag == "MagmaCollision")
        {

            //Si c'est un arbre, on le brule
            if ((isTree || isBush) && !isBurn)
            {
                PlayFire();
            }
        }
    }


    public void PlayFire()
    {
        isBurn = true;

        StartCoroutine(CouroutineFire());
    }


    /// <summary>
    /// couroutine de lancement du feu sur un arbre 
    /// </summary>
    /// <returns></returns>
    IEnumerator CouroutineFire()
    {
        #region Instantiation
        //instanciation de la particules
        GameObject newParticle = Instantiate(goVfxFire, this.transform);
        newParticle.transform.parent = null;

        //On attend le temps de timefire + / - random
        yield return new WaitForSeconds(Random.Range(timeToFire - randomTimeFire, timeToFire + randomTimeFire));

        //Récupère tous les renderers et assigne les nouveaux matériaux
        for (int i =0; i < allMeshesToDissolve.Length; i++)
        {
            DissolveMeshes dm = allMeshesToDissolve[i];

            dm.rend = dm.objectToDissolve.GetComponent<Renderer>();

            dm.rend.materials = dm.mat;
        }
        #endregion

        #region Dissolve
        //Initialisation du lerp pour le materials
        float lerpDissolve = 0f;

        //Activation et calcul du lerp --> Dissous
        while (lerpDissolve < 1f) 
        {
            lerpDissolve += speedToDissolve * Time.deltaTime;

            foreach (DissolveMeshes dm in allMeshesToDissolve)
            {
                foreach (Material mat in dm.objectToDissolve.GetComponent<Renderer>().materials)
                    mat.SetFloat("DissolveAmount", lerpDissolve);
            }

            yield return new WaitForEndOfFrame();
        }

        #endregion

        #region Fin

        //Désactive le spawn des particules
        newParticle.GetComponent<VisualEffect>().SetFloat("Spawn", 0);
        //Désactive les colliders nécéssaires
        if (isTree)
        {
            foreach (DissolveMeshes dm in allMeshesToDissolve)
            {
                if (dm.desactivateCollider)
                {
                    dm.objectToDissolve.GetComponent<Collider>().enabled = false;
                }
            }
        }

        if (isBush)
            GetComponent<Collider>().enabled = false;

        yield return new WaitForSeconds(3f);

        //destruction des particules
        Destroy(newParticle);
        Destroy(this.gameObject);
        #endregion
    }
}

