﻿using UnityEngine;
public class WaypointsID : MonoBehaviour 
{ 
    public int ID;
    public int IDBloc;

    private void Start()
    {
        GameObject procManager = FindObjectOfType<GenProcedurale>().gameObject;

        for (int i = 0; i < procManager.GetComponent<GenProcedurale>().blockSpawned.Count; i++)
        {
            if (procManager.GetComponent<GenProcedurale>().blockSpawned[i] == this.gameObject.transform.parent.parent.gameObject)
            {
                IDBloc = i;
            }
        }
    }
}