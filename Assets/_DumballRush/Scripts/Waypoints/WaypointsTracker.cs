﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * Obtient les différents waypoints de la course et les tris dans le bon ordre.
 * 
 * Le script se trouve sur le waypoint manager dans la scène.
 * 
 * 
 */

public class WaypointsTracker : MonoBehaviour
{
    [Tooltip("La liste des waypoints triés dans l'ordre de leur ID (WaypointsID)")]
    public List<GameObject> waypointsInOrder;

    //Liste des objets non trié
    private List<GameObject> waypoints;

    public List<GameObject> blocs;

    public void StartWhenRaceIsGen()
    {
        if (FindObjectOfType<GenProcedurale>().blockSpawned.Count != 0)
        {
            blocs = FindObjectOfType<GenProcedurale>().blockSpawned;
        }
        else
        {
            for (int i = 0; i < FindObjectOfType<GenProcedurale>().terrain.transform.childCount; i++)
            {
                blocs.Add(FindObjectOfType<GenProcedurale>().terrain.transform.GetChild(i).gameObject);
            }
        }

        foreach (GameObject bloc in blocs)
        {
            if (bloc.GetComponent<BlocInfos>().waypointsBloc != null)
            {
                foreach (GameObject waypoint in bloc.GetComponent<BlocInfos>().waypointsBloc)
                {
                    waypointsInOrder.Add(waypoint);
                }
            }
        }

        //Calcule des spawn du joueur sur tel ou tel spawner
        FindObjectOfType<GameSetupController>().CalculateSpawners();
        //Création du joueur
        FindObjectOfType<GameSetupController>().CreatePlayer();
        FindObjectOfType<GameManager>().ActivateCineCam(true);
    }
}
